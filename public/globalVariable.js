const globalVariable = {
  dateTypeList: [{
    label: '日累',
    value: 'DAY'
  }, {
    label: '月累',
    value: 'MONTH'
  }, {
    label: '年累',
    value: 'YEAR'
  }],
  dateType: 'DAY',
  dataSourceList: [{
    label: 'final',
    value: 'FINAL'
  }],
  dataSource: 'FINAL',
  orgId: '',
  assetPanoramaTitle: '万链·资产全景图',
  catalogLimitNum: 7,
  defaultDayDimensionId: 1310085661852672, // 内置日维度
  //自测环境logo
  loginLogo:'logo@1x',
  titleLogo:'logo_login_2.0',
  searchLogo:'title',
  //海尔
  // loginLogo:'logoHaier',
  // titleLogo:'logoHaier',
  // searchLogo:'titleHaier',
}
