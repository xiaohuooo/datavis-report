import axios from "@/libs/api.request";

//目标管理新增目标
export const addObject = (data) => {
  return axios.request({
    url: `monitor/aim/add`,
    method: "post",
    data
  });
};

//查询所有目标
export const getObjectList = (data) => {
  return axios.request({
    url: `monitor/aim/page`,
    method: 'post',
    data
  });
};

//查询我的目标（当前登录用户）
export const getMyObject = (data) => {
  return axios.request({
    url: `monitor/aim/findMyResource`,
    method: 'post',
    data
  });
};

//查询部门目标
export const getDeptObject = (data) => {
  return axios.request({
    url: `monitor/aim/findByDept`,
    method: 'post',
    data
  });
};

//查询单个目标详情
export const getObjectDetail = (data) => {
  return axios.request({
    url: `monitor/aim/detail/${data}`,
    method: 'get',
  });
};

//查询单个目标详情
export const getObjectById = (id) => {
  return axios.request({
    url: `monitor/aim/tree/${id}`,
    method: 'get',
  });
};

//编辑目标
export const editObject = (data) => {
  return axios.request({
    url: `monitor/aim/update`,
    method: 'post',
    data
  });
};

//删除目标
export const deleteObject = (data) => {
  return axios.request({
    url: `monitor/aim/${data.id}/${data.type}`,
    method: 'delete',
  });
};