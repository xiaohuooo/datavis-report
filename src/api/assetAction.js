import axios from "@/libs/api.request";

/**
 * 发布(维度/指标)
 */
export const publishAsset = data => {
  return axios.request({
    url: `/meta/common/publish`,
    method: "put",
    data
  });
};

/**
 * 下线(维度/指标)
 */
export const offlineAsset = data => {
  return axios.request({
    url: `/meta/common/offLine`,
    method: "put",
    data
  });
};

/**
 * 变更所有者(维度/指标)
 */
export const updateAssetOwner = data => {
  return axios.request({
    url: `/meta/common/updateOwner`,
    method: "put",
    data
  });
};

/**
 * 权限分享(维度/指标)
 */
export const shareAssetAuth = data => {
  return axios.request({
    url: `/meta/common/shareAndSendMessage`,
    method: "put",
    data
  });
};

/**
 * 权限申请(维度/指标)
 */
export const applyAssetAuth = data => {
  return axios.request({
    url: `/meta/common/apply`,
    method: "put",
    data
  });
};
