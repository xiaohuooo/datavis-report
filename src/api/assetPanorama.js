import axios from "@/libs/api.request";

/**
 * 访问前十指标
 */
export const topIndicator = () => {
  return axios.request({
    url: `meta/topView/topIndicator`,
    method: "get",
  });
};

/**
 * 访问前十报告
 */
export const topReport = () => {
  return axios.request({
    url: `meta/topView/topReport`,
    method: "get",
  });
};

/**
 * 最新发布的指标
 */
export const latestIndicator = () => {
  return axios.request({
    url: `meta/indicator/latestIndicator`,
    method: "get",
  });
};

/**
 * 最新发布的报告
 */
export const latestReport = () => {
  return axios.request({
    url: `meta/report/latestReport`,
    method: "get",
  });
};

/**
 * 一级目录
 */
export const firstCatalogue = () => {
  return axios.request({
    url: `meta/dataAssets/firstCatalogue`,
    method: "get",
  });
};

/**
 * 业务域
 */
export const firstCatalog = () => {
  return axios.request({
    url: `meta/sysDept/firstCatalog`,
    method: "get",
  });
};

/**
 * 模糊查询指标和报告
 */
export const findVagueResource = (data) => {
  return axios.request({
    url: `meta/report/findVagueResource`,
    method: 'post',
    data
  });
};
/**
 * 热门搜索
 */
export const findMostSearch = () => {
  return axios.request({
    url: `meta/report/findMostSearch`,
    method: "get",
  });
};

/**
 * 最热
 */
export const findMostSearchResource = (data) => {
  return axios.request({
    url: `meta/report/findMostSearchResource`,
    method: 'post',
    data
  });
};
// 某个指标的目标信息
export const queryTargetInfo = id => {
  return axios.request({
    url: `/monitor/aim/indicator/${id}`,
    method: "put"
  });
};
