import axios from '@/libs/api.request'

export const getBusinessModelList = (params) => {
  return axios.request({
    url: '/meta/businessModel/list',
    method: 'get',
    params: params
  })
}

export const getBusinessModelGroupList = () => {
  return axios.request({
    url: '/meta/businessModel/group/list',
    method: 'get'
  })
}

//新增保存
export const addBusinessModel = (data) => {
  return axios.request({
    url: '/meta/businessModel',
    method: 'post',
    data
  })
}

export const updateBusinessModel = (data) => {
  return axios.request({
    url: '/meta/businessModel',
    method: 'put',
    data
  })
}

export const deleteBusinessModelById = (id) => {
  return axios.request({
    url: `/meta/businessModel/${id}`,
    method: 'delete'
  })
}

// 强制删除业务模型
export const deleteBusinessModelForceById = id => {
  return axios.request({
    url: `/meta/businessModel/${id}/force`,
    method: 'delete'
  })
}

export const getBusinessModelById = (id) => {
  return axios.request({
    url: `/meta/businessModel/${id}`,
    method: 'get'
  })
}


export const queryAllBusinessModelAndGroup = () => {
  return axios.request({
    url: `/meta/businessModelGroup/list`,
    method: 'get'
  })
}

export const addBusinessModelGroup = (data) => {
  return axios.request({
    url: '/meta/businessModelGroup',
    method: 'post',
    data
  })
}

export const updateBusinessModelGroup = (data) => {
  return axios.request({
    url: '/meta/businessModelGroup',
    method: 'put',
    data
  })
}

export const deleteBusinessModelGroupById = (id) => {
  return axios.request({
    url: `/meta/businessModelGroup/${id}`,
    method: 'delete'
  })
}

export const queryBusinessModelForPage = (params) => {
  return axios.request({
      url: `/meta/businessModel/page`,
      method: 'get',
      params
    })
}

export const queryBusinessModelList = (params) => {
  return axios.request({
      url: `/meta/businessModel/page`,
      method: 'get',
      params
    })
}
