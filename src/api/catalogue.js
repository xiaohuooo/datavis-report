import axios from '@/libs/api.request'

// 数据资产-目录-根据分组查询指标和维度
export const selectAssets = data => {
  return axios.request({
    url: `/meta/dataAssets/page`,
    method: 'post',
    data,
  })
}

// 批量移动资源
export const batchMoveAssets = data => {
  return axios.request({
    url: `/meta/dataAssets/batchMove`,
    method: "put",
    data
  });
}
