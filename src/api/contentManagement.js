import axios from '@/libs/api.request'

// 新增内容
// 新增内容不需要传参 id
export function addContent(params) {
  return axios.request({
    url: '/meta/sysContent/add',
    method: 'put',
    data: params
  })
}

// 查询所有业务分组
export function queryBusinessGroups(params) {
  return axios.request({
    url: '/meta/sysContent/group',
    method: 'get',
    params
  })
}

// 查询所有业务标签
export function queryBusinessLabels(params) {
  return axios.request({
    url: '/meta/sysContent/label',
    method: 'get',
    params
  })
}

// 查询所有内容列表
export function queryContentList(params) {
  return axios.request({
    url: '/meta/sysContent/list',
    method: 'post',
    data: params
  })
}

// 分页查询内容列表
export function queryContentListByPage(params) {
  return axios.request({
    url: '/meta/sysContent/page',
    method: 'post',
    data: params
  })
}

// 查询环境/内容信息
export function queryEnvAndContent(params) {
  return axios.request({
    url: '/meta/sysContent/queryForEnv',
    method: 'post',
    data: params
  })
}

// 修改内容
export function updateContent(params) {
  return axios.request({
    url: '/meta/sysContent/update',
    method: 'post',
    data: params
  })
}

// 删除内容
export function deleteContent(id) {
  return axios.request({
    url: `/meta/sysContent/${id}`,
    method: 'delete'
  })
}

// 校验资源是否重名
export function checkDuplicatedContentName(name) {
  return axios.request({
    url: `/meta/sysContent/queryByName?name=${name}`,
    method: 'get'
  })
}

export function addNewGroup(data) {
  return axios.request({
    url: '/meta/sysContent/addGroup',
    method: 'POST',
    data
  })
}

export function deleteGroup(id) {
  return axios.request({
    url: `/meta/sysContent/delGroup/${id}`,
    method: 'DELETE'
  })
}

export function addNewLabel(data) {
  return axios.request({
    url: '/meta/sysContent/addLabel',
    method: 'POST',
    data
  })
}

export function deleteLabel(id) {
  return axios.request({
    url: `/meta/sysContent/delLabel/${id}`,
    method: 'DELETE'
  })
}
