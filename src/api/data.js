import axios from '@/libs/api.request'

export const addDataSourceGroup = (data) => {
  return axios.request({
    url: '/data/dataSourceGroup',
    method: 'post',
    data
  })
}

export const updateById = (data) => {
  return axios.request({
    url: '/data/dataSourceGroup',
    method: 'put',
    data
  })
}

export const deleteById = (id) => {
  return axios.request({
    url: `/data/dataSourceGroup/${id}`,
    method: 'delete'

  })
}


export const getMaintainTreeList = param => {
  return axios.request({
    url: '/meta/indicator/maintenance/getTreeData',
    method: 'get'
  })
}

export const getMaintainTableData = params => {
  return axios.request({
    url: '/meta/indicator/maintenance/getData',
    method: 'get',
    params
  })
}

export const updateMaintainData = data => {
  return axios.request({
    url: '/meta/indicator/maintenance/updateIndexValue',
    method: 'put',
    data
  })
}
