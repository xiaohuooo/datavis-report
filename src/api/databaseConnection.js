import axios from '@/libs/api.request'

export const testConnection = (data) => {
  return axios.request({
    url: '/data/dataConnect/test',
    method: 'post',
    data
  })
}
export const checkMapping = (id) => {
  return axios.request({
    url: `/data/dataConnect/checkMapping/${id}`,
    method: 'get'
  })
}

export const connectionList = (data) => {
  return axios.request({
    url: '/data/dataConnect/page',
    method: 'get',
    params: data
  })
}
// 删除连接
export const delConnection = (id) => {
  return axios.request({
    url: `/data/dataConnect/${id}`,
    method: 'delete'
  })
}
// 强制删除连接
export const delConnectionForce = (id) => {
  return axios.request({
    url: `/data/dataConnect/${id}/force`,
    method: 'delete'
  })
}
export const createDataConnect = (data) => {
  return axios.request({
    url: '/data/dataConnect',
    method: 'post',
    data
  })
}


export const updateDataConnect = (data) => {
  return axios.request({
    url: '/data/dataConnect',
    method: 'put',
    data
  })
}

// id查已同步源表信息
export const queryMetaDataByID = (id) => {
  return axios.request({
    url: `/data/originTable/queryMetadataByID?connectId=${id}`,
    method: 'get',
  })
}
// id查源表信息
export const queryOriginalMetaDataByID = (id) => {
  return axios.request({
    url: `/data/originTable/metadata?connectId=${id}`,
    method: 'get',
  })
}
// 新建时下一步
export const createNextStep = (data) => {
  return axios.request({
    url: `/data/dataConnect/nextStep`,
    method: 'post',
    data
  })
}
// 新建时查源表信息
export const createByDataConnect = (data) => {
  return axios.request({
    url: `/data/originTable/metadataByDataConnect`,
    method: 'post',
    data
  })
}
// 新建时创建连接
export const createConnect = (data) => {
  return axios.request({
    url: `/data/dataConnect/create`,
    method: 'post',
    data
  })
}
// 编辑时同步
export const syncOriginalTable = (data) => {
  return axios.request({
    url: `/data/originTable/sync`,
    method: 'post',
    data
  })
}
// 编辑源表配置
export const updateOriginalTable = (data) => {
  return axios.request({
    url: `/data/originTable/update`,
    method: 'put',
    data
  })
}
// 同步影响的数据集
export const queryRelationDatas = (data) => {
  return axios.request({
    url: `/data/originTable/queryRelationDatas`,
    method: 'post',
    data
  })
}
// 同步影响的数据集
export const testAfterConnect = (id) => {
  return axios.request({
    url: `/data/dataConnect/testAfterConnect/${id}`,
    method: 'post',
  })
}

