import axios from "@/libs/api.request";

export const create = (data) => {
  return axios.request({
    url: "/data/dataset",
    method: "post",
    data,
  });
};

export const update = (data) => {
  return axios.request({
    url: "/data/dataset",
    method: "put",
    data,
  });
};

export const uploadFile = (data, options) => {
  return axios.request({
    url: `/data/upload/uploadFile`,
    method: "post",
    data,
    ...options,
  });
};

// 删除文件数据集
export const deleteFileDataset = (id) => {
  return axios.request({
    url: `/data/fileDataset/${id}`,
    method: "delete",
  });
};

// 强制删除文件数据集
export const deleteFileDatasetForce = (id) => {
  return axios.request({
    url: `/data/fileDataset/${id}/force`,
    method: "delete",
  });
};

export const publish = (data) => {
  return axios.request({
    url: `/data/dataset/${data}/publish`,
    method: "put",
  });
};

export const copy = (datasetId) => {
  return axios.request({
    url: `/data/dataset/${datasetId}/copy`,
    method: "post",
  });
};

export const stopStartUsing = (data) => {
  return axios.request({
    url: "/data/dataset/status",
    method: "put",
    data,
  });
};

export const list = (data) => {
  return axios.request({
    url: "/data/dataset/list",
    method: "post",
    data,
  });
};
export const listFolders = (data) => {
  return axios.request({
    url: "/data/dataset/dataSourceGroup/" + data,
    method: "get",
  });
};

export const getFileList = (connectId) => {
  return axios.request({
    url: `/data/fileDataset/list/${connectId}`,
    method: "get",
  });
};

export const delSetById = (data) => {
  return axios.request({
    url: `/data/upload/deleteById/${data}`,
    method: "delete",
  });
};
export const awareHadMapping = (data) => {
  return axios.request({
    url: `/data/upload/checkDataForMapping/${data}`,
    method: "get",
  });
};
export const del = (id) => {
  return axios.request({
    url: `/data/dataset/${id}`,
    method: "delete",
  });
};

// 强制删除数据模型
export const deleteDataModelForce = (id) => {
  return axios.request({
    url: `/data/dataset/${id}/force`,
    method: "delete",
  });
};

export const delFolder = (id) => {
  return axios.request({
    url: `/data/dataSourceGroup/${id}`,
    method: "delete",
  });
};

export const updateFolder = (data) => {
  return axios.request({
    url: "/data/dataset/move",
    method: "put",
    data,
  });
};

export const getDatasetData = (datasetId) => {
  return axios.request({
    url: "/data/dataset/" + datasetId,
    method: "get",
  });
};
export const getDatasetMetaData = (data) => {
  return axios.request({
    url: "/data/dataset/getDatasetMetaData",
    method: "post",
    data: data,
  });
};
//日志
export const getDatasetLogRes = (params) =>
  axios.request({
    url: "data/task/dataset/result",
    method: "get",
    params,
  });

//根据tableId 查询dataset
export const existDataset = (params) =>
  axios.request({
    url: "data/dataset/existDataset",
    method: "get",
    params: params,
  });

export const bigTableData = (data) => {
  return axios.request({
    url: "/data/dataset/tempResultSet",
    method: "post",
    data: data,
  });
};
export const findDataSetByName = (data) => {
  return axios.request({
    url: "/data/dataset/findDatasetByName",
    method: "post",
    data: data,
  });
};
export const getGroupByColumns = (columnId) =>
  axios.request({
    url: "/data/dataset/columnData?columnId=" + columnId,
    method: "get",
  });
/**
 * @description 根据表id获取表数据
 * @param tableId
 * @param pageSize
 * @param pageNum
 * @return {never}
 */
export const getTableData = (tableId, pageSize = 10, pageNum = 1) =>
  axios.request({
    url: `/data/dataset/tableData?tableId=${tableId}&pageSize=${pageSize}&pageNum=${pageNum}`,
    method: "get",
  });
export const updateJobTime = (data) =>
  axios.request({
    url: "/data/dataset/jobConfigInfo",
    method: "put",
    data,
  });

export const getDatasetJobTime = (id) =>
  axios.request({
    url: `data/dataset/${id}/jobConfigInfo`,
    method: "get",
  });

// 数据集指标维护
export const getindicatorMaintain = (params) => {
  return axios.request({
    url: "data/cube/index",
    method: "get",
    params: params,
  });
};

// 数据集指标维护排序
export const updateSort = (params) => {
  return axios.request({
    url: "data/cube/updateSort",
    method: "get",
    params: params,
  });
};

// 数据集指标维护更改指标值
export const updateIndex = (data) => {
  return axios.request({
    url: "/data/cube/updateIndex",
    method: "put",
    data,
  });
};

export const reloadBigTableTaskForMoreDataSet = (data) => {
  return axios.request({
    url: "/data/task/reload/dataset",
    method: "post",
    data,
  });
};

// 数据模型-文件上传-客户端解析上传（excel文件）
export const clientUpload = (data, options) => {
  return axios.request({
    url: "/data/fileDataset/upload/client",
    method: "post",
    data,
    ...options,
  });
};

// 数据模型-文件上传-服务器端解析上传（excel文件）
export const serverUpload = (data, options) => {
  return axios.request({
    url: "/data/fileDataset/upload/server",
    method: "post",
    data,
    ...options,
  });
};

// 数据模型-文件上传-详情
export const fileDetail = (id) => {
  return axios.request({
    url: `/data/fileDataset/detail?tableId=${id}`,
    method: "get",
  });
};

// 数据模型-文件上传-修改文件名
export const updateFileName = ({ tableId, label }) => {
  return axios.request({
    url: `/data/fileDataset/${tableId}?label=${label}`,
    method: "put",
  });
};

// 数据模型-文件上传-修改某个上传记录的名称
export const renameRecord = ({ id, name }) => {
  return axios.request({
    url: `/data/fileDataset/record/${id}?name=${name}`,
    method: "put",
  });
};

// 数据模型-文件上传-删除某个上传记录（excel文件）
export const deleteRecord = (id) => {
  return axios.request({
    url: `/data/fileDataset/record/${id}`,
    method: "delete",
  });
};

// 数据模型-文件上传-获取某个上传记录的数据
export const getRecordData = (id) => {
  return axios.request({
    url: `/data/fileDataset/record/data?recordId=${id}`,
    method: "get",
  });
};

// 数据模型-文件上传-修改上传文件的表名、字段配置
export const updateTable = (data) => {
  return axios.request({
    url: "/data/fileDataset/upload/client",
    method: "post",
    data,
  });
};

// 全局变量列表
export const getVariableList = (params) => {
  return axios.request({
    url: "/data/globalVariable/list",
    method: "get",
    params,
  });
};

// 新增/修改全局变量
export const addOrUpdataVariable = (data) => {
  return axios.request({
    url: "/data/globalVariable/save",
    method: "post",
    data,
  });
};

// 删除全局变量
export const deleteVariable = (id) => {
  return axios.request({
    url: `/data/globalVariable/${id}/delete`,
    method: "delete",
  });
};

// 查询一级日志列表
export const getIndicatorLog = (params) => {
  return axios.request({
    url: `/meta/indicatorLog/info`,
    method: "get",
    params,
  });
};

// 查询二级日志列表
export const getIndicatorLogDetail = (pid) => {
  return axios.request({
    url: `/meta/indicatorLog/detail/${pid}`,
    method: "get",
  });
};

// 根据页面类型查询全局变量条件
export const getGlobalVariableByPage = (pageType) => {
  return axios.request({
    url: `/data/globalVariable/resource?resource=` + pageType,
    method: "get",
  });
};
