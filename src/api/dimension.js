import axios from '@/libs/api.request'
export const findDimensionById = (id) => {
  return axios.request({
    url: '/meta/dimension/' + id,
    method: 'get',
  })
}

export const addDimension = (data) => {
  return axios.request({
    url: '/meta/dimension',
    method: 'post',
    data
  })
}

export const updateDimension = (data) => {
  return axios.request({
    url: '/meta/dimension',
    method: 'put',
    data
  })
}

export const deleteDimensionById = (id) => {
  return axios.request({
    url: `/meta/dimension/${id}`,
    method: 'delete'
  })
}

// 查询所有的维度、不分页
export const findAllDimension = () => {
  return axios.request({
    url: '/meta/dimension/list',
    method: 'get'
  })
}

// 新版维度页面-查询血缘关系
export const queryDimensionBlood = (id) => {
  return axios.request({
    url: `/meta/dimension/blood/${id}`,
    method: 'get'
  })
}

// 新版维度页面-查询可关联指标
export const queryIndicatorRelationByDimensionId = (id) => {
  return axios.request({
    url: `/meta/dimension/relationIndicator/${id}`,
    method: 'get'
  })
}

export const updateDimensionGroup = (data) => {
  return axios.request({
    url: '/meta/dataAssets',
    method: 'put',
    data
  })
}

export const addDimensionGroup = (data) => {
  return axios.request({
    url: '/meta/dataAssets',
    method: 'post',
    data
  })
}

export const deleteDimensionGroup = (id) => {
  return axios.request({
    url: `/meta/dataAssets/${id}`,
    method: 'delete'
  })
}

// 查询所有的维度组、包括维度信息、不分页
export const getAllDimensionGroupList = () => {
  return axios.request({
    url: '/meta/dataAssets/groupWithDimension',
    method: 'get'
  })
}

// 新版维度页面-查询维度树状菜单
export const getDimensionTreeData = () => {
  return axios.request({
    url: '/meta/dataAssets/groupAndDimension',
    method: 'get'
  })
}

// 根据人员组织架构关系查询资产分组

export const getDimensionTreeDatagetAssets = () => {
  return axios.request({
    url: '/meta/sysDeptAssets/getAssets',
    method: 'get'
  })
}


// 新版维度页面-查询维度列表（分页）
export const getDimensionList = (params) => {
  return axios.request({
    url: '/meta/dimension/page',
    method: 'get',
    params
  })
}
// 校验code
export const checkAssetsCode = (assetsCode) => {
  return axios.request({
    url: `/meta/dimension/assetsCode?assetsCode=${assetsCode}`,
    method: 'get',
  })
}

