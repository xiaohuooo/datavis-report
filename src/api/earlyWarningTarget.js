import axios from "@/libs/api.request";

// 获取所有规则的接口
export const getRuleTaskList = (data) => {
  return axios.request({
    url: "/monitor/rule/page",
    method: "post",
    data,
  });
};

// 编辑
export const editRuleTask = (data) => {
  return axios.request({
    url: "/monitor/rule/edit",
    method: "post",
    data,
  });
};

// 查看规则详情
export const GetRuleTask = (id) => {
  return axios.request({
    url: `/monitor/rule/${id}`,
    method: "put",
  });
};

// 新增规则任务
export const addRulesTask = (data) => {
  return axios.request({
    url: "/monitor/rule/add",
    method: "post",
    data,
  });
};

// 删除某个预警任务
export const deleteRuleTask = (id) => {
  return axios.request({
    url: `/monitor/rule/${id}`,
    method: "delete",
  });
};

// 获取所有预警任务
export const getWarnTaskList = (data) => {
  return axios.request({
    url: "/monitor/early/warn/page",
    method: "post",
    data,
  });
};

// 新增预警任务
export const addEarlyWarningTask = (data) => {
  return axios.request({
    url: "/monitor/early/warn/add",
    method: "post",
    data,
  });
};

// 编辑预警任务
export const editEarlyWarningTask = (data) => {
  return axios.request({
    url: "/monitor/early/warn/edit",
    method: "post",
    data,
  });
};

// 改变某个预警任务的运行状态
export const changeEarlyWarningTaskStatus = (params) => {
  return axios.request({
    url: `/monitor/early/warn/${params.id}/${params.status}`,
    method: "put",
  });
};

// 获取某个预警任务的详情
export const getEarlyWarningTaskDetail = (id) => {
  return axios.request({
    url: `/monitor/early/warn/${id}`,
    method: "put",
  });
};

// 删除某个预警任务
export const deleteEarlyWarningTask = (id) => {
  return axios.request({
    url: `/monitor/early/warn/${id}`,
    method: "delete",
  });
};

// 处理某个预警任务
export const handleEarlyWarningTask = (data) => {
  return axios.request({
    url: `/monitor/early/warn/record/allot`,
    method: "post",
    data,
  });
};

// 完成某个预警任务
export const completeEarlyWarningTask = (id) => {
  return axios.request({
    url: `/monitor/early/warn/record/deal/${id}`,
    method: "put",
  });
};

// 分享某个预警任务到站内信
export const shareEarlyWarningTask = (data) => {
  return axios.request({
    url: `/notice/message/direct/send`,
    method: "post",
    data,
  });
};

// 某个指标的部分预警信息
export const getTaskInfo = (id) => {
  return axios.request({
    url: `/monitor/early/warn/indicator/level/${id}`,
    method: "put",
  });
};

// 某个预警的触发历史
export const getTiggerHistoryList = (params) => {
  return axios.request({
    url: `/monitor/early/warn/record/page/${params.id}?pageSize=${params.pageSize}&pageNum=${params.pageNum}&dealStatus=${params.dealStatus}&level=${params.level}&startDate=${params.startDate}&endDate=${params.endDate}`,
    method: "put",
  });
};
// 获取管理的部门
export const getDeptList = () => {
  return axios.request({
    url: `/meta/deptUser/manage/dept/list`,
    method: "get",
  });
};
