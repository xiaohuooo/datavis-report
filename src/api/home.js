import axios from '@/libs/api.request'

export const globalQueryData = (params) => {
  return axios.request({
    url: '/meta/search/globalQueryData',
    method: 'get',
    params: params
  })
}

export const getHotData = () => {
  return axios.request({
    url: '/meta/search/hotData',
    method: 'get'
  })
}

export const saveChart = (data) => {
  return axios.request({
    url: '/meta/report/chart',
    method: 'post',
    data
  })
}
