import axios from '@/libs/api.request'

export const addIndicator = (data) => {
  return axios.request({
    url: '/meta/atomIndicator',
    method: 'post',
    data,
  })
}

export const updateIndicator = (data) => {
  return axios.request({
    url: '/meta/atomIndicator',
    method: 'put',
    data,
  })
}

export const deleteIndicatorById = (id) => {
  return axios.request({
    url: `/meta/atomIndicator/${id}`,
    method: 'delete',
  })
}

export const getIndicatorById = (id) => {
  return axios.request({
    url: `/meta/atomIndicator/${id}`,
    method: 'get',
  })
}

// 查询原子指标是否被其他模块使用
export const queryAtomIndicatorUsedById = (id) => {
  return axios.request({
    url: `/meta/atomIndicator/${id}/check`,
    method: 'get',
  })
}

// 查询原子指标是否被用于union
export const queryAtomIndicatorUnionById = id => {
  return axios.request({
    url: `/meta/atomIndicator/check/union?indicatorId=${id}`,
    method: 'get',
  })
}

export const addDerivativeIndicator = data => {
  return axios.request({
    url: '/meta/derivativeIndicator',
    method: 'post',
    data,
  })
}

export const updateDerivativeIndicator = data => {
  return axios.request({
    url: '/meta/derivativeIndicator',
    method: 'put',
    data,
  })
}

export const deleteDerivativeIndicatorById = id => {
  return axios.request({
    url: `/meta/derivativeIndicator/${id}`,
    method: 'delete',
  })
}

export const getDerivativeIndicatorById = id => {
  return axios.request({
    url: `/meta/derivativeIndicator/${id}`,
    method: 'get',
  })
}

export const getBloodIndicatorById = (id, type) => {
  return axios.request({
    url: `meta/indicator/blood?indicatorId=${id}&indicatorType=${type}`,
    method: 'get',
  })
}

export const getAllBusinessModelList = () => {
  return axios.request({
    url: `/meta/businessModel/list`,
    method: 'get',
  })
}

export const queryDataSourceForCategory = (param) => {
  return axios.request({
    url: `/data/dataConnect/dataSourceGroup`,
    method: 'get',
    params: param,
  })
}

// 存映射
export const doMapping = (data) => {
  return axios.request({
    url: `/meta/businessModel/mapping`,
    method: 'put',
    data,
  })
}

export const addComplexIndicator = (data) => {
  return axios.request({
    url: '/meta/compositeIndicator',
    method: 'post',
    data,
  })
}

export const getComplexIndicatorById = (id) => {
  return axios.request({
    url: `/meta/compositeIndicator/${id}`,
    method: 'get',
  })
}
export const deleteComplexIndicatorById = (id) => {
  return axios.request({
    url: `/meta/compositeIndicator/${id}`,
    method: 'delete',
  })
}

export const updateComplexIndicator = (data) => {
  return axios.request({
    url: '/meta/compositeIndicator',
    method: 'put',
    data,
  })
}

// 验证SQL语句
export const testExecuteSql = (data) => {
  return axios.request({
    url: '/data/dataBase/testExecuteSql',
    method: 'post',
    data,
  })
}

// 查询SQL结果
export const queryDataForExecuteSql = (data) => {
  return axios.request({
    url: '/data/dataConnect/queryDataForExecuteSql',
    method: 'post',
    data,
  })
}

// 查询SQL结果集的元数据
export const getSqlData = (data) => {
  return axios.request({
    url: '/data/dataConnect/sql/data',
    method: 'post',
    data,
  })
}

// 查询所有的指标组、包括指标信息、不分页
export const getAllIndicatorGroupList = () => {
  return axios.request({
    url: '/meta/dataAssets/groupWithIndicator',
    method: 'get',
  })
}

// 查询所有的指标、不分页
export const findAllIndicator = () => {
  return axios.request({
    url: '/meta/indicator/list',
    method: 'get',
  })
}
//查询指标组树状菜单
export const getIndicatorTreeData = () => {
  return axios.request({
    url: '/meta/dataAssets/groupAndIndicator',
    method: 'get',
  })
}

// 检测指标编码是否存在
export const checkIndicatorCodeExist = ({ type, assetsCode, groupId }) => {
  return axios.request({
    url: `/meta/indicator/${type}/exist?assetsCode=${assetsCode}&groupId=${groupId}`,
    method: 'get',
  })
}

// 根据指标获取相关联的指标
export const getRelationIndicator = data => {
  return axios.request({
    url: '/meta/indicator/relationIndicator',
    method: 'post',
    data,
  })
}

// 添加外部指标
export const addOutIndicator = data => {
  return axios.request({
    url: "/meta/outIndicator",
    method: "post",
    data
  });
};

// 获取外部指标详情
export const getOutIndicatorById = id => {
  return axios.request({
    url: `/meta/outIndicator/${id}`,
    method: "get"
  });
};

// 删除外部指标
export const deleteOutIndicatorById = id => {
  return axios.request({
    url: `/meta/outIndicator/${id}`,
    method: "delete"
  });
};

// 更新外部指标
export const updateOutIndicator = data => {
  return axios.request({
    url: "/meta/outIndicator",
    method: "put",
    data
  });
};

// 获取业务模型分组及其列
export const getBusinessModelGroupAndColumn = () => {
  return axios.request({
    url: `/meta/dataAssets/groupAndBusinessModel`,
    method: 'get'
  })
}

export const getDataTaskResult = (indicatorId, processDate, indicatorType= 1) => {
  return axios.request({
    url: `/meta/indicator/dataTask?indicatorId=${indicatorId}&indicatorType=${indicatorType}&processDate=${processDate}`,
    method: 'get'
  })
}

// 新增静态指标
export const addStaticIndicator = data => {
  return axios.request({
    url: '/meta/staticIndicator',
    method: 'post',
    data,
  })
}

export const updateStaticIndicator = data => {
  return axios.request({
    url: '/meta/staticIndicator',
    method: 'put',
    data,
  })
}

export const deleteStaticIndicatorById = id => {
  return axios.request({
    url: `/meta/staticIndicator/${id}`,
    method: 'delete',
  })
}

export const getStaticIndicatorById = id => {
  return axios.request({
    url: `/meta/staticIndicator/${id}`,
    method: 'get',
  })
}

export const verifyExpression = data => {
  return axios.request({
    url: '/report/verifyFormula',
    method: 'post',
    data,
  })
}
