import axios from '@/libs/api.request'
export const reloadCube = data => {
  return axios.request({
    url: '/data/task/reload/cube',
    method: 'post',
    data,
  })
}
