import axios from "@/libs/api.request";

// 获取所有tab
export const getIndicatorTab = params => {
  return axios.request({
    url: `/meta/tab/${params}/find`,
    method: "put"
  });
};

// 添加自定义tab
export const addCustomTab = data => {
  return axios.request({
    url: "/meta/tab/add",
    method: "post",
    data
  });
};

// 编辑自定义tab
export const editCustomTab = data => {
  return axios.request({
    url: "/meta/tab/edit",
    method: "post",
    data
  });
};

// 删除自定义tab
export const deleteCustomTab = id => {
  return axios.request({
    url: `/meta/tab/${id}`,
    method: "delete"
  });
};

/**
 * 1. 向某个标签添加指标
 * 2. 收藏
 * @param {*} tabId 标签id
 * @param {*} indicatorId 指标id
 * @returns
 */
export const syncToOtherTab = params => {
  return axios.request({
    url: `/meta/tab/${params.tabId}/${params.indicatorId}/add`,
    method: "put"
  });
};

/**
 * 1. 移除某个标签的指标
 * 2. 取消收藏
 * @param {*} tabId 标签id
 * @param {*} indicatorId 指标id
 * @returns
 */
export const cancelCollect = params => {
  return axios.request({
    url: `/meta/tab/${params.tabId}/${params.indicatorId}/delete`,
    method: "delete"
  });
};

/**
 * 删除指标
 * @param {*} id 指标id
 * @param {*} type 指标类型
 * @returns
 */
export const deleteIndicator = (id, type) => {
  return axios.request({
    url: `/meta/indicator/${id}/${type}/delete`,
    method: "delete"
  });
};

/**
 * 复制指标
 * @param {*} id 指标id
 * @param {*} type 指标类型
 * @returns
 */
export const copyIndicator = (id, type) => {
  return axios.request({
    url: `/meta/indicator/${id}/${type}/copy`,
    method: "put"
  });
};

/**
 * 变更指标所有者
 * @param {*} id 指标id
 * @param {*} type 指标类型
 * @returns
 */
export const changeIndicatorOwner = (id, type, userName) => {
  return axios.request({
    url: `/meta/indicator/${id}/${type}/${userName}/owner`,
    method: "put"
  });
};

/**
 * 分页查询某个tab下的指标
 * @param {Number} tabId tab id
 * @param {String} name 指标名称
 * @param {Number} status 状态
 * @param {Array} groupIds 资产分组
 * @param {String} versionCode 版本号
 * @param {String} attribute 属性
 * @param {Array} labelIds 标签id
 * @param {Number} pageSize 每页条数
 * @param {Number} pageNum 页码
 */
export const getIndicatorByTabOnPage = data => {
  return axios.request({
    url: "/meta/indicator/getPageByTab",
    method: "post",
    data,
    cancalKey : 'getIndicatorByTabOnPage'
  });
};

/**
 * 查询某个指标的关联维度
 * @param {Number} indicatorId 指标id
 * @param {Number} type 指标类型
 */
export const getRelationDimension = params => {
  return axios.request({
    url: "/meta/dimension/indicator",
    method: "get",
    params
  });
};

export const outIndicatorImport = (data, groupId) => {
  return axios.request({
    url: `/meta/outIndicator/batch?groupId=${groupId}`,
    method: "post",
    data,
  });
};

/**
 * 查询复合指标血缘关系指标
 * @param params
 * @returns {*}
 */
export const getCompositeBlood = params => {
  return axios.request({
    url: `/meta/compositeIndicator/compositeBlood?id=${params.id}`,
    method: "get",
  });
};


export const getCompositeAnalysis = params => {
  return axios.request({
    url: `/meta/compositeIndicator/analysis?id=${params.id}&startDate=${params.startDate}&endDate=${params.endDate}`,
    method: "get",
  });
};
