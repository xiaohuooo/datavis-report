import axios from "@/libs/api.request";

// 消息归档
export const msgArchiving = data => {
  return axios.request({
    url: "/meta/answer/msgArchiving",
    method: "post",
    data
  });
};

// 消息列表
export const msgList = data => {
  return axios.request({
    url: "/meta/answer/msgList",
    method: "post",
    data
  });
};

// 清空对话
export const deleteAnswer = id => {
  return axios.request({
    url: `/meta/answer/${id}`,
    method: "delete"
  });
};

// 名称查询维度和指标
export const queryRefIndicatorAndDimension = data => {
  return axios.request({
    url: "/meta/answer/queryRefIndicatorAndDimension",
    method: "post",
    data
  });
};
