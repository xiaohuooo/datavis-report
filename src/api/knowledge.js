import axios from "@/libs/api.request";

// 获取知识列表
export const getKnowledgeList = params => {
  return axios.request({
    url: "/meta/knowledge/getKnowledgeList",
    method: "get",
    params
  });
};

// 查看知识详情
export const getKnowledgeInfo = params => {
  return axios.request({
    url: "/meta/knowledge/getKnowledgeInfo?knowledgeId=" + params.id,
    method: "get"
  });
};

// 创建知识
export const knowledgeAdd = data => {
  return axios.request({
    url: "/meta/knowledge/create",
    method: "post",
    data
  });
};

// 点赞
export const knowledgeLike = params => {
  return axios.request({
    url: "/meta/knowledge/like?knowledgeId=" + params.id,
    method: "post",
  });
};
// 取消点赞
export const knowledgeUnlike = params => {
  return axios.request({
    url: "/meta/knowledge/unLike?knowledgeId=" + params.id,
    method: "post"
  });
};
// 收藏
export const knowledgeCollect = params => {
  return axios.request({
    url: "/meta/knowledge/collect?knowledgeId=" + params.id,
    method: "post"
  });
};
// 取消收藏
export const knowledgeUnCollect = params => {
  return axios.request({
    url: "/meta/knowledge/unCollect?knowledgeId=" + params.id,
    method: "post"
  });
};

// 上下线
export const updateKnowledgeStatus = params => {
  return axios.request({
    url: "/meta/knowledge/isView?knowledgeId=" + params.id + '&isView=' + params.view,
    method: "post"
  });
};

// 删除知识
export const deleteKnowledgeById = id => {
  return axios.request({
    url: "/meta/knowledge/" + id,
    method: "delete"
  });
};

// 修改知识
export const updateKnowledge = data => {
  return axios.request({
    url: "/meta/knowledge/edit",
    method: "post",
    data
  });
};

// 分享
export const knowledgeShare = params => {
  return axios.request({
    url: "/meta/knowledge/share?knowledgeId=" + params.id,
    method: "post"
  });
};
