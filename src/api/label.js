import axios from '@/libs/api.request'
// 查询所有的维度组、包括维度信息、不分页
export const getLabelGroup = () => {
  return axios.request({
    url: '/meta/label/group',
    method: 'get'
  })
}

export const updateLabelGroup = (data) => {
  return axios.request({
    url: '/meta/label/group',
    method: 'put',
    data
  })
}

export const addLabelGroup = (data) => {
  return axios.request({
    url: '/meta/label/group',
    method: 'post',
    data
  })
}

export const deleteLabelGroup = (id) => {
  return axios.request({
    url: `/meta/label/group/${id}`,
    method: 'delete'
  })
}

export const addLabel = (data) => {
  return axios.request({
    url: '/meta/label',
    method: 'post',
    data
  })
}

export const updateLabel = (data) => {
  return axios.request({
    url: '/meta/label',
    method: 'put',
    data
  })
}

export const deleteLabel = (id) => {
  return axios.request({
    url: `/meta/label/${id}`,
    method: 'delete'
  })
}

export const getLabelByGroupId = (id) => {
  return axios.request({
    url: `/meta/label?groupId=${id}`,
    method: 'get'
  })
}

// 维度、指标添加标签
export const addDimensionAndIndicatorLabel = (data) => {
  return axios.request({
    url: '/meta/label/source',
    method: 'put',
    data,
  })
}

// 维度、指标去除标签
export const removeDimensionAndIndicatorLabel = (params) => {
  return axios.request({
    url: `/meta/label/source?sourceId=${params.sourceId}&labelId=${params.labelId}`,
    method: 'delete',
  })
}

// 根据标签查维度、指标
export const queryDimensionAndIndicatorByLabel = (params) => {
  return axios.request({
    url: `/meta/label/source?name=${params.name}&type=${params.type}`,
    method: 'get',
  })
}


// 查询版本号
export const queryAllVersion = params => {
  return axios.request({
    url: "/meta/version/findAll",
    method: "get",
    params
  });
};

// 修改版本号等级
export const changeVersionLevel = params => {
  return axios.request({
    url: "/meta/version/changeLevel",
    method: "get",
    params
  });
};

// 添加版本号
export const addVersion = data => {
  return axios.request({
    url: "/meta/version/add",
    method: "post",
    data
  });
};

// 编辑版本号
export const editVersion = data => {
  return axios.request({
    url: "/meta/version/edit",
    method: "post",
    data
  });
};

// 删除版本号
export const deleteVersion = id => {
  return axios.request({
    url: `/meta/version/delete/${id}`,
    method: "delete"
  });
};
