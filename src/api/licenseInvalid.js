import axios from "@/libs/api.request";

// 获取机器信息
export const getMachineInfo = () => {
  return axios.request({
    url: "/meta/cipher/machineCode",
    method: "get"
  });
};
