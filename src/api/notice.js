import axios from '@/libs/api.request'

export const getMessageList = (data) => {
  return axios.request({
    url: '/notice/message/page',
    method: 'post',
    data
  })
}

export const updateMessage = (receiverId, status) => {
  return axios.request({
    url: `/notice/message/${receiverId}/${status}/update`,
    method: 'put',
  })
}
