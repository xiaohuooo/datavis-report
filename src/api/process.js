import axios from '@/libs/api.request'

export const getProcessType = () => {
  return axios.request({
    url: "/flow/process/template/processType",
    method: "get"
  });
}

export const getRoleIdList = () => {
  return axios.request({
    url: '/meta/process/template/roleIdList',
    method: 'get'
  })
}

export const saveProcessTemplate = (data) => {
  return axios.request({
    url: '/meta/process/template',
    method: 'post',
    data
  })
}

export const queryProcessTemplateForPage = (params) => {
  return axios.request({
    url: '/meta/process/template/page',
    method: 'get',
    params: params
  })
}

export const deleteProcessTemplate = (id) => {
  return axios.request({
    url: `/meta/process/template/${id}`,
    method: 'delete'
  })
}

export const processTemplateDetail = (id) => {
  return axios.request({
    url: `/meta/process/template/${id}`,
    method: 'get'
  })
}

export const updateStatus = (params) => {
  return axios.request({
    url: `/meta/process/template/updateStatus/${params.id}/${params.status}`,
    method: 'get'
  })
}

// 流程列表
export const findFlowData = (params) => {
  return axios.request({
    url: "/flow/flow/page",
    method: "get",
    params: params
  });
}

export const findFlowDetailData = (id) => {
  return axios.request({
    url: `/flow/flow/${id}`,
    method: "get"
  });
}

export const reviewFlow = (data) => {
  return axios.request({
    url: "/flow/flow/reviewFlow",
    method: "post",
    data
  });
}
