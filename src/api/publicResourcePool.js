import axios from "@/libs/api.request";

/**
 * 获取已发布的数据(维度/指标)
 */
export const getDataOnPage = data => {
  return axios.request({
    url: `/meta/common/page`,
    method: "post",
    data
  });
};
