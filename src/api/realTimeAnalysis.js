import axios from '@/libs/api.request'

export const matchingRelation = data => {
  return axios.request({
    url: '/meta/indicator/matchingRelation',
    method: 'post',
    data,
  })
}
