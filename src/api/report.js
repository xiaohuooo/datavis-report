import axios from '@/libs/api.request'
let cancelToken = null;
export const queryChartData = (data) => {
  return axios.request({
    url: '/report/data',
    method: 'post',
    data
  })
}

//更新文本框在预览时的views
export const updateTextareaContent = (data) => {
  return axios.request({
    url: '/meta/report/updateTextareaContent',
    method: 'put',
    data
  })
}

// 带有分页
export const queryChartDataForPage = (data) => {
  return axios.request({
    url: '/report/data',
    method: 'post',
    data
  })
}

// 查询维度是否是日期类型
export const queryDimensionProp = (data) => {
  return axios.request({
    url: '/report/queryDimensionProp',
    method: 'post',
    data
  })
}

export const queryMoreCompareChartData = (data) => {
  return axios.request({
    url: '/report/data/getDataForMoreDate',
    method: 'post',
    data
  })
}

// 查询维成员
export const queryDimensionMember = (data) => {
  return axios.request({
    url: '/report/dimensionMember',
    method: 'post',
    data
  })
}

export const queryReportList = (params) => {
  return axios.request({
    url: '/meta/report/list',
    method: 'get',
    params
  })
}

export const saveReport = (data) => {
  return axios.request({
    url: '/meta/report',
    method: 'post',
    data
  })
}
export const updateReport = (data) => {
  return axios.request({
    url: '/meta/report',
    method: 'put',
    data
  })
}
export const deleteReportById = (reportId) => {
  return axios.request({
    url: `/meta/report/${reportId}`,
    method: 'delete'
  })
}
export const listReport = (param) => {
  return axios.request({
    url: `/meta/report/list?currentPage=${param.currentPage}&pageSize=${param.pageSize}`,
    method: 'get'
  })
}

export const getReportTempDetail = (param) => {
  let url
  if (param.startDate) {
    url = `/meta/report/detail?reportTemplateId=${param.id}&startDate=${param.startDate}&endDate=${param.endDate}`
  } else {
    url = `/meta/report/detail?reportTemplateId=${param.id}`
  }
  return axios.request({
    url,
    method: 'get'
  })
}

export const findDimensionsByIndicator = (indicatorId,type) => {
  return axios.request({
    url: `/meta/dimension/indicators?indicatorId=${indicatorId}&type=${type}`,
    method: 'get'
  })
}

export const findIndicatorsByDimension = (dimensionId) => {
  return axios.request({
    url: `/meta/atomIndicator/dimensions?dimensionId=${dimensionId}`,
    method: 'get'
  })
}

export const getReportSnapshots = (reportId) => {
  return axios.request({
    url: `/meta/report/snapshot/${reportId}`,
    method: 'get'
  })
}

//表格数据接口
export const getReportCompareDataAndHeaders = data => {
  return axios.request({
    url: 'report/data',
    method: 'post',
    data
  });
}


export const getReportcardData= data => {


  return axios.request({
    url: 'report/cardData',
    method: 'post',
    data,
    
  });
}


// 获取首页生成的资源
export const queryChartResource = (params) => {
  return axios.request({
    url: '/meta/chartResource/list',
    method: 'post',
    params: params
  })
}

// 修改资源名
export const updateSingleChartResource = (data) => {
  return axios.request({
    url: '/meta/chartResource/single',
    method: 'put',
    data
  })
}

export const updateMoreChartResource = (data) => {
  return axios.request({
    url: '/meta/chartResource/more',
    method: 'put',
    data
  })
}

export const deleteMoreChartResource = (resourceId) => {
  return axios.request({
    url: `/meta/chartResource/more/${resourceId}`,
    method: 'delete'
  })
}

export const deleteSingleChartResource = (resourceId) => {
  return axios.request({
    url: `/meta/chartResource/single/${resourceId}`,
    method: 'delete'
  })
}

// 图片上传
export const uploadImg = (data, opt = {}) => {
  return axios.request({
    url: '/storage/file/upload',
    method: 'post',
    data,
    ...opt
  })
}

// 图片预览
export const previewImg = (fileId) => {
  return axios.request({
    url: '/storage/file/download?fileId='+fileId,
    method: 'get',
    responseType: 'blob'
  })
}
