import axios from '@/libs/api.request'

// 查看备份列表
export const getBackupList = params => {
  return axios.request({
    url: '/data/backup/list',
    method: 'get',
    params: params
  })
}

// 手动备份
export const touchBackup = params => {
  return axios.request({
    url: '/data/backup/touch',
    method: 'get',
    params: params
  })
}

// 查询自动备份配置
export const getBackupRule = params => {
  return axios.request({
    url: '/data/backup/rule',
    method: 'get',
    params: params
  })
}

// 下载备份
export const downLoadBackup = id => {
  return axios.request({
    url: `/data/backup/download?backupId=${id}`,
    method: 'get',
    responseType: 'blob',
    // headers:{ 'Content-Type': 'application/json; application/octet-stream'},
  })
}

// 删除某个备份
export const deleteBackup = id => {
  return axios.request({
    url: `/data/backup/delete?backupId=${id}`,
    method: 'delete'
  })
}

// 修改自动备份配置
export const updateRule = (data) => {
  return axios.request({
    url: '/data/backup/rule',
    method: 'post',
    data: data
  })
}

// 还原某个备份
export const revertBackup = (id) => {
  return axios.request({
    url: `/data/backup/restore?backupId=${id}`,
    method: 'get'
  })
}
