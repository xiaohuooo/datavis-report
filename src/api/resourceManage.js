import axios from '@/libs/api.request'

// 获取列表
export const getChartResource = data => {
  return axios.request({
    url: '/meta/chartResource/list',
    method: 'post',
    data
  })
}

// 单组件资源创建
export const addSingleResource = data => {
  return axios.request({
    url: '/meta/chartResource/single',
    method: 'post',
    data
  })
}

// 单组件资源修改
export const updateSingleResource = data => {
  return axios.request({
    url: '/meta/chartResource/single',
    method: 'put',
    data
  })
}

// 单组件资源查询
export const getSingleResource = id => {
  return axios.request({
    url: `/meta/chartResource/single/${id}`,
    method: 'get'
  })
}

// 单组件资源状态修改
export const updateSingleResourceStatus = params => {
  return axios.request({
    url: `/meta/chartResource/single/${params.resourceId}/${params.status}`,
    method: 'put'
  })
}

// 单组件资源删除
export const deleteSingleResource = id => {
  return axios.request({
    url: `/meta/chartResource/single/${id}`,
    method: 'delete'
  })
}

// 组合组件资源创建
export const addMoreResource = data => {
  return axios.request({
    url: '/meta/chartResource/more',
    method: 'post',
    data
  })
}

// 组合组件资源修改
export const updateMoreResource = data => {
  return axios.request({
    url: '/meta/chartResource/more',
    method: 'put',
    data
  })
}

// 组合组件资源查询
export const getMoreResource = id => {
  return axios.request({
    url: `/meta/chartResource/more/${id}`,
    method: 'get'
  })
}

// 组合组件资源状态修改
export const updateMoreResourceStatus = params => {
  return axios.request({
    url: `/meta/chartResource/more/${params.resourceId}/${params.status}`,
    method: 'put'
  })
}

// 组合组件资源删除
export const deleteMoreResource = id => {
  return axios.request({
    url: `/meta/chartResource/more/${id}`,
    method: 'delete'
  })
}

// 定制化组件资源查询
export const getCustomResource = id => {
  return axios.request({
    url: `/meta/chartResource/custom/${id}`,
    method: 'get'
  })
}

// 定制化组件资源状态修改
export const updateCustomResourceStatus = params => {
  return axios.request({
    url: `/meta/chartResource/custom/${params.resourceId}/${params.status}`,
    method: 'put'
  })
}

// 定制化组件资源删除
export const deleteCustomResource = id => {
  return axios.request({
    url: `/meta/chartResource/custom/${id}`,
    method: 'delete'
  })
}

// 移动分组接口
export const moveResource = data => {
  return axios.request({
    url: `/meta/dataAssets/move`,
    method: 'put',
    data
  })
}

// 导出为服务接口
export const exportApi = id => {
  return axios.request({
    url: `/meta/chartResource/export/${id}`,
    method: 'put'
  })
}
