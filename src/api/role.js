import axios from '@/libs/api.request'
//角色配置-角色分类
export const getsortList = (params) => {
  return axios.request({
    url: '/meta/group/list',
    method: 'get',
    params:params
  })
}

// 获取所有的角色
export const listGroupRole = () => {
  return axios.request({
    url: '/meta/group/listGroupRole',
    method: 'get'
  })
}

// 根据用户id查询角色
export const getRolesByUserId = (params) => {
  return axios.request({
    url: '/meta/roles/getRolesByUserId',
    method: 'get',
    params: params
  })
}

// 根据用户id查询部门和主管部门
export const getDeptByUserId = (params) => {
  return axios.request({
    url: '/meta/sysDep/getDeptByUserId',
    method: 'get',
    params: params
  })
}

// 根据角色id获取用户
export const obtainUserByRoleId = (params) => {
  return axios.request({
    url: '/meta/roles/obtainUserByRoleId',
    method: 'get',
    params: params
  })
}

// 根据角色id获取未授权的用户，带分页
export const getNoPermissionUserByRoleId = (params) => {
  return axios.request({
    url: '/meta/roles/getNoPermissionUserByRoleId',
    method: 'get',
    params: params
  })
}

// 给角色绑定用户
export const bindUser = (data) => {
  return axios.request({
    url: '/meta/userRole/bindUser',
    method: 'post',
    data
  })
}

// 给用户解绑角色
export const unBindUser = (data) => {
  return axios.request({
    url: '/meta/userRole/unBindUser',
    method: 'post',
    data
  })
}


// 角色删除
export const deleteRole = id => {
    return axios.request({
      url: '/meta/roles/delByIds?ids=' + id,
      method: 'get'
    })
  }

  export const getRoleList = (params) => {
    return axios.request({
      url: '/meta/roles/listOrOne',
      method: 'post',
      data: params
    })
  }


  export const saveOrUpdateRole = (data) => {
    return axios.request({
      url: '/meta/roles/saveOrUpdate',
      method: 'post',
      data: data
    })
  }

// 根据角色id获取资源菜单
export const getCheckedResIdsByRoleId = (params) => {
  return axios.request({
    url: '/meta/roles/getCheckedResIdsByRoleId',
    method: 'get',
    params:params
  })
}

// 给某个用户id绑定资源菜单
export const bindResToRole = (data) => {
  return axios.request({
    url: '/meta/res/bindResToRole',
    method: 'post',
    data
  })
}

// 根据用户id获取资源菜单
export const getResIdsByUserId = (params) => {
  return axios.request({
    url: '/meta/resources/getResIdsByUserId',
    method: 'get',
    params:params
  })
}

// 根据资源id查角色
export const getRolesByResId = (params) => {
  return axios.request({
    url: '/meta/roles/getRolesByResId',
    method: 'get',
    params: params
  })
}

// 根据资源id查未绑定的角色
export const getUnBindRolesByResId = (params) => {
  return axios.request({
    url: '/meta/roles/getUnBindRolesByResId',
    method: 'get',
    params: params
  })
}

// 给资源绑定角色
export const bindRolesToRes = (data) => {
  return axios.request({
    url: '/meta/res/bindRolesToRes',
    method: 'post',
    data
  })
}

// 为资源解绑角色
export const unBindRolesToRes = (data) => {
  return axios.request({
    url: '/meta/res/unBindRolesToRes',
    method: 'post',
    data
  })
}
