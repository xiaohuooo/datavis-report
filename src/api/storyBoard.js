import axios from '@/libs/api.request'

export const addStoryBoard = (data) => {
  return axios.request({
    url: '/meta/storyBoard',
    method: 'post',
    data,
  })
}

export const checkNameUnique = (data) => {
  return axios.request({
    url: '/meta/storyBoard/nameUnique',
    method: 'post',
    data,
  })
}

export const updateStoryBoard = (data) => {
  return axios.request({
    url: '/meta/storyBoard',
    method: 'put',
    data,
  })
}

export const deleteStoryBoard = (id) => {
  return axios.request({
    url: `/meta/storyBoard/${id}`,
    method: 'delete',
  })
}

export const getStoryBoardDetail = (id) => {
  return axios.request({
    url: `/meta/storyBoard/${id}`,
    method: 'get',
  })
}

export const getStoryBoardGroupList = () => {
  return axios.request({
    url: '/meta/storyBoardGroup/list',
    method: 'get'
  })
}

export const getStoryBoardGroupWithStoryBoard = () => {
  return axios.request({
    url: '/meta/storyBoardGroup/listWithStoryBoard',
    method: 'get'
  })
}

export const addStoryBoardGroup = (data) => {
  return axios.request({
    url: '/meta/storyBoardGroup',
    method: 'post',
    data,
  })
}

export const updateStoryBoardGroup = (data) => {
  return axios.request({
    url: '/meta/storyBoardGroup',
    method: 'put',
    data,
  })
}

export const deleteStoryBoardGroup = (id) => {
  return axios.request({
    url: `/meta/storyBoardGroup/${id}`,
    method: 'delete',
  })
}

export const moveStoryBoardGroup = (data) => {
  return axios.request({
    url: `/meta/storyBoard/move`,
    method: 'put',
    data
  })
}

