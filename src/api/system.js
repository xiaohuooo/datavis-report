import axios from '@/libs/api.request'


export const login = ({ username, password,credentials }) => {
  const data = {
    username,
    password,
    credentials
  }
  return  axios.request({
    url: '/meta/password/login',
    method: 'post',
    data
  })
}

export const register = (data) => {
  return  axios.request({
    url: '/meta/users/register',
    method: 'post',
    data
  })
}

export const changePassword = (data) => {
  return  axios.request({
    url: '/meta/users/changePassword',
    method: 'post',
    data
  })
}

export const resetPassword = (data) => {
  return  axios.request({
    url: '/meta/users/resetPassword',
    method: 'put',
    data
  })
}

export const changePasswordByUserSelf = (data) => {
  return  axios.request({
    url: '/meta/users/resetPassword',
    method: 'post',
    data
  })
}

// 验证当前电话号码或邮箱是否绑定了账户
export const queryUser = (params)=>{
  return  axios.request({
    url: '/meta/users/validation',
    method: 'get',
    params
  })
}

// 发送验证码
export const sendAuthCode = (params) => {
  return  axios.request({
    url: '/meta/users/sendVerificationCode',
    method: 'get',
    params
  })
}
//所有部门
export const getDepTree = () => {
  return  axios.request({
    url: 'meta/sysDept/auth',
    method: 'get',
  })
  // return  axios.request({
  //   url: 'meta/sysDep/obtainCompleteTree',
  //   method: 'get',
  // })
}

// /sysDep/saveDepOrModify 编辑或新增部门
export const saveDepOrModify = (data) => {
  return  axios.request({
    url: 'meta/sysDep/saveDepOrModify',
    method: 'post',
    data: data
  })
}
// DELETE /sysDep/delDep 删除部门
export const removeOrg = (data) => {
  return  axios.request({
    url: 'meta/sysDep/delDep',
    method: 'delete',
    data: data
  })
}

// 根据部门id查询当前部门下的用户列表
export const getUsersByDepId = (params) => {
  return  axios.request({
    url: 'meta/sysDep/getUsersByDepId',
    method: 'get',
    params: params
  })
}
//组织架构管理-根节点
export const getOrgParent = (params) => {
  return  axios.request({
    url: 'meta/sysDep/originalParent',
    method: 'post',
    data: params
  })
}
//组织架构管理-子节点
export const getOrgChildById = (id) => {
  return  axios.request({
    url: 'meta/sysDep/obtainChildById',
    method: 'get',
    params: id
  })
}
//组织架构管理-保存修改
export const modifyStatus = (data) => {
  return  axios.request({
    url: 'meta/sysDep/modifyStatus',
    method: 'post',
    data: data
  })
}
//组织架构管理-查询
export const searchOrg = (data) => {
  return  axios.request({
    url: 'meta/sysDep/search',
    method: 'post',
    data: data
  })
}
//组织架构管理
export const getOrgAll = () => {
  return  axios.request({
    url: 'meta/sysDep/obtainAll',
    method: 'get',
  })
}
//资源管理-根节点
export const getRootRes = () => {
  return  axios.request({
    url: 'meta/resources/getRootRes',
    method: 'get',
  })
}

//资源管理-子节点
export const getChildRes = (id) => {
  return  axios.request({
    url: 'meta/resources/getChildrenById?id=' + id,
    method: 'get'
  })
}
export const getUserList = (params) => {
  return  axios.request({
    url: '/meta/deptUser/user/page',
    method: 'post',
    data: params
  })
}
export const getAuthRole = () => {
  return  axios.request({
    url: '/meta/sysUser/authRole',
    method: 'get'
  })
}

export const saveOrUpdateUser = (data) => {
  let url = ''
  let arr = []
  let params = {}
  if (data.id) {
    arr.push(data)
    params = arr
    url = '/meta/users/updateUser' //修改
  } else {
    url = '/meta/users/insertUser' //新增
    params = data
  }
  return  axios.request({
    url: url,
    method: 'post',
    data: params
  })
}

export const getMyInfo = () => {
  return  axios.request({
    url: '/meta/users/current',
    method: 'get'
  })
}

export const deleteUser = params => {
  return  axios.request({
    url: 'meta/users/delUsers',
    method: 'get',
    params: params
  })
}


export const grantedMenu = (data) => {
  return  axios.request({
    url: '/meta/resources/granted',
    method: 'post',
    data: data
  })
}

export const getAuthorizeMenuTree = (roleId) => {
  return  axios.request({
    url: '/meta/resources/' + roleId + '/resources',
    method: 'get'
  })
}

export const saveOrUpdateMenu = data => {
  return  axios.request({
    url: '/meta/resources/saveOrUpdate',
    data,
    method: 'post'
  })
}

export const deleteMenu = id => {
  return  axios.request({
    url: '/meta/resources/' + id,
    method: 'delete'
  })
}

// 更新或新增资源
export const updateOrInsertRes = data => {
  return  axios.request({
    url: '/meta/resources/updateOrInsertRes',
    data,
    method: 'post'
  })
}

// 根据id删除资源
export const delRes = id => {
  return  axios.request({
    url: '/meta/resources/delRes?ids='+id,
    method: 'get'
  })
}

export const getRoleList = (params) => {
  return  axios.request({
    url: '/meta/roles/listOrOne',
    method: 'post',
    data: params
  })
}

export const saveOrUpdateRole = (data) => {
  return  axios.request({
    url: '/meta/roles/saveOrUpdate',
    method: 'post',
    data: data
  })
}

export const logout = () => {
  return  axios.request({
    url: 'logout',
    method: 'post'
  })
}
//获取按钮权限
export const getBtnByPageId = (id) => {
  return  axios.request({
    url: 'meta/resources/getChildrenById?id=' + id,
    method: 'get'
  })
}



//根据应用id查询业务菜单
export const getResByApply = (params) => {
  return  axios.request({
    url: 'meta/resources/getResByApply',
    method: 'get',
    params
  })
}


//查询环境列表-分页
export const querySysEnvironment = (data) => {
  return  axios.request({
    url: 'meta/sysEnvironmentApi/querySysEnvironment',
    method: 'post',
    data
  })
}


//查询环境列表-全部
export const querySysEnvironmentAll = (data) => {
  return  axios.request({
    url: '/meta/sysEnvironmentApi/querySysEnvironmentAll',
    method: 'post',
    data
  })
}

// GET /sysEnvironmentApi/queryInternalEnvironment查询移动端内置环境
export const queryInternalEnvironment = (params) => {
  return  axios.request({
    url: '/meta/sysEnvironmentApi/queryInternalEnvironment',
    method: 'get',
    params
  })
}


//环境保存
export const saveSysEnvironment = (data) => {
  return  axios.request({
    url: '/meta/sysEnvironmentApi/saveSysEnvironment',
    method: 'post',
    data
  })
}


//环境删除

export const removeById = (data) => {
  return  axios.request({
    url: '/meta/sysEnvironmentApi/removeById',
    method: 'post',
    data
  })
}



//修改布局方式
export const updateApplyAtResource = (data) => {
  return  axios.request({
    url: '/meta/resources/updateApplyAtResource',
    method: 'post',
    data
  })
}



//修改布局方式
export const queryHomeResource = (params) => {
  return  axios.request({
    url: '/meta/sysApply/queryHomeResource',
    method: 'get',
    params
  })
}



//菜单拖拽排序功能
export const moveMenuRes = (params) => {
  return  axios.request({
    url: '/meta/resources/moveRes',
    method: 'get',
    params
  })
}


//查询应用的数据列表
export const querySysApplyDetails = (params) => {
  return  axios.request({
    url: '/meta/sysApply/querySysApplyDetails',
    method: 'get',
    params
  })
}


//更新应用
export const saveSysApply = (data) => {
  return  axios.request({
    url: '/meta/sysApply/saveSysApply',
    method: 'post',
    data
  })
}

//删除应用
export const removeappById = (params) => {
  return  axios.request({
    url: '/meta/sysApply/removeById',
    method: 'get',
    params
  })
}



export const imgAdd = (data ,type =1) => {
  return  axios.request({
    url: `/storage/file/upload`,
    method: 'post',
    data
  })
}



export const getEnvironmentByRes = (params) => { // 根据菜单id查询环境
  return  axios.request({
    url: `/meta/resources/getEnvironmentByRes`,
    method: 'get',
    params
  })
}

export const getsysContent  = (id='') => { // 根据菜单id查询环境
  return  axios.request({
    url: `/meta/sysContent/${id}`,
    method: 'put',
  })
}



//查询子应用
export const queryAllSubApply = (params ) => {
  return  axios.request({
    url: `/meta/sysApply/queryAllSubApply`,
    method: 'get',
    params
  })
}


//查询报告/报表列表
export function querySysEnvironmentDetail(params){
  return axios.request({
    url: '/meta/sysEnvironmentApi/querySysEnvironmentDetail',
    method: 'get',
    params,
  })
 }

 //用户管理- 是否首次用户
export function isFirst(){
  return axios.request({
    url: '/meta/users/sync/isFirst',
    method: 'get',

  })
 }
// 立即同步
 export function syncTrigger(params){
  return axios.request({
    url: '/meta/users/sync/trigger',
    method: 'get',
    params
  })
 }


 export function getStatus(params){
  return axios.request({
    url: '/meta/users/sync/status',
    method: 'get',
    params
  })
 }

 export function historyOld(data){
  return axios.request({
    url: '/meta//users/sync/history',
    method: 'post',
    data
  })
 }


//  GET /users/sync/truncate 清空同步
export function syncTruncate(params){
  return axios.request({
    url: '/meta/users/sync/truncate',
    method: 'get',
    params
  })
 }

//  /users/sync/view查看同步用户设置
export function syncView(params){
  return axios.request({
    url: '/meta/users/sync/view',
    method: 'get',
    params
  })
 }

 // POST /users/sync/config修改同步用户设置
export function syncConfig(data){
  return axios.request({
    url: '/meta/users/sync/config',
    method: 'post',
    data
  })
 }

export function downloadImage(params) {
  return axios.request({
    url: '/storage/file/download?fileId='+ params.fileId,
    method: 'get',
    responseType: 'blob'
  })
}


export function getsysPost(params) { //获取岗位
  return axios.request({
    url: '/meta/sysPost',
    method: 'get',
    params,
  })
}
export function deletesysPost(params) { // 删除岗位
  return axios.request({
    url: '/meta/sysPost',
    method: 'delete',
    params,
  })
}
export function addsysPost(data) { // 新增岗位
  return axios.request({
    url: '/meta/sysPost',
    method: 'post',
    data,
  })
}
export function editsysPost(data) { // 编辑岗位
  return axios.request({
    url: '/meta/sysPost',
    method: 'put',
    data,
  })
}

export function  unbindPost (data){ // 解除职位绑定部门关系
  return axios.request({
    url: '/meta/sysPost/unbindPost',
    method: 'post',
    data,
  })
}

export function  findallList (data){ // 查询部门的用户
  return axios.request({
    url: '/meta/users/allList',
    method: 'post',
    data,
  })
}

export function  changeUser (data){ // 批量绑定用户
  return axios.request({
    url: '/meta/sysDep/changeUser',
    method: 'post',
    data,
  })
}

// 校验环境是否重名
export function checkDuplicatedEnvName(name) {
  return axios.request({
    url: `/meta/sysEnvironmentApi/queryByName?name=${name}`,
    method: 'get'
  })
}
