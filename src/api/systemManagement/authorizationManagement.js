import axios from '@/libs/api.request'

// 根据部门id查询业务目录、系统管理
export const getDirectoryByDeptId = id => {
  return axios.request({
    url: `/meta/deptResource?id=${id}`,
    method: 'get',
  })
}

// 根据角色id查询业务目录、系统管理
export const getDirectoryByRoleId = id => {
  return axios.request({
    url: `/meta/roleResource?id=${id}`,
    method: 'get',
  })
}

// 根据用户id查询业务目录、系统管理
export const getDirectoryByUserId = params => {
  return axios.request({
    url: `/meta/userResource`,
    method: 'get',
    params,
  })
}

// 打开部门权限开关
export const openDeptAuth = data => {
  return axios.request({
    url: '/meta/deptResource',
    method: 'post',
    data,
  })
}

// 关闭部门权限开关
export const closeDeptAuth = data => {
  return axios.request({
    url: '/meta/deptResource',
    method: 'delete',
    data,
  })
}

// 部门权限复用
export const deptAuthCopy = data => {
  return axios.request({
    url: '/meta/deptResource/copy',
    method: 'post',
    data,
  })
}

// 角色权限复用
export const roleAuthCopy = data => {
  return axios.request({
    url: '/meta/roleResource/copy',
    method: 'post',
    data,
  })
}

// 用户权限复用
export const userAuthCopy = data => {
  return axios.request({
    url: '/meta/domainAuthDomain/' + data.type + '/copy',
    method: 'post',
    data,
  })
}

// 查询可授权部门
export const getAuthDept = () => {
  return axios.request({
    url: '/meta/sysUser/authDept',
    method: 'get'
  })
}

// 查询可授权角色
export const getAuthRole = () => {
  return axios.request({
    url: '/meta/sysUser/authRole',
    method: 'get'
  })
}

// 打开角色权限开关
export const openRoleAuth = data => {
  return axios.request({
    url: '/meta/roleResource',
    method: 'post',
    data,
  })
}

// 关闭角色权限开关
export const closeRoleAuth = data => {
  return axios.request({
    url: '/meta/roleResource',
    method: 'delete',
    data,
  })
}

// 打开用户权限开关
export const openUserAuth = data => {
  return axios.request({
    url: '/meta/userResource',
    method: 'post',
    data,
  })
}

// 关闭用户权限开关
export const closeUserAuth = data => {
  return axios.request({
    url: '/meta/userResource',
    method: 'delete',
    data,
  })
}

// 根据部门、角色、用户实体id查询人员管理目录
export const getDirectoryListByDomainId = (domainId, type) => {
  return axios.request({
    url: `/meta/domainAuthDomain?domainId=${domainId}&type=${type}`,
    method: 'get',
  })
}

// 部门、角色、用户对人员管理打开权限
export const openPersonnelManagementAuth = data => {
  return axios.request({
    url: '/meta/domainAuthDomain',
    method: 'post',
    data,
  })
}

// 部门、角色、用户对人员管理关闭权限
export const closePersonnelManagementAuth = data => {
  return axios.request({
    url: '/meta/domainAuthDomain',
    method: 'delete',
    data,
  })
}

// 根据业务目录的资源id获取部门目录
export const getDepartmentDirectoryByResourceId = id => {
  return axios.request({
    url: `/meta/deptResource/resource?id=${id}`,
    method: 'get',
  })
}

// 根据业务目录的资源id获取角色目录
export const getRoleDirectoryByResourceId = id => {
  return axios.request({
    url: `/meta/roleResource/resource?id=${id}`,
    method: 'get',
  })
}

// 根据用户id和资源id，查询权限来源
export const getOriginByUserIdAndResourceId = (userId, resourceId) => {
  return axios.request({
    url: `/meta/userResource/origin?userId=${userId}&resId=${resourceId}`,
    method: 'get',
  })
}

// 根据用户id和资源id，查询人员管理的权限来源
export const getPersonnelOriginByUserIdAndResourceId = (userId, resourceId, type) => {
  return axios.request({
    url: `/meta/domainAuthDomain/origin?userId=${userId}&resId=${resourceId}&type=${type}`,
    method: 'get',
  })
}

// 数据权限 - 列表查询
export const getDomainResource = data => {
  return axios.request({
    url: '/meta/common/findDomainResource',
    method: 'post',
    data
  })
}

//  数据权限 - 授权
export const dataAuthShare = data => {
  return axios.request({
    url: '/meta/common/share',
    method: 'put',
    data,
  })
}

//  数据权限 - 取消权限
export const dataNoAuth = data => {
  return axios.request({
    url: '/meta/common/unAuth',
    method: 'delete',
    data,
  })
}

//  数据权限 - 申请模式
export const updateApplyModel = id => {
  return axios.request({
    url: `/meta/common/updateApplyModel?apply=${id}`,
    method: 'get',
  })
}

// 关闭修改权限时的提醒
export const closePrompt = () => {
  return axios.request({
    url: '/meta/sysUser/userAuthPriorityWarn/false',
    method: 'put',
  })
}

// 恢复继承权限--业务目录、系统管理
export const restoreBusinessAndSystemInherit = data => {
  return axios.request({
    url: '/meta/userResource/restoreInherit',
    method: 'post',
    data,
  })
}

// 恢复继承权限--人员管理
export const restorePersonalInherit = data => {
  return axios.request({
    url: '/meta/domainAuthDomain/restoreInherit',
    method: 'post',
    data,
  })
}
