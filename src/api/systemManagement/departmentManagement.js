import axios from '@/libs/api.request'

// 获取组织架构列表
export const getDepartmentList = () => {
  return axios.request({
    url: 'meta/sysDept/list',
    method: 'get',
  })
}

// 获取该用户下有权限的组织架构列表
export const getAuthDepartmentList = () => {
  return axios.request({
    url: '/meta/sysDept/auth',
    method: 'get',
  })
}

// 创建新部门
export const createDept = data => {
  return axios.request({
    url: '/meta/sysDept',
    method: 'post',
    data: data,
  })
}

// 更新部门
export const updateDept = data => {
  return axios.request({
    url: '/meta/sysDept',
    method: 'put',
    data,
  })
}

// 删除部门
export const deleteDept = id => {
  return axios.request({
    url: `/meta/sysDept/${id}`,
    method: 'delete',
  })
}

// 拖拽移动部门
export const moveDept = param => {
  return axios.request({
    url: `/meta/sysDept/moveDept?deptId=${param.deptId}&pId=${param.pId}&toDeptId=${param.toDeptId}&type=${param.type}`,
    method: 'get',
  })
}
