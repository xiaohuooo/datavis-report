import axios from '@/libs/api.request'

//资源管理-根节点
export const getRootRes = () => {
  return axios.request({
    url: '/meta/sysResource/list',
    method: 'get',
  })
}

// 新增资源
export const createResource = data => {
  return axios.request({
    url: '/meta/sysResource',
    data,
    method: 'post'
  })
}

// 更新资源
export const updateResource = data => {
  return axios.request({
    url: '/meta/sysResource',
    data,
    method: 'put'
  })
}

// 根据id删除资源
export const delResource = id => {
  return axios.request({
    url: '/meta/sysResource/'+id,
    method: 'delete'
  })
}

// 数智工作台点击菜单统计次数
export const clickMenu = data => {
  return axios.request({
    url: '/meta/userResourceRank/click',
    data,
    method: 'post'
  })
}
