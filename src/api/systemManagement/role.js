import axios from '@/libs/api.request'

// 根据角色id获取资源菜单
export const getCheckedResIdsByRoleId = (params) => {
  return axios.request({
    url: '/meta/roles/getCheckedResIdsByRoleId',
    method: 'get',
    params: params,
  })
}

// 给某个用户id绑定资源菜单
export const bindResToRole = (data) => {
  return axios.request({
    url: '/meta/res/bindResToRole',
    method: 'post',
    data,
  })
}

// 根据用户id获取资源菜单
export const getResIdsByUserId = (params) => {
  return axios.request({
    url: '/meta/resources/getResIdsByUserId',
    method: 'get',
    params: params,
  })
}

// 根据资源id查角色
export const getRolesByResId = (params) => {
  return axios.request({
    url: '/meta/roles/getRolesByResId',
    method: 'get',
    params: params,
  })
}

// 根据资源id查未绑定的角色
export const getUnBindRolesByResId = (params) => {
  return axios.request({
    url: '/meta/roles/getUnBindRolesByResId',
    method: 'get',
    params: params,
  })
}

// 给资源绑定角色
export const bindRolesToRes = (data) => {
  return axios.request({
    url: '/meta/res/bindRolesToRes',
    method: 'post',
    data,
  })
}

// 为资源解绑角色
export const unBindRolesToRes = (data) => {
  return axios.request({
    url: '/meta/res/unBindRolesToRes',
    method: 'post',
    data,
  })
}
