import axios from '@/libs/api.request'

// 获取所有角色
export const getGroupAndRole = () => {
  return axios.request({
    url: '/meta/sysGroup/groupAndRole',
    method: 'get',
  })
}

// 根据角色id获取已绑定用户
export const getBindUserByRoleId = data => {
  return axios.request({
    url: '/meta/userRole/bindUser/page',
    method: 'post',
    data,
  })
}

// 创建新角色
export const createRole = data => {
  return axios.request({
    url: '/meta/sysRole',
    method: 'post',
    data: data,
  })
}

// 更新角色
export const updateRole = data => {
  return axios.request({
    url: '/meta/sysRole',
    method: 'put',
    data,
  })
}

// 删除角色
export const deleteRole = id => {
  return axios.request({
    url: `/meta/sysRole/${id}`,
    method: 'delete',
  })
}

// 根据角色id获取未绑定用户
export const getUnbindUserByRoleId = data => {
  return axios.request({
    url: '/meta/userRole/unbindUser/page',
    method: 'post',
    data,
  })
}

// 给角色绑定用户
export const bindUser = (data) => {
  return axios.request({
    url: '/meta/userRole',
    method: 'post',
    data,
  })
}

// 给用户解绑角色
export const unbindUser = (data) => {
  return axios.request({
    url: '/meta/userRole',
    method: 'delete',
    data,
  })
}

// 获取所有的角色
export const listGroupRole = () => {
  return axios.request({
    url: '/meta/sysGroup/list',
    method: 'get',
  })
}

// 新建分组
export const createRoleGroup = (data) => {
  return axios.request({
    url: '/meta/sysGroup',
    method: 'post',
    data,
  })
}

// 编辑分组
export const updateRoleGroup = (data) => {
  return axios.request({
    url: '/meta/sysGroup',
    method: 'put',
    data,
  })
}

// 删除分组
export const deleteRoleGroup = (id) => {
  return axios.request({
    url: `/meta/sysGroup/${id}`,
    method: 'delete',
  })
}

// 删除角色前的校验
export const deleteRoleCheck = (id) => {
  return axios.request({
    url: `/meta/sysRole/check?id=${id}`,
    method: 'get',
  })
}
