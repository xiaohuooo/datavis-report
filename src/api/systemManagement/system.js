import axios from '@/libs/api.request'

export const login = ({ username, password,credentials }) => {
  const data = {
    username,
    password,
    credentials
  }
  return axios.request({
    url: '/meta/password/login',
    method: 'post',
    data
  })
}

// 根据部门id查询当前部门下的用户列表
export const getUsersByDepId = (params) => {
  return axios.request({
    url: 'meta/sysDep/getUsersByDepId',
    method: 'get',
    params: params
  })
}

//组织架构管理-根节点
export const getOrgParent = (params) => {
  return axios.request({
    url: 'meta/sysDep/originalParent',
    method: 'post',
    data: params
  })
}
//组织架构管理-子节点
export const getOrgChildById = (id) => {
  return axios.request({
    url: 'meta/sysDep/obtainChildById',
    method: 'get',
    params: id
  })
}
//组织架构管理-保存修改
export const modifyStatus = (data) => {
  return axios.request({
    url: 'meta/sysDep/modifyStatus',
    method: 'post',
    data: data
  })
}
//组织架构管理-查询
export const searchOrg = (data) => {
  return axios.request({
    url: 'meta/sysDep/search',
    method: 'post',
    data: data
  })
}
//组织架构管理
export const getOrgAll = () => {
  return axios.request({
    url: 'meta/sysDep/obtainAll',
    method: 'get',
  })
}

//资源管理-子节点
export const getChildRes = (id) => {
  return axios.request({
    url: 'meta/sysResource/children?id=' + id,
    method: 'get'
  })
}

// 根据username获取人员列表
export const getUserListAll = data => {
  return axios.request({
    url: 'meta/sysUser/page',
    method: 'post',
    data,
  })
}

// 变更所有者获取人员列表
export const getAuthUserList = params => {
  return axios.request({
    url: 'meta/sysUser/authUser',
    method: 'get',
    params,
  })
}

export const getMyInfo = () => {
  return axios.request({
    url: '/meta/sysUser/currentInfo',
    method: 'get'
  })
}

export const register = (data) => {
  return axios.request({
    url: '/meta/sysUser/register',
    method: 'post',
    data
  })
}

export const changePasswordByUserSelf = data => {
  return axios.request({
    url: '/meta/sysUser/password',
    method: 'put',
    data,
  })
}

// 验证当前电话号码或邮箱是否绑定了账户
export const queryUser = (params)=>{
  return axios.request({
    url: '/meta/sysUser/validation',
    method: 'get',
    params
  })
}

// 发送验证码
export const sendAuthCode = (params) => {
  return axios.request({
    url: '/meta/sysUser/sendVerificationCode',
    method: 'get',
    params
  })
}


export const grantedMenu = (data) => {
  return axios.request({
    url: '/meta/resources/granted',
    method: 'post',
    data: data
  })
}

export const getAuthorizeMenuTree = (roleId) => {
  return axios.request({
    url: '/meta/resources/' + roleId + '/resources',
    method: 'get'
  })
}

export const getRoleList = (params) => {
  return axios.request({
    url: '/meta/roles/listOrOne',
    method: 'post',
    data: params
  })
}

export const logout = (token) => {
  return axios.request({
    url: 'logout',
    method: 'post'
  })
}

//飞书登录
export const feishuLogin = (code, redirectUri) => {
  return axios.request({
    url: 'meta/feishu/login?code=' + code + '&redirectUri=' + redirectUri,
    method: 'get'
  })
}
//飞书clientId
export const getFeiShuClientId = () => {
  return axios.request({
    url: 'meta/feishu/clientId',
    method: 'get'
  })
}

export const sendSmsLogin = (params) => { //发送验证码用于登录
  return axios.request({
    url: 'meta/login/sendSms',
    method: 'get',
    params
  })
}

export const smsMobileLogin = (data) => { //使用验证码登录
  return axios.request({
    url: 'meta/mobile/login',
    method: 'post',
    data
  })
}



export const registerValidation = (params) => { //注册时验证手机号或邮箱是否已经注册
  return axios.request({
    url: 'meta/sysUser/registerValidation',
    method: 'get',
    params
  })
}
