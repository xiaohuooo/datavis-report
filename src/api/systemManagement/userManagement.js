import axios from '@/libs/api.request'

// 根据部门id获取人员列表
export const getUserListByDeptId = data => {
  return axios.request({
    url: '/meta/deptUser/user/page',
    method: 'post',
    data,
  })
}

// 更新人员状态 1-启用 0-停用
export const updateUserStatus = (id, status) => {
  return axios.request({
    url: `/meta/sysUser/${id}/${status}`,
    method: 'put',
  })
}

// 创建用户
export const addUser = data => {
  return axios.request({
    url: '/meta/sysUser',
    method: 'post',
    data,
  })
}

// 更新用户
export const updateUser = data => {
  return axios.request({
    url: '/meta/sysUser',
    method: 'put',
    data,
  })
}

// 更新用户角色
export const updateUserRole = data => {
  return axios.request({
    url: '/meta/userRole',
    method: 'put',
    data,
  })
}

// 删除用户
export const deleteUser = id => {
  return axios.request({
    url: `meta/sysUser/${id}`,
    method: 'delete',
  })
}

// 重置用户密码
export const resetPassword = id => {
  return axios.request({
    url: `/meta/sysUser/password/${id}`,
    method: 'put',
  })
}
