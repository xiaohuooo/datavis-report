import axios from "@/libs/api.request";

// 查询任务列表
export const getTaskList = params => {
  return axios.request({
    url: "/meta/sysTask/page",
    method: "get",
    params
  });
};

// 查询任务详情
export const getTaskDetail = params => {
  return axios.request({
    url: "/meta/sysTask/detail",
    method: "get",
    params
  });
};

// 添加任务
export const addTask = data => {
  return axios.request({
    url: "/meta/sysTask",
    method: "post",
    data
  });
};

// 编辑任务
export const editTask = data => {
  return axios.request({
    url: "/meta/sysTask/update",
    method: "post",
    data
  });
};

// 删除任务
export const deleteTask = id => {
  return axios.request({
    url: `/meta/sysTask/${id}`,
    method: "delete"
  });
};

// 更新任务状态
export const updateTaskStatus = (id, status) => {
  return axios.request({
    url: `/meta/sysTask/updateStatus/${id}/${status}`,
    method: "put"
  });
};

// 处理任务
export const handleTask = data => {
  return axios.request({
    url: `/meta/sysTask/dealTask`,
    method: "post",
    data
  });
};
