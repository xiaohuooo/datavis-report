import { getDayNum } from '../../utils/chartDate'

const weeks = ['日', '一', '二', '三', '四', '五', '六']
const months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
import './Calendar.css'


export default class Calendar {
  constructor({ node, givenDate, chooseDateRange, dateClick, blackDate, beyondTodayIsDisable }) {
    this.calendarMain = node
    this.date = givenDate || new Date()
    this.blackDate = blackDate
    this.dateClick = dateClick
    this.calendarDate = {}
    this.beyondTodayIsDisable = beyondTodayIsDisable;
    if (chooseDateRange) {
      if (!chooseDateRange.startDate || !chooseDateRange.endDate) {
        throw new Error('startDate and endDate must be not null')
      }
      this.setDateRange(chooseDateRange)
    }
  }

  setDateRange(chooseDateRange, blackDate) {
    let { startDate, endDate } = chooseDateRange
    this.startDate = {
      startYear: startDate.getFullYear(),
      startMonth: startDate.getMonth() + 1,
      startDate: startDate.getDate()
    }
    this.endDate = {
      endYear: endDate.getFullYear(),
      endMonth: endDate.getMonth() + 1,
      endDate: endDate.getDate()
    }
    this.blackDate = blackDate
    this.create()
  }

  setGivenDate(givenDate) {
    this.date = givenDate
    this.create()
  }

  create() {
    this.calendarMain.innerHTML = ''
    let calendar = this.creatElement('div')
    calendar.className = 'calendar-container'
    this.calendarMain.appendChild(calendar)

    // 获取当前时间保存在this.calendarDate对象里
    this.calendarDate.year = this.date.getFullYear()
    this.calendarDate.month = months[this.date.getMonth()]
    this.calendarDate.day = this.date.getDate()
    this.calendarDate.week = weeks[this.date.getDay()]

    //获取当月天数并展示主体
    this.calendarDate.Alldays = getDayNum(this.calendarDate.year, this.calendarDate.month)
    let calendarDate = this.calendarDate
    this.draw(calendarDate, calendar)
  }

  draw(date, calendarBox) {
    calendarBox.innerHTML = ''
    this.dateTable(calendarBox, date)
  }

  dateTable(calendarBox, date) {
    let _this = this
    let table = this.creatElement('div')
    table.className = 'date-box'
    let thead = this.creatElement('div')
    thead.className = 'date-thead'
    weeks.forEach(function (val) {
      let th = _this.creatElement('span')
      th.innerHTML = val
      thead.appendChild(th)
    })

    table.appendChild(thead)
    let tbody = this.creatElement('div')
    tbody.className = 'date-tbody'
    let firstDay = new Date(date.year + '/' + date.month + '/' + 1).getDay()
    let tbodyTr
    let today = new Date()
    let currentYear = today.getFullYear()
    let currentMonth = today.getMonth() + 1
    let currentDate = today.getDate()
    for (let i = 0; i < date.Alldays + firstDay; i++) {
      if (i === 0 || i % 7 === 0) {
        tbodyTr = this.creatElement('div')
        tbodyTr.className = 'date-tr'
        if (i === 0) {
          tbodyTr.style.marginTop = '0px'
        }
        if (i === 28) {
          tbodyTr.style.marginBottom = '0px'
        }
      }
      let td = this.creatElement('span')

      if (i >= firstDay) {
        let a = this.creatElement('a')
        let currentDay = i - firstDay + 1
        a.innerHTML = currentDay < 10 ? '0' + currentDay : currentDay

        // 如果在已选的区间内，添加选中效果
        let r = this.isInRange(this.startDate, this.endDate, date.year, date.month, currentDay)
        if (r === 1) {
          td.className = 'calendar-td-equal-start'
        } else if (r === 2) {
          td.className = 'calendar-td-equal-end'
        } else if (r === 3) {
          td.className = 'calendar-td-range'
        }

        let num = 0
        // 超过今天的日期或者黑名单中的日期，置灰不许操作
        const beyondToday =
          date.year > currentYear ||
          (date.year === currentYear && date.month > currentMonth) ||
          (date.year === currentYear && date.month === currentMonth && currentDay > currentDate);
        const isDisable = this.beyondTodayIsDisable ? beyondToday || this.isInBlack(date, currentDay) : this.isInBlack(date, currentDay);
        if (isDisable) {
          num++;
          td.className = "calendar-td-disabled";
        }

        if (num === 2) {
          td.className = 'calendar-td-comparable-disabled'
        }

        let _this = this
        a.addEventListener('click', function (e) {
          if (e.target.parentNode && e.target.parentNode.className !== 'calendar-td-disabled') {
            _this.dateClick(new Date(date.year + '/' + date.month + '/' + currentDay))
            e.stopPropagation()
          }
        })

        td.appendChild(a)
      } else {
        let a = this.creatElement('a')
        td.appendChild(a)
      }
      tbodyTr.appendChild(td)
      if (i === 0 || i % 7 === 0) {
        tbody.appendChild(tbodyTr)
      }
    }
    table.appendChild(tbody)
    calendarBox.appendChild(table)
  }

  creatElement(tag) {
    return document.createElement(tag)
  }

  isInRange(_startDate, _endDate, year, month, date) {
    let { startYear, startMonth, startDate } = _startDate
    let { endYear, endMonth, endDate } = _endDate

    if (year === startYear && month === startMonth && date === startDate) {
      return 1
    }

    if (year === endYear && month === endMonth && date === endDate) {
      return 2
    }

    if (!(year > endYear || (year === endYear && month > endMonth) || (year === endYear && month === endMonth && date > endDate))
      && !(year < startYear || (year === startYear && month < startMonth) || (year === startYear && month === startMonth && date < startDate))) {
      return 3
    }

    return -1
  }

  isInBlack(date, currentDay) {
    if (this.blackDate && this.blackDate.open) {
      let startDate = this.blackDate.limitDate
      let year = startDate.getFullYear()
      let month = startDate.getMonth() + 1
      let _date = startDate.getDate()
      if (this.blackDate.currentPosition) {
        return (date.year < year || (date.year === year && date.month < month)
          || (date.year === year && date.month === month && currentDay < _date))
      } else {
        return (date.year > year || (date.year === year && date.month > month)
          || (date.year === year && date.month === month && currentDay > _date))
      }
    }
    return false
  }
}
