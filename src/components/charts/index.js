import ChartPie from './pie.vue'
import ChartBar from './bar.vue'
import ChartLine from './line.vue'
import ChartLineBar from './linebar.vue'
import ChartDoublePie from './doublePie.vue'
import ChartGauge from './gauge.vue'
import ChartMap from './chinaMap.vue'
import ChartBubble from './bubble.vue'
import ChartScatter from './scatter.vue'
import ChartMultiplePie from './multiplePie.vue'
import ChartRadar from './radar.vue'
export {
  ChartPie, ChartBar,
  ChartLine, ChartLineBar,
  ChartDoublePie,ChartGauge,
  ChartMap,ChartBubble,ChartScatter,
  ChartMultiplePie, ChartRadar
}
