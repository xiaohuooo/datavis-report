import * as echarts from "echarts";
import BigNumber from "bignumber.js";
import { getOriginName } from "@/utils/common";
import { mapActions, mapGetters } from "vuex";

export default {
  inject: ["recordSnapshot", "refreshCharts"],
  beforeDestroy() {
    this.$erd.uninstall(this.$refs.dom);
  },
  methods: {
    ...mapActions(["setChartList"]),
    /**
     * 通知被联动图表清除联动：调用父容器的重置图表联动关系方法
     * @param relativeComponentList
     */
    noticePassiveChartsResetLink(relativeComponentList) {
      if (this.$parent.$parent.$parent.refreshCharts) {
        // 查看态
        this.$parent.$parent.$parent.refreshCharts(relativeComponentList);
      } else {
        // 编辑态
        this.refreshCharts(relativeComponentList);
      }
    },
    /**
     * 清除联动: 所有图表恢复初始显示状态
     */
    clearChartLink(needRefreshClickItemIndex) {
      const chartList = this.$store.state.chart.chartList;
      // 找到所有支持联动的图表
      chartList
        .filter(item => {
          const configData = item.data.configData;
          const linkOption = configData.linkOption;
          const isSupportLink = linkOption && (linkOption.isSupportActiveLink || linkOption.isSupportPassiveLink);
          return isSupportLink;
        })
        .forEach(item => {
          const configData = item.data.configData;
          let linkOption = configData.linkOption;
          linkOption = _.assign(linkOption, {
            conditionValue: "",
            filterDimension: "",
            chartClickItemIndex: needRefreshClickItemIndex ? -1 : linkOption.chartClickItemIndex,
            isMainLink: linkOption.isSupportActiveLink
          });
        });
    },
    /**
     *点击图表热区联动处理
     * 1、将当前点击的维成员作为过滤条件（conditionValue、filterDimension）记录到所有被联动图表configData中
     * 2、更新当前图表为主联动图表（isMainLink=true），其他支持被联动的图表作为被联动图表（isMainLink = false）
     * 3、实时更新画布中可被联动的图表
     */
    handleChartLinkAction(name, dataIndex, needRefreshClickItemIndex, dimensionId) {
      const chartList = this.$store.state.chart.chartList; //所有图表
      // 找到所有支持联动的图表
      chartList
        .filter(item => {
          const configData = item.data.configData;
          const linkOption = configData.linkOption;
          const isSupportLink = linkOption && (linkOption.isSupportActiveLink || linkOption.isSupportPassiveLink);
          return isSupportLink;
        })
        .forEach(item => {
          // item.data.configData.isLinking = true // 联动状态
          const configData = item.data.configData;
          let linkOption = configData.linkOption;
          if (item.chartId !== this.chartId) {
            linkOption = _.assign(linkOption, {
              isMainLink: false,
              conditionValue: name,
              filterDimension: dimensionId ? dimensionId : this.propertyData.dimension[0],
              chartClickItemIndex: -1,
              chartLinkIsOpen: true
            });
          } else {
            //更新被联动图表:获取全部chart中支持被联动的组件
            const relativeComponentList = chartList
              .filter(item => item.data.configData.isSupportPassiveLink && item.chartId !== this.chartId)
              .map(item => item.chartId);
            linkOption = _.assign(linkOption, {
              isMainLink: true,
              conditionValue: "",
              filterDimension: "",
              chartClickItemIndex: needRefreshClickItemIndex ? dataIndex : linkOption.chartClickItemIndex, //联动时支持记录当前已点击的柱体的索引
              relativeComponentList: relativeComponentList,
              chartLinkIsOpen: true
            });
          }
        });
    },

    /**
     * 获取echarts的grid
     * @param legendShow 显示图例
     * @param legendPosition 图例位置
     * @param borderColor 边框颜色
     * @param barType 类型
     * @returns {{borderColor: string, top: (string), left: (string), bottom: (string), show: boolean, right: (string), containLabel: boolean}}
     */
    refreshGrid({ legendShow = true, legendPosition = "下", borderColor = "transparent", barType }) {
      let grid;
      // 柱状图类型选择条形图，有两个grid
      if (barType === "comparison") {
        grid = [
          {
            right: legendShow ? (legendPosition === "左" ? "18%" : "5%") : "5%",
            top: legendShow ? (legendPosition === "上" ? "20%" : "10%") : "10%",
            bottom: legendShow ? (legendPosition === "下" ? "15%" : "5%") : "5%",
            containLabel: true,
            show: true,
            borderColor: borderColor,
            width: "45%"
          },
          {
            left: legendShow ? (legendPosition === "右" ? "18%" : "5%") : "5%",
            top: legendShow ? (legendPosition === "上" ? "20%" : "10%") : "10%",
            bottom: legendShow ? (legendPosition === "下" ? "15%" : "5%") : "5%",
            containLabel: true,
            show: true,
            borderColor: borderColor,
            width: "45%"
          }
        ];
      } else {
        grid = {
          left: legendShow ? (legendPosition === "左" ? "18%" : "5%") : "5%",
          right: legendShow ? (legendPosition === "右" ? "18%" : "5%") : "5%",
          top: legendShow ? (legendPosition === "上" ? "20%" : "10%") : "10%",
          bottom: legendShow ? (legendPosition === "下" ? (this.isIntelligentAnswer ? "22%" : "15%") : "5%") : "5%",
          containLabel: true,
          show: true,
          borderColor: borderColor
        };
      }
      return grid;
    },
    /**
     * x轴 y轴设置
     * @param xAxisData x轴数据
     * @param numberUnitLabel y轴单位
     * @param style 样式
     */
    refreshAxis(xAxisData, numberUnitLabel, style) {
      const {
        axisLineColor,
        axisLabelColor,
        axisLabelFontSize = 10,
        splitLineType = "solid",
        splitLineColor,
        splitLineHidden = false,
        axisLabelHidden = false,
        barType
      } = style;
      let xAxis, yAxis;
      // 柱状图类型选择条形图，有两个xAxis和yAxis
      if (barType === "comparison") {
        xAxis = [];
        yAxis = [];
        for (let i = 0; i < 2; i++) {
          xAxis[i] = {
            gridIndex: i,
            zlevel: -2,
            type: "category",
            data: xAxisData,
            boundaryGap: true, // 坐标轴两边不留白
            axisLine: {
              onZero: false,
              lineStyle: {
                color: splitLineHidden ? "transparent" : axisLineColor
              }
            },
            axisTick: {
              show: false
            },
            axisLabel: {
              show: i === 1 ? !axisLabelHidden : false,
              color: axisLabelColor,
              fontSize: axisLabelFontSize
            },
            splitLine: {
              show: false
            }
          };
          yAxis[i] = {
            gridIndex: i,
            inverse: i === 1, // 对比图需要翻转
            zlevel: -1,
            type: "value",
            splitLine: {
              show: splitLineType === "none" ? false : true,
              lineStyle: {
                color: splitLineHidden ? "transparent" : splitLineColor,
                type: splitLineType
              }
            },
            axisLine: {
              show: false,
              onZero: false,
              lineStyle: {
                color: axisLineColor
              }
            },
            axisTick: {
              show: false
            },
            axisLabel: {
              show: !axisLabelHidden,
              color: axisLabelColor,
              fontSize: axisLabelFontSize,
              formatter: `{value}${numberUnitLabel}`
            },
            nameTextStyle: {
              color: "#999"
            },
            splitNumber: 3
          };
        }
      } else {
        xAxis = {
          zlevel: -2,
          type: "category",
          data: xAxisData,
          boundaryGap: true, // 坐标轴两边不留白
          axisLine: {
            onZero: false,
            lineStyle: {
              color: splitLineHidden ? "transparent" : axisLineColor
            }
          },
          axisTick: {
            show: false
          },
          axisLabel: {
            show: !axisLabelHidden,
            color: axisLabelColor,
            fontSize: axisLabelFontSize
          },
          splitLine: {
            show: false
          }
        };
        yAxis = {
          zlevel: -1,
          type: "value",
          splitLine: {
            show: splitLineType === "none" ? false : true,
            lineStyle: {
              color: splitLineHidden ? "transparent" : splitLineColor,
              type: splitLineType
            }
          },
          axisLine: {
            show: false,
            onZero: false,
            lineStyle: {
              color: axisLineColor
            }
          },
          axisTick: {
            show: false
          },
          axisLabel: {
            show: !axisLabelHidden,
            color: axisLabelColor,
            fontSize: axisLabelFontSize,
            formatter: `{value}${numberUnitLabel}`
          },
          nameTextStyle: {
            color: "#999"
          }
        };
      }

      return { xAxis, yAxis };
    },
    /**
     * 图例处理
     * @param {图例数据} legendData
     * 默认 ['图例1', '图例2', '图例3']
     * 或者 [{ name, originName, parentName }]
     * @param {样式} param1
     * legendPosition 图例位置
     * legendShow 图例是否显示
     * legendFontFamily 图例字体
     * legendFontSize 图例字体大小
     * @returns
     */
    refreshLegend(legendData, { legendPosition = "下", legendShow = true, legendFontFamily = "SimSun", legendFontSize = 11 }) {
      let legend = {
        show: legendShow,
        data: legendData,
        itemHeight: 8,
        itemWidth: 8,
        itemGap: 5,
        inactiveColor: this.reportThemeStyle === "1" ? "#ccc" : "#555",
        textStyle: {
          color: this.reportThemeStyle === "1" ? "#555" : "#ccc",
          fontFamily: legendFontFamily,
          fontSize: legendFontSize
        },
        tooltip: {
          show: true
        }
      };
      // 图例左右时，宽度为echarts宽度的18%，至少保留70px
      let widthGrid = parseInt(this.$refs.dom.clientWidth * 0.18);
      let leftRightGrid = Math.max(70, widthGrid);
      // 判断是否是当前name和父name拼接
      let isLongName = false;
      if (legendData.length && legendData[0] instanceof Object) {
        isLongName = true;
      }
      if (legendPosition === "上") {
        legend = _.assign(legend, {
          x: "center",
          y: "8%",
          orient: "horizontal",
          formatter: function (name) {
            return isLongName ? getOriginName(name, legendData) : name;
          }
        });
      } else if (legendPosition === "右") {
        legend = _.assign(legend, {
          x: "right",
          y: "center",
          orient: "vertical",
          formatter: function (name) {
            return echarts.format.truncateText(isLongName ? getOriginName(name, legendData) : name, leftRightGrid, "14px Microsoft Yahei", "…");
          }
        });
      } else if (legendPosition === "下") {
        legend = _.assign(legend, {
          x: "center",
          y: this.isIntelligentAnswer ? "78%" : "86%",
          orient: "horizontal",
          formatter: function (name) {
            return isLongName ? getOriginName(name, legendData) : name;
          }
        });
      } else if (legendPosition === "左") {
        legend = _.assign(legend, {
          x: "left",
          y: "center",
          orient: "vertical",
          formatter: function (name) {
            return echarts.format.truncateText(isLongName ? getOriginName(name, legendData) : name, leftRightGrid, "14px Microsoft Yahei", "…");
          }
        });
      }
      return legend;
    },
    /**
     * 添加标签相关的设置
     * @param {series数据} seriesItem
     * @param {样式} param1
     * showLabel 标签是否展示
     * labelPosition 标签位置
     * labelFontFamily 标签字体
     * labelFontSize 标签字号
     * labelFontColor 标签字体颜色
     * labelRange 全部/最大和最小/首尾
     * labelOverlap 标签是否重叠
     */
    refreshLabel(
      seriesItem,
      {
        showLabel = false,
        labelPosition = "outside",
        labelFontFamily = "SimSun",
        labelFontSize = 12,
        labelFontColor = "#333333",
        labelRange = "all",
        labelOverlap = true,
        barType
      }
    ) {
      if (showLabel) {
        seriesItem.labelLayout = {
          hideOverlap: !labelOverlap
        };
        seriesItem.label = {
          show: true,
          fontFamily: labelFontFamily,
          fontSize: labelFontSize,
          color: labelFontColor,
          formatter: params => {
            return params.data.showValue;
          }
        };
        let isSimplePie = this.chartId.startsWith("SimplePie");
        if (isSimplePie) {
          seriesItem.label.position = labelPosition === "inside" ? "inside" : "outside";
          seriesItem.labelLine = {
            show: labelPosition === "inside" ? false : true
          };
          seriesItem.emphasis = {
            label: {
              show: true
            },
            labelLine: {
              show: false
            }
          };
        } else {
          seriesItem.data.forEach(dataItem => {
            dataItem.label = {
              position: labelPosition === "inside" ? "inside" : dataItem.value >= 0 ? "top" : "bottom"
            };
          });
        }
        let maxVal, minVal;
        if (labelRange === "maxmin") {
          let isBasicRadar = this.chartId.startsWith("BasicRadar");
          if (!isBasicRadar) {
            maxVal = Math.max.apply(
              Math,
              seriesItem.data.map(item => Number(item.value))
            );
            minVal = Math.min.apply(
              Math,
              seriesItem.data.map(item => Number(item.value))
            );
            seriesItem.data.forEach(dataItem => {
              if (!dataItem.label) {
                dataItem.label = {};
              }
              if (Number(dataItem.value) === maxVal || Number(dataItem.value) === minVal) {
                dataItem.label.show = true;
              } else {
                dataItem.label.show = false;
                if (isSimplePie) {
                  dataItem.emphasis = {};
                  dataItem.emphasis.label = {
                    show: false
                  };
                }
              }
            });
          }
        }
        // 折线图添加首尾设置
        const isSimpleLine = this.chartId.startsWith("SimpleLine");
        if (labelRange === "startend" && isSimpleLine) {
          seriesItem.data.forEach((dataItem, index) => {
            if (!dataItem.label) {
              dataItem.label = {};
            }
            if (index === 0 || index === seriesItem.data.length - 1) {
              dataItem.label.show = true;
            } else {
              dataItem.label.show = false;
            }
          });
        }
      } else {
        seriesItem.label = {
          show: false
        };
        seriesItem.emphasis = {
          label: {
            show: false
          }
        };
      }
    },
    /**
     * 轴标签文本设置
     * @param {grid} param0
     * left grid组件离容器左侧的距离
     * right grid组件离容器右侧的距离
     * @param {轴数据个数} axisDataLength
     * @param {样式} param2
     * axisLabelSetting 轴标签文本的设置值
     * @returns
     */
    refreshAxisLabel({ left = "5%", right = "5%" }, axisDataLength = 10, { axisLabelSetting = "interval" }) {
      let axisLabel = {};
      if (axisLabelSetting === "interval") {
        // 间隔显示
        axisLabel.interval = "auto";
      } else if (axisLabelSetting === "ellipsis" || axisLabelSetting === "break") {
        // 缩略显示 或 换行显示 都需要计算width
        let echartsWidth = this.$refs.dom.offsetWidth;
        let leftRightGrid = Number(left.substr(0, left.length - 1)) + Number(right.substr(0, right.length - 1));
        let axisWidth = BigNumber(echartsWidth)
          .times(100 - leftRightGrid)
          .div(100)
          .toNumber();
        let axisItemWidth = BigNumber(axisWidth).div(axisDataLength).toNumber();
        axisLabel.interval = 0;
        axisLabel.width = axisItemWidth;
        if (axisLabelSetting === "ellipsis") {
          // 缩略显示
          axisLabel.overflow = "truncate";
        } else {
          // 换行显示
          axisLabel.overflow = "breakAll";
        }
      } else if (axisLabelSetting === "rotate") {
        // 旋转显示
        axisLabel.interval = 0;
        axisLabel.rotate = -45;
      }
      return axisLabel;
    },
    /**
     * 坐标轴值定义设置
     * @param {各种坐标轴值定义设置} param0
     */
    refreshAxisValue({
      axisValueCustomRange = false,
      axisValueCustomMax = null,
      axisValueCustomMin = null,
      axisValueCustomLog = false,
      axisValueCustomLogBase = null
    }) {
      let axisValue = {};
      if (axisValueCustomLog && axisValueCustomLogBase) {
        // 自定义对数底数
        axisValue.type = "log";
        axisValue.logBase = axisValueCustomLogBase;
        axisValue.min = 1;
      } else if (axisValueCustomRange) {
        // 自定义范围
        if (axisValueCustomMax !== null && axisValueCustomMin !== null) {
          if (axisValueCustomMax === axisValueCustomMin) {
            axisValue.min = axisValueCustomMin;
            axisValue.max = axisValueCustomMin + Math.abs(axisValueCustomMin);
          } else {
            axisValue.max = Math.max(axisValueCustomMax, axisValueCustomMin);
            axisValue.min = Math.min(axisValueCustomMax, axisValueCustomMin);
          }
        } else if (axisValueCustomMax !== null) {
          axisValue.max = axisValueCustomMax;
        } else if (axisValueCustomMin !== null) {
          axisValue.min = axisValueCustomMin;
        }
      }
      return axisValue;
    },
    /**
     * 数据相关的处理，不包含千分位分隔符
     * @param {configData} configData
     * @param {区分百分比格式} isPercent
     * @returns
     */
    getValueConfig(configData, isPercent = false) {
      let config = {};
      if (!isPercent) {
        config.numberUnit = configData.numberUnit;
      }
      config.decimalDigits = configData.decimalPlaces;
      return config;
    },
    /**
     * 展示数据相关的处理，包含千分位分隔符
     * @param {configData} configData
     * @param {区分百分比格式} isPercent
     * @returns
     */
    getShowValueConfig(configData, isPercent = false) {
      let config = {};
      if (!isPercent) {
        config.numberUnit = configData.numberUnit;
      }
      config.decimalDigits = configData.decimalPlaces;
      config.thousandth = configData.thousandsSeparator;
      return config;
    },
    /**
     * y轴刻度处 以及悬浮时添加的文本
     * @param {configData} configData
     * @param {区分百分比格式} isPercent
     * @returns
     */
    getNumberUnitLabel(configData, isPercent = false) {
      let numberUnitLabel = "";
      if (isPercent) {
        numberUnitLabel = "%";
      } else if (configData.numberUnit === "wan") {
        numberUnitLabel = "万";
      } else if (configData.numberUnit === "yi") {
        numberUnitLabel = "亿";
      }
      return numberUnitLabel;
    },

    checkChartDrillDown(drillDownOption) {
      let msg = "";
      // 下钻校验
      if (drillDownOption.opened) {
        const realDrillDownTagList = drillDownOption.drillDownTagList.filter(item => item.dimension); // 有效维度层级
        //下钻时如果没选维度
        if (realDrillDownTagList.length <= 1) {
          msg = "请添加下钻维度";
        } else if (drillDownOption.drillDownStep > realDrillDownTagList.length - 1) {
          // 下钻时点击柱体如果超出层级
          msg = "超出配置下钻深度！";
        }
      }
      return msg;
    },
    /**
     * 下钻处理: 1、将当前层级的维度成员作为过滤条件传给下层取数
     *         2、获取下层的维度的id作为取数参数中dimension的值
     */
    handleChartDrillDownAction(drillDownOption, name) {
      let drillDownTagList = drillDownOption.drillDownTagList, //总下钻深度
        drillDownStep = drillDownOption.drillDownStep, //当前深度
        nextDrillDownDimension = drillDownTagList[drillDownStep], //需要钻入的深度
        prevDrillDownDimension = drillDownTagList[drillDownStep - 1];
      this.propertyData = _.assign(this.propertyData, {
        dimension: [nextDrillDownDimension.dimension.id],
        configData: _.assign(this.configData, {
          drillDownOption: _.assign(drillDownOption, {
            drillDownStep: drillDownStep + 1,
            drillDownTagList: _.map(drillDownOption.drillDownTagList, (item, index) => {
              if (index === drillDownStep) {
                return {
                  ...item,
                  arrive: true,
                  conditionValue: name,
                  filterDimension: prevDrillDownDimension.dimension.id
                };
              } else {
                return {
                  ...item,
                  arrive: index < drillDownStep
                };
              }
            })
          })
        })
      });
      //将当前下钻深度保存用于判断并准备继续向下
      this.drillDownActive = drillDownOption.drillDownStep;
    },
    /**
     * 下钻复原
     */
    resetChartDrillDown() {
      const { drillDownOption } = this.configData;
      this.propertyData = _.assign(this.propertyData, {
        dimension: [drillDownOption.drillDownTagList[0].dimension.id],
        configData: _.assign(this.configData, {
          drillDownOption: _.assign(drillDownOption, {
            drillDownStep: 1,
            drillDownTagList: _.map(drillDownOption.drillDownTagList, (item, index) => {
              return {
                ...item,
                arrive: false,
                conditionValue: "",
                filterDimension: ""
              };
            })
          })
        })
      });
    },

    handleChartItemClick(params, option, needRefreshClickItemIndex) {
      let { dataIndex, name, dimensionId } = params, //dataIndex 柱体索引
        chart = this.propertyData, //当前图表
        drillDownOption = this.configData.drillDownOption, //下钻配置
        jumpOption = this.configData.jumpOption, //跳转配置
        linkOption = this.configData.linkOption; //联动配置

      // 优化：http://129.211.167.119:8090/pages/viewpage.action?pageId=1573908
      // 前置条件是当前图表配置了维度及指标
      const hasDataPropertyConfig = Object.keys(chart).length && chart.dimension.length && chart.indicator.length;
      if (hasDataPropertyConfig) {
        const isLinkOpened = linkOption && linkOption.chartLinkIsOpen;
        const isDrillDownOpened = drillDownOption && drillDownOption.opened;
        const isJumpOpened = jumpOption && jumpOption.openJump;
        if (isLinkOpened) {
          // 开启了联动
          //判断当前点击的是不是同一个维度成员
          const isClickSameItem = linkOption.chartClickItemIndex === dataIndex;
          // 是否当前是主联动图表
          const isMainLink = linkOption.isMainLink;
          if (isMainLink && !isClickSameItem) {
            /**
             * 如果开启的是联动，当前图表为主联动，通知所有被联动图表响应联动
             **/
            this.handleChartLinkAction(name, dataIndex, needRefreshClickItemIndex, dimensionId);
          } else {
            /**
             *1、开启联动时点击相同的柱体清除联动
             * 2、如果当前图表在被联动状态，应先还原自身并通知所有支持被联动图表还原
             **/
            this.clearChartLink(needRefreshClickItemIndex);
          }
          if (this.dom) {
            this.dom.setOption(option, true);
          }
        } else if (isDrillDownOpened) {
          // 开启了下钻
          // 下钻校验
          const msg = this.checkChartDrillDown(drillDownOption);
          if (msg) {
            this.$Message.warning(msg);
            return;
          } else {
            // 执行下钻
            this.handleChartDrillDownAction(drillDownOption, name);
          }
          if (this.dom) {
            this.dom.setOption(option, true);
          }
        } else if (isJumpOpened) {
          //跳转
          this.$bus.$emit("jump-report", {
            chartId: this.chartId,
            position: {
              left: event.zrX,
              top: event.zrY
            },
            jumpFilterParam: chart.dimension[0],
            jumpConditionValue: name
          });
        }
      }
      // 编辑态需要记录快照
      this.recordSnapshot && this.recordSnapshot();
    }
  }
};
