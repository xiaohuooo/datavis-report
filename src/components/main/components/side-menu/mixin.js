import CommonIcon from '_c/common-icon'
import { showTitle } from '@/libs/util'

export default {
  components: {
    CommonIcon
  },
  methods: {
    showTitle(item) {
      return showTitle(item, this)
    },
    brightKeyword(val, keyword){
      if(val.indexOf(keyword) !== -1){
        return val.replace(keyword, `<font color='#3F73FF'>${keyword}</font>`)
      } else {
        return val
      }
    },
    showChildren(item) {
      return item.children && (!this.isVerticalMode && item.children.length >= 1 ||
      this.isVerticalMode && item.children.length > 1 || (item.meta && item.meta.showAlways))
    },
    getIdOrHref(item, children) {
      item = children ? item.children[0] : item
      // blank 新开窗口
      if (item.type === 2) {
        // _^#$_ 使用复杂符号防止split引用的url时出现相同符号
        return `blank@${item.id}_^#$_${item.path}`
      } else if (item.type === 1) { // 内部打开链接
        return `inner@${item.id}_^#$_${item.path}`
      } else if (item.type === 4) {
        return item.name
      } else if (item.type === 7) { // 内部菜单资源
        return 'top_' + item.path
      }else { // 内部报告
        return item.id
      }
    }
  }
}
