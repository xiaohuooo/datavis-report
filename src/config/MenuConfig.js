export const REPORT_FRAGMENT_MENU_TYPE = 0 // 报告片段
export const URL_INNER_MENU_TYPE = 1 // 外链（当前窗口打开）
export const URL_OUT_MENU_TYPE = 2  // 外链（新窗口打开）
export const REPORT_MENU_TYPE = 3 // 系统报告
export const SYSTEM_MENU_TYPE = 4 // 系统菜单
export const CATALOGUE_TYPE = 5  // 目录
export const ROUTER_MENU_TYPE = 7 // 内部开发业务菜单
