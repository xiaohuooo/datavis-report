/**
 * 全局变量
 */
export const GLOBAL = {
  rootDimensionName: '维度组', //根维度组名称
  rootIndicatorName: '指标组', //根指标组名称
  separator: '——', //指标名和指标组名间的分隔符
  verificationMode:"email",  // email, phone
  emailDomain:["qq.com", "163.com","139.com","sohu.com","aliyun.com","189.com","hotmail.com","gmail.com","sina.com","yahoo.com","yeah.net","finebi.com"]
}

/**
 * 图表默认配置
 */
export const CHART_DEFAULT_CONFIG = {
  SimpleTextarea: {
    width: 360,
    height: 270,
    config: {
      isOpenPrivateDate: false,
      isSupportActiveLink: false, // 不支持作为主联动图表
      isSupportPassiveLink: false, // 不支持作为被联动
    },
    resizable: true
  },
  SimpleBar: {
    width: 360,
    height: 270,
    defaultImg: require('../assets/icons/chart/chartDefault_new/simpleBar.svg'),
    config: {
      isSupportDrillDown: true, // 支持下钻设置
      drillDownOption: { // 下钻
        drillDownTagList: [],
        opened: false,
        drillDownStep: 1
      },
      jumpOption: { // 跳转
        openJump: false,
        openDimensionParam: false,
        optionType: 'report',
        reportId: '',
        reportUrl: ''
      },
      linkOption: { // 联动
        isSupportActiveLink: true, // 支持作为联动图表
        isSupportPassiveLink: true, // 支持作为被联动图表
        isMainLink: false, // 支持当前不是主联动图表
        chartClickItemIndex: -1,
        conditionValue: '',
        filterDimension: ''
      },
      isOpenPrivateDate: true,
      displayPrivateDate: 0, // 默认显示私有日期
      linkPublicDate: 0, // 默认关联公共日期
      periodType: '2',
      dateOrRange: 1,
      dateBefore: 1,
      beforeRange: 1,
      fixedStartDate: null,
      fixedEndDate: null,
      fixedPeriodType: '99',
      isOpenThreshold: false,
      supportChangeChartTypes: ['CustomTable', 'SimpleLine', 'MixLineBar', 'SimplePie', 'IndexBoard'],
      displayDirection: 'vertical', //默认纵向显示
      isSort: 'false', // 是否排序
      sortBy: '', // 排序依据
      sortOrder: '', //次序
      showType: 0,  // 数据格式 1-百分比 0-数值
      numberUnit: 'original', // 数值单位，默认原始值
      prefix: '', // 前缀
      suffix: '', // 后缀
      decimalPlaces: 2, // 小数位数，默认2位小数
      thousandsSeparator: false, // 默认不使用千位分隔符
    },
    resizable: true
  },
  SimplePie: {
    width: 360,
    height: 270,
    defaultImg: {
      pie: require('../assets/icons/chart/chartDefault_new/pie.svg'),
      dbPie: require('../assets/icons/chart/chartDefault_new/db_pie.svg')
    },
    config: {
      isSupportDrillDown: true, // 支持下钻设置
      drillDownOption: {
        drillDownTagList: [],
        opened: false,
        drillDownStep: 1,
      },
      jumpOption: {
        openJump: false,
        openDimensionParam: false,
        optionType: 'report',
        reportId: '',
        reportUrl: ''
      },
      linkOption: { // 联动
        isSupportActiveLink: true, // 支持作为联动图表
        isSupportPassiveLink: true, // 支持作为被联动图表
        isMainLink: false, // 支持当前不是主联动图表
        chartClickItemIndex: -1,
        conditionValue: '',
        filterDimension: ''
      },
      isOpenPrivateDate: true,
      displayPrivateDate: 0, // 默认不显示私有日期
      linkPublicDate: 0, // 默认不关联公共日期
      periodType: '2',
      dateOrRange: 1,
      dateBefore: 1,
      beforeRange: 1,
      fixedStartDate: null,
      fixedEndDate: null,
      fixedPeriodType: '99',
      isOpenCompareBeforeTime: true,
      supportChangeChartTypes: ['CustomTable', 'SimpleLine', 'SimpleBar', 'MixLineBar', 'IndexBoard'],
      showType: 0,  // 数据格式 1-百分比 0-数值
      numberUnit: 'original', // 数值单位，默认原始值
      prefix: '', // 前缀
      suffix: '', // 后缀
      decimalPlaces: 2, // 小数位数，默认2位小数
      thousandsSeparator: false, // 默认不使用千位分隔符
      ringRatioUnitValue: 'num', // 环比单位值，默认123
      yearOnYearTypeValue: 'mom', // 同比类型值，默认月同比
    },
    resizable: true
  },
  CustomTable: {
    width: 360,
    height: 270,
    defaultImg: require('../assets/icons/chart/chartDefault_new/customTable.svg'),
    config: {
      isOpenTotal: true,
      openTop: {
        indicatorId: null,
        topNum: 20,
      },
      isSupportDrillDown: true, // 支持下钻设置
      drillDownOption: {
        drillDownTagList: [],
        opened: false,
        drillDownStep: 1
      },
      linkOption: { // 联动
        isSupportActiveLink: true, // 支持作为联动图表
        isSupportPassiveLink: true, // 支持作为被联动图表
        isMainLink: false, // 支持当前不是主联动图表
        chartClickItemIndex: -1, // 点击图表热区的索引
        conditionValue: '',
        filterDimension: ''
      },
      isOpenPrivateDate: true,
      displayPrivateDate: 0, // 默认显示私有日期
      linkPublicDate: 0, // 默认关联公共日期
      periodType: '2',
      dateOrRange: 1,
      dateBefore: 1,
      beforeRange: 1,
      fixedStartDate: null,
      fixedEndDate: null,
      fixedPeriodType: '99',
      isOpenCompareBeforeTime: true,
      calculateTitleOption: {},
      isOpenProportion: true,
      isOpenThreshold: false,
      indicatorAlias: {},
      supportChangeChartTypes: ['Indicator', 'SimpleLine', 'SimpleBar', 'MixLineBar', 'SimplePie', 'DoublePie', 'IndexBoard'],
    },
    resizable: true
  },
  GroupTable: {
    width: 360,
    height: 270,
    defaultImg: require('../assets/icons/chart/chartDefault_new/customTable.svg'),
    config: {
      isOpenTotal: true,
      openTop: {
        indicatorId: null,
        topNum: 20,
      },
      isSupportDrillDown: true, // 支持下钻设置
      drillDownOption: {
        drillDownTagList: [],
        opened: false,
        drillDownStep: 1
      },
      linkOption: { // 联动
        isSupportActiveLink: true, // 支持作为联动图表
        isSupportPassiveLink: true, // 支持作为被联动图表
        isMainLink: false, // 支持当前不是主联动图表
        chartClickItemIndex: -1, // 点击图表热区的索引
        conditionValue: '',
        filterDimension: ''
      },
      isOpenPrivateDate: true,
      displayPrivateDate: 0, // 默认显示私有日期
      linkPublicDate: 0, // 默认关联公共日期
      periodType: '2',
      dateOrRange: 1,
      dateBefore: 1,
      beforeRange: 1,
      fixedStartDate: null,
      fixedEndDate: null,
      fixedPeriodType: '99',
      isOpenCompareBeforeTime: true,
      calculateTitleOption: {},
      isOpenProportion: true,
      isOpenThreshold: false,
      indicatorAlias: {},
      supportChangeChartTypes: ['Indicator', 'SimpleLine', 'SimpleBar', 'MixLineBar', 'SimplePie', 'DoublePie', 'IndexBoard'],
    },
    resizable: true
  },
  SimpleDatePicker: {
    width: 360,
    height: 70,
    config: {
      isOpenPrivateDate: false,
      isSupportActiveLink: false, // 不支持作为主联动图表
      isSupportPassiveLink: false, // 不支持作为被联动
    },
    resizable: true
  },
  SimpleIframe: {
    width: 360,
    height: 270,
    defaultImg: require('../assets/icons/chart/chartDefault_new/iframe.svg'),
    config: {
      isOpenPrivateDate: false,
      isSupportActiveLink: false, // 不支持作为主联动图表
      isSupportPassiveLink: false, // 不支持作为被联动
    },
    resizable: true
  },
  // 折线图
  SimpleLine: {
    width: 360,
    height: 270,
    defaultImg: require('../assets/icons/chart/chartDefault_new/simpleLine.svg'),
    config: {
      isSupportDrillDown: true, // 支持下钻设置
      drillDownOption: {
        drillDownTagList: [],
        opened: false,
        drillDownStep: 1,
      },
      jumpOption: {
        openJump: false,
        openDimensionParam: false,
        optionType: 'report',
        reportId: '',
        reportUrl: ''
      },
      linkOption: { // 联动
        isSupportActiveLink: true, // 支持作为联动图表
        isSupportPassiveLink: true, // 支持作为被联动图表
        isMainLink: false, // 支持当前不是主联动图表
        chartClickItemIndex: -1, // 点击图表热区的索引
        conditionValue: '',
        filterDimension: ''
      },
      isOpenPrivateDate: true,
      displayPrivateDate: 0, // 默认不显示私有日期
      linkPublicDate: 0, // 默认不关联公共日期
      periodType: '2',
      dateOrRange: 1,
      dateBefore: 1,
      beforeRange: 1,
      fixedStartDate: null,
      fixedEndDate: null,
      fixedPeriodType: '99',
      isOpenThreshold: false,
      supportChangeChartTypes: ['CustomTable', 'SimpleBar', 'MixLineBar', 'SimplePie', 'IndexBoard'],
      isSort: 'false', // 是否排序
      sortBy: '', // 排序依据
      sortOrder: '', //次序
      showType: 0,  // 数据格式 1-百分比 0-数值
      numberUnit: 'original', // 数值单位，默认原始值
      prefix: '', // 前缀
      suffix: '', // 后缀
      decimalPlaces: 2, // 小数位数，默认2位小数
      thousandsSeparator: false, // 默认不使用千位分隔符
    },
    resizable: true
  },
  // 双轴图
  MixLineBar: {
    width: 360,
    height: 270,
    defaultImg: require('../assets/icons/chart/chartDefault_new/mixLine.svg'),
    config: {
      isOpenPrivateDate: true,
      isSupportDrillDown: true, // 支持下钻设置
      drillDownOption: {
        drillDownTagList: [],
        opened: false,
        drillDownStep: 1
      },
      jumpOption: {
        openJump: false,
        openDimensionParam: false,
        optionType: 'report',
        reportId: '',
        reportUrl: ''
      },
      linkOption: { // 联动
        isSupportActiveLink: true, // 支持作为联动图表
        isSupportPassiveLink: true, // 支持作为被联动图表
        isMainLink: false, // 支持当前不是主联动图表
        chartClickItemIndex: -1, // 点击图表热区的索引
        conditionValue: '',
        filterDimension: ''
      },
      displayPrivateDate: 0, // 默认不显示私有日期
      linkPublicDate: 0, // 默认不关联公共日期
      periodType: '2',
      dateOrRange: 1,
      dateBefore: 1,
      beforeRange: 1,
      fixedStartDate: null,
      fixedEndDate: null,
      fixedPeriodType: '99',
      isOpenThreshold: false,
      supportChangeChartTypes: ['CustomTable', 'SimpleLine', 'SimpleBar', 'SimplePie', 'IndexBoard'],
      isSort: 'false', // 是否排序
      sortBy: '', // 排序依据
      sortOrder: '', //次序
      showType: 0,  // 数据格式 1-百分比 0-数值
      numberUnit: 'original', // 数值单位，默认原始值
      prefix: '', // 前缀
      suffix: '', // 后缀
      decimalPlaces: 2, // 小数位数，默认2位小数
      thousandsSeparator: false, // 默认不使用千位分隔符
    },
    resizable: true
  },
  // 仪表盘
  SimpleGauge: {
    width: 360,
    height: 270,
    defaultImg: require('../assets/icons/chart/chartDefault_new/simpleGauge.svg'),
    min: 20,
    max: 1400,
    num: 3,
    config: {
      jumpOption: {
        openJump: false,
        openDimensionParam: false,
        optionType: 'report',
        reportId: '',
        reportUrl: ''
      },
      isOpenPrivateDate: true,
      displayPrivateDate: 0, // 默认不显示私有日期
      linkPublicDate: 0, // 默认不关联公共日期
      periodType: '2',
      dateOrRange: 1,
      dateBefore: 1,
      beforeRange: 1,
      fixedStartDate: null,
      fixedEndDate: null,
      fixedPeriodType: '99',
      isOpenCompareBeforeTime: true,
      supportChangeChartTypes: ['Indicator', 'CustomTable'],
      showType: 0,  // 数据格式 1-百分比 0-数值
      numberUnit: 'original', // 数值单位，默认原始值
      prefix: '', // 前缀
      suffix: '', // 后缀
      decimalPlaces: 2, // 小数位数，默认2位小数
      thousandsSeparator: false, // 默认不使用千位分隔符
      ringRatioUnitValue: 'num', // 环比单位值，默认123
      yearOnYearTypeValue: 'mom', // 同比类型值，默认月同比
      isSupportActiveLink: false, // 不支持作为主联动图表
      isSupportPassiveLink: false, // 不支持作为被联动
    },
    resizable: true
  },
  // 基础输入框
  BasicInput: {
    width: 360,
    height: 270,
    config: {
      isOpenPrivateDate: false,
      isSupportActiveLink: false, // 不支持作为主联动图表
      isSupportPassiveLink: false, // 不支持作为被联动
    },
    resizable: true
  },
  Indicator: {
    width: 298,
    height: 150,
    defaultImg: require('../assets/icons/chart/chartDefault_new/indicatorBoard.svg'),
    config: {
      isOpenPrivateDate: false,
      jumpOption: {
        openJump: false,
        openDimensionParam: false,
        optionType: 'report',
        reportId: '',
        reportUrl: ''
      },
      isOpenCompareBeforeTime: true,
      supportChangeChartTypes: ['SimpleGauge', 'CustomTable'],
      showType: 0,  // 数据格式 1-百分比 0-数值
      numberUnit: 'original', // 数值单位，默认原始值
      prefix: '', // 前缀
      suffix: '', // 后缀
      decimalPlaces: 2, // 小数位数，默认2位小数
      thousandsSeparator: false, // 默认不使用千位分隔符
      ringRatioUnitValue: 'num', // 环比单位值，默认123
      yearOnYearTypeValue: 'mom', // 同比类型值，默认月同比
      isSupportActiveLink: false, // 不支持作为主联动图表
      isSupportPassiveLink: false, // 不支持作为被联动
    },
    resizable: true
  },
  ComplexTextarea: {
    width: 360,
    height: 270,
    config: {
      isOpenPrivateDate: true,
      displayPrivateDate: 0, // 默认显示私有日期
      linkPublicDate: 0, // 默认关联公共日期
      periodType: '2',
      dateOrRange: 1,
      dateBefore: 1,
      beforeRange: 1,
      fixedStartDate: null,
      fixedEndDate: null,
      fixedPeriodType: '99',
      indicators: [], // 文本框选择的指标值对应的指标
      daySelectIndicators: [], // 日环比指标
      monthSelectIndicators: [], // 月环比指标
      isSupportActiveLink: false, // 不支持作为主联动图表
      isSupportPassiveLink: false, // 不支持作为被联动
    },
    resizable: true
  },
  DoublePie: {
    width: 360,
    height: 270,
    defaultImg: require('../assets/icons/chart/chartDefault_new/roundPie.svg'),
    config: {
      isOpenPrivateDate: true,
      displayPrivateDate: 0, // 默认不显示私有日期
      linkPublicDate: 0, // 默认不关联公共日期
      periodType: '2',
      dateOrRange: 1,
      dateBefore: 1,
      beforeRange: 1,
      fixedStartDate: null,
      fixedEndDate: null,
      fixedPeriodType: '99',
      supportChangeChartTypes: ['CustomTable'],
      showType: 0,  // 数据格式 1-百分比 0-数值
      numberUnit: 'original', // 数值单位，默认原始值
      prefix: '', // 前缀
      suffix: '', // 后缀
      decimalPlaces: 2, // 小数位数，默认2位小数
      thousandsSeparator: false, // 默认不使用千位分隔符
      ringRatioUnitValue: 'num', // 环比单位值，默认123
      yearOnYearTypeValue: 'mom', // 同比类型值，默认月同比
      isSupportActiveLink: false, // 不支持作为主联动图表
      isSupportPassiveLink: false, // 不支持作为被联动
    },
    resizable: true
  },
  // 地图
  SimpleMap: {
    width: 720,
    height: 540,
    defaultImg: {
      map: require('../assets/icons/chart/chartDefault_new/pie.svg'),
    },
    config: {
      isOpenPrivateDate: true,
      displayPrivateDate: 0, // 默认显示私有日期
      linkPublicDate: 0, // 默认关联公共日期
      periodType: '2',
      dateOrRange: 1,
      dateBefore: 1,
      beforeRange: 1,
      fixedStartDate: null,
      fixedEndDate: null,
      fixedPeriodType: '99',
      isSupportActiveLink: false, // 不支持作为主联动图表
      isSupportPassiveLink: false, // 不支持作为被联动
    },
    resizable: true
  },
  // 气泡图
  SimpleBubble: {
    width: 360,
    height: 270,
    defaultImg: {
      coverImg: require('../assets/icons/chart/chartDefault_new/pie.svg'),
    },
    config: {
      isSupportDrillDown: true, // 支持下钻设置
      drillDownOption: {
        drillDownTagList: [],
        opened: false,
        drillDownStep: 1
      },
      linkOption: { // 联动
        isSupportActiveLink: true, // 支持作为联动图表
        isSupportPassiveLink: true, // 支持作为被联动图表
        isMainLink: false, // 支持当前不是主联动图表
        chartClickItemIndex: -1, // 点击图表热区的索引
        conditionValue: '',
        filterDimension: ''
      },
      isOpenPrivateDate: true,
      displayPrivateDate: 0, // 默认显示私有日期
      linkPublicDate: 0, // 默认关联公共日期
      periodType: '2',
      dateOrRange: 1,
      dateBefore: 1,
      beforeRange: 1,
      fixedStartDate: null,
      fixedEndDate: null,
      fixedPeriodType: '99',
      supportChangeChartTypes: ['CustomTable', 'SimpleLine', 'SimpleBar', 'MixLineBar', 'IndexBoard'],
    },
    resizable: true
  },
  // 散点图
  SimpleScatter: {
    width: 360,
    height: 270,
    defaultImg: {
      coverImg: require('../assets/icons/chart/chartDefault_new/pie.svg'),
    },
    config: {
      isSupportDrillDown: true, // 支持下钻设置
      drillDownOption: {
        drillDownTagList: [],
        opened: false,
        drillDownStep: 1
      },
      linkOption: { // 联动
        isSupportActiveLink: true, // 支持作为联动图表
        isSupportPassiveLink: true, // 支持作为被联动图表
        isMainLink: false, // 支持当前不是主联动图表
        chartClickItemIndex: -1, // 点击图表热区的索引
        conditionValue: '',
        filterDimension: ''
      },
      isOpenPrivateDate: true,
      displayPrivateDate: 0, // 默认显示私有日期
      linkPublicDate: 0, // 默认关联公共日期
      periodType: '2',
      dateOrRange: 1,
      dateBefore: 1,
      beforeRange: 1,
      fixedStartDate: null,
      fixedEndDate: null,
      fixedPeriodType: '99',
      supportChangeChartTypes: ['CustomTable', 'SimpleLine', 'SimpleBar', 'MixLineBar', 'IndexBoard'],
    },
    resizable: true
  },
  // 指标看板
  IndexBoard: {
    width: 360,
    height: 240,
    defaultImg: require('../assets/icons/chart/chartDefault_new/indicatorBoard.svg'),
    config: {
      jumpOption: {
        openJump: false,
        openDimensionParam: false,
        optionType: 'report',
        reportId: '',
        reportUrl: ''
      },
      isOpenPrivateDate: true,
      drillDownOption: {
        drillDownTagList: [],
        opened: false,
        drillDownStep: 1,
      },
      displayPrivateDate: 0, // 默认显示私有日期
      linkPublicDate: 0, // 默认关联公共日期
      periodType: '2',
      dateOrRange: 1,
      dateBefore: 1,
      beforeRange: 1,
      fixedStartDate: null,
      fixedEndDate: null,
      fixedPeriodType: '99',
      supportChangeChartTypes: ['CustomTable', 'SimpleLine', 'SimpleBar', 'MixLineBar'],
      isSupportActiveLink: false, // 不支持作为主联动图表
      isSupportPassiveLink: false, // 不支持作为被联动
    },
    resizable: true
  },
  // 多环图
  MultiplePie: {
    width: 360,
    height: 270,
    defaultImg: {
      coverImg: require('../assets/icons/chart/chartDefault_new/pie.svg'),
    },
    config: {
      isOpenPrivateDate: true,
      displayPrivateDate: 0, // 默认不显示私有日期
      linkPublicDate: 0, // 默认不关联公共日期
      periodType: '2',
      dateOrRange: 1,
      dateBefore: 1,
      beforeRange: 1,
      fixedStartDate: null,
      fixedEndDate: null,
      fixedPeriodType: '99',
      isSupportActiveLink: false, // 不支持作为主联动图表
      isSupportPassiveLink: false, // 不支持作为被联动
    },
    resizable: true
  },
  //tab
  FaTab: {
    width: 467,
    height: 400,
    defaultImg: {
      coverImg: require('../assets/icons/chart/chartDefault_new/pie.svg'),
    },
    config: {
      isOpenPrivateDate: false,
      displayPrivateDate: 0, // 默认不显示私有日期
      linkPublicDate: 0, // 默认不关联公共日期
      periodType: '2',
      dateOrRange: 1,
      dateBefore: 1,
      beforeRange: 1,
      fixedStartDate: null,
      fixedEndDate: null,
      fixedPeriodType: '99',
      isSupportActiveLink: false, // 不支持作为主联动图表
      isSupportPassiveLink: false, // 不支持作为被联动
    },
    resizable: true
  },
  // 维度筛选器
  DimensionFilter: {
    width: 360,
    height: 45,
    config: {
      filterDimension: '',
      filterDimensionTitle: '',
      relativeComponentList: [],
      conditionValue: '',
      isSupportActiveLink: false, // 不支持作为主联动图表
      isSupportPassiveLink: false, // 不支持作为被联动
    }
  },
  // 全局变量筛选器
  GlobalVarFilter: {
    width: 360,
    height: 45,
    config: {
      filterGlobalVar: [],
      filterGlobalVarTitle: '',
      relativeComponentList: [],
      conditionValue: '',
      isSupportActiveLink: false, // 不支持作为主联动图表
      isSupportPassiveLink: false, // 不支持作为被联动
    }
  },
  // 指标筛选器
  IndicatorFilter: {
    width: 360,
    height: 45,
    config: {
      filterIndicator: '',
      filterIndicatorTitle: '',
      relativeComponentList: [],
      conditionValue: '',
      isSupportActiveLink: false, // 不支持作为主联动图表
      isSupportPassiveLink: false, // 不支持作为被联动
    }
  },
  // 组合筛选器
  MixFilter: {
    width: 600,
    height: 48,
    config: {
      relativeFilterList: [],
      isSupportActiveLink: false, // 不支持作为主联动图表
      isSupportPassiveLink: false, // 不支持作为被联动
    }
  },
  // 图片组件
  CustomImage: {
    width: 360,
    height: 270,
    config: {
      isSupportActiveLink: false, // 不支持作为主联动图表
      isSupportPassiveLink: false, // 不支持作为被联动
    }
  },
  // 折线图
  BasicRadar: {
    width: 360,
    height: 270,
    defaultImg: require('../assets/icons/chart/chartDefault_new/simpleLine.svg'),
    config: {
      isSupportDrillDown: true, // 支持下钻设置
      drillDownOption: {
        drillDownTagList: [],
        opened: false,
        drillDownStep: 1,
      },
      jumpOption: {
        openJump: false,
        openDimensionParam: false,
        optionType: 'report',
        reportId: '',
        reportUrl: ''
      },
      linkOption: { // 联动
        isSupportActiveLink: true, // 支持作为联动图表
        isSupportPassiveLink: true, // 支持作为被联动图表
        isMainLink: false, // 支持当前不是主联动图表
        chartClickItemIndex: -1, // 点击图表热区的索引
        conditionValue: '',
        filterDimension: ''
      },
      isOpenPrivateDate: true,
      displayPrivateDate: 0, // 默认不显示私有日期
      linkPublicDate: 0, // 默认不关联公共日期
      periodType: '2',
      dateOrRange: 1,
      dateBefore: 1,
      beforeRange: 1,
      fixedStartDate: null,
      fixedEndDate: null,
      fixedPeriodType: '99',
      isOpenThreshold: false,
      supportChangeChartTypes: ['CustomTable', 'SimpleBar', 'MixLineBar', 'SimplePie', 'IndexBoard'],
      isSort: 'false', // 是否排序
      sortBy: '', // 排序依据
      sortOrder: '', //次序
      showType: 0,  // 数据格式 1-百分比 0-数值
      numberUnit: 'original', // 数值单位，默认原始值
      prefix: '', // 前缀
      suffix: '', // 后缀
      decimalPlaces: 2, // 小数位数，默认2位小数
      thousandsSeparator: false, // 默认不使用千位分隔符
    },
    resizable: true
  },
}

/**
 * 图表类型
 */
export const CHART_TYPE_LIST = [
  {
    key: '001',
    id: 'CustomTable',
    type: 'md-grid',
    title: '明细表',
    icon: require('../assets/icons/chart/chart_type/table_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/table_blue.svg')
  },
  {
    key: '020',
    id: 'GroupTable',
    type: 'md-grid',
    title: '分组表',
    icon: require('../assets/icons/chart/chart_type/table_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/table_blue.svg'),
  },
  {
    key: '002',
    id: 'SimpleLine',
    type: 'md-trending-up',
    title: '折线图',
    icon: require('../assets/icons/chart/chart_type/line_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/line_blue.svg')
  },
  {
    key: '003',
    id: 'SimpleBar',
    type: 'ios-stats',
    title: '柱状图',
    icon: require('../assets/icons/chart/chart_type/bar_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/bar_blue.svg')
  },
  {
    key: '004',
    id: 'SimplePie',
    type: 'md-pie',
    title: '饼图',
    icon: require('../assets/icons/chart/chart_type/pie_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/pie_blue.svg')
  },
  {
    key: '005',
    id: 'SimpleScatter',
    type: 'md-scatter',
    title: '散点图',
    icon: require('../assets/icons/chart/chart_type/scatter_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/scatter_blue.svg')
  },
  {
    key: '016',
    id: 'SimpleBubble',
    type: 'md-bubble',
    title: '气泡图',
    icon: require('../assets/icons/chart/chart_type/bubble_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/bubble_blue.svg'),
  },
  {
    key: '019',
    id: 'BasicRadar',
    type: 'md-radar',
    title: '雷达图',
    icon: require('../assets/icons/chart/chart_type/radar_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/radar_blue.svg')
  },
  {
    key: '006',
    id: 'MixLineBar',
    type: 'md-analytics',
    title: '双轴图',
    icon: require('../assets/icons/chart/chart_type/lineBar_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/lineBar_blue.svg')
  },
  {
    key: '007',
    id: 'DoublePie',
    type: 'md-radio-button-on',
    title: '双环图',
    icon: require('../assets/icons/chart/chart_type/dbCircle_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/dbCircle_blue.svg')
  },
  {
    key: '008',
    id: 'Indicator',
    type: 'md-pricetag',
    title: '指标卡',
    icon: require('../assets/icons/chart/chart_type/dashboard_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/dashboard_blue.svg')
  },
  {
    key: '009',
    id: 'IndexBoard',
    type: 'md-IndexBoard',
    title: '指标组',
    icon: require('../assets/icons/chart/chart_type/indactorBoard_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/indactorBoard_blue.svg')
  },
  {
    key: '010',
    id: 'SimpleGauge',
    type: 'md-speedometer',
    title: '仪表盘',
    icon: require('../assets/icons/chart/chart_type/gauge_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/gauge_blue.svg')
  },
  {
    key: '011',
    id: 'ComplexTextarea',
    type: 'md-pricetag',
    title: '文本框',
    icon: require('../assets/icons/chart/chart_type/text_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/text_blue.svg')
  },
  {
    key: '012',
    id: 'SimpleIframe',
    type: 'md-paper',
    title: 'iframe',
    icon: require('../assets/icons/chart/chart_type/iframe_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/iframe_blue.svg')
  },
  {
    key: '013',
    id: 'FaTab',
    type: 'md-paper',
    title: 'Tab',
    icon: require('../assets/icons/chart/chart_type/tab_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/tab_blue.svg')
  },
  {
    key: '015',
    id: 'SimpleMap',
    type: 'md-map',
    title: '地图',
    icon: require('../assets/icons/chart/chart_type/map_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/map_blue.svg')
  },
  // {
  //   key: '018',
  //   id: 'MultiplePie',
  //   type: 'md-multiplePie',
  //   title: '多环图',
  //   icon: require('../assets/icons/chart/new_icon/pie_new.svg')
  // }
]

/**
 * 筛选器
 */
export const FILTER_LIST = [
  {
    id: 'DimensionFilter',
    title: '维度筛选',
    icon: require('../assets/icons/chart/chart_type/dimensionFilter_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/dimensionFilter_blue.svg')
  },
  {
    id: 'IndicatorFilter',
    title: '指标筛选',
    icon: require('../assets/icons/chart/chart_type/indicatorFilter_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/indicatorFilter_blue.svg')
  },
  {
    id: 'MixFilter',
    title: '组合筛选',
    icon: require('../assets/icons/chart/chart_type/mixFilter_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/mixFilter_blue.svg')
  },
  {
    id: 'GlobalVarFilter',
    title: '全局变量筛选',
    icon: require('../assets/icons/chart/chart_type/mixFilter_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/mixFilter_blue.svg')
  }
]

/**
 * 图片
 */
export const IMAGE_LIST = [
  {
    id: 'CustomImage',
    title: '图片',
    icon: require('../assets/icons/chart/chart_type/image_black.svg'),
    hovericon: require('../assets/icons/chart/chart_type/image_blue.svg')
  }
]

// 图例位置
export const LEGEND_POSITION_LIST = [
  { label: '上', value: '上' },
  { label: '下', value: '下' },
  { label: '左', value: '左' },
  { label: '右', value: '右' }
]

/**
 * 标题支持的字体
 * */
export const FONT_FAMILY_LIST = [
  { label: '阿里普惠体', value: 'aliPuHui Regular' },
  { label: '微软雅黑', value: 'Microsoft YaHei' },
  { label: '宋体', value: 'SimSun' },
  { label: '楷体', value: 'KaiTi' },
  { label: '黑体', value: 'SimHei' }
]
/**
 * 标签支持的字体
 */
export const LABEL_FAMILY_LIST = [
  { label: '微软雅黑', value: 'Microsoft YaHei' },
  { label: '宋体', value: 'SimSun' },
  { label: '楷体', value: 'KaiTi' }
]
/**
 * 字体大小
 */
export const FONT_SIZE_ARR = [
  {
    value: 8
  },
  {
    value: 10
  },
  {
    value: 12
  },
  {
    value: 14
  },
  {
    value: 16
  },
  {
    value: 18
  },
  {
    value: 20
  },
  {
    value: 24
  },
  {
    value: 32
  },
  {
    value: 40
  },
  {
    value: 48
  }]
/**
 * 边框样式
 * */
export const BORDER_STYLE_ARR = [
  { label: '无', value: 'none' },
  { label: '实线', value: 'solid' },
  { label: '虚线', value: 'dashed' },
  { label: '点线', value: 'dotted' }
]
/**
 * 边框宽度
 * */
export const BORDER_WIDTH_ARR = [
  { label: '1px', value: '1px' },
  { label: '2px', value: '2px' },
  { label: '3px', value: '3px' },
  { label: '4px', value: '4px' },
  { label: '5px', value: '5px' }
]

// 移动端机型
export const MOBILE_LIST = [
  {
    label: 'IOS',
    children: [
      {
        label: 'iPhone6/7/8',
        value: '375,667'
      }
    ]
  },
  {
    label: 'Android',
    children: [
      {
        label: '通用尺寸',
        value: '360,640'
      }
    ]
  }
]

export const CANVAS_COLOR_CONFIG = {
  'default': {
    'componentBackground': '#ffffff',
    'defaultShowComponentBackground': 'rgba(0, 0, 0, 0.1)',
    'fontStyleColor': '#333333',
    'indicatorTitleColor': '#ffffff',
    'valueBackgroundColor': '#ffffff',
    'countColor': '#000000',
    'splitLineColor': '#cccccc',
    'axisLineColor': '#999999',
    'axisLabelColor': '#999999'
  },
  'black': {
    'componentBackground': '#192241',
    'defaultShowComponentBackground': 'rgba(255, 255, 255, 0.4)',
    'fontStyleColor': '#ffffff',
    'indicatorTitleColor': '#ffffff',
    'valueBackgroundColor': '#303A57',
    'countColor': '#ffffff',
    'splitLineColor': '#7E8BB0',
    'axisLineColor': '#7E8BB0',
    'axisLabelColor': '#ffffff'
  }
}

// 数值单位
export const NUMBER_UNIT_LIST = [
  {
    label: '原始值',
    value: 'original'
  },
  {
    label: '万',
    value: 'wan'
  },
  {
    label: '亿',
    value: 'yi'
  }
]

// 数据格式
export const DATA_FORMAT_LIST = [
  {
    label: '数值',
    value: 0
  },
  {
    label: '百分比',
    value: 1
  }
]

// 仪表盘区间颜色
export const GAUGE_COLOR_LIST = [
  '#84AEFF',
  '#88CE76',
  '#FFD800',
  '#F7B500',
  '#FA6400',
]

// 阈值符号
export const THRESHOLD_SYMBOL_LIST = ['<', '≤', '>', '≥', '＞', '＜']

// 快速计算单位
export const RATIO_UNIT_LIST = [
  {
    label: '123',
    value: 'num'
  },
  {
    label: '%',
    value: 'per'
  }
]

// 同比列表
export const YEAR_ON_YEAR_LIST = [
  {
    label: '月同比',
    value: 'mom'
  },
  {
    label: '年同比',
    value: 'yoy'
  }
]

// 数值类型，null对应的默认展示
export const NULL_DATA_NUMBER_SHOW = '--'

// 当该列合计为0时，占比展示当值
export const PROPORTION_DENOMINATOR_ZERO_SHOW = '--'

// 百分比类型，null对应的默认展示
export const NULL_DATA_PERCENTAGE_SHOW = '--'

// 环比或者同比 前一期为0时，展示
export const ZERO_PRE_COMPARE_DATA_SHOW = '--'

// 阈值
export const THRESHOLD_SECTION_UP = [
  {
    label: '>',
    value: 0
  },
  {
    label: '≥',
    value: 1
  }
]
export const THRESHOLD_SECTION_DOWN = [
  {
    label: '<',
    value: 0
  },
  {
    label: '≤',
    value: 1
  }
]

/**
 *
 * 是否使用节假日，针对证券行业日报，过滤节假日
 * @type {boolean}
 */
export const DAY_REPORT_USE_HOLIDAYS = false

// 同比列表
export const NULL_VALUE_SHOW_LIST = [
  {
    label: '显示为"--"',
    value: 1
  },
  {
    label: '显示为"null"',
    value: 2
  },
  {
    label: '显示为"0"',
    value: 3
  },
  {
    label: '不显示',
    value: 4
  },
]

export const ENVTYPES = Object.freeze(
  [
    { label: "报告", value: 3 },
    // { label: "内部链接", value: 8 },
    { label: "报表", value: 9 },
    { label: "客户系统", value: 10 }
  ]
)

export const FILTER_TITLE_CONFIG = [{
  id: 'DimensionFilter',
  title: '维度筛选器'
}, {
  id: 'IndicatorFilter',
  title: '指标筛选器'
}, {
  id: 'GlobalVarFilter',
  title: '全局变量筛选器'
}]
