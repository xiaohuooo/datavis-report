export const filterList = [
  {
    label: '大于(>)',
    value: 1
  },
  {
    label: '小于(<)',
    value: 2
  },
  {
    label: '不等于(!=或<>)',
    value: 3
  },
  {
    label: '等于(=)',
    value: 4
  },
  {
    label: '大于等于(>=)',
    value: 5
  },
  {
    label: '小于等于(<=)',
    value: 6
  },
  {
    label: '两者之间(between)',
    value: 7
  },
  {
    label: '包含(⊇)',
    value: 8
  },
  {
    label: '不包含(⊄)',
    value: 9
  },
  {
    label: '以XXX开头(like)',
    value: 10
  },
  {
    label: '不以XXX开头(not like)',
    value: 11
  },
  {
    label: '以XXX结尾(like)',
    value: 12
  },
  {
    label: '不以XXX结尾(not like)',
    value: 13
  },
  {
    label: '在XXX范围内(in)',
    value: 14
  },
  {
    label: '不在XXX范围内(not in)',
    value: 15
  }
]

export const variableTypeList = [
  {
    label: "常量",
    value: 1
  },
  {
    label: "变量",
    value: 2
  },
  {
    label: "SQL",
    value: 3
  },
  {
    label: "全局变量",
    value: 4
  }
]
