import icon_basic_indicator from '@/assets/icons/indicatorIcon/icon_basic_indicator.svg' // 原子
import icon_basic_indicator_active from '@/assets/icons/indicatorIcon/icon_basic_indicator_active.svg'
import icon_derivative_indicator from '@/assets/icons/indicatorIcon/icon_derivative_indicator.svg' // 派生
import icon_derivative_indicator_active from '@/assets/icons/indicatorIcon/icon_derivative_indicator_active.svg'
import icon_composite_indicator from '@/assets/icons/indicatorIcon/icon_composite_indicator.svg' // 复合
import icon_composite_indicator_active from '@/assets/icons/indicatorIcon/icon_composite_indicator_active.svg'
import icon_out_indicator from '@/assets/icons/indicatorIcon/icon_out_indicator.svg' // 外部
import icon_out_indicator_active from '@/assets/icons/indicatorIcon/icon_out_indicator_active.svg'
import icon_static_indicator from '@/assets/icons/indicatorIcon/icon_static_indicator.svg'
import icon_static_indicator_active from '@/assets/icons/indicatorIcon/icon_static_indicator_active.svg'

const BASIC_INDICATOR_TYPE = 1
const COMPOSITE_INDICATOR_TYPE = 2
const DERIVATIVE_INDICATOR_TYPE = 3
export const OUT_INDICATOR_TYPE = 4
const STATIC_INDICATOR_TYPE = 5
export const INDICATOR_TYPE_ARR = [BASIC_INDICATOR_TYPE, COMPOSITE_INDICATOR_TYPE, DERIVATIVE_INDICATOR_TYPE,OUT_INDICATOR_TYPE,STATIC_INDICATOR_TYPE]
// 指标数据
export const INDICATOR_OPTION_LIST = [
  {
    label: '原子指标',
    value: BASIC_INDICATOR_TYPE,
    initial: 'A', // 编码前缀
    icon: icon_basic_indicator,
    iconActive: icon_basic_indicator_active,
    iconClass: 'icon-basic-indicator',
    moduleType: 2, // 该指标在后台接口对应的模块类型
  },
  {
    label: '派生指标',
    value: DERIVATIVE_INDICATOR_TYPE,
    initial: 'D', // 编码前缀
    icon: icon_derivative_indicator,
    iconActive: icon_derivative_indicator_active,
    iconClass: 'icon-derivative-indicator',
    moduleType: 8,
  },
  {
    label: '复合指标',
    value: COMPOSITE_INDICATOR_TYPE,
    initial: 'C', // 编码前缀
    icon: icon_composite_indicator,
    iconActive: icon_composite_indicator_active,
    iconClass: 'icon-composite-indicator',
    moduleType: 3,
  },
  {
    label: '外部指标',
    value: OUT_INDICATOR_TYPE,
    initial: 'O', // 编码前缀
    icon: icon_out_indicator,
    iconActive: icon_out_indicator_active,
    iconClass: 'icon-out-indicator',
    moduleType: 11,
  },
  {
    label: '静态指标',
    value: STATIC_INDICATOR_TYPE,
    initial: 'S', // 编码前缀
    icon: icon_static_indicator,
    iconActive: icon_static_indicator_active,
    iconClass: 'icon-static-indicator',
    moduleType: 15,
  },
]
