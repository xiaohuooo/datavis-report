/** 定制化图表 */
export const CUSTOM_CHART_LIST = ["BarDemo", "TextDemo"];

/** 不能保存为资源的图表 */
export const NO_RESOURCE_CHART_LIST = ["SimpleIframe", "FaTab", "DimensionFilter", "IndicatorFilter", "MixFilter"];

/** 仅需要指标的图表 */
export const ONLY_INDICATOR_CHART_LIST = ["CustomTable", "Indicator", "SimpleGauge"];

/** 需要维度+指标的图表 */
export const DIMENSION_INDICATOR_CHART_LIST = [
  "SimpleLine",
  "SimpleBar",
  "SimplePie",
  "MixLineBar",
  "DoublePie",
  "IndexBoard",
  "SimpleMap",
  "BasicRadar"
];
