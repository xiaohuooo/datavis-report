import publishIcon from "@/assets/icons/earlyWarningTarget/publish.svg";
import offLineIcon from "@/assets/icons/earlyWarningTarget/downLine.svg";
import onLineIcon from "@/assets/icons/earlyWarningTarget/upLine.svg";

export const NOT_ONLINE = 0;
export const IN_APPROVAL = 1;
export const PUBLISH_FAIL = 2;
export const IN_ONLINE = 3;
export const IN_OFFLINE = 4;

export const PUBLISH_ACTION_LIST = [
  {
    label: "未发布",
    value: NOT_ONLINE,
    prefixColor: "#D8D8D8", // 前缀颜色
    actionIcon: publishIcon, // 该状态对应的操作icon
    actionLabel: "发布" // 该状态对应的操作
  },
  {
    label: "审批中",
    value: IN_APPROVAL,
    prefixColor: "#FF8100",
    actionIcon: null,
    actionLabel: ""
  },
  {
    label: "发布不通过",
    value: PUBLISH_FAIL,
    prefixColor: "#EE2629",
    actionIcon: publishIcon,
    actionLabel: "发布"
  },
  {
    label: "已发布",
    value: IN_ONLINE,
    prefixColor: "#31C40E",
    actionIcon: offLineIcon,
    actionLabel: "下线"
  },
  {
    label: "已下线",
    value: IN_OFFLINE,
    prefixColor: "#EE2629",
    actionIcon: publishIcon,
    actionLabel: "发布"
  }
];

export const REPORT_ASSET = "report";
export const DIMENSION_ASSET = "dimension";
export const INDICATOR_ASSET = "indicator";
export const DATA_MODEL_ASSET = "dataModel";
