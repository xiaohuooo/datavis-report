import axios from "@/libs/api.request";

// 数据连接列表
export const dataConnectList = params => {
  return axios.request({
    url: "meta/syncDataConnect/list",
    method: "get",
    params: params
  });
};

// 新增数据连接
export const dataConnectCreate = data => {
  return axios.request({
    url: "meta/syncDataConnect/create",
    method: "post",
    data
  });
};

// 编辑数据连接
export const dataConnectUpdate = data => {
  return axios.request({
    url: "meta/syncDataConnect/update",
    method: "post",
    data
  });
};

// 删除数据连接
export const dataConnectDelete = id => {
  return axios.request({
    url: `meta/syncDataConnect/${id}/delete`,
    method: "DELETE"
  });
};

// 测试数据连接
export const dataConnectTest = data => {
  return axios.request({
    url: `meta/syncDataConnect/test`,
    method: "post",
    data
  });
};

// 数据集列表
export const dataSetPage = data => {
  return axios.request({
    url: `meta/syncDataset/page`,
    method: "post",
    data
  });
};

//
export const dataSetList = data => {
  return axios.request({
    url: `meta/syncDataset/list`,
    method: "post",
    data
  });
};

// 数据集详情
export const dataSetDetail = id => {
  return axios.request({
    url: `meta/syncDataset/${id}`,
    method: "get"
  });
};

// 数据集创建
export const dataSetCreate = data => {
  return axios.request({
    url: `meta/syncDataset/create`,
    method: "post",
    data
  });
};

// 数据集更新
export const dataSetUpdate = data => {
  return axios.request({
    url: `meta/syncDataset/update`,
    method: "post",
    data
  });
};

//
export const dataSetDelete = id => {
  return axios.request({
    url: `meta/syncDataset/${id}/delete`,
    method: "DELETE"
  });
};

// SQL预览
export const dataSetSqlView = data => {
  return axios.request({
    url: `meta/syncDataset/sql/view`,
    method: "post",
    data
  });
};

// 树数据集预览
export const dataSetTree = data => {
  return axios.request({
    url: `meta/syncDataset/tree/view`,
    method: "post",
    data
  });
};

// POST /task/startTimedSync开始任务
/* export const startTimedSync = data => {
  return axios.request({
    url: `meta/task/startTimedSync`,
    method: "post",
    data
  });
}; */

// /task/stopTimedSync 停止任务
/* export const stopTimedSync = data => {
  return axios.request({
    url: `meta/task/stopTimedSync`,
    method: "post",
    data
  });
}; */

// 获取下次执行时间
export const nextTime = params => {
  return axios.request({
    url: `meta/task/nextTime`,
    method: "get",
    params
  });
};
