import axios from "@/libs/api.request";

// 用户管理- 是否首次用户
export function isFirst(params) {
  return axios.request({
    url: "meta/sysUser/sync/isFirst",
    method: "get",
    params
  });
}

// 立即同步
export function syncTrigger(params) {
  return axios.request({
    url: "meta/sysUser/sync/trigger",
    method: "get",
    params
  });
}

export function getStatus(params) {
  return axios.request({
    url: "meta/sysUser/sync/status",
    method: "get",
    params
  });
}

export function historyOld(data) {
  return axios.request({
    url: "meta/sysUser/sync/history",
    method: "post",
    data
  });
}

// 清空同步
export function syncTruncate(params) {
  return axios.request({
    url: "meta/sysUser/sync/truncate",
    method: "get",
    params
  });
}

// 查看同步用户设置
export function syncView(params) {
  return axios.request({
    url: "meta/sysUser/sync/view",
    method: "get",
    params
  });
}

// 修改同步用户设置
export function syncConfig(data) {
  return axios.request({
    url: "meta/sysUser/sync/config",
    method: "post",
    data
  });
}
