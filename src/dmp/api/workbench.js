import axios from "@/libs/api.request";

// 工作台首页
// export const workbenchShow = params => {
//   return axios.request({
//     url: "meta/workbench/show",
//     method: "get",
//     params: params
//   });
// };

// 工作台列表
export const workbenchPage = params => {
  return axios.request({
    url: "meta/workbench/page",
    method: "get",
    params: params
  });
};

// 工作台详情
export const workbenchDetail = (id = "") => {
  return axios.request({
    url: "meta/workbench/detail/" + id,
    method: "put"
  });
};

//  新增工作台
export const workbenchCreate = params => {
  return axios.request({
    url: "meta/workbench/create",
    method: "get",
    params
  });
};

//  新增个人工作台
// export const workbenchCreatePersonal = params => {
//   return axios.request({
//     url: "meta/workbench/createPersonal",
//     method: "get",
//     params
//   });
// };

//  修改工作台
export const workbenchUpdate = data => {
  return axios.request({
    url: "meta/workbench/update",
    method: "post",
    data
  });
};

//  修改个人工作台
// export const workbenchUpdatePersonal = data => {
//   return axios.request({
//     url: "meta/workbench/updatePersonal",
//     method: "post",
//     data
//   });
// };

//  删除工作台
export const workbenchDelete = (id = "") => {
  return axios.request({
    url: "meta/workbench/" + id,
    method: "DELETE"
  });
};

// 根据用户id查询用户常用菜单集合
export const getUserResourceRank = params => {
  return axios.request({
    url: "meta/userResourceRank/getRank",
    method: "get",
    params
  });
};
