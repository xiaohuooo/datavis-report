// dmp迁移
import Vue from "vue";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import "./icons"; // 导入自定义 icon 图标
import "./styles/index.scss";

Vue.use(ElementUI);
