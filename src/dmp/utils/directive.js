/**
 * 防抖指令
 */
export const debounce = {
  //handler：绑定的方法  threshhold：多长时间执行一次  eventment：执行的事件    v-debounce="{handler:执行方法,wait:600,eventment:事件名称}"
  inserted: (el, binding) => {
    if (!binding.value.handler || typeof binding.value.handler != "function") {
      return;
    }
    const wait = binding.value.wait || 600; //等待时间
    let timer;
    const eventment = binding.value.eventment || "input"; //事件名称
    el.addEventListener(eventment, () => {
      if (timer) {
        clearTimeout(timer);
      }
      timer = setTimeout(() => {
        binding.value.handler();
      }, wait);
    });
  }
};
