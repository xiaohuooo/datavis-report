/**
 * Created by PanJiaChen on 16/11/18.
 */

/**
 * Parse the time to string
 * @param {(Object|string|number)} time
 * @param {string} cFormat
 * @returns {string | null}
 */
export function parseTime(time, cFormat) {
  if (arguments.length === 0 || !time) {
    return null
  }
  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if ((typeof time === 'string')) {
      if ((/^[0-9]+$/.test(time))) {
        // support "1548221490638"
        time = parseInt(time)
      } else {
        // support safari
        // https://stackoverflow.com/questions/4310953/invalid-date-in-safari
        time = time.replace(new RegExp(/-/gm), '/')
      }
    }

    if ((typeof time === 'number') && (time.toString().length === 10)) {
      time = time * 1000
    }
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const time_str = format.replace(/{([ymdhisa])+}/g, (result, key) => {
    const value = formatObj[key]
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') { return ['日', '一', '二', '三', '四', '五', '六'][value ] }
    return value.toString().padStart(2, '0')
  })
  return time_str
}

/**
 * @param {number} time
 * @param {string} option
 * @returns {string}
 */
export function formatTime(time, option) {
  if (('' + time).length === 10) {
    time = parseInt(time) * 1000
  } else {
    time = +time
  }
  const d = new Date(time)
  const now = Date.now()

  const diff = (now - d) / 1000

  if (diff < 30) {
    return '刚刚'
  } else if (diff < 3600) {
    // less 1 hour
    return Math.ceil(diff / 60) + '分钟前'
  } else if (diff < 3600 * 24) {
    return Math.ceil(diff / 3600) + '小时前'
  } else if (diff < 3600 * 24 * 2) {
    return '1天前'
  }
  if (option) {
    return parseTime(time, option)
  } else {
    return (
      d.getMonth() +
      1 +
      '月' +
      d.getDate() +
      '日' +
      d.getHours() +
      '时' +
      d.getMinutes() +
      '分'
    )
  }
}

/**
 * @param {string} url
 * @returns {Object}
 */
export function param2Obj(url) {
  const search = decodeURIComponent(url.split('?')[1]).replace(/\+/g, ' ')
  if (!search) {
    return {}
  }
  const obj = {}
  const searchArr = search.split('&')
  searchArr.forEach(v => {
    const index = v.indexOf('=')
    if (index !== -1) {
      const name = v.substring(0, index)
      const val = v.substring(index + 1, v.length)
      obj[name] = val
    }
  })
  return obj
}


export function deepClone(source) {
  if (!source && typeof source !== 'object') {
    throw new Error('error arguments', 'deepClone')
  }
  const targetObj = source.constructor === Array ? [] : {}
  Object.keys(source).forEach(keys => {
    if (source[keys] && typeof source[keys] === 'object') {
      targetObj[keys] = deepClone(source[keys])
    } else {
      targetObj[keys] = source[keys]
    }
  })
  return targetObj
}


export function hexToRgb(val = '#333') {
  //HEX十六进制颜色值转换为RGB(A)颜色值
  // 16进制颜色值的正则
  var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/
  // 把颜色值变成小写
  var color = val.toLowerCase()
  var result = ''
  if (reg.test(color)) {
    // 如果只有三位的值，需变成六位，如：#fff => #ffffff
    if (color.length === 4) {
      var colorNew = '#'
      for (var i = 1; i < 4; i += 1) {
        colorNew += color.slice(i, i + 1).concat(color.slice(i, i + 1))
      }
      color = colorNew
    }
    // 处理六位的颜色值，转为RGB
    var colorChange = []
    for (var i = 1; i < 7; i += 2) {
      colorChange.push(parseInt('0x' + color.slice(i, i + 2)))
    }
    result = 'rgb(' + colorChange.join(',') + ')'
    return {
      rgb: result,
      rgbs: `${colorChange[0]},${colorChange[1]},${colorChange[2]}`,
      r: colorChange[0],
      g: colorChange[1],
      b: colorChange[2],
    }
  } else {
    result = '无效'
    return { rgb: result }
  }
}

//树形结构转平级 - 会改变原数组
export function  toArr  (tree)  {
  let arrs = [];
  let result = [];
  arrs = arrs.concat(tree);
  // 把数组中每一项全部拉平 
  while (arrs.length) {
    let first = arrs.shift(); // 弹出第一个元素
    // 直到每一项数据都没有children
    if (first.children) {
      //如果有children
      arrs = arrs.concat(first.children);
      delete first['children'];
    }
    result.push(first);
  }
  return result;
}
export function toArrnotchange(tree) { // 树形结构转平级 不改变原数组
  let result = [];
  function traverse(node) {
    let { children, ...rest } = node;
    result.push(rest);
    if (children && children.length) {
      children.forEach((child) => traverse(child));
    }
  }
  tree.forEach((node) => traverse(node));
  return result;
}

export  function  toTree(data) {   // 平级转树形结构         
  let result = []            
  //如果值是 Array，则为true; 否则为false。           
  if(!Array.isArray(data)) {                  
      return result       
  }                
  //根据父节点进行拼接子节点，             
  data.forEach(item => {                
    delete item.children; //已经有的话就删掉              
  });               
  //把每一项的引用放入map对象里             
  let map = {};             
  data.forEach(item => {                 
       map[item.id] = item;          
  });                
  //再次遍历数组 决定item的去向             
  data.forEach(item => {                
    let parent = map[item.pid];                
    if(parent) {                      
     // 如果 map[item.pid] 有值 则 parent 为 item 的父级
     // 判断 parent 里有无children 如果没有则创建 如果有则直接把 item push到children里  
     (parent.children || (parent.children = [])).unshift(item);                 
    } else {                 
      // 如果 map[item.pid] 找不到值 说明此 item 为 第一级              
      result.unshift(item);                  
    }              
  });  
  return result;          
}            






export function checkedMenu(arr ,path){ // 判断系统菜单是否有权限
  for (let i = 0; i < arr.length; i++) {
    const element = arr[i];
    if (element.path == path) {
      return true
    }else {
      if (element.children && element.children.length >0 ) {
          if (checkedMenu(element.children ,path )) {
             return true
          }
      }
    }
  }
  return false
}

export function timestampToTime(timestamp) { // 时间戳转换成时间
  var date = new Date(timestamp.length === 10 ? timestamp * 1000 : timestamp);
  var Y = date.getFullYear();
  var M = date.getMonth() + 1;
  var D = date.getDate();
  var h = date.getHours();
  var m = date.getMinutes();
  var s = date.getSeconds();
  return Y + '-' + addZero(M) + '-' + addZero(D) + ' ' + addZero(h) + ':' + addZero(m) + ':' + addZero(s);
}

function addZero(number) {
  if (number < 10) {
    return '0' + number;
  }
  return number;
}



export function countURLParameters(url) { // 获取URL参数的数量
  try {
    // 解析 URL
    var parsedURL = new URL(url);

    // 获取查询参数部分
    var queryParams = parsedURL.search;

    // 创建 URLSearchParams 对象
    var urlParams = new URLSearchParams(queryParams);

    // 获取查询参数的数量
    var numberOfParams = Array.from(urlParams.keys()).length;

    return numberOfParams;
  } catch (error) {
    return 0; // URL 无效时返回 0 个参数
  }
}
