import XLSX from 'xlsx';

export function exportFile(data, fileName) {
    let sheet = XLSX.utils.aoa_to_sheet(data);
    let blob = sheet2blob(sheet);
    let url = URL.createObjectURL(blob);
    let aLink = document.createElement('a');
    aLink.href = url;
    aLink.download = fileName;
    let event;
    if (window.MouseEvent) event = new MouseEvent('click');
    else {
        event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    }
    aLink.dispatchEvent(event);
}


function sheet2blob(sheet) {
    let sheetName = 'sheet1';
    let workbook = {
        SheetNames: [sheetName],
        Sheets: {}
    };
    workbook.Sheets[sheetName] = sheet;
    // 生成excel的配置项
    let config = {
        bookType: 'xlsx',
        bookSST: false,
        type: 'binary'
    };
    let content = XLSX.write(workbook, config);
    return new Blob([s2ab(content)], {type: "application/octet-stream"});
}

// 字符串转ArrayBuffer
function s2ab(s) {
    let buf = new ArrayBuffer(s.length);
    let view = new Uint8Array(buf);
    for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
}
