import axios from 'axios'
import {getToken, setToken} from '@/libs/util'
import router from "@/router"
import Vue from 'vue'

class HttpRequest {
  constructor(baseUrl = baseURL) {
    this.baseUrl = baseUrl
    this.queue = {}
  }

  getInsideConfig() {
    const config = {
      baseURL: this.baseUrl,
      headers: {}
    }
    let token = getToken();
    if (token) {
      config.headers['Authorization'] = token;
    }
    return config
  }

  destroy(url) {
    // delete this.queue[url]
    // if (!Object.keys(this.queue).length) {
      // Spin.hide()
    // }
  }

  interceptors(instance, url) {
    // 请求拦截
    instance.interceptors.request.use(config => {
      // 添加全局的loading...
      // if (!Object.keys(this.queue).length) {
        // Spin.show() // 不建议开启，因为界面不友好
      // }
      // this.queue[url] = true
      if(config.cancalKey) {
        config.cancelToken = new axios.CancelToken(cancel => { // 取消请求
          window['cancle'+config.cancalKey] = cancel;
        })
      }
      return config
    }, error => {
      return Promise.reject(error)
    })
    // 响应拦截
    instance.interceptors.response.use(res => {
      this.destroy(url)
      const {data, status} = res
        return {data, status}
    }, error => {
      this.destroy(url)
      let errorInfo = error.response
      if (errorInfo)  {
        // license失效
        if (errorInfo.status === 403) {
          router.push("/licenseInvalid");
        } else if(errorInfo.status === 401) {
          // token过期，或者token解析失败
          setToken("");
          router.push("/login");
          Vue.prototype.$Message.error("token过期或解析失败");
          if (window.location.hash.indexOf('template_add') > -1) {
            console.log(Vue.prototype.$bus, 'Vue')
            Vue.prototype.$bus.$emit('login-expired')
          }
        } else if(errorInfo.data.path === '/report/detail' && errorInfo.status === 500){
          return errorInfo
        }
      }
      return Promise.reject(error)  // 不走页面组件处理
    })
  }

  request(options) {
    const instance = axios.create()
    options = Object.assign(this.getInsideConfig(), options)
    this.interceptors(instance, options.url)
    return instance(options)
  }
}

export default HttpRequest
