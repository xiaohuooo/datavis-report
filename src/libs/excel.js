/* eslint-disable */
import XLSX from 'xlsx';
import moment from 'moment';

function auto_width(ws, data){
  /*set worksheet max width per col*/
  const colWidth = data.map(row => row.map(val => {
    /*if null/undefined*/
    if (val == null) {
      return {'wch': 10};
    }
    /*if chinese*/
    else if (val.toString().charCodeAt(0) > 255) {
      return {'wch': val.toString().length * 2};
    } else {
      return {'wch': val.toString().length};
    }
  }))
  /*start in the first row*/
  let result = colWidth[0];
  for (let i = 1; i < colWidth.length; i++) {
    for (let j = 0; j < colWidth[i].length; j++) {
      if (result[j]['wch'] < colWidth[i][j]['wch']) {
        result[j]['wch'] = colWidth[i][j]['wch'];
      }
    }
  }
  ws['!cols'] = result;
}

function json_to_array(key, jsonData){
  return jsonData.map(v => key.map(j => { return v[j] }));
}

// fix data,return string
function fixdata(data) {
  let o = ''
  let l = 0
  const w = 10240
  for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)))
  o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)))
  return o
}

function analysisHeader(start, end, curLevel, totalLevelLength, headers) {
  let data = []
  let curLevelHeaders = headers[curLevel]
  let curStart = 0
  let curend = 0
  let sort = 1
  // 当前层
  for (let i = start; i <= end; i++){
    if(curLevelHeaders[i] !== null){
      let obj = {
        label: curLevelHeaders[i],
        isGroup: false,
        columnName: null,
        sort: sort++,
        children: []
      }
      // 下一列如果为null，当前col是字段，否则是分组
      if(curLevel+1 < totalLevelLength && headers[curLevel+1][i] !== null){
        obj.isGroup = true;
        curStart = i
        for(let j=i+1; j<end; j++){
          if(curLevelHeaders[j] !== null){
            curend = j-1;
            break;
          }
        }
        if(curend < curStart){
          curend = headers[0].length-1
        }
        if(curend !== curStart){
          obj.children = analysisHeader(curStart, curend, curLevel+1, totalLevelLength, headers)
        }
        curStart = curend+1
      } else {
        obj.columnName = 'C'+i
      }
      data.push(obj)
    }
  }
  return data
}

// get head from excel file,return array
function get_header_row(sheet, totalLevelLength) {
  const headers = []
  const range = XLSX.utils.decode_range(sheet['!ref'])
  let C
  const R = range.s.r /* start in the first row */
  for (let i = 0; i < totalLevelLength; i++) {
    let row = []
    for (C = range.s.c; C <= range.e.c; ++C) { /* walk every column in the range */
      let cell = sheet[XLSX.utils.encode_cell({ c: C, r: i })]
      let hdr = null // <-- replace with your desired default
      if (cell && cell.t) hdr = XLSX.utils.format_cell(cell)
      row.push(hdr)
    }
    headers.push(row)
  }
  // let data = analysisHeader(0, headers[0].length-1, 0, totalLevelLength, headers)
  // console.log(data, 'data')
  // console.log(headers,'headers')

  // 导出再导入时，相邻相同数据表头消除
  for (C = range.s.c; C <= range.e.c; ++C){
    for (let i = totalLevelLength-1; i > 0; i--){
      if((headers[i][C] === headers[i-1][C]) && (headers[i][C] !== null)){
        headers[i][C] = null
      }
    }
  }
  for (let i = 0; i < totalLevelLength; i++){
    for (C = range.e.c; C > range.s.c; --C){
      if((headers[i][C] === headers[i][C-1]) && (headers[i][C] !== null)){
        headers[i][C] = null
      }
    }
  }

  return headers
}

export const export_table_to_excel= (id, filename) => {
  const table = document.getElementById(id);
  const wb = XLSX.utils.table_to_book(table);
  XLSX.writeFile(wb, filename);

  /* the second way */
  // const customTable = document.getElementById(id);
  // const wb = XLSX.utils.book_new();
  // const ws = XLSX.utils.table_to_sheet(customTable);
  // XLSX.utils.book_append_sheet(wb, ws, filename);
  // XLSX.writeFile(wb, filename);
}

export const export_json_to_excel = ({data, key, title, filename, autoWidth}) => {
  const wb = XLSX.utils.book_new();
  data.unshift(title);
  const ws = XLSX.utils.json_to_sheet(data, {header: key, skipHeader: true});
  if(autoWidth){
    const arr = json_to_array(key, data);
    auto_width(ws, arr);
  }
  XLSX.utils.book_append_sheet(wb, ws, filename);
  XLSX.writeFile(wb, filename + '.xlsx');
}

export const export_array_to_excel = ({key, data, title, filename, autoWidth}) => {
  const wb = XLSX.utils.book_new();
  const arr = json_to_array(key, data);
  arr.unshift(title);
  const ws = XLSX.utils.aoa_to_sheet(arr);
  if(autoWidth){
    auto_width(ws, arr);
  }
  XLSX.utils.book_append_sheet(wb, ws, filename);
  XLSX.writeFile(wb, filename + '.xlsx');
}

// 获取预览的表头和表数据
export const readPreview = (data, type, levelSize) => {
  const workbook = XLSX.read(data, { type: type, cellDates: true });
  const firstSheetName = workbook.SheetNames[0];
  const worksheet = workbook.Sheets[firstSheetName];
  let totalLevelLength = levelSize;
  const header = get_header_row(worksheet, totalLevelLength);
  const previewHeader = analysisHeader(0, header[0].length-1, 0, totalLevelLength, header);
  const previewResults = []
  const range = XLSX.utils.decode_range(worksheet['!ref'])
  // 循环数据
  for(let i = totalLevelLength; i<=range.e.r; i++) {
    // i为行
    let obj = {}
    for(let j = 0 ; j<=range.e.c; j++){
      // j为列
      let cell = worksheet[XLSX.utils.encode_cell({ c: j, r: i})]
      let hdr = null
      if(cell && cell.t === "d"){
        hdr = convertExcelDateFormat(cell.v)
      } else if(cell && cell.t){
        hdr = cell.v
      }
      if(hdr != null){
        obj['C'+j] = hdr
      }
    }
    previewResults.push(obj);
  }
  return {previewHeader, previewResults, firstSheetName}
}

export const read = (data, type, model, levelSize) => {
  /* if type == 'base64' must fix data first */
  // const fixedData = fixdata(data)
  // const workbook = XLSX.read(btoa(fixedData), { type: 'base64' })
  const workbook = XLSX.read(data, { type: type, cellDates: true });
  const firstSheetName = workbook.SheetNames[0];
  const worksheet = workbook.Sheets[firstSheetName];
  let totalLevelLength = levelSize;
  const header = get_header_row(worksheet, totalLevelLength);
  // const results = XLSX.utils.sheet_to_json(worksheet);
  const headers = []
  const results = []
  const newLabelDtoList = []
  const range = XLSX.utils.decode_range(worksheet['!ref'])
  let C
  const R = range.s.r
  // 循环表头
  for (C = range.s.c; C <= range.e.c; ++C) {
    let row = {}
    let columnName = null
    let parentLabel = null
    let grandpaLabel = null
    let curLevel = totalLevelLength-1
    for (let i = totalLevelLength-1 ; i >= 0; i--) {
      let hdr = null
      hdr = header[i][C]
      if(hdr) {
        curLevel = i
        row.label = hdr
        row.position = C
        if(curLevel !== 0) {
          for(let i = C; i >= 0; i--){
            let hdr = null
            hdr = header[curLevel-1][i]
            if(hdr) {
              parentLabel = hdr
              if(curLevel !== 1){
                for(let j = i; j >= 0; j--){
                  let hdr = null
                  hdr = header[curLevel-2][i]
                  if(hdr){
                    grandpaLabel = hdr
                    break
                  }
                }
              }
              break
            }
          }
        }
        let modelItem = findModelItemByLabel(hdr, parentLabel, model)
        if(modelItem && modelItem.columnName){
          columnName = modelItem.columnName
        }
        break
      }
    }
    row.columnName = columnName
    // columnName为null时 增加parentLabel parentId属性
    if(!columnName){
      row.parentLabel = parentLabel
      if(parentLabel == null){
        // 新增在一级，parentLabel为null
        row.parentId = -1
      } else if(grandpaLabel == null){
        // 新增在二级，grandpaLabel为null
        if(model.filter(item => item.label === parentLabel).length > 0){
          row.parentId = model.filter(item => item.label === parentLabel)[0].id
        } else {
          row.parentId = -1
        }
      } else {
        // 新增在三级或四级，根据parentLabel 和 grandpaLabel确定唯一的父级，
        let parentItem = findModelItemByLabel(parentLabel, grandpaLabel, model)
        if(parentItem && parentItem.id){
          row.parentId = parentItem.id
        } else {
          row.parentId = -1
        }
      }
    }
    headers.push(row)
  }
  // 循环数据
  for(let i = totalLevelLength; i<=range.e.r; i++) {
    let obj = {}
    headers.forEach((item) => {
      let cell = worksheet[XLSX.utils.encode_cell({ c: item.position, r: i })]
      let hdr = null
      if(cell && cell.t === "d"){
        // hdr = XLSX.utils.format_cell(cell)
        hdr = convertExcelDateFormat(cell.v)
      } else if(cell && cell.t){
        hdr = cell.v
      }
      if(hdr != null && item.columnName){
        obj[item.columnName] = hdr
      }
    })
    results.push(obj);
  }
  // 循环获取新增列
  headers.forEach((item) => {
    if(!item.columnName && item.label!=='序号'){
      let obj = {}
      obj.label = item.label;
      obj.parentLabel = item.parentLabel;
      obj.parentId = item.parentId;
      obj.value = [];
      for(let i = totalLevelLength; i<=range.e.r; i++) {
        let cell = worksheet[XLSX.utils.encode_cell({ c: item.position, r: i })]
        let hdr = null
        if(cell && cell.t === "d"){
          // hdr = XLSX.utils.format_cell(cell)
          hdr = convertExcelDateFormat(cell.v)
        } else if(cell && cell.t){
          hdr = cell.v
        }
        obj.value.push(hdr)
      }
      newLabelDtoList.push(obj)
    }
  })
  return {headers, results, newLabelDtoList};
}

// 日期转换
function convertExcelDateFormat(date){
  if(date === undefined || date === null || date === ""){
    return null
  }
  // 非时间格式，返回Invalid date
  let retFormat = moment(date).format('YYYY-MM-DD')
  if(retFormat === "Invalid date"){
    return retFormat;
  }
  return moment(date).add(1, 'days').format('YYYY-MM-DD')
}

function findModelItemByLabel(label, parentLabel, model) {
  let result = null
  // if(parentLabel == null){
  //     return;
  // }
  if(!model) {
    return;
  }
  for(let i=0; i<model.length; i++) {
    let item = model[i];
    if(parentLabel == null) {
      let filterModel = model.filter(childItem => childItem.label === label)
      if(filterModel.length > 0){
        result = filterModel[0]
        break
      }
    } else if (item.label === parentLabel && item.children && item.children.length > 0){
      let filterModel = item.children.filter(childItem => childItem.label === label)
      if(filterModel.length > 0){
        result = filterModel[0]
        break
      }
    } else if (item.children && item.children.length > 0) {
      result = findModelItemByLabel(label, parentLabel, item.children)
      if(result){
        break;
      }
    }
  }
  return result;
}

export default {
  export_table_to_excel,
  export_array_to_excel,
  export_json_to_excel,
  read,
  readPreview
}
