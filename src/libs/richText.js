const KEY_LEFT = 37, KEY_RIGHT = 39, KEY_UP = 38, KEY_DOWN = 40, KEY_ENTER = 13, KEY_SPACE = 32, KEY_BACKSPACE = 8
  , KEY_DELETE = 46, KEY_AT = 50, KEY_JIN = 51, KEY_ALT = 17, KEY_CTRL = 18, KEY_LOCK_BORD = 144, KEY_PAGE_UP = 33
  , KEY_PAGE_DN = 34, KEY_HOME = 36, KEY_END = 35;
const direction = [KEY_LEFT, KEY_RIGHT, KEY_DOWN, KEY_UP]
const notContent = [KEY_LEFT, KEY_RIGHT, KEY_DOWN, KEY_UP, KEY_ENTER, KEY_ALT, KEY_CTRL, KEY_LOCK_BORD, KEY_PAGE_UP, KEY_PAGE_DN, KEY_HOME, KEY_END]

function RichText(el, prams) {
  const _this = this;
  this.commandMap = {
    fontSize: 'fontSize',
    fontFamily: 'fontName',
    color: 'foreColor',
    fontWeight: 'bold',
    fontStyle: 'italic',
    underline: 'underline',
    strikeThrough: 'strikeThrough',
    alignRight: 'justifyRight',
    alignCenter: 'justifyCenter',
    alignLeft: 'justifyLeft',
    outdent: 'outdent',//缩进
  }
  this.placeholder = '';
  this.styleSwitch = true;
  this.rowTag = 'p';
  this.recover = null;
  this.editable = false;
  this.atomicAttribute = {atomicFlag: {key: 'atomic', val: 'true'}, tag: 'span', replaceFlag: 'atomic'}
  this.useExternalSettingBar = false//如果是true 那我就不再构造此bar
  this.mouseKeyEvents = {
    onFocus: null,
    onBlur: null,
    mouseUp: null,
    mouseDown: null,
    keyDown: null,
    keyUp: null,
    click: null,
    dbClick: null
  };
  this.selectConfig = {
    selectDirection: '',
    currentAtomicEl: undefined
  };
  this.parentContainer = el;
  this.editorBox = null;
  this.closeAfterData = {
    condition: null,
    _result_: undefined,
    //result:{
    // atomicValue:value, 原子标签的值
    // atomicEl:null 原子标签(如果有就用其,没有就自己创建)
    //}
    get result() {
      return this._result_;
    },
    set result(value) {
      this._result_ = value
      dialogHasClose.call(_this, this)
    },
  };
  this.dialogConfig = {
    external: false,
    type: '',//vueRef vue的文档应用，html html片段，reactRef reactRef引用，selfFrame 默认弹框
    refEl: null,//弹框的文档引用
    conditions: [{//
      specialKey: '',
      char: '',//触发条件
      action: null,
      postBefore: null,//触发前的方法
      postAfter: null,//触发后的方法
      postAround: null,//触发环绕的方法
    }],//出发弹框的条件
    beforeAction: beforeAction,//弹出之前的操作
    closeAction: closeAction,//弹出关闭后的操作
  };
  this.curerntKeyConfig = {};
  this.curerntMouseConfig = {
    selection: this.selection
  };
  this.getSelectedStyleConfig = null; //如果由此函数就将当前选中的样式给她传出去
  this.deleteAtomic = null; // 删除原子标签通知
  this.currentSelectedStyle = {
    font: {},
    border: {},
    lineHeight: 0,
    background: {},
    textAlign: '',
    height: '',
    width: '',
    display: '',
    alignItems: '',
    justifyContent: '',
  };
  this.currentStyle = {};
  this.selection = null;
  this.currentRange = null;
  this.labelAttrMap = {
    'u': {key: 'underline', val: 'underline'},
    'i': {key: 'fontStyle', val: 'italic'},
    'b': {key: 'fontWeight', val: 'bolder'},
    'strike': {key: 'strikeThrough', val: 'line-through'},
    'face': {key: 'fontFamily'},
    'color': {key: 'color'},
    'font-size': {key: 'fontSize'}
  };
  this.fontDefaultVal = null;
  this.localCache = {//防止数据的双向绑定问题
    preStyleConfig: null,
    globalStyleConfig: null,
    padding: {textIndentVal: '', direct: ''}
  }
  if (prams) {
    init.call(this, prams);
  }
}

function init(prams) {
  if (prams) {
    const {fontDefaultVal, styleSwitch, deleteAtomic, dialogConfig, useExternalSettingBar, atomicAttribute, getSelectedStyleConfig, placeholder} = prams;
    if (useExternalSettingBar) this.useExternalSettingBar = useExternalSettingBar
    if (dialogConfig) configSpringFrame.call(this, dialogConfig);
    if (atomicAttribute) setAtomicAttribute.call(this, atomicAttribute);
    if (styleSwitch) this.styleSwitch = styleSwitch;
    if (getSelectedStyleConfig) this.getSelectedStyleConfig = getSelectedStyleConfig
    if (placeholder) this.placeholder = placeholder
    if (deleteAtomic) this.deleteAtomic = deleteAtomic
    if (fontDefaultVal) this.fontDefaultVal = fontDefaultVal

  }
  this.selection = getSelection()
  mountToEl.call(this);
  setOtherOpt.call(this);
}

function mountToEl() {
  let settingBar = null;
  if (!this.useExternalSettingBar) {
    settingBar = createSettingBar()
  }
  const contner = this.parentContainer

  contner.style.outline = 'none';
  if (!settingBar) {
    // editBox.style.height = '100%';
  } else {
    // editBox.style.height = 'calc(100% - 80px)';
    contner.appendChild(settingBar);
  }
  contner.setAttribute('placeholder', this.placeholder);
  contner.setAttribute('contenteditable', this.editable);
  contner.addEventListener('dbclick', textareaDBClick.bind(this), false);
  contner.addEventListener('click', textareaClick.bind(this), false);
  contner.addEventListener('mouseup', textareaMouseUp.bind(this), false);
  contner.addEventListener('mousedown', textareaMouseDown.bind(this), false);
  contner.addEventListener('keyup', textareaKeyUp.bind(this), false);
  contner.addEventListener('keydown', textareaKeyDown.bind(this), false);
  contner.addEventListener('blur', textareaKeyOnBlur.bind(this), false);
  contner.addEventListener('mouseleave', textareaKeyOnBlur.bind(this), false);
  this.editorBox = contner
}

function setOtherOpt(...prams) {

}

function textareaMouseUp(e) {
  if (!this.editable) return
  e.stopPropagation && e.stopPropagation();
  extendCollapse.call(this, this.selection);
  if (this.styleSwitch) {
    notifyStyleConfig.call(this);
  }

  this.currentRange = getCurrentRange.call(this).cloneRange();
  const mouseUp = this.mouseKeyEvents['mouseUp'];
  if (mouseUp) {
    mouseUp(e, {})
  }
}

function textareaMouseDown(e) {
  if (!this.editable) return
  e.stopPropagation && e.stopPropagation();
  const mouseDown = this.mouseKeyEvents['mouseDown'];
  if (mouseDown) {
    mouseDown(e, {})
  }
}

function insertNodeOrOtherOpt(e) {
  if (!this.editable) return
  e.stopPropagation && e.stopPropagation();
  const keyCode = e.keyCode;
  const result = checkOpenDialogBox.call(this, e);
  if (!result) {
    if (keyCode === KEY_ENTER) {
      if (checkTextAreaIsEmpty.call(this)) {
        createSpaceRow.call(this);
      } else if (currentRangeInAtomic.call(this).find) {
        const result = currentRangeInAtomic.call(this)
        const range = getCurrentRange.call(this);
        const preSibling = result.atomic.previousSibling;
        const span = document.createElement("span");
        span.innerHTML = '&nbsp;'
        if (preSibling === null || preSibling === undefined) {
          range.setStartAfter(result.atomic);
        }
        range.insertNode(span);
        range.selectNodeContents(span);
      }
    } else if (keyCode === KEY_BACKSPACE || keyCode === KEY_DELETE) {
      const isCollapsed = this.selection.isCollapsed;
      const focusOffset = this.selection.focusOffset;
      const anchorOffset = this.selection.anchorOffset;
      if (focusOffset === anchorOffset || isCollapsed) {
        //当前没有选择范围
        if (keyCode === KEY_BACKSPACE) {
          deleteAtomicTag.call(this);
        } else {
          //delete 删除
        }
      }
    } else if (!notContent.includes(keyCode)) {
      //输入文本
      if (checkTextAreaIsEmpty.call(this)) {
        createSpaceRow.call(this);
      } else {
        handleTextInput.call(this)
      }
    }
  }
}

function textareaKeyUp(e) {
  if (!this.editable) return
  e.stopPropagation && e.stopPropagation();
  const shiftKey = e.shiftKey;
  const keyCode = e.keyCode;

  if (shiftKey) {
    if (direction.includes(keyCode)) {
      // extendCollapse.call(this, this.selection);
      //这是选择文本；要将选择的 样式返回出去
      if (this.styleSwitch) {
        notifyStyleConfig.call(this);
      }
    }
  } else if (keyCode !== KEY_ENTER) {
    //输入文本
  }
  if (this.placeholder) {
    if (!contentIsNotEmpty.call(this)) {
      setTextAreaPlaceholder.call(this)
    }
  }
  this.currentRange = getCurrentRange.call(this).cloneRange()
  const keyUp = this.mouseKeyEvents['keyUp'];
  if (keyUp) {
    keyUp(e, {})
  }
}

function setTextAreaPlaceholder(placeholder) {
  if (!placeholder) placeholder = this.placeholder
  this.editorBox.setAttribute('placeholder', placeholder)
}

function textareaKeyOnBlur(e) {
  const onBlur = this.mouseKeyEvents['onBlur'];
  if (onBlur) {
    onBlur(e, {})
  }
}

function textareaKeyDown(e) {
  if (!this.editable) return
  e.stopPropagation && e.stopPropagation();
  insertNodeOrOtherOpt.call(this, e)
  const keyDown = this.mouseKeyEvents['keyDown'];
  if (keyDown) {
    keyDown(e, {})
  }
}

function setCurrentStyle(style) {

  if (this.editable) {
    if (this.useExternalSettingBar) {
      style = filterEmptyVal.call(this, style);
      recoverSelect.call(this)
      const selectRang = getSelectedHtml.call(this);
      if (selectRang) {
        doChangeFont.call(this, style);
      } else {
        // doChangeGlobalFont.call(this,style)
      }
    }
  }
}

function filterEmptyVal(obj) {
  if (!obj) return;
  let objClone = Array.isArray(obj) ? [] : {}
  if (obj && typeof obj === 'object') {
    for (let key in obj) {
      const val = obj[key];
      if (val && typeof val === 'object') {
        objClone[key] = filterEmptyVal.call(this, val)
      } else if (val) {
        objClone[key] = val
      }
    }
  }
  return objClone
}

function changeFontStyle(newValue, scope) {
  if (!newValue) return
  const fontStyleOld = (scope === 'global') ? this.localCache.globalStyleConfig : this.localCache.preStyleConfig.fontItem;
  const keys = Object.keys(newValue);
  if (newValue && fontStyleOld) {
    keys.forEach(key => {
      const value = newValue[key];
      const valueOld = fontStyleOld[key];
      changeFont.call(this, key, value, value !== valueOld);
    })
  } else {

    keys.forEach(key => {
      const value = newValue[key];
      changeFont.call(this, key, value, true);
    })
  }
}

function checkAndSetStyleDefault(newValue, scope) {
  if (scope === 'global') {
    setGlobalStyleDefault.call(this, newValue)
  } else {
    setStyleDefault.call(this, newValue)
  }
}

function doChangeFont(newValue) {
  changeFontStyle.call(this, newValue);
  checkAndSetStyleDefault.call(this, newValue)
}

function changeFont(key, value, condition) {
  if (condition) {

    const command = this.commandMap[key];
    if (command) {
      setFontChange.call(this, command, value)
    }
  }
}

function setFontChange(type, param) {
  if ('fontSize' === type) {
    execFontSize.call(this, param)
  } else if ('foreColor' === type) {

    this.selection.removeAllRanges();
    this.selection.addRange(this.currentRange);
    document.execCommand(type, true, param);
  } else {
    document.execCommand(type, true, param);
  }
  this.currentRange = getCurrentRange.call(this).cloneRange();
  let el = document.getElementById(this.textareaId);
  if (el) {
    this.propertyData.innerHtml = el.innerHTML
  }
}

function execFontSize(size, unit) {
  const documentFragment = getDocumentFragment.call(this);
  const len = documentFragment.childNodes.length;
  const fontSize = size + (unit ? unit : 'px');
  const selectHtml = getSelectedHtml.call(this, documentFragment)
  let div = document.createElement('div');
  div.innerHTML = selectHtml;
  setFontSize.call(this, div, fontSize);
  if (len <= 1) {
    const range = getCurrentRange.call(this);
    range.deleteContents();
    const child = div.firstChild;
    range.insertNode(child);
    range.selectNodeContents(child);
    this.currentRange = range.cloneRange();
    this.selection.removeAllRanges();
    this.selection.addRange(this.currentRange);
    this.selection.selectAllChildren(child)
  } else {
    let spanString = '';

    spanString += div.innerHTML;
    if (spanString) {
      document.execCommand('insertHTML', false, spanString);
    }
  }

}

function setFontSize(el, fontSize) {
  if (!el || !fontSize || el.nodeType !== 1) return;
  el.style.fontSize = fontSize;
  let childNodes = el.childNodes
  for (let i = 0; i < childNodes.length; i++) {
    const child = childNodes[i];
    setFontSize.call(this, child, fontSize);
  }
}

function formatDialogConfig4Condition() {
  const conditions = this.dialogConfig.conditions;
  conditions.forEach(condition => {
    const specialKey = condition.specialKey;
    const char = condition.char;
    if (specialKey !== undefined) {
      switch (specialKey) {
        case "ctrl":
          condition.specialKey = 'ctrlKey'
          break
        case "alt":
          condition.specialKey = 'altKey'
          break
        case "shift":
        default:
          condition.specialKey = 'shiftKey'
          break
      }
    }
    if (char) {
      switch (char) {
        case "!":
          condition.keyCode = 49;
          break
        case "@":
          condition.keyCode = 50;
          break;
        case "#":
          condition.keyCode = 51;
          break
        case "$":
          condition.keyCode = 52;
          break
        case "%":
          condition.keyCode = 53;
          break
        case "^":
          condition.keyCode = 54;
          break
        case "&":
          condition.keyCode = 55;
          break
        case "*":
          condition.keyCode = 56;
          break
      }
    }
  })
}

function createAtomicTag(atomicItem) {
  const atomicAttribute = this.atomicAttribute;
  const atomicAttributeKeys = Object.keys(atomicItem);
  let tagName = this.atomicAttribute['tag'];
  if (!tagName) {
    tagName = 'span';
  }

  const tag = document.createElement(tagName);
  tag.style.color = 'red';
  const atomicFlag = atomicAttribute.atomicFlag
  const filterAttributes = ['tag', 'replaceFlag', 'atomicFlag', 'atomicValue'];
  tag.setAttribute(atomicFlag.key, atomicFlag.val);
  atomicAttributeKeys.forEach(attribute => {
    if (filterAttributes.includes(attribute)) return;
    const val = atomicItem[attribute];
    if (val !== undefined) {
      tag.setAttribute(attribute, val)
    }
  })
  if (atomicItem.atomicValue) {
    tag.innerText = atomicItem.atomicValue;
  }
  return tag;
}

function getCurrentRange() {
  if (!this.selection) {
    this.selection = getSelection();
  }
  const selection = this.selection
  if (selection.rangeCount > 0)
    if (selection.createRange) {
      return selection.createRange();
    }
  if (selection.rangeCount > 0) {
    return selection.getRangeAt(0)
  }
  return null;
}

function createSettingBar() {
  const settingBox = document.createElement('div');
  settingBox.appendChild(document.createTextNode("asdfjkllasdkfjlaskdfjlaksjdlakjsdl"))
  return settingBox;
}

function closeAction(cb) {
  if (cb) {
    const result = {
      keyEvent: this.curerntKeyConfig,
      mouseEvent: this.curerntMouseConfig
    };
    cb(result);
  }
}

function beforeAction(cb) {
  if (cb) {
    const result = {
      keyEvent: this.curerntKeyConfig,
      mouseEvent: this.curerntMouseConfig
    };
    cb(result);
  }
}

function setAtomicAttribute(atomicAttribute) {
  const attributes = Object.keys(atomicAttribute)
  attributes.forEach(attribute => {
    const val = atomicAttribute[attribute];
    if (val !== undefined) {
      this.atomicAttribute[attribute] = val
    }

  })
}

function mountEvent(events = {}) {
  const eventKeys = Object.keys(this.mouseKeyEvents);
  eventKeys.forEach(key => {
    this.mouseKeyEvents[key] = events[key]
  })
}

function setRecoverHtml(recoverData = {}) {
  this.recover = {html: recoverData.recoverHtml, recoverCondition: recoverData.recoverCondition}
  if (this.editorBox) {
    recoverContent.call(this, recoverData.recoverHtml)
  } else {
    console.log('请先初始化！')
  }
}

function recoverContent(html) {
  this.editorBox.innerHTML = html
}

function configSpringFrame(dialogConfig = {}) {
  const selfFrameKeys = Object.keys(this.dialogConfig);
  const frameKeys = Object.keys(dialogConfig);
  frameKeys.forEach(key => {
    if (selfFrameKeys.includes(key)) {
      this.dialogConfig[key] = dialogConfig[key]
    }
  })
  formatDialogConfig4Condition.call(this);
  this.dialogConfig.external = true;
}

function getSelection() {
  if (window.getSelection) {
    return window.getSelection();
  } else if (document.selection) {
    return document.selection.createRange();
  }
}

function checkOpenDialogBox(event) {
  const conditions = this.dialogConfig.conditions;
  const keyCode = event.keyCode;
  const code = event.code;
  for (let i = 0; i < conditions.length; i++) {
    const condition = conditions[i];
    if (event[condition.specialKey] && (keyCode === condition.keyCode || code === condition.sGCNCode)) {
      if (condition.action) {
        this.closeAfterData.condition = condition;
        condition.action(this.closeAfterData);
        return true;
      } else {
        console.log(`没有找到dialogConfig.conditions[${i}].action方法,无法打开弹框！！！！`);
      }
      break;
    }

  }

}

function dialogHasClose(data) {
  const result = data.result;
  if (result !== undefined && result !== null) {
    let atomicTag = null;
    if (result instanceof Object && result.atomicEl) {
      atomicTag = result.atomicEl
    } else {
      atomicTag = createAtomicTag.call(this, result.atomicItem);
    }
    this.selection.removeAllRanges();
    this.selection.addRange(this.currentRange)
    let range = getCurrentRange.call(this);
    range.insertNode(atomicTag);
    range.setEnd(atomicTag, 1);
    const text = range.startContainer.data;
    const len = text.length;
    const repLen = data.condition.char.length;
    range.startContainer.data = text.substr(0, len - repLen);
    this.selection.collapseToEnd();
  }
}

function getCurrentRowPadding() {
  let start = this.selection.focusNode;
  if (this.selectConfig.selectDirection.startsWith('backend-')) {
    start = this.selection.anchorNode;
  }
  const startRow = findTheLatelyAncestorRow.call(this, start, this.rowTag, this.editable);
  if (!startRow) return
  const left = startRow.style['text-indent'];
  if (left) {
    this.localCache.padding.textIndentVal = left
    this.localCache.padding.direct = 'left'
  }
}

function notifyStyleConfig() {
  getCurrentRowPadding.call(this)
  getSelectRangeFontSameConfig.call(this, this.selection);
  let style = this.currentSelectedStyle
  if (isEmpty.call(this, style)) return;
  style = deepCopy(style);
  const padding = this.localCache.padding
  if (padding) {
    style.textIndent = padding.textIndentVal
    style.textIndentDirect = padding.direct
  }

  this.localCache.preStyleConfig = style
  if (this.getSelectedStyleConfig) {
    this.getSelectedStyleConfig(deepCopy(style));
  } else {
    console.log('style---通知自身携带的设置按钮回显', style)
    //todo  通知自身携带的设置按钮回显
  }
}

function findTheLatelyAncestorRow(el, ancestorRowTag, editor) {
  if (!el || el === editor) return;
  const tagName = el?.tagName?.toLowerCase();
  if (tagName === ancestorRowTag) {
    return el;
  } else {
    const ancestorTag = findTheLatelyAncestorRow.call(this, el.parentNode, ancestorRowTag, editor);
    if (ancestorTag) return ancestorTag;
  }
}

function getNextSiblings(el) {
  const siblings = []
  let sibling = el.nextSibling
  while (sibling) {
    siblings.push(sibling)
    sibling = sibling.nextSibling
  }
  return siblings
}

function isEmpty(obj) {
  if (obj === undefined) return true
  if (obj === null) return true
  if (typeof obj === 'object') {
    return Object.keys(obj).length <= 0
  } else {
    return obj === undefined || obj === null || obj.length <= 0
  }
}

function getSelectRangeFontSameConfig(selection) {
  const editorBox = this.editorBox;
  let anchorNode = selection.anchorNode;
  let focusNode = selection.focusNode;
  const selectDirection = this.selectConfig.selectDirection
  if ((!selectDirection.startsWith('forward')) && selectDirection === 'backend-') {
    focusNode = selection.anchorNode;
    anchorNode = selection.focusNode;
  }
  const startN = anchorNode.nodeType !== 1 ? anchorNode.parentNode : anchorNode;
  if (startN === editorBox) return
  let result = [];
  findStyle.call(this, anchorNode, focusNode, 'start', result, {});

  setFontData.call(this, findSameConfig.call(this, result));
}

function findTheElStyle(el, style) {
  if (!el) return {};
  if (!style) style = {};
  if (el.nodeType === 1)
    assembleStyleConfig.call(this, el, style);
  if (el?.tagName?.toLowerCase() === this.rowTag) return style
  const data = findTheElStyle.call(this, el.parentNode, style)
  if (data) return data;
}

function handleNextSibling(el, end, allStyles, commonStyle) {
  const siblings = getNextSiblings.call(this, el);
  if (siblings && siblings.length) {
    const len = siblings.length
    for (let i = 0; i < len; i++) {
      const result = findStyle.call(this, siblings[i], end, 'sibling', allStyles, commonStyle);
      if (result) return result
    }
  }
}

function handleChildNodes(el, end, allStyles, commonStyle) {
  const children = el.childNodes;
  const len = children.length;
  for (let i = 0; i < len; i++) {
    const result = findStyle.call(this, children[i], end, 'child', allStyles, commonStyle);
    if (result) return result
  }
}

function findPrentSibling(el) {
  if (el === this.editorBox || el.parentNode === this.editorBox) return -1;
  const parent = el.parentNode.nextSibling;
  if (parent) return parent;
  else {
    const result = findPrentSibling.call(this, el.parentNode);
    if (result) return result;
  }
}

function findStyle(start, end, direction, allStyles = [], commonStyle = {}) {
  let shouldEnd = void 0;
  if (start === end) {
    allStyles.push(findTheElStyle.call(this, start.parentNode));
    return 1;
  }
  if (start.nodeType !== 1 || (start.nodeType == 1 && direction === 'start')) {
    allStyles.push(findTheElStyle.call(this, start));
    shouldEnd = handleNextSibling.call(this, start, end, allStyles, commonStyle);
    if (shouldEnd) return shouldEnd;
    shouldEnd = handleChildNodes.call(this, start, end, allStyles, commonStyle);
    if (shouldEnd) return shouldEnd;
  } else {
    shouldEnd = handleChildNodes.call(this, start, end, allStyles, commonStyle);
    if (shouldEnd) return shouldEnd;
  }
  if (direction === 'start') {
    const parent = findPrentSibling.call(this, start);
    if (parent !== -1) {
      shouldEnd = findStyle.call(this, parent, end, direction, allStyles, commonStyle);
      if (shouldEnd) return shouldEnd
    } else return -1
  }
}

function getSelectedHtml(document) {
  const selection = this.selection;
  if (selection.focusNode === selection.anchorNode) {
    const start = selection.focusNode
    if (!start) return;
    const el = findAtomicEl(start.nodeType === 1 ? start : start.parentNode, this.editorBox, this.atomicAttribute.atomicFlag);
    if (el) {
      const div = window.document.createElement('div');
      div.appendChild(el.cloneNode(true));
      return div.innerHTML;
    }
  }
  let selectedHtml = "";
  try {
    let documentFragment = document ? document : getDocumentFragment.call(this);
    for (let i = 0; i < documentFragment.childNodes.length; i++) {
      const childNode = documentFragment.childNodes[i];
      if (childNode.nodeType === 3) {  // Text 节点
        selectedHtml += '<span>' + childNode.nodeValue + '</span>';
      } else {
        selectedHtml += childNode.outerHTML;
      }
    }
  } catch (err) {
  }
  return selectedHtml;
}

function setFontData(fontItem) {
  const fontConfig = {fontItem: fontItem}
  setFontDefaultValSelf.call(this, fontConfig);
  this.currentSelectedStyle = fontConfig;
}

function setFontDefaultValSelf(fontConfig) {
  const itemKeys = Object.keys(fontConfig.fontItem);
  if (itemKeys.length === 0) {
    fontConfig.fontItem = deepCopy(this.fontDefaultVal)
  } else {
    const fontKeys = Object.keys(this.fontDefaultVal);
    fontKeys.forEach(key => {

      if (!fontConfig.fontItem[key] || fontConfig.fontItem[key] === undefined) {
        fontConfig.fontItem[key] = this.fontDefaultVal[key];
      }
    })
  }
}

function findSameConfig(arr = []) {
  if (!arr.length) return {};
  let result = arr[0];
  let common = {}
  arr.forEach(item => {
    const keys = Object.keys(item);
    keys.forEach(key => {
      const valR = result[key];
      const itVal = item[key];
      if (itVal === valR) {
        if (common[key] === undefined) common[key] = itVal
      }
    })
    result = common;
    common = {}
  })
  return result;
}

function assembleStyleConfig(el, result = {}) {
  const style = el.style;
  const attrs = el.getAttributeNames();
  const tagNameB = el.tagName.toLowerCase();
  const keyItem = this.labelAttrMap[tagNameB];
  if (keyItem) {
    if (!result[keyItem.key]) {
      result[keyItem.key] = keyItem.val;
    }
  }
  attrs.forEach(item => {
    const attrKey = this.labelAttrMap[item];
    if (attrKey) {
      if (!result[attrKey.key]) {
        const val = attrKey.val;
        if (!val) {
          result[attrKey.key] = el.getAttribute(item)
        } else {
          result[attrKey.key] = attrKey.val;
        }
      }

    }
  })
  for (let i = 0; ; i++) {
    const keyStyle = style[i];
    if (keyStyle) {
      const resultKey = this.labelAttrMap[keyStyle];
      if (resultKey) {
        const itemKey = result[resultKey.key];
        if (!itemKey) {
          let val = style[keyStyle];
          if (resultKey.key === 'fontSize') {
            val = parseInt(val?.replaceAll('px', ''))
          }
          result[resultKey.key] = val;
        }

      }
    } else break
  }
}

function checkTextAreaIsEmpty() {
  const innerHtml = this.editorBox.innerHTML;
  return !!!innerHtml;
}

function createSpaceRow() {
  const range = getCurrentRange.call(this);
  this.selection.collapseToEnd();
  const row = document.createElement(this.rowTag);
  row.style.fontSize = '14px'
  row.style.color = '#000000'
  row.style.fontFamily = 'aliPuHui Regular'
  row.setAttribute('face', 'aliPuHui Regular')
  row.innerHTML = '<br/>'
  range.insertNode(row);
  range.selectNodeContents(row)
}

function handleTextInput() {
  const result = currentRangeInAtomic.call(this);
  if (result.find) {
    moveCursorToOutAndEnd.call(this, result.atomic)
  }
}

function moveCursorToOutAndEnd(atomic) {
  const anchorOffset = this.selection.anchorOffset
  const focusOffset = this.selection.focusOffset;
  let range = getCurrentRange.call(this);
  if (anchorOffset === focusOffset && anchorOffset === 0) {
    const previousSibling = atomic.previousSibling;
    const span = document.createElement('span');
    range.setStartBefore(atomic);
    if (previousSibling?.nodeType !== 1) {
      span.innerHTML = '&nbsp;';
      range.insertNode(span);
      range.selectNodeContents(span);
    } else {
      //当sibling 的sibling也是一个原子标签的时候有问题
      range.selectNodeContents(previousSibling);
      range.setStart(previousSibling, 1);
      range.selectNodeContents(span);
    }
  } else {
    const nextSibling = atomic.nextSibling;
    const span = document.createElement('span');
    if (!nextSibling) {
      span.innerHTML = '&nbsp;';
      atomic.parentNode.append(span);
      this.selection.selectAllChildren(span);
      this.currentRange.setStart(span, 1);
      this.currentRange.setEnd(span, 1);
      range.selectNodeContents(span);
    } else {

      range.setStartAfter(atomic);
      if (nextSibling.nodeType !== 1) {
        span.innerHTML = nextSibling.data ? nextSibling.data : '&nbsp;';
        range.insertNode(span);
        range.selectNodeContents(span);
      } else {
        //当sibling 的sibling也是一个原子标签的时候有问题
        span.innerHTML = nextSibling.data ? nextSibling.data : '&nbsp;';
        range.insertNode(span);
        range.setStart(span, 1);
        range.selectNodeContents(span);
      }
    }
  }
}

function currentRangeInAtomic() {
  const node = this.selection.anchorNode;
  const atomicFlag = this.atomicAttribute.atomicFlag;
  const atomic = findAtomicEl.call(this, node.nodeType !== 1 ? node.parentNode : node, this.editorBox, atomicFlag);
  return {find: atomic !== undefined, atomic: atomic}
}

function findAtomicEl(el, editorBox, flag) {
  if (!el || el.nodeType !== 1 || !editorBox) return;
  const value = el.getAttribute(flag.key);
  if (value && value === flag.val) {
    return el;
  }
  let parent = el.parentNode;
  if (editorBox === el || parent === editorBox) {
    return;
  }
  if (parent) {
    const result = findAtomicEl(parent, editorBox, flag);
    if (result) return result;
  }
}

function deleteAtomicTag() {
  const result = currentRangeInAtomic.call(this)
  if (result.atomic) {
    const preSibling = result.atomic.previousSibling;
    if (preSibling) {
      this.selection.selectAllChildren(result.atomic);
      if (this.deleteAtomic) {
        this.deleteAtomic(result.atomic)
      }
    } else {
      const range = getCurrentRange.call(this);
      const span = document.createElement('span');
      range.setStartBefore(result.atomic)
      range.insertNode(span);
      range.selectNodeContents(span);
    }

  }
}

function textareaClick(e) {
  if (!this.editable) return
  e.stopPropagation && e.stopPropagation();
  const dbClick = this.mouseKeyEvents['dbClick'];
  if (dbClick) {
    dbClick(e, {})
  }
}

function textareaDBClick(e) {
  if (!this.editable) return;
  e.stopPropagation && e.stopPropagation();
  const click = this.mouseKeyEvents['click'];
  if (click) {
    click(e, {})
  }
}

function getInnerHtml() {
  return this.editorBox.innerHTML
}

function setEditAble(param) {

  this.editable = param;
  this.editorBox.setAttribute('contenteditable', param);
  if (param) {
    this.selection.collapse(this.editorBox, 0);
    this.currentRange = getCurrentRange.call(this).cloneRange()
  }
}

function extendCollapse(selection) {

  if (!selection) return;
  const forward = getDirection4Forward.call(this, selection);
  let direction = forward ? 'forward-' : 'backend-';
  const startN = selection.anchorNode.parentElement;
  const flag = this.atomicAttribute.atomicFlag;

  let elStart = findAtomicEl.call(this, startN, this.editorBox, flag);
  const valueStart = elStart ? elStart.getAttribute(flag.key) : '';
  const containsAtomicStart = valueStart && valueStart === 'true';
  const endN = selection.focusNode.parentElement;
  let elEnd = findAtomicEl.call(this, endN, this.editorBox, flag);
  const valueEnd = elEnd ? elEnd.getAttribute(flag.key) : '';
  const containsAtomicEnd = valueEnd && valueEnd === 'true';
  const origin = selection.anchorOffset === selection.focusOffset && startN === endN;
  let active = false;
  let firstN, endNode;
  this.selectConfig.currentAtomicEl = null;
  if (origin && containsAtomicStart && containsAtomicEnd) {
    active = true;
    firstN = elStart;
  } else {
    if (containsAtomicStart && containsAtomicEnd) {
      //这种情况时选中了一个原子标签，或者只选中了原子标签的子标签；
      this.selectConfig.currentAtomicEl = elStart
      direction += 'start-end';
      if (forward) {
        firstN = elStart ? elStart : startN;
        endNode = elEnd ? elEnd : endN;
      } else {
        firstN = elEnd ? elEnd : endN;
        endNode = elStart ? elStart : startN;
      }
      active = true;
    } else if (containsAtomicStart) {
      direction += 'start';
      firstN = elStart ? elStart : startN;
      active = true;
    } else if (containsAtomicEnd) {
      direction += 'end';
      firstN = elEnd ? elEnd : endN;
      active = true;
    }
  }

  this.selectConfig.selectDirection = direction
  if (active) {
    const newRange = getCurrentRange.call(this).cloneRange();
    setSelectArea.call(this, newRange, direction, firstN, endNode);
    this.currentRange = newRange;
    this.selection.removeAllRanges();
    this.selection.addRange(newRange);
  }
}

function setSelectArea(range, direction, firstNode, endNode) {
  switch (direction) {
    case 'forward-start':
      range.setStart(firstNode, 0);
      break;
    case 'forward-end':
      range.setEnd(firstNode, 1);
      break;
    case 'backend-start':
      range.setEnd(firstNode, 1);
      break;
    case 'backend-end':
      range.setStart(firstNode, 0);
      break;
    case 'forward-start-end':
    case 'backend-start-end':
      range.setStart(firstNode, 0);
      range.setEnd(endNode, 1);
      break
    default:
      range.selectNodeContents(firstNode);
      break
  }
}

function getDirection4Forward(selection) {
  const startN = selection.anchorNode.parentElement;
  const endN = selection.focusNode.parentElement;

  if (startN === endN) {
    return selection.anchorOffset < selection.focusOffset
  } else {
    const range = getCurrentRange.call(this);
    const result = {
      wholeText: selection.toString(),
      startOffset: range.startOffset,
      endOffset: range.endOffset,
      startHadFind: false,
      endHadFind: false,
      forward: false,
      done: false,
      s: startN,
      e: endN
    }
    findDirection.call(this, this.editorBox, result);
    return result.forward;
  }
}

function findDirection(rootEl, result) {
  if (rootEl) {

    let childList = rootEl.childNodes;
    for (let i = 0; i < childList.length; i++) {
      let child = childList[i];
      if (!result.done) {
        if (result.s === child) {
          let pre = sameLine.call(this, result);
          if (pre !== undefined) {
            result.forward = result.wholeText.startsWith(pre);
          } else {
            result.forward = true;
          }
          result.done = true;
        }
        if (result.e === child) {
          //这里倒换一下，因为开始的光标是结束光标标签的子标签
          let resultI = {s: result.e, e: result.s}
          let pre = sameLine.call(this, resultI);
          if (pre) {
            result.forward = result.wholeText.endsWith(pre);
          }
          result.done = true;
        }
        if (child.childNodes.length > 0) {
          findDirection(child, result);
        }
      } else return;
    }
  }
}

function sameLine(result) {
  const containChild = findChild.call(this, result.s, result.e);
  let startValue = '';
  if (containChild === 1) {
    let documentFragment = getDocumentFragment();
    for (let j = 0; j < documentFragment.childNodes.length; j++) {
      let childNode = documentFragment.childNodes[j];
      if (childNode.nodeType === 3) {
        if (startValue.length > 0) break;
        else
          startValue += childNode.nodeValue;
      }
    }
  }
  return startValue;
}

function findChild(root, child) {
  if (root) {
    if (root === child) return 1;
    const children = root.childNodes;
    for (let i = 0; i < children.length; i++) {
      const childItem = children[i];
      if (childItem === child) {
        return 1;
      }
      if (childItem.childNodes.length > 0) {
        const result = findChild(childItem, child);
        if (result === 1) {
          return result;
        }
      }
    }
  }
}

function getDocumentFragment() {
  if (window.getSelection) {
    const sel = window.getSelection();
    if (sel && sel.rangeCount > 0) {
      return sel.getRangeAt(0).cloneContents();
    }
  } else if (document.selection) {
    return document.selection.createRange().HtmlText;
  }
}

function recoverSelect() {

  if (this.editable) {
    this.selection.removeAllRanges();
    this.selection.addRange(this.currentRange);
    this.currentRange = getCurrentRange.call(this).cloneRange();
  }
}

function contentIsNotEmpty() {
  const content = this.editorBox.innerHTML
  if (content !== undefined && content !== null && content.length > 0) return content + ''
  else return false;
}

function setOtherStyle(config = {}) {

}

function setStyleDefault(style) {
  this.localCache.preStyleConfig.fontItem = deepCopy(style);
}

function setGlobalStyleDefault(style) {
  this.localCache.globalStyleConfig = deepCopy(style)
}

function deepCopy(obj) {
  if (!obj) return;
  let objClone = Array.isArray(obj) ? [] : {}
  if (obj && typeof obj === 'object') {
    for (let key in obj) {
      const val = obj[key];
      if (val && typeof val === 'object') {
        objClone[key] = deepCopy(val)
      } else {
        objClone[key] = val
      }
    }
  }
  return objClone
}

function setTransparent() {
  this.editorBox.style.backgroundColor = 'transparent'
}

function textIndent(param) {
  const {ancestorRow, direct, val} = param
  switch (direct) {
    case 'left':
      let textIndent = parseInt(ancestorRow.style.textIndent)
      textIndent = isNaN(textIndent) ? 0 : textIndent + parseInt(val)
      ancestorRow.style.textIndent = textIndent < 0 ? '0rem' : textIndent + 'rem'
      break
    case 'right':
      ancestorRow.style.textIndent = val
      break
  }
}

function changeCurrentRowPadding(data) {
  textIndent.call(this, {...data})
}

function changeGlobalRowPadding(data) {
  const {startRow, endRow} = data;
  const children = this.editorBox.childNodes;
  let startIndex = 0;
  let endIndex = 0;
  const len = children.length;
  for (let i = 0; i < len; i++) {
    const child = children[i];
    if (child === startRow) {
      startIndex = i
    } else if (child === endRow) {
      endIndex = i;
      break
    }
  }
  for (let i = startIndex; i <= endIndex; i++) {
    const child = children[i];
    textIndent.call(this, {...data, ancestorRow: child})
  }
}

function checkAncestorRowInEditor(ancestorRow) {
  const children = this.editorBox.childNodes
  const len = children.length;
  for (let i = 0; i < len; i++) {
    if (ancestorRow === children[i]) return true
  }
  return false
}

function setTextIndent(textIndentVal, direction) {
  if (this.editable) {
    if (this.useExternalSettingBar) {
      const paddingConfig = this.localCache.padding;
      if (paddingConfig.textIndentVal !== textIndentVal && (!paddingConfig.direct || paddingConfig.direct === direction)) {
        const startN = this.selection.anchorNode.parentElement;
        const ancestorRow = findTheLatelyAncestorRow.call(this, startN, this.rowTag, this.editorBox);
        if (!checkAncestorRowInEditor.call(this, ancestorRow)) return
        paddingConfig.textIndentVal = textIndentVal
        paddingConfig.direct = direction;
        const endN = this.selection.focusNode.parentElement;
        const ancestorRowEnd = findTheLatelyAncestorRow.call(this, endN, this.rowTag, this.editorBox);
        const param = {val: textIndentVal, direct: direction};
        if (ancestorRow === ancestorRowEnd) {
          param.ancestorRow = ancestorRow;
          changeCurrentRowPadding.call(this, param);
        } else {
          param.startRow = ancestorRow;
          param.endRow = ancestorRowEnd;
          changeGlobalRowPadding.call(this, param)
        }
        recoverSelect.call(this)
      }
    }
  }
}

RichText.prototype.init = init;
RichText.prototype.setCurrentStyle = setCurrentStyle;
RichText.prototype.setStyleDefault = setStyleDefault;
RichText.prototype.recoverSelect = recoverSelect;
RichText.prototype.setEditAble = setEditAble;
RichText.prototype.setRecoverHtml = setRecoverHtml;
RichText.prototype.mountEvent = mountEvent;
RichText.prototype.getInnerHtml = getInnerHtml;
RichText.prototype.contentIsNotEmpty = contentIsNotEmpty;
RichText.prototype.getSelectedHtml = getSelectedHtml;
RichText.prototype.setTextIndent = setTextIndent;
RichText.prototype.setOtherStyle = setOtherStyle;
RichText.prototype.setTransparent = setTransparent;

export default RichText
