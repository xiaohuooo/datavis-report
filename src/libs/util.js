import Cookies from 'js-cookie'
import config from '@/config'
import {forEach, hasOneOf, objEqual} from '@/libs/tools'
import _ from 'lodash'
import dictionaryData from '@/config/DimensionSortDictionary'

const {title, cookieExpires, useI18n} = config

export const TOKEN_KEY = 'token'

export const setToken = (token) => {
  Cookies.set(TOKEN_KEY, token, {expires: cookieExpires || 1})
}
export const getToken = () => {
  const token = Cookies.get(TOKEN_KEY)
  if (token) return token
  else return false
}

export const hasChild = (item) => {
  return item.children && item.children.length !== 0
}

const showThisMenuEle = (item, access) => {
  return _.find(access, {
    path: item.path
  })
}
/**
 * 获取本地路由中授过权的菜单
 * @param authenticateRoutes 所有需要授权才能访问的路由
 * @param access 该用户能访问的菜单
 * @returns {Array}
 */
export const getAccessibleMenu = (publicRoutes, authenticateRoutes, access) => {
  let publicMenus = recursiveMenu(true, publicRoutes);
  let allowMenus = recursiveMenu(false, authenticateRoutes, access);
  return publicMenus.concat(allowMenus);
}

export const recursiveMenu = (directAdd, routes, access) => {
  let res = []
  forEach(routes, item => {
    if (!item.meta || !item.meta.hideInMenu) {
      if (directAdd || showThisMenuEle(item, access)) {
        let obj = {
          icon: item.meta.icon || '',
          name: item.name,
          meta: item.meta
        }
        if (hasChild(item)) {
          obj.children = recursiveMenu(directAdd, item.children, access)
        }
        res.push(obj)
      }
    }
  })
  return res
}

export const getAccessibleRoute = (authenticateRoutes, access) => {
  let res = []
  forEach(authenticateRoutes, item => {
    let currentItem = showThisMenuEle(item, access)
    if (currentItem) {
      let obj = {
        path: item.path,
        name: item.name,
        component: item.component,
        meta: {
          icon: item.meta.icon,
          title: item.meta.title
        }
      }
      if (hasChild(item)) {
        obj.children = getAccessibleRoute(item.children, access)
      }
      res.push(obj)
    }
  })
  return res
}


export const getRouteTitleHandled = (route) => {
  let router = {...route}
  let meta = {...route.meta}
  let title = ''
  if (meta.title) {
    if (typeof meta.title === 'function') {
      meta.__titleIsFunction__ = true
      title = meta.title(router)
    } else title = meta.title
  }
  meta.title = title
  router.meta = meta
  return router
}

export const showTitle = (item, vm) => {
  return (item.meta && item.meta.title) || item.name
}

/**
 * @description 本地存储和获取标签导航列表
 */
export const setTagNavListInLocalstorage = list => {
  localStorage.tagNaveList = JSON.stringify(list)
}
/**
 * @returns {Array} 其中的每个元素只包含路由原信息中的name, path, meta三项
 */
export const getTagNavListFromLocalstorage = () => {
  const list = localStorage.tagNaveList
  return list ? JSON.parse(list) : []
}

/**
 * @param {Array} routers 路由列表数组
 * @description 用于找到路由列表中name为home的对象
 */
export const getHomeRoute = (routers, homeName = 'home') => {
  let i = -1
  let len = routers.length
  let homeRoute = {}
  while (++i < len) {
    let item = routers[i]
    if (item.children && item.children.length) {
      let res = getHomeRoute(item.children, homeName)
      if (res.name) return res
    } else {
      if (item.name === homeName) homeRoute = item
    }
  }
  return homeRoute
}

/**
 * @param {*} list 现有标签导航列表
 * @param {*} newRoute 新添加的路由原信息对象
 * @description 如果该newRoute已经存在则不再添加
 */
export const getNewTagList = (list, newRoute) => {
  const {name, path, meta} = newRoute
  let newList = [...list]
  if (newList.findIndex(item => item.name === name) >= 0) return newList
  else newList.push({name, path, meta})
  return newList
}

/**
 * @param {*} access 用户权限数组，如 ['super_admin', 'admin']
 * @param {*} route 路由列表
 */
const hasAccess = (access, route) => {
  if (route.meta && route.meta.access) return hasOneOf(access, route.meta.access)
  else return true
}

/**
 * 权鉴
 * @param {*} name 即将跳转的路由name
 * @param {*} access 用户权限数组
 * @param {*} routes 路由列表
 * @description 用户是否可跳转到该页
 */
export const canTurnTo = (name, access, routes) => {
  const routePermissionJudge = (list) => {
    return list.some(item => {
      if (item.children && item.children.length) {
        return routePermissionJudge(item.children)
      } else if (item.name === name) {
        return hasAccess(access, item)
      }
    })
  }

  return routePermissionJudge(routes)
}

/**
 * @param {String} url
 * @description 从URL中解析参数
 */
export const getParams = url => {
  const keyValueArr = url.split('?')[1].split('&')
  let paramObj = {}
  keyValueArr.forEach(item => {
    const keyValue = item.split('=')
    paramObj[keyValue[0]] = keyValue[1]
  })
  return paramObj
}

/**
 * @param {Array} list 标签列表
 * @param {String} name 当前关闭的标签的name
 */
export const getNextRoute = (list, route) => {
  let res = {}
  if (list.length === 2) {
    res = getHomeRoute(list)
  } else {
    const index = list.findIndex(item => routeEqual(item, route))
    if (index === list.length - 1) res = list[list.length - 2]
    else res = list[index + 1]
  }
  return res
}

/**
 * @param {Number} times 回调函数需要执行的次数
 * @param {Function} callback 回调函数
 */
export const doCustomTimes = (times, callback) => {
  let i = -1
  while (++i < times) {
    callback(i)
  }
}

/**
 * @param {Object} file 从上传组件得到的文件对象
 * @returns {Promise} resolve参数是解析后的二维数组
 * @description 从Csv文件中解析出表格，解析成二维数组
 */
export const getArrayFromFile = (file) => {
  let nameSplit = file.name.split('.')
  let format = nameSplit[nameSplit.length - 1]
  return new Promise((resolve, reject) => {
    let reader = new FileReader()
    reader.readAsText(file) // 以文本格式读取
    let arr = []
    reader.onload = function (evt) {
      let data = evt.target.result // 读到的数据
      let pasteData = data.trim()
      arr = pasteData.split((/[\n\u0085\u2028\u2029]|\r\n?/g)).map(row => {
        return row.split('\t')
      }).map(item => {
        return item[0].split(',')
      })
      if (format === 'csv') resolve(arr)
      else reject(new Error('[Format Error]:你上传的不是Csv文件'))
    }
  })
}

/**
 * @param {Array} array 表格数据二维数组
 * @returns {Object} { columns, tableData }
 * @description 从二维数组中获取表头和表格数据，将第一行作为表头，用于在iView的表格中展示数据
 */
export const getTableDataFromArray = (array) => {
  let columns = []
  let tableData = []
  if (array.length > 1) {
    let titles = array.shift()
    columns = titles.map(item => {
      return {
        title: item,
        key: item
      }
    })
    tableData = array.map(item => {
      let res = {}
      item.forEach((col, i) => {
        res[titles[i]] = col
      })
      return res
    })
  }
  return {
    columns,
    tableData
  }
}

export const findNodeUpper = (ele, tag) => {
  if (ele.parentNode) {
    if (ele.parentNode.tagName === tag.toUpperCase()) {
      return ele.parentNode
    } else {
      return findNodeUpper(ele.parentNode, tag)
    }
  }
}

export const findNodeUpperByClasses = (ele, classes) => {
  let parentNode = ele.parentNode
  if (parentNode) {
    let classList = parentNode.classList
    if (classList && classes.every(className => classList.contains(className))) {
      return parentNode
    } else {
      return findNodeUpperByClasses(parentNode, classes)
    }
  }
}

export const findNodeDownward = (ele, tag) => {
  const tagName = tag.toUpperCase()
  if (ele.childNodes.length) {
    let i = -1
    let len = ele.childNodes.length
    while (++i < len) {
      let child = ele.childNodes[i]
      if (child.tagName === tagName) return child
      else return findNodeDownward(child, tag)
    }
  }
}

export const showByAccess = (access, canViewAccess) => {
  return hasOneOf(canViewAccess, access)
}

/**
 * @description 根据name/params/query判断两个路由对象是否相等
 * @param {*} route1 路由对象
 * @param {*} route2 路由对象
 */
export const routeEqual = (route1, route2) => {
  const params1 = route1.params || {}
  const params2 = route2.params || {}
  const query1 = route1.query || {}
  const query2 = route2.query || {}
  return (route1.name === route2.name) && objEqual(params1, params2) && objEqual(query1, query2)
}

/**
 * 判断打开的标签列表里是否已存在这个新添加的路由对象
 */
export const routeHasExist = (tagNavList, routeItem) => {
  let len = tagNavList.length
  let res = false
  doCustomTimes(len, (index) => {
    if (routeEqual(tagNavList[index], routeItem)) res = true
  })
  return res
}

export const localSave = (key, value) => {
  localStorage.setItem(key, value)
}

export const localRead = (key) => {
  return localStorage.getItem(key) || ''
}

// scrollTop animation
export const scrollTop = (el, from = 0, to, duration = 500, endCallback) => {
  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = (
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.msRequestAnimationFrame ||
      function (callback) {
        return window.setTimeout(callback, 1000 / 60)
      }
    )
  }
  const difference = Math.abs(from - to)
  const step = Math.ceil(difference / duration * 50)

  const scroll = (start, end, step) => {
    if (start === end) {
      endCallback && endCallback()
      return
    }

    let d = (start + step > end) ? end : start + step
    if (start > end) {
      d = (start - step < end) ? end : start - step
    }

    if (el === window) {
      window.scrollTo(d, d)
    } else {
      el.scrollTop = d
    }
    window.requestAnimationFrame(() => scroll(d, end, step))
  }
  scroll(from, to, step)
}

/**
 * @description 根据当前跳转的路由设置显示在浏览器标签的title
 * @param {Object} routeItem 路由对象
 * @param {Object} vm Vue实例
 */
export const setTitle = (routeItem, vm) => {
  const handledRoute = getRouteTitleHandled(routeItem)
  const pageTitle = showTitle(handledRoute, vm)
  const resTitle = pageTitle ? `${title} - ${pageTitle}` : title
  window.document.title = resTitle
}

export const toDateString = (e, t) => {
  let a = new Date(e || new Date()),
    o = [digit(a.getFullYear(), 4), digit(a.getMonth() + 1), digit(a.getDate())],
    r = [digit(a.getHours()), digit(a.getMinutes()), digit(a.getSeconds())]
  return t = t || 'yyyy-MM-dd HH:mm:ss', t.replace(/yyyy/g, o[0]).replace(/MM/g, o[1]).replace(/dd/g, o[2]).replace(/HH/g, r[0]).replace(/mm/g, r[1]).replace(/ss/g, r[2])
}
export const digit = (e, t) => {
  let i = ''
  e = String(e), t = t || 2
  for (let a = e.length; a < t; a++) i += '0'
  return e < Math.pow(10, t) ? i + (0 | e) : e
}

export const sortData = (data, i = 0) => {
  let arr = [];
  data.forEach((item, index) => {
    let v = dictionaryData[item[i].trim()];
    if (v) {
      arr.push({
        index,
        sortValue: v
      });
    }
  });
  if (arr.length > 0) {
    arr.sort((a, b) => a.sortValue - b.sortValue);
    let sortIndexArray = arr.map(item => item.index);
    if (sortIndexArray.length > 0) {
      sortIndexArray.forEach(index => {
        let last = [...data[index]];
        data.push(last);
      });

      for (let i = 0; i< sortIndexArray.length; i++) {
        data.splice(sortIndexArray[i], 1);
        for (let j = i+1; j< sortIndexArray.length; j++) {
          if (sortIndexArray[j] > sortIndexArray [i]) {
            sortIndexArray[j] = sortIndexArray[j] - 1;
          }
        }
      }
    }
  }
  return data;
}

function group(arr) {

  sortData(arr, 0);
  let temp = {};
  arr.forEach(item => {
    let firstDimensionMember = item[0].trim();
    if (temp[firstDimensionMember]) {
      temp[firstDimensionMember].push(item);
    } else {
      temp[firstDimensionMember] = [item];
    }
  })
  let newArr = [];
  for (let k in temp) {
    let v = temp[k];
    if (v.length > 1) {
      sortData(v, 1);
    }
    newArr = newArr.concat(v)
  }
  return newArr;
}

export const mergeDimension = (mergeData) => {
  // 先排序
  mergeData = group(mergeData);
  let key = {}, mergeThead = false;
  let mainDimensionMembers = mergeData.map(item => item[0]);
  // 查询需要合并的一级维度
  mainDimensionMembers.forEach((item, index) => {
    if (key[item]) {
      key[item].count++;
    } else {
      key[item] = {
        count: 1,
        index
      }
    }
  });
  // 行合并
  let mergeCells = [];
  for (let k in key) {
    if (key[k].count > 1) {
      mergeCells.push({
        row: key[k].index,
        col: 0,
        rowspan: key[k].count,
        colspan: 1
      })
    }
  }

  // 列合并
  mergeData.forEach((item, index) => {
    // 如果第二列维度为空{，那么合并第一列和第二列
    if (!item[1]) {
      mergeCells.push({
        row: index,
        col: 0,
        rowspan: 1,
        colspan: 2
      })
      mergeThead = true;
    }
  })
  return {mergeCells, mergeData, mergeThead};
}

/**
 * @param {*} file 文件
 * @description 将文件流转为二进制
 */
export const readFile = (file) => {
  return new Promise(resolve => {
    let reader = new FileReader()
    reader.readAsBinaryString(file)
    reader.onload = e => {
      resolve(e.target.result)
    }
  })
}

/**
 * 构建树
 * @param list 列表
 * @param parentId 父节点id
 * @param nodeDisabled 子节点是否禁用
 * @param folderDisabled 目录节点是否禁用
 * @returns {*[]}
 */
export const buildTree = (list, parentId, nodeDisabled = false, folderDisabled = true, expand=false) => {
  let tree = []

  for (let item of list) {
    if (item.parentId === parentId) {
      let children = buildTree(list, item.id, nodeDisabled, folderDisabled)
      if (children.length > 0) {
        item.children = children
        item.disabled = folderDisabled
      } else {
        item.disabled = nodeDisabled
      }
      item.expand = expand
      tree.push(item)
    }
  }

  return tree
}

/**
 * 构建树
 * @param list 列表
 * @param parentId 父节点id
 * @param nodeDisabled 子节点是否禁用
 * @param folderDisabled 目录节点是否禁用
 * @returns {*[]}
 */
export const buildTreeRole = (list, parentId, nodeDisabled = false, folderDisabled = true) => {
  let tree = []

  for (let item of list) {
    if (item.parentId === parentId) {
      let children = buildTreeRole(list, item.id, nodeDisabled, folderDisabled)
      if (children.length > 0 || item.type === 'group') {
        item.children = children
        item.disabled = folderDisabled
      } else {
        item.disabled = nodeDisabled
      }
      item.expand = true
      tree.push(item)
    }
  }

  return tree
}

/**
 * 获取树中的第一项节点
 * @param tree
 * @returns {*|null}
 */
export const getFirstNode = (tree) => {
  if (tree.length > 0) {
    let node = tree[0]
    if (node.children && node.children.length > 0) {
      return getFirstNode(node.children)
    } else {
      return node
    }
  } else {
    return null
  }
}
