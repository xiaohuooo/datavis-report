// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import ViewUI from 'view-design'
import "@/dmp"
import config from '@/config'
// import importDirective from '@/directive'
import VueDraggableResizable from 'vue-draggable-resizable-gorkys'
import ElementResizeDetectorMaker from 'element-resize-detector'
import { directive as clickOutside } from 'v-click-outside-x'
import './index.less'
// import '@/assets/icons/iconfont.css'
import '@/assets/icons/report-icon/iconfont.css'
import TreeTable from 'tree-table-vue'
import VOrgTree from 'v-org-tree'
import 'v-org-tree/dist/v-org-tree.css'
import 'vue-draggable-resizable-gorkys/dist/VueDraggableResizable.css'

import Plain from "./styles/theme";
/* eslint-disable */
Vue.component('vue-draggable-resizable', VueDraggableResizable)
import vcolorpicker from 'vcolorpicker'
vcolorpicker.colorPicker.mounted = function(){
  this.$nextTick(() => {
    // 初始box框设置display none
    this.$el.lastElementChild.style.display = 'none'
    // 点击时box框设置display block
    this.$el.addEventListener('click', function(e) {
      if(e.target.classList.contains('colorBtn')) {
        e.target.nextElementSibling.style.display = 'block'
      }
    })
  })
}
Vue.use(ViewUI)
Vue.use(TreeTable)
Vue.use(VOrgTree)
Vue.use(vcolorpicker)

Vue.prototype.$erd = ElementResizeDetectorMaker()
Vue.prototype.$Message.config({ duration:3 });

// 全局变量
import { GLOBAL } from './config/ReportConstants'
Vue.prototype.GLOBAL = GLOBAL

/**
 * @description 生产环境关掉提示
 */
Vue.config.productionTip = false
/**
 * @description 全局注册应用配置
 */
Vue.prototype.$config = config
/**
 * 注册指令
 */
// importDirective(Vue)
Vue.directive('clickOutside', clickOutside);
/**
 * 按钮权限控制
 */
Vue.directive('allow', {
  inserted: function (ele, binding) {
    let allowArr = store.state.user.btnAllowList.map(item => item.id), allowBoo
    for(let i = 0; i < allowArr.length; i++) {
      if(binding.value.includes(allowArr[i])) {
        allowBoo = true;
        break;
      } else {
        allowBoo = false
      }
    }
    !allowBoo ? ele.parentNode.removeChild(ele) : true
  }
});
// 按钮权限函数,用于判断table操作列是否有权限
Vue.prototype.getBtnAllow = (value) => {
  let allowArr = store.state.user.btnAllowList.map(item => item.id), allowBoo
  for(let i = 0; i < allowArr.length; i++) {
    if(value.includes(allowArr[i])) {
      allowBoo = true;
      break;
    } else {
      allowBoo = false
    }
  }
  return allowBoo
}

// 事件总线
Vue.prototype.$bus = new Vue()

/**
 * 配置报告主题
 */
 Vue.use(Plain);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})

