import { formatData } from "@/utils/BusinessDataHandleUtil";

const UNIT_LIST = [
  {
    type: "wan",
    value: "(万)"
  },
  {
    type: "yi",
    value: "(亿)"
  }
];

export default {
  computed: {
    // 前缀
    indicatorValue_Prefix() {
      return configData => configData?.prefix || "";
    },
    // 处理千分位和小数以及数值单位
    indicatorValue_YXQ() {
      return (configData, indicatorValue) => {
        const config = {
          decimalDigits: configData?.decimalPlaces,
          numberUnit: configData?.numberUnit ? configData?.numberUnit : "original",
          thousandth: !!configData?.thousandsSeparator,
          showType: configData?.showType
        };
        return formatData(indicatorValue, config);
      };
    },
    // 单位
    indicatorValue_Unit() {
      return ({ showType, numberUnit }) => (showType === 0 ? UNIT_LIST.find(ele => ele.type === numberUnit)?.value || "" : "");
    },
    // 后缀
    indicatorValue_Suffix() {
      return configData => configData?.suffix || "";
    }
  }
};
