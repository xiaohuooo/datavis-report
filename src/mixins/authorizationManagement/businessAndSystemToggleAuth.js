import {
  closeDeptAuth,
  closeRoleAuth,
  closeUserAuth,
  openDeptAuth,
  openRoleAuth,
  openUserAuth,
  restoreBusinessAndSystemInherit,
} from '@/api/systemManagement/authorizationManagement'
import { mapState } from 'vuex'

export default {
  data() {
    return {
      showUserAuthPromptModal: false, // 显示用户修改权限提示模态框
      showRoleAuthPromptModal: false, // 显示角色修改权限提示模态框
      totalList: [], // 用于查找相关节点
    }
  },
  computed: {
    ...mapState('authorizationManagement', ['blackList']),
  },
  created() {
    this.authType = null // 权限类型 1-查看 2-授权
    this.resource = null // 当前正在操作的权限实体
  },
  methods: {
    /**
     * 判断是否有权限
     * @param type 权限类型 1-查看 2-授权
     * @param data 当前树节点数据
     */
    hasAuth(type, data) {
      // 在黑名单中无权限，或者自己的权限和继承权限都为false
      const isBlack = !!this.blackList.find(item => item.resId === data.id && item.type === type)
      const noAuth = type === 1 ? !data.viewFromOther && !data.canView : !data.authFromOther && !data.canAuth
      return (this.leftTab === 'user' && !isBlack && !noAuth) || (this.leftTab !== 'user' && !isBlack && !noAuth)
    },

    /**
     * 显示恢复继承权限按钮
     * @param type 权限类型 1-查看 2-授权
     * @param data 当前树节点数据
     */
    showRestore(type, data) {
      // 要么在blackList中，都显示恢复继承按钮，要么继承权限为false，自己权限为true
      const isBlack = !!this.blackList.find(item => item.resId === data.id && item.type === type)
      const different = type === 1 ? !data.viewFromOther && data.canView : !data.authFromOther && data.canAuth
      return this.leftTab === 'user' && !this.isDisabled && (isBlack || different)
    },

    /**
     * 显示来源按钮
     * @param data 当前树节点数据
     */
    showOrigin(data) {
      // 不在黑名单中，并且有继承的查看权限或者继承的授权权限
      const isBlack = !!this.blackList.find(item => item.resId === data.id)
      return !isBlack && (data.viewFromOther || data.authFromOther)
    },

    /**
     * 获取数据
     */
    async getData() {
      if (this.leftTab === 'department') {
        await this.getDepartmentListAndDirectoryList(this.domainId)
      } else if (this.leftTab === 'role') {
        await this.getRoleListAndDirectoryList(this.domainId)
      } else {
        await this.getUserListAndDirectoryList(this.domainId)
      }
    },

    /**
     * 设置查看/授权权限 查看维度为用户组时使用
     * @param listName 列表名称 用于区别业务目录、系统管理、人员管理
     * @param root
     * @param authType 权限类型 1-查看 2-授权
     * @param data 当前树节点数据
     */
    onSetAuth(listName, root, authType, data) {
      return (e) => {
        e.stopPropagation()
        if (!this.isOperable()) {
          return
        }
        this.saveTreeNode(listName, this.domainId, root)
        if (this.leftTab === 'user' && this.$store.state.user.configData.userAuthPriorityWarnEnable) {
          // 开启了提醒--默认配置
          this.showUserAuthPromptModal = true
          this.authType = authType
          this.resource = data
        }
        // else if (this.leftTab === 'role') {
        //   // 用户权限配置后再对角色权限配置时提醒
        //   this.showRoleAuthPromptModal = true
        //   this.authType = authType
        //   this.resource = data
        // }
        else {
          this.onToggleAuth(authType, data)
        }
      }
    },

    /**
     * 恢复继承权限
     * @param listName 列表名称 用于区别业务目录、系统管理、人员管理
     * @param root
     * @param authType 权限类型 1-查看 2-授权
     * @param data 当前树节点数据
     */
    onRestoreAuth(listName, root, authType, data) {
      return async (e) => {
        e.stopPropagation()
        if (!this.isOperable()) {
          return
        }
        this.saveTreeNode(listName, this.domainId, root)

        const params = this.buildRestoreAuthParams(authType, data)
        const res = await restoreBusinessAndSystemInherit(params)
        if (res.data.state === 'success') {
          await this.getData()
          this.$Message.success('权限恢复成功！')
        } else {
          this.$Message.error('权限恢复失败！')
        }
      }
    },

    /**
     * 关闭用户修改权限提示模态框
     */
    async onUserAuthPromptOk() {
      await this.onToggleAuth(this.authType, this.resource)
      this.authType = null
      this.resource = null
      this.showUserAuthPromptModal = false
    },

    /**
     * 关闭角色修改权限提示模态框
     */
    async onRoleAuthPromptOk() {
      await this.onToggleAuth(this.authType, this.resource)
      this.authType = null
      this.resource = null
      this.showRoleAuthPromptModal = false
    },

    /**
     * 切换权限开关
     * @param type 权限类型 1-查看 2-授权
     * @param resource 资源
     */
    async onToggleAuth(type, resource) {
      let request
      if (this.leftTab === 'department') {
        request = this.hasAuth(type, resource) ? closeDeptAuth : openDeptAuth
      } else if (this.leftTab === 'role') {
        request = this.hasAuth(type, resource) ? closeRoleAuth : openRoleAuth
      } else {
        request = this.hasAuth(type, resource) ? closeUserAuth : openUserAuth
      }

      const params = this.buildSetAuthParams(type, resource)
      const res = await request(params)
      if (res.data.state === 'success') {
        await this.getData()
      } else {
        this.$Message.error('权限设置失败！')
      }
    },

    /**
     * 设置权限--构建请求参数
     * @param type 权限类型 1-查看 2-授权
     * @param resource 资源
     */
    buildSetAuthParams(type, resource) {
      const { viewFromOther, authFromOther, canView, canAuth } = resource
      let parent = []
      if ((type === 1 && !viewFromOther && !canView) || (type === 2 && !authFromOther && !canAuth)) {
        // 打开权限，会影响所有父节点
        parent = this.getSetAuthParentResource(resource)
      }
      // 会影响所有子节点
      const children = this.getSetAuthChildrenResource(resource.children)
      let resourceList = [resource, ...parent, ...children]

      return resourceList.map(item => {
        const { viewFromOther, authFromOther } = item
        const inherit = type === 1 ? viewFromOther : authFromOther
        return {
          domainId: this.domainId,
          inherit,
          resourceId: item.id,
          type,
        }
      })
    },

    /**
     * 恢复权限--构建请求参数
     * @param type 权限类型 1-查看 2-授权
     * @param resource 资源
     */
    buildRestoreAuthParams(type, resource) {
      // 恢复权限只影响所有支持恢复的子节点
      const children = this.getRestoreAuthChildrenResource(resource.children, type)
      const resourceList = [resource, ...children]
      const resourceId = resourceList.map(item => item.id)

      return {
        domainId: this.domainId,
        resourceId,
        type,
      }
    },

    /**
     * 设置权限--获取该资源的所有父资源
     * @param resource 当前资源
     */
    getSetAuthParentResource(resource) {
      let list = []
      const parentId = resource.pid || resource.groupId
      if (parentId === -1) return list

      const parent = this.totalList.find(total => total.id === parentId)
      if (parent) {
        list.push(parent)
        const grandparent = this.getSetAuthParentResource(parent)
        list.push(...grandparent)
      }

      return list
    },

    /**
     * 设置权限--获取该资源的所有子资源
     * @param tree 当前资源的children
     */
    getSetAuthChildrenResource(tree) {
      let list = []
      if (!tree) return list

      for (let treeItem of tree) {
        list.push(treeItem)
        if (treeItem.children) {
          const children = this.getSetAuthChildrenResource(treeItem.children)
          list.push(...children)
        }
      }

      return list
    },

    /**
     * 恢复权限--获取该资源的所有支持恢复的子资源
     * @param tree 当前资源的children
     * @param type 权限类型 1-查看 2-授权
     */
    getRestoreAuthChildrenResource(tree, type) {
      let list = []
      if (!tree) return list

      for (let treeItem of tree) {
        if (this.showRestore(type, treeItem)) {
          list.push(treeItem)
        }
        if (treeItem.children) {
          const children = this.getRestoreAuthChildrenResource(treeItem.children)
          list.push(...children)
        }
      }

      return list
    },
  },
}
