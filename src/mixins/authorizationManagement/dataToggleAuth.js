import { dataAuthShare, dataNoAuth } from "@/api/systemManagement/authorizationManagement";
import { deepCopy } from "@/utils/common";

export default {
  computed: {
    settings() {
      let settings = ['授权']
      return settings
    },
  },
  methods: {
    /**
     * 切换权限开关
     * @param resourceType 资源类型 1-使用 4-编辑
     * @param resource 资源
     */
    async onToggleAuth(resourceType, resource, dataAuthTab) {

      if (!this.domainId) {
        // 没有选择实体
        let domain = ''
        if (this.leftTab === 'department') {
          domain = '部门'
        } else if (this.leftTab === 'role') {
          domain = '角色'
        } else {
          domain = '用户'
        }
        this.$Message.warning(`请先选择一个${domain}实体！`)
        return
      }
      console.log('resource', resource)
      const request = (resource.controlView && resourceType === 1) || (resource.controlEdit && resourceType === 4) ? dataNoAuth : dataAuthShare

      const params = {
        resourceId: resource.id,
        moduleType: resource.moduleType,
        authType: resourceType
      }
      let type = 3
      if (this.leftTab === 'department') {
        params.deptList = [this.domainId]
        type = 1
      } else if (this.leftTab === 'role') {
        params.roleList = [this.domainId]
        type = 2
      } else {
        params.userList = [this.domainId]
      }
      const res = await request(params)
      if (res.data.state === 'success') {
        await this.getDomainResourceList()
      } else {
        this.$Message.error('权限设置失败！')
      }
    },
    filterIndicatorTree(tree) {
      let newTree = []
      for (let treeItem of tree) {
        if (treeItem.moduleType) {
          newTree.push(treeItem)
          continue
        }
        let newChildren = []
        if (treeItem.children) {
          newChildren = this.filterIndicatorTree(treeItem.children)
        }
        let newTreeItem = deepCopy(treeItem)
        if (newChildren?.length) {
          newTreeItem.children = newChildren
          newTree.push(newTreeItem)
        }
      }
      return newTree
    },
  },
}
