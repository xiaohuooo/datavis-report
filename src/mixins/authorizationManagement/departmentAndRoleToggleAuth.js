import { openDeptAuth, closeDeptAuth, openRoleAuth, closeRoleAuth } from '@/api/systemManagement/authorizationManagement'

export default {
  methods: {
    /**
     * 切换权限开关
     * @param type 权限类型 1-查看 2-授权
     * @param domain 实体
     * @returns {(function(*): void)|*}
     */
    async onToggleAuth(type, domain) {
      if (!this.resourceId) {
        this.$Message.warning(`请先选择一个资源！`)
        return
      }

      const { canView, canAuth } = domain
      let request
      if (this.rightTab === 'department') {
        if (type === 1) {
          request = canView ? closeDeptAuth : openDeptAuth
        } else {
          request = canAuth ? closeDeptAuth : openDeptAuth
        }
      } else {
        if (type === 1) {
          request = canView ? closeRoleAuth : openRoleAuth
        } else {
          request = canAuth ? closeRoleAuth : openRoleAuth
        }
      }

      const params = {
        type,
        resourceId: this.resourceId,
        domainId: domain.id,
      }
      const res = await request(params)
      if (res.data.state === 'success') {
        if (this.rightTab === 'department') {
          await this.getBusinessDirectoryListAndDepartmentList()
        } else {
          await this.getBusinessDirectoryListAndRoleList()
        }
      } else {
        this.$Message.error('权限设置失败！')
      }
    },
  },
}
