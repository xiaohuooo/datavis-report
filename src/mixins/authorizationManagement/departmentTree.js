export default {
  methods: {
    /**
     * 构建树
     * @param list 列表
     * @param renderNode 子节点渲染函数
     * @param parentId 父节点id
     * @returns {*[]}
     */
    buildTree(list, renderNode, parentId = null) {
      let tree = []

      for (let item of list) {
        if (item.parentId === parentId) {
          let children = this.buildTree(list, renderNode, item.id)
          if (children.length > 0) {
            item.children = children
          } else {
            item.render = renderNode
          }
          item.disabled = !item.auth
          item.expand = true
          item.selected = false
          tree.push(item)
        }
      }

      return tree
    },
  },
}
