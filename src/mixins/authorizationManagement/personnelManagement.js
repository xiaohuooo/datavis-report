import { closePersonnelManagementAuth, openPersonnelManagementAuth, restorePersonalInherit } from "@/api/systemManagement/authorizationManagement";
import { mapState } from 'vuex'

export default {
  data() {
    return {
      showUserAuthPromptModal: false, // 显示用户修改权限提示模态框
      showRoleAuthPromptModal: false, // 显示角色修改权限提示模态框
      totalList: [], // 用于查找相关节点
    }
  },
  computed: {
    ...mapState('authorizationManagement', ['blackDept', 'blackRole']),
  },
  created() {
    this.resourceType = null // 资源类型 1-部门 2-角色
    this.resource = null // 当前正在操作的权限实体
  },
  methods: {
    /**
     * 判断是否有权限
     * @param resourceType 资源类型 1-部门 2-角色
     * @param data 当前树节点数据
     */
    hasAuth(resourceType, data) {
      // 在黑名单中无权限，或者自己的权限和继承权限都为false
      const blackList = resourceType === 1 ? this.blackDept : this.blackRole
      const isBlack = !!blackList.find(item => item === data.id)
      const noAuth = !data.authFromOther && !data.canAuth
      return (this.leftTab === 'user' && !isBlack && !noAuth) || (this.leftTab !== 'user' && !isBlack && !noAuth)
    },

    /**
     * 显示恢复继承权限按钮
     * @param resourceType 资源类型 1-部门 2-角色
     * @param data 当前树节点数据
     */
    showRestore(resourceType, data) {
      // 要么在blackList中，都显示恢复继承按钮，要么继承权限为false，自己权限为true
      const blackList = resourceType === 1 ? this.blackDept : this.blackRole
      const isBlack = !!blackList.find(item => item === data.id)
      const different = !data.authFromOther && data.canAuth
      return this.leftTab === 'user' && !this.isDisabled && (isBlack || different)
    },

    /**
     * 显示来源按钮
     * @param resourceType 资源类型 1-部门 2-角色
     * @param data 当前树节点数据
     */
    showOrigin(resourceType, data) {
      // 不在黑名单中，并且有继承的查看权限或者继承的授权权限
      const blackList = resourceType === 1 ? this.blackDept : this.blackRole
      const isBlack = !!blackList.find(item => item === data.id)
      return !isBlack && (data.viewFromOther || data.authFromOther)
    },

    /**
     * 设置查看/授权权限 查看维度为用户组时使用
     * @param listName 列表名称 用于区别业务目录、系统管理、人员管理
     * @param root
     * @param resourceType 资源类型 1-部门 2-角色
     * @param data 当前树节点数据
     */
    onSetAuth(listName, root, resourceType, data) {
      return (e) => {
        e.stopPropagation()
        if (!this.isOperable()) {
          return
        }
        this.saveTreeNode(listName, this.domainId, root)
        if (this.leftTab === 'user' && this.$store.state.user.configData.userAuthPriorityWarnEnable) {
          // 开启了提醒--默认配置
          this.showUserAuthPromptModal = true
          this.resourceType = resourceType
          this.resource = data
        }
        // else if (this.leftTab === 'role') {
        //   // 用户权限配置后再对角色权限配置时提醒
        //   this.showRoleAuthPromptModal = true
        //   this.resourceType = resourceType
        //   this.resource = data
        // }
        else {
          this.onToggleAuth(resourceType, data) // 业务目录、系统管理共用一个方法，人员管理的方法在自己的mixin中
        }
      }
    },

    /**
     * 恢复继承权限
     * @param listName 列表名称 用于区别业务目录、系统管理、人员管理
     * @param root
     * @param resourceType 资源类型 1-部门 2-角色
     * @param data 当前树节点数据
     */
    onRestoreAuth(listName, root, resourceType, data) {
      return async (e) => {
        e.stopPropagation()
        if (!this.isOperable()) {
          return
        }
        this.saveTreeNode(listName, this.domainId, root)

        //  1-部门授权部门 2-角色授权部门 3-用户授权部门 4-部门授权角色 5-角色授权角色 6-用户授权角色
        let type
        if (this.leftTab === 'department') {
          type = resourceType === 1 ? 1 : 4
        } else if (this.leftTab === 'role') {
          type = resourceType === 1 ? 2 : 5
        } else {
          type = resourceType === 1 ? 3 : 6
        }

        const params = this.buildRestoreAuthParams(type, data)
        const res = await restorePersonalInherit(params)
        if (res.data.state === 'success') {
          await this.getPersonnelManagementDirectoryList()
          this.$Message.success('权限恢复成功！')
        } else {
          this.$Message.error('权限恢复失败！')
        }
      }
    },

    /**
     * 关闭用户修改权限提示模态框
     */
    async onUserAuthPromptOk() {
      await this.onToggleAuth(this.resourceType, this.resource)
      this.resourceType = null
      this.resource = null
      this.showUserAuthPromptModal = false
    },

    /**
     * 关闭角色修改权限提示模态框
     */
    async onRoleAuthPromptOk() {
      await this.onToggleAuth(this.resourceType, this.resource)
      this.resourceType = null
      this.resource = null
      this.showRoleAuthPromptModal = false
    },

    /**
     * 切换权限开关
     * @param resourceType 资源类型 1-部门 2-角色
     * @param resource 资源
     */
    async onToggleAuth(resourceType, resource) {
      //  1-部门授权部门 2-角色授权部门 3-用户授权部门 4-部门授权角色 5-角色授权角色 6-用户授权角色
      let domain = '', type
      if (this.leftTab === 'department') {
        domain = '部门'
        type = resourceType === 1 ? 1 : 4
      } else if (this.leftTab === 'role') {
        domain = '角色'
        type = resourceType === 1 ? 2 : 5
      } else {
        domain = '用户'
        type = resourceType === 1 ? 3 : 6
      }

      if (!this.domainId) {
        this.$Message.warning(`请先选择一个${domain}实体！`)
        return
      }

      const request = this.hasAuth(resourceType, resource) ? closePersonnelManagementAuth : openPersonnelManagementAuth
      const params = this.buildSetAuthParams(resourceType, type, resource)
      const res = await request(params)
      if (res.data.state === 'success') {
        await this.getPersonnelManagementDirectoryList()
      } else {
        this.$Message.error('权限设置失败！')
      }
    },

    /**
     * 设置权限--构建请求参数
     * @param resourceType 资源类型 1-部门 2-角色
     * @param type 1-部门授权部门 2-角色授权部门 3-用户授权部门 4-部门授权角色 5-角色授权角色 6-用户授权角色
     * @param resource 资源
     */
    buildSetAuthParams(resourceType, type, resource) {
      // 对人员管理进行权限操作，不影响父节点和子节点
      let resourceList = [resource]

      return resourceList.map(item => {
        const inherit = !!item.authFromOther
        return {
          domainId: this.domainId,
          inherit,
          resourceId: item.id,
          domainType: type,
        }
      })
    },

    /**
     * 恢复权限--构建请求参数
     * @param type 1-部门授权部门 2-角色授权部门 3-用户授权部门 4-部门授权角色 5-角色授权角色 6-用户授权角色
     * @param resource 资源
     */
    buildRestoreAuthParams(type, resource) {
      // 对人员管理进行权限操作，不影响父节点和子节点
      const resourceId = [resource.id]

      return {
        domainId: this.domainId,
        resourceId,
        type,
      }
    },
  },
}
