import { mapState } from 'vuex'
import { filterTree } from '@/utils/common'

export default {
  data() {
    return {
      settingTreeData: [], // 设置树数据
      treeNodeStatusList: [], //所有树状图的node展开状态
    }
  },
  computed: {
    ...mapState('authorizationManagement', [
      'rightTabKeywords',
      'domainId',
      'leftTab',
      'superAdmin',
      'resourceId',
      'rightTab',
    ]),
    // 左侧Tab为用户时，需展示来源
    fullSettings() {
      return this.leftTab === 'user' ? [...this.settings, '来源'] : this.settings
    },
    // 判断权限按钮是否失效
    isDisabled() {
      return !this.domainId || this.domainId === 1 || this.superAdmin
    },
  },
  created() {
    this.settingTreeDataBak = []
  },
  mounted() {
    this.getSettingTree()
  },
  watch: {
    rightTabKeywords(val) {
      this.settingTreeData = filterTree(this.settingTreeDataBak, val)
    },
  },
  methods: {
    buildTree(list, parentId, listName, domainId) {
      let nodeListObj = null
      //如果listName和domainId没传且没有保存的树状图列表 不考虑找对应node状态 直接全部默认展开
      if (listName && domainId && this.treeNodeStatusList.length > 0) {
        nodeListObj = this.treeNodeStatusList.find(item => item.listName == listName && item.domainId == domainId)
      }
      let tree = []
      for (let item of list) {
        if (item.parentId === parentId || item.groupId === parentId) {
          let children = this.buildTree(list, item.id, listName, domainId)
          if (children.length > 0) {
            item.children = children
          }
          item.disabled = true
          let nodeExpand = nodeListObj?.nodeList.find(it => it.id == item.id) //找到相同id的node状态
          item.expand = nodeExpand == undefined ? true : nodeExpand.expand //找到就赋对应的值（true或者false） 没有默认展开
          tree.push(item)
        }
      }
      return tree
    },

    //保存当前树状图node开关状态
    saveTreeNode(listName, domainId, rootList) {
      let nodeList = []
      rootList.forEach(item => {
        let nodeStatus = {
          id: item.node.id,
          expand: item.node.expand
        }
        nodeList.push(nodeStatus)
      })
      let nodeListObj = {
        nodeList, listName, domainId
      }
      if (this.treeNodeStatusList.length > 0) {
        this.treeNodeStatusList.forEach(item => {
          if (item.listName == listName && item.domainId == domainId) {
            item.nodeList = nodeList
            return
          }
        })
      }
      this.treeNodeStatusList.push(nodeListObj)
    },

    /**
     * 是否可操作
     */
    isOperable() {
      if (!this.domainId) {
        // 没有选择实体
        let domain = ''
        if (this.leftTab === 'department') {
          domain = '部门'
        } else if (this.leftTab === 'role') {
          domain = '角色'
        } else {
          domain = '用户'
        }
        this.$Message.warning(`请先选择一个${domain}实体！`)
        return false
      }

      if (this.domainId === 1 || this.superAdmin) {
        // 所选用户为管理员
        this.$Message.warning('管理员权限不可修改！')
        return false
      }

      return true
    },
  },
}
