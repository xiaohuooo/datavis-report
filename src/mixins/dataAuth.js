import { mapGetters } from "vuex";

export default {
  computed: {
    ...mapGetters(["isSuperAdmin"]),
    // 是否属于自身
    isOwn() {
      return creatorId => this.isSuperAdmin || creatorId === this.$store.state.user.userId;
    },
    // 是否拥有`使用/编辑`中的某一种权限
    isHasAuth() {
      return ({ creatorId, controlView, controlEdit }) => this.isOwn(creatorId) || controlView === true || controlEdit === true;
    },
    // 是否有编辑权限
    isEditAuth() {
      return ({ creatorId, controlEdit }) => this.isOwn(creatorId) || controlEdit === true;
    },
    // 是否没有任何权限
    isNotAuth() {
      return ({ creatorId, controlView, controlEdit }) => !this.isOwn(creatorId) && controlView === false && controlEdit === false;
    },
    // 是否拥有所有权限
    isHasAllAuth() {
      return ({ creatorId, controlView, controlEdit }) => this.isOwn(creatorId) || (controlView === true && controlEdit === true);
    }
  }
};
