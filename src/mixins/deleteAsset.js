export default {
  data() {
    return {
      showDeleteModal: false, // 控制删除弹框显示
      deletedAsset: {
        name: '',
        type: '',
      }, // 不能删除情况，被删除的资源
      resourceList: [], // 不能删除情况，资源列表
    }
  },
  methods: {
    /**
     * 指标无法删除的情况
     * @param deletedAsset 被删除的资产
     * @param relatedResource 被引用的资源
     */
    unableToDelete(deletedAsset, relatedResource) {
      let resourceList = []
      for (let key in relatedResource) {
        for (let item of relatedResource[key]) {
          resourceList.push({ name: item, type: Number(key) })
        }
      }
      this.deletedAsset = deletedAsset
      this.resourceList = resourceList
      this.showDeleteModal = true
    },
  },
}
