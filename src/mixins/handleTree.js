export default {
  data() {
    return {
      treeExpandStatusBak: [] //备份树的展开状态
    };
  },
  methods: {
    /**
     * 更新树的展开、选中状态
     * @param newTree 新树
     * @param oldTree 参照物，已备份的带有状态的树
     */
    updateTreeDataStatus(newTree, oldTree) {
      for (let newTreeItem of newTree) {
        for (let oldTreeItem of oldTree) {
          if (newTreeItem.id === oldTreeItem.id) {
            newTreeItem.expand = oldTreeItem.expand;
            if (newTreeItem.children?.length && oldTreeItem.children?.length) {
              this.updateTreeDataStatus(newTreeItem.children, oldTreeItem.children);
            }
          }
        }
      }
    }
  }
};
