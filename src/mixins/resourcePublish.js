import { PUBLISH_ACTION_LIST, NOT_ONLINE, IN_APPROVAL, PUBLISH_FAIL, IN_ONLINE, IN_OFFLINE } from "@/config/resourcePublish";
import { publishAsset, offlineAsset } from "@/api/assetAction";
import deleteAsset from "@/mixins/deleteAsset";
import dataAuth from "@/mixins/dataAuth";

export default {
  mixins: [deleteAsset, dataAuth],
  data() {
    return {
      // 变更所有者弹窗
      showChangeOwnerModal: false,
      changeModalStatus: {
        resourceId: null, // 资产id
        moduleType: null // 资产类型
      },
      // 申请权限弹窗
      showApplyAuthModal: false,
      applyList: [],
      // 权限分享弹窗
      showAuthShare: false,
      authShareModalStatus: {
        resourceId: null, // 资产id
        moduleType: null // 资产类型
      }
    };
  },
  computed: {
    showStatusDataByKey() {
      return (status, key) => PUBLISH_ACTION_LIST.find(ele => ele.value === status)?.[key] || "";
    },
    // 是否展示发布/下线按钮
    isShowChangeStatusIcon() {
      return ({ creatorId, status }) => this.isOwn(creatorId) && status !== IN_APPROVAL;
    },
    // 是否发布成功
    isPublishSuccess() {
      return ({ creatorId, status }) => this.isOwn(creatorId) && status === IN_ONLINE;
    }
  },
  methods: {
    /**
     * 展示弹窗
     * @param {String} type 哪一个弹窗
     * @param {Object} row 附带的数据
     */
    showModal({ type, row }) {
      if (type === "变更所有者") {
        const { id, moduleType } = row;
        this.changeModalStatus = {
          resourceId: id,
          moduleType
        };
        this.creator = row.creator;
        this.showChangeOwnerModal = true;
      } else if (type === "申请权限" || type === "批量申请权限") {
        let list = [];
        if (type === "申请权限") {
          const { id, moduleType, controlView, controlEdit } = row;
          list = [{ id, moduleType, controlView, controlEdit }];
        } else {
          list = row.map(ele => ({
            id: ele.id,
            moduleType: ele.moduleType,
            controlView: ele.controlView,
            controlEdit: ele.controlEdit
          }));
        }
        this.applyList = list;
        this.showApplyAuthModal = true;
      } else if (type === "权限分享") {
        const { id, name, moduleType } = row;
        this.authShareModalStatus = {
          resourceId: id,
          name,
          moduleType
        };
        this.showAuthShare = true;
      } else if (type === "删除") {
        const { id, name, type, resData } = row;
        const deletedAsset = { id, name, type };
        this.unableToDelete(deletedAsset, resData);
      }
    },
    /**
     * 改变状态（发布/下线）
     */
    async changeStatus({ id, name, moduleType, status }) {
      let resourceTypeName = "";
      if (moduleType === 5) {
        resourceTypeName = "报告";
      } else if (moduleType === 1) {
        resourceTypeName = "维度";
      } else if (moduleType === 2 || moduleType === 8 || moduleType === 3 || moduleType === 11) {
        resourceTypeName = "指标";
      }

      let content = "";
      let typeName = "";
      let changeStatusFunc = null;
      let newStatus = null;
      if (status === NOT_ONLINE || status === PUBLISH_FAIL || status === IN_OFFLINE) {
        content = `发布后该${resourceTypeName}将允许被使用。`;
        typeName = "发布";
        changeStatusFunc = publishAsset;
        newStatus = IN_APPROVAL;
      } else if (status === IN_ONLINE) {
        content = `下线后该${resourceTypeName}将无法被使用。`;
        typeName = "下线";
        changeStatusFunc = offlineAsset;
        newStatus = IN_OFFLINE;
      }
      this.$Modal.confirm({
        title: `确认${typeName}${resourceTypeName}“${name}”吗？`,
        content,
        okText: "确认",
        onOk: async () => {
          const res = await changeStatusFunc({ moduleType, resourceId: id });
          if (res.data.state === "success") {
            this.$Message.success(`${resourceTypeName}${typeName}成功！`);
            this.$emit("updateData:end"); // 调接口更新数据
          } else {
            this.$Message.warning(res.data.message);
          }
        }
      });
    },
    /**
     * 关闭弹窗-变更所有者
     */
    closeChangeModal() {
      this.changeModalStatus = {
        resourceId: null,
        moduleType: null
      };
    }
  }
};
