export default {
  methods: {
    /**
     * 点击文件夹，展开/收起
     * @param e
     */
    onClick(e) {
      if (e.target.closest('.ivu-tree-title')) {
        // 只有点击的是.ivu-tree-title或者内部时
        const arrow = e.target.closest('li').querySelector('.ivu-tree-arrow')
        if (arrow.children.length) {
          // 文件夹的arrow下有子节点
          arrow.click()
        }
      }
    },
  },
}
