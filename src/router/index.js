import Vue from 'vue'
import Router from 'vue-router'
import { publicRoutes } from './routers'
import store from '@/store'
import iView from 'view-design'
import { setToken, getToken, setTitle } from '@/libs/util'
import config from '@/config'
const { homeName } = config

Vue.use(Router)
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
const router = new Router({
  routes: publicRoutes,
  mode: 'hash' // history
})

const LOGIN_PAGE_NAME = 'login'
const LOGIN_FRONT_NAMES = ["register","forgetPassword","agreement"]

router.beforeEach((to, from, next) => {
  iView.LoadingBar.start()

  // license失效，直接放行
  if (to.path.endsWith("/licenseInvalid")) {
    next();
    return
  }

  if (to.path.endsWith('/template_list')) {
    let credentials = to.query.params;
    if (credentials) {
      credentials = credentials.replace(/ /g, '+');
      store.dispatch('handleLogin', {credentials}).then(u => {
        if (u.state === 'success' && u.data['token']) {
          store.dispatch('getUserInfo').then(r => {
            next({path: '/manager/templateManage/template_list' })
          })
        } else {
          this.$Message.error('用户名或者密码错误');
          next({
            name: LOGIN_PAGE_NAME
          })
        }
      }).catch(() => {
        setToken('')
        next({
          name: LOGIN_PAGE_NAME
        })
      });
      return;
    }
  }

  const token = getToken();
  if (!token && to.name !== LOGIN_PAGE_NAME) {
    // 未登录且要跳转的页面不是登录页
    if(LOGIN_FRONT_NAMES.includes(to.name)){
      next() // 放行
    }else{
      next({
        name: LOGIN_PAGE_NAME // 跳转到登录页
      })
    }
  } else if (!token && to.name === LOGIN_PAGE_NAME) {
    // 未登陆且要跳转的页面是登录页
    next() // 放行
  } else if (token && to.name === LOGIN_PAGE_NAME) {
    // 已登录且要跳转的页面是登录页
    const routeName = store.state.user.adminWorkbench ? "工作台" : homeName;
    next({
      name: routeName // 跳转到homeName页
    });
  } else {
    if (store.state.user.hasGetInfo) {
      next()
    } else {
      // 重新加载，store被清空
      store.dispatch('getUserInfo').then(u => {
        next({ ...to, replace: true })
      }).catch(() => {
        setToken('')
        next({
          name: 'login'
        })
      })
    }
  }
})

router.afterEach(to => {
  setTitle(to, router.app)
  iView.LoadingBar.finish()
  window.scrollTo(0, 0)
})

export default router
