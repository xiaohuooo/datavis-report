import Main from "@/components/main";
import Manager from "@/view/system/Manager";

/**
 * 路由共有三种，公共路由直接加入路由集合；需要权限的页面路由需要权限过滤；iframe类型的路由动态创建路由对象
 *
 * 1.公共路由，无需任何权限都可访问，比如登录、首页等
 * 2.需要权限控制的页面路由，比如系统菜单、维度、指标等
 * 3.动态iframe类型的页面路由
 *
 */
export const publicRoutes = [
  {
    path: "/login",
    name: "login",
    meta: {
      title: "登录",
      hideInMenu: true,
    },
    component: () => import("@/view/login/login.vue"),
  },
  {
    path: "/register",
    name: "register",
    meta: {
      title: "注册",
      hideInMenu: true,
    },
    component: () => import("@/view/register/register.vue"),
  },
  {
    path: "/forgetPassword",
    name: "forgetPassword",
    meta: {
      title: "忘记密码",
      hideInMenu: true,
    },
    component: () => import("@/view/forgetPassword/forgetPassword.vue"),
  },
  {
    path: "/template_add",
    name: "创建报告",
    meta: {
      icon: "md-grid",
      title: "创建报告",
      notCache: true,
      hideInMenu: true,
    },
    component: () => import("@/view/report/reportTemplate/ReportManager.vue"),
  },
  {
    path: "/reportView",
    name: "报告查看",
    meta: {
      icon: "md-grid",
      title: "报告查看",
      notCache: true,
      hideInMenu: true,
    },
    component: () => import("@/view/report/ReportFragmentCalendar.vue"),
  },
  {
    path: "/report",
    name: "通用报告",
    component: Main,
    meta: {
      hideInBread: true,
      title: "通用报告",
      icon: "ios-cog",
    },
    children: [
      {
        path: "reportPage",
        name: "通用 ",
        meta: {
          icon: "ios-analytics",
          title: "通用",
        },
        component: () => import("@/view/report/ReportPageForMenu.vue"),
      },
      {
        path: "operationCenter",
        name: "通用iFrame",
        meta: {
          title: "通用iFrame",
        },
        component: () => import("@/view/iframe/index.vue"),
      },
    ],
  },
  {
    path: "/",
    name: "_home",
    redirect: "/home",
    component: Main,
    meta: {
      hideInMenu: true,
      notCache: true,
    },
    children: [],
  },
  {
    path: "/401",
    name: "error_401",
    meta: {
      hideInMenu: true,
    },
    component: () => import("@/view/error-page/401.vue"),
  },
  {
    path: "/500",
    name: "error_500",
    meta: {
      hideInMenu: true,
    },
    component: () => import("@/view/error-page/500.vue"),
  },
  {
    path: "/home",
    name: "搜索分析",
    component: Main,
    children: [
      {
        path: "/home",
        name: "搜索分析",
        meta: {
          title: "搜索分析",
          hideInMenu: true,
        },
        component: () => import("@/view/home/home.vue"),
      },
    ],
  },
  {
    path: "/message",
    name: "消息",
    component: Main,
    meta: {
      title: "消息",
    },
    children: [
      {
        path: "/message",
        name: "消息",
        meta: {
          title: "消息",
          hideInMenu: true,
        },
        component: () => import("@/view/message/index.vue"),
      },
    ],
  },
  {
    path: "/licenseInvalid",
    name: "licenseInvalid",
    meta: {
      title: "license失效",
    },
    component: () => import("@/view/licenseInvalid/index.vue"),
  },
  {
    path: "/commonResourceEdit",
    name: "常规资源编辑",
    meta: {
      title: "常规资源编辑",
    },
    component: () => import("@/view/templateManages/resourceEdit/commonResourceEdit.vue"),
  },
  {
    path: "/combineResourceEdit",
    name: "组合资源编辑",
    meta: {
      title: "组合资源编辑",
    },
    component: () => import("@/view/templateManages/resourceEdit/combineResourceEdit.vue"),
  },
  {
    path: "/",
    component: Main,
    children: [
      {
        path: "/operationCenter",
        name: "运营概览",
        meta: {
          title: "运营概览",
        },
        component: () => import("@/view/iframe/index.vue"),
      },
    ],
  },
  {
    path: "/workbenchPreview",
    name: "工作台",
    component: Main,
    children: [
      {
        path: "/workbenchPreview",
        name: "工作台",
        meta: {
          title: "工作台",
          hideInMenu: true,
        },
        component: () => import("@/view/workbenchPreview/index.vue"),
      },
    ],
  },
  {
    path: "/knowledgeAdd",
    name: "创建知识",
    meta: {
      title: "创建知识",
      hideInMenu: true,
    },
    component: () => import("@/view/knowledgeDetails/knowledgeAdd/knowledgeAdd.vue"),
  },
];

export const authenticateRoutes = {
  path: "/manager",
  name: "系统菜单",
  meta: {
    icon: "md-cog",
    title: "系统菜单",
  },
  component: Main,
  children: [
    {
      path: "dataAssets",
      name: "数据资产",
      component: Manager,
      meta: {
        title: "数据资产",
        icon: "dataAssets", // dataAssets、dataAssetsSelected
      },
      children: [
        {
          path: "dataAssets_catalogue",
          name: "目录",
          meta: {
            title: "目录",
          },
          component: () => import("@/view/report/dataAssets/Catalogue.vue"),
        },
        {
          path: "dataAssets_config",
          name: "配置",
          meta: {
            title: "配置",
          },
          component: () => import("@/view/report/dataAssets/assetConfig/index.vue"),
        },
      ],
    },
    {
      path: "story",
      name: "故事管理",
      component: Manager,
      meta: {
        title: "故事管理",
        icon: "ios-cog",
      },
      children: [
        {
          path: "storyManage",
          name: "故事管理",
          meta: {
            icon: "ios-cog",
            title: "故事管理",
          },
          component: () => import("@/view/indicatorWorkbench/indicatorWorkbenchIndex/components/StoryList/index.vue"),
        },
      ],
    },
    {
      path: "dimension",
      name: "维度管理",
      component: Manager,
      meta: {
        title: "维度管理",
        icon: "ios-cog",
      },
      children: [
        {
          path: "dimension_manage",
          name: "维度管理",
          meta: {
            icon: "ios-cog",
            title: "维度管理",
          },
          component: () => import("@/view/report/dimension/DimensionManage.vue"),
        },
        {
          path: "dimension_detail",
          name: "维度详情",
          meta: {
            icon: "ios-cog",
            title: "维度详情",
          },
          component: () => import("@/view/report/dimension/Dimension.vue"),
        },
      ],
    },
    {
      path: "businessModel",
      name: "业务模型管理",
      component: Manager,
      meta: {
        hideInBread: true,
        icon: "logo-codepen",
        title: "业务模型",
      },
      children: [
        {
          path: "businessModel_manage",
          name: "业务模型",
          meta: {
            icon: "ios-cube",
            title: "业务模型",
          },
          component: () => import("@/view/report/business-model/BusinessModelManage.vue"),
          // component: () => import('@/view/report/business-model/BusinessModel.vue')
        },
      ],
    },
    {
      path: "modelMapping_manage",
      name: "数据映射",
      component: Manager,
      children: [
        {
          path: "modelMapping_manage",
          name: "数据映射",

          meta: {
            icon: "ios-navigate",
            title: "数据映射",
          },
          component: () => import("@/view/modelMapping/index.vue"),
        },
      ],
    },
    {
      path: "indicator_browser",
      name: "即席分析",
      component: Manager,
      meta: {
        icon: "md-grid",
        title: "即席分析",
      },
      children: [
        {
          path: "indicator_browser",
          name: "即席分析",
          meta: {
            icon: "md-grid",
            title: "即席分析",
            notCache: true,
            hideInMenu: true,
          },
          component: () => import("@/view/report/indicator-browser"),
        },
      ],
    },
    {
      path: "indicator",
      name: "指标管理",
      component: Manager,
      meta: {
        icon: "ios-link",
        title: "指标管理",
      },
      children: [
        {
          path: "indicator_manage_new",
          name: "指标",
          meta: {
            icon: "ios-link",
            title: "指标管理新",
          },
          component: () => import("@/view/report/indicator/index.vue"),
        },
        {
          path: "indicator_detail",
          name: "指标",
          meta: {
            icon: "ios-link",
            title: "指标详情",
          },
          component: () => import("@/view/report/indicator/IndicatorDetail/index.vue"),
        },
      ],
    },
    {
      path: "templateManage",
      name: "报告管理",
      component: Manager,
      meta: {
        hideInBread: true,
        title: "报告管理",
        icon: "md-grid",
      },
      children: [
        {
          path: "template_list",
          name: "报告管理",
          meta: {
            icon: "md-grid",
            title: "报告管理",
            // hideInMenu: true
          },
          component: () => import("@/view/templateManages/reportManage/templateManage.vue"),
        },
        {
          path: "resourceManage",
          name: "资源管理",
          meta: {
            icon: "md-grid",
            title: "资源管理",
          },
          component: () => import("@/view/templateManages/resourceManage/ResourceManage.vue"),
        },
      ],
    },
    {
      path: "sys",
      name: "系统管理",
      meta: {
        icon: "md-cog",
        title: "系统管理",
      },
      component: Manager,
      children: [
        {
          path: "authorizationManagement",
          name: "权限管理", // 权限管理 名字无法改变 有配置
          meta: {
            icon: "md-people",
          },
          // component: () => import("@/view/system/Rulesmanagement/index.vue"), // 即将代码封装的路由
          // 真实路径
          component: () => import("@/view/system/authorizationManagement/index.vue"),
        },
        {
          path: "employee",
          name: "用户管理",
          meta: {
            icon: "md-person",
            title: "用户管理",
          },
          component: () => import("@/view/system/user/index.vue"),
        },
        {
          path: "organization",
          name: "部门管理",
          meta: {
            icon: "md-people",
            title: "部门管理",
          },
          component: () => import("@/view/system/organization/index.vue"),
        },
        {
          path: "role",
          name: "角色管理",
          meta: {
            icon: "md-people",
            title: "角色管理",
          },
          component: () => import("@/view/system/role/RoleList.vue"),
        },
        {
          path: "menu",
          name: "菜单管理",
          meta: {
            icon: "md-menu",
            title: "菜单管理",
          },
          component: () => import("@/view/system/menu/MenuList.vue"),
        },
        {
          path: "menuRole",
          name: "功能分配",
          meta: {
            icon: "md-people",
            title: "功能分配",
          },
          component: () => import("@/view/system/menuRole/MenuRoleList.vue"),
        },
        {
          path: "authentication",
          name: "权限分配",
          meta: {
            icon: "md-people",
            title: "权限分配",
          },
          component: () => import("@/view/system/resources/resourcesList.vue"),
        },
        {
          path: "template",
          name: "模板管理",
          meta: {
            icon: "md-person",
            title: "模板管理",
          },
          component: () => import("@/view/process/ProcessTemplate.vue"),
        },
        {
          path: "processList",
          name: "流程列表",
          meta: {
            icon: "md-person",
            title: "流程列表",
          },
          component: () => import("@/view/process/ProcessList.vue"),
        },
        {
          path: "cubeIndicator",
          name: "加速指标维护",
          meta: {
            icon: "md-person",
            title: "加速指标维护",
          },
          component: () => import("@/view/indicator-maintain"),
        },
        {
          path: "resourceBackup",
          name: "资源备份",
          meta: {
            icon: "md-person",
            title: "资源备份",
          },
          component: () => import("@/view/resourceBackup"),
        },
        {
          path: "workbenchManagement",
          name: "工作台管理",
          meta: {
            icon: "md-people",
          },
          component: () => import("@/view/system/workbenchManagement/index.vue"),
        },

        {
          path: "content",
          name: "内容管理",
          meta: {
            icon: "md-people",
          },
          component: () => import("@/view/contentManagement/index.vue"),
        },
      ],
    },
    {
      path: "databaseInfo",
      name: "数据库连接",
      meta: {
        icon: "md-cog",
        title: "数据库连接",
      },
      component: Manager,
      children: [
        {
          path: "connectionList",
          name: "数据库连接列表",
          meta: {
            icon: "md-person",
            title: "数据库连接列表",
          },
          component: () => import("@/view/databaseConnection/ConnectionInfoList.vue"),
        },
        {
          path: "add",
          name: "数据库编辑",
          meta: {
            icon: "md-person",
            title: "数据库编辑",
          },
          component: () => import("@/view/databaseConnection/ConnectionInfoAddOrEdit.vue"),
        },
      ],
    },
    {
      path: "dataset",
      name: "数据集",
      meta: {
        icon: "md-cog",
        title: "数据集",
      },
      component: Manager,
      children: [
        {
          path: "list",
          name: "数据集管理",
          meta: {
            icon: "md-person",
            title: "数据集管理",
          },
          component: () => import("@/view/dataset"),
        },
        {
          path: "datasetEdit",
          name: "数据集编辑",
          meta: {
            icon: "md-person",
            title: "数据集编辑",
          },
          component: () => import("@/view/dataset/Build/index.vue"),
        },
        {
          path: "view",
          name: "数据集查看",
          meta: {
            icon: "md-person",
            title: "数据集查看",
          },
          component: () => import("@/view/dataset/View/index.vue"),
        },
      ],
    },
    {
      path: "process",
      name: "流程管理",
      meta: {
        icon: "md-cog",
        title: "流程管理",
      },
      component: Manager,
      children: [],
    },
    {
      path: "assetPanorama",
      name: "资产全景图",
      component: Manager,
      meta: {
        title: "资产全景图",
      },
      children: [
        {
          path: "assetPanorama_manage",
          name: "资产全景图",
          meta: {
            title: "资产全景图",
          },
          component: () => import("@/view/assetPanorama/index.vue"),
        },
        {
          path: "assetPanorama_detail",
          name: "资产全景详情页",
          meta: {
            title: "资产全景详情页",
          },
          component: () => import("@/view/assetPanorama/details.vue"),
        },
      ],
    },
    {
      path: "indicatorHall",
      name: "指标大厅",
      component: Manager,
      meta: {
        title: "指标大厅",
      },
      children: [
        {
          path: "indicatorHall",
          name: "指标大厅",
          meta: {
            title: "指标大厅",
          },
          component: () => import("@/view/indicator-hall/indicatorHall.vue"),
        },
      ],
    },
    {
      path: "insupermarket",
      name: "指标超市",
      component: Manager,
      meta: {
        title: "指标超市",
      },
      children: [
        {
          path: "insupermarket",
          name: "指标超市",
          meta: {
            title: "指标超市",
          },
          component: () => import("@/view/indicator-hall/insupermarket.vue"),
        },
      ],
    },
    {
      path: "objectManage",
      name: "目标管理",
      component: Manager,
      meta: {
        title: "目标管理",
        icon: "ios-cog",
      },
      children: [
        {
          path: "objectManage",
          name: "目标管理",
          meta: {
            title: "目标管理",
            icon: "ios-cog",
          },
          component: () => import("@/view/objectManage/index.vue"),
        },
      ],
    },
    {
      // 因为路由没有配置所以现在这个路由无法注册使用
      path: "RulesManagement",
      name: "规则管理",
      component: Manager,
      meta: {
        title: "规则管理",
        icon: "ios-cog",
      },
      children: [
        {
          path: "RulesManagement",
          name: "规则管理",
          meta: {
            title: "规则管理",
            icon: "ios-cog",
          },
          component: () => import("@/view/Rulesmanagement/index.vue"), // 即将代码封装的路由
          // component: () => import("@/view/objectManage/index.vue"),
        },
      ],
    },
    {
      path: "earlyWarningTarget",
      name: "预警中心",
      component: Manager,
      meta: {
        title: "预警中心",
        icon: "ios-cog",
      },
      children: [
        {
          path: "earlyWarningTargetIndex",
          name: "预警中心",
          meta: {
            icon: "ios-cog",
            title: "预警中心",
          },
          component: () => import("@/view/earlyWarningTargetNew/index.vue"),
        },
      ],
    },
    {
      path: "taskManage",
      name: "任务管理",
      component: Manager,
      meta: {
        title: "任务管理",
        icon: "ios-cog",
      },
      children: [
        {
          path: "taskManage",
          name: "任务管理",
          meta: {
            title: "任务管理",
          },
          component: () => import("@/view/taskManage/index.vue"),
        },
      ],
    },
    {
      path: "knowledge",
      name: "知识库",
      component: Manager,
      meta: {
        title: "知识库",
      },
      children: [
        {
          path: "knowledge",
          name: "知识库",
          meta: {
            title: "知识库",
            hideInMenu: true,
          },
          component: () => import("@/view/knowledgeDetails/index.vue"),
        },
        {
          path: "knowledgeAdd",
          name: "创建知识",
          meta: {
            title: "创建知识",
            hideInMenu: true,
          },
          component: () => import("@/view/knowledgeDetails/knowledgeAdd/knowledgeAdd.vue"),
        },
        {
          path: "knowledgeDetail",
          name: "知识详情",
          meta: {
            title: "知识详情",
            hideInMenu: true,
          },
          component: () => import("@/view/knowledgeDetails/knowledgeDetail/knowledgeDetail.vue"),
        },
      ],
    },
  ],
};

// 需要权限，且放在头部菜单
export const authMenuRoutes = {
  path: "/",
  component: Main,
  children: [
    {
      path: "indicatorWorkbench",
      name: "指标工作台",
      component: () => import("@/view/indicatorWorkbench/index.vue"),
      children: [
        {
          path: "indicatorWorkbenchIndex",
          name: "指标工作台",
          component: () => import("@/view/indicatorWorkbench/indicatorWorkbenchIndex/index.vue"),
        },
        {
          path: "indicatorWorkbenchDetail",
          name: "指标工作台详情",
          meta: {
            icon: "ios-link",
            title: "指标工作台详情",
          },
          component: () => import("@/view/indicatorWorkbench/indicatorWorkbenchDetail/index.vue"),
        },
        {
          path: "storyBoard",
          name: "故事看板",
          meta: {
            icon: "ios-link",
            title: "故事看板",
          },
          component: () => import("@/view/indicatorWorkbench/storyBoard/index.vue"),
        },
        {
          path: "storyBoardAdd",
          name: "故事看板新建",
          meta: {
            icon: "ios-link",
            title: "故事看板新建",
          },
          component: () => import("@/view/indicatorWorkbench/storyBoardAdd/index.vue"),
        },
      ],
    },
    {
      path: "/earlyWarningTarget",
      name: "预警中心",
      component: () => import("@/view/earlyWarningTargetNew/index.vue"),
      children: [
        {
          path: "earlyWarningTargetList",
          name: "预警中心",
          component: () => import("@/view/earlyWarningTargetNew/index.vue"),
        },
      ],
    },
    {
      path: "publicResourcePool",
      name: "top_公共资源池",
      component: () => import("@/view/publicResourcePool/index.vue"),
    },
    {
      path: "assetPanorama_manage",
      name: "top_资产全景图",
      component: () => import("@/view/assetPanorama/index.vue"),
    },
    {
      path: "assetPanorama_detail",
      name: "资产全景详情页",
      meta: {
        title: "资产全景详情页",
      },
      component: () => import("@/view/assetPanorama/details.vue"),
    },
    {
      path: "knowledge",
      name: "top_知识库",
      component: () => import("@/view/knowledgeDetails/index.vue"),
    },
    {
      path: "knowledgeDetail",
      name: "知识详情",
      meta: {
        title: "知识详情",
      },
      component: () => import("@/view/knowledgeDetails/knowledgeDetail/knowledgeDetail.vue"),
    },
    {
      path: "RulesManagement",
      name: "top_规则管理",
      meta: {
        title: "top_规则管理",
      },
      component: () => import("@/view/Rulesmanagement/index.vue"),
    },
    {
      path: "objectManage",
      name: "top_目标管理",
      component: () => import("@/view/objectManage/index.vue"),
    },
    {
      path: "taskManage",
      name: "top_任务管理",
      component: () => import("@/view/taskManage/index.vue"),
    },
    {
      path: "earlyWarningTargetList",
      name: "top_预警中心",
      component: () => import("@/view/earlyWarningTargetNew/index.vue"),
    },
    {
      path: "indicatorWorkbenchIndex",
      name: "top_指标工作台",
      component: () => import("@/view/indicatorWorkbench/indicatorWorkbenchIndex/index.vue"),
    },
    {
      path: "indicator_browser",
      name: "top_即席分析",
      component: () => import("@/view/report/indicator-browser"),
    },
    {
      path: "indicatorHall",
      name: "top_指标大厅",
      component: () => import("@/view/indicator-hall/indicatorHall.vue"),
    },
    {
      path: "insupermarket",
      name: "top_报表超市",
      component: () => import("@/view/indicator-hall/insupermarket.vue"),
    },
  ],
};
