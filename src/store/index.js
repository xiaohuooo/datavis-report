import Vue from 'vue'
import Vuex from 'vuex'
import user from './module/user'
import app from './module/app'
import chart from './module/chart'
import realTime from './module/real-time'
import resourceManage from './module/resourceManage'
import indicator from './module/indicator'
import dimension from './module/dimension'
import indicatorWorkbench from './module/indicatorWorkbench'
import dataset from './module/dataset'
import story from './module/story'
import earlyWarningTarget from './module/earlyWarningTarget'
import authorizationManagement from '@/store/module/authorizationManagement'
import task from './module/task'
import contentManage from './module/contentManage'
import reasonAnalysis from "./module/reasonAnalysis";
import intelligentAnswer from "./module/intelligentAnswer"
import department from '@/store/module/system/department'
import role from '@/store/module/system/role'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    Authorization: '',
  },
  mutations: {
    set_token(state, Authorization) {
      state.Authorization = Authorization.Authorization
      sessionStorage.Authorization = Authorization.Authorization
    },
  },
  modules: {
    user,
    app,
    chart,
    realTime,
    resourceManage,
    indicator,
    dimension,
    indicatorWorkbench,
    dataset,
    story,
    earlyWarningTarget,
    authorizationManagement,
    task,
    contentManage,
    reasonAnalysis,
    intelligentAnswer,
    department,
    role
  },
})
