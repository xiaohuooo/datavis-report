import {
  getHomeRoute,
  getRouteTitleHandled,
  getTagNavListFromLocalstorage,
  routeHasExist,
  setTagNavListInLocalstorage
} from '@/libs/util'
import { buildMenus, getDynamicRoutes } from '@/utils/RouterUtil'
import router from '@/router'
import { authenticateRoutes, authMenuRoutes } from '@/router/routers'
import config from '@/config'
import { ROUTER_MENU_TYPE} from '@/config/MenuConfig'
const { homeName } = config

export default {
  state: {
    tagNavList: [],
    iframeMenus: [],
    systemMenus: [], // 需要权限的菜单路由
    businessMenus: [], // 顶部业务菜单路由
    topAuthMenus: [], // 顶部权限菜单路由
    homeRoute: {},
    hasSystemAuth: false,
    dataIsDeleted: false // (指标/故事)是否被删除
  },
  getters: {
    businessMenus: state => state.businessMenus,
    systemMenus: state => state.systemMenus,
    hasSystemAuth: state => state.hasSystemAuth
  },
  mutations: {
    setDynamicRoute(state, menus) {
      const [systemMenus, businessMenus] = menus
      const topAuthMenus = businessMenus.filter(menu => menu.type == ROUTER_MENU_TYPE)
      const authSystemMenus = systemMenus.filter(
        ele => !ele.path.includes("indicatorWorkbench") && !ele.path.includes("earlyWarningTargetList") && !ele.path.includes("publicResourcePool")
      );
      state.businessMenus = buildMenus(businessMenus, false)
      // console.log('businessMenus=========' , state.businessMenus)
      state.topAuthMenus = buildMenus(topAuthMenus, false)
      // console.log('state.topAuthMenus=========' , state.topAuthMenus)
      const newAuthSystemMenus = buildMenus(authSystemMenus)
      // console.log('systemMenus========', newAuthSystemMenus)
      state.systemMenus = newAuthSystemMenus
      state.hasSystemAuth = newAuthSystemMenus.length > 0

      let authMenus = systemMenus.concat(businessMenus.filter(item => item.type === ROUTER_MENU_TYPE))
      // 向router添加动态路由-顶部权限
      const authMenuRoutesArr = getDynamicRoutes(authMenuRoutes, authMenus) || []
      router.addRoutes(authMenuRoutesArr)
      // 向router添加动态路由-左侧权限
      let dynamicRoutes = getDynamicRoutes(authenticateRoutes, authMenus) || []
      dynamicRoutes.push({
        path: '*',
        name: 'error_401',
        meta: {
          hideInMenu: true
        },
        component: () => import('@/view/error-page/401.vue')
      })
      router.addRoutes(dynamicRoutes)
    },
    setHomeRoute(state, routes) {
      state.homeRoute = getHomeRoute(routes, homeName)
    },
    setTagNavList(state, list) {
      let tagList = []
      if (list) {
        tagList = [...list]
      } else {
        tagList = getTagNavListFromLocalstorage() || []
      }
      if (tagList[0] && tagList[0].name !== homeName) tagList.shift()
      let homeTagIndex = tagList.findIndex(item => item.name === homeName)
      if (homeTagIndex > 0) {
        let homeTag = tagList.splice(homeTagIndex, 1)[0]
        tagList.unshift(homeTag)
      }
      state.tagNavList = tagList
      setTagNavListInLocalstorage([...tagList])
    },
    addTag(state, { route, type = 'unshift' }) {
      let router = getRouteTitleHandled(route)
      if (!routeHasExist(state.tagNavList, router)) {
        if (type === 'push') {
          state.tagNavList.push(router)
        } else {
          if (router.name === homeName) {
            state.tagNavList.unshift(router)
          } else {
            state.tagNavList.splice(1, 0, router)
          }
        }
        setTagNavListInLocalstorage([...state.tagNavList])
      }
    },
    setDataIsDeleted(state, payload) {
      state.dataIsDeleted = payload;
    },
  },
  actions: {
    /**
     * 设置动态路由
     */
    setDynamicRoute({ commit, state }, menu) {
      commit("setDynamicRoute", menu);
    }
  }
};
