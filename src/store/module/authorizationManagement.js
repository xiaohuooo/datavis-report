import { AUTHORIZATION_MANAGEMENT } from '../mutation-types'
import {
  getDepartmentDirectoryByResourceId,
  getDirectoryByDeptId,
  getDirectoryByRoleId,
  getDirectoryByUserId,
  getDirectoryListByDomainId,
  getRoleDirectoryByResourceId,
  getDomainResource
} from '@/api/systemManagement/authorizationManagement'

const state = {
  viewDimension: 'user', // 查看维度 user-用户组 project-资源项目
  departmentList: [], // 部门列表
  roleList: [], // 角色列表
  userList: [], // 用户列表
  userCount: 0, // 用户总数
  userListPageNum: 1, // 用户列表页码
  userListPageSize: 20, // 用户列表每页条数
  userListKeywords: '', // 用户列表搜索关键字
  totalBusinessDirectoryList: [], // 总业务目录列表--设置
  totalSystemDirectoryList: [], // 总系统管理列表--设置
  totalPersonnelDepartmentDirectoryList: [], // 总人员管理部门列表--设置
  totalPersonnelRoleDirectoryList: [], // 总人员管理角色列表--设置
  reportManagerList: [], // 报告管理目录
  quotaManagerList: [], // 指标管理目录
  dimensionManagerList: [], // 维度管理目录
  contentManagerList: [], // 内容管理目录
  assetsManagerList: [], // 数据资产目录
  dataAuthTab: "5", // 数据权限tab
  rightTabKeywords: '', // 右侧tab搜索关键字
  domainId: '', // 查看维度为用户组-左侧tab选择的实体
  leftTab: 'department', // 左侧选中的tab
  superAdmin: false, // 角色是否是超管，超管无法操作权限，置灰
  userListDeptIds: '', // 用于筛选用户的部门ids
  resourceId: '', // 查看维度为资源项目-左侧tab选择的资源
  businessDirectoryList: [], // 业务目录列表
  totalDepartmentList: [], // 总部门列表--设置
  totalRoleList: [], // 总角色列表-设置
  rightTab: 'businessDirectory', // 右侧选中的tab
  authorizedItem: {}, // 需要被赋权的项
  showAuthSourceModal: false, // 是否展示权限来源弹窗
  personManageType: null, // 人员管理中的类型 1-部门 2-角色
  blackList: [], // 权限黑名单，如果有继承权限，被用户关闭了，则放入黑名单
  blackDept: [], // 人员管理-部门 权限黑名单
  blackRole: [], // 人员管理-角色 权限黑名单
}

const actions = {
  /**
   * 设置查看维度
   * @param commit
   * @param payload
   */
  setViewDimension({ commit }, payload) {
    commit(AUTHORIZATION_MANAGEMENT.SET_VIEW_DIMENSION, payload)
  },
  /**
   * 获取部门列表及业务目录、系统管理列表
   * @param commit
   * @param state
   * @param deptId 部门id
   * @returns {Promise<void>}
   */
  async getDepartmentListAndDirectoryList({ commit, state }, deptId = '') {
    const res = await getDirectoryByDeptId(deptId)
    if (res.data.state === 'success') {
      let { totalDomain, totalResource, currentResource } = res.data.data

      // 设置角色不为超管
      commit(AUTHORIZATION_MANAGEMENT.SET_SUPER_ADMIN, false)

      // 设置部门列表
      if (!state.departmentList.length) {
        // 避免刷新列表后，导致选中效果丢失
        totalDomain.sort((a, b) => a.id - b.id)
        commit(AUTHORIZATION_MANAGEMENT.SET_DEPARTMENT_LIST, totalDomain)
      }

      // 设置目录列表
      setDirectoryList(totalResource, currentResource, commit)
    } else {
      this.$Message.error('获取部门列表及业务目录、系统管理列表失败！')
    }
  },
  /**
   * 获取角色列表及业务目录、系统管理列表
   * @param commit
   * @param state
   * @param roleId 角色id
   * @returns {Promise<void>}
   */
  async getRoleListAndDirectoryList({ commit, state }, roleId = '') {
    const res = await getDirectoryByRoleId(roleId)
    if (res.data.state === 'success') {
      let { superAdmin, roleGroup, totalDomain, totalResource, currentResource } = res.data.data

      // 设置角色是否为超管
      commit(AUTHORIZATION_MANAGEMENT.SET_SUPER_ADMIN, superAdmin)

      // 设置角色列表
      if (!state.roleList.length) {
        // 避免刷新列表后，导致选中效果丢失
        roleGroup.sort((a, b) => a.id - b.id)
        totalDomain.sort((a, b) => a.id - b.id)
        let roleList = [
          ...roleGroup.map(item => ({
            ...item,
            groupId: -1,
            type: 'group',
          })),
          ...totalDomain,
        ]
        commit(AUTHORIZATION_MANAGEMENT.SET_ROLE_LIST, roleList)
      }

      // 设置目录列表
      setDirectoryList(totalResource, currentResource, commit)
    } else {
      this.$Message.error('获取角色列表及业务目录、系统管理列表失败！')
    }
  },
  /**
   * 获取用户列表及业务目录、系统管理列表
   * @param commit
   * @param state
   * @param params 参数 id-用户id  pageNum-页码  pageSize-分页大小
   * @returns {Promise<void>}
   */
  async getUserListAndDirectoryList({ commit, state }, userId = '') {
    const res = await getDirectoryByUserId({
      id: userId,
      name: state.userListKeywords,
      deptIds: state.userListDeptIds,
      pageNum: state.userListPageNum,
      pageSize: state.userListPageSize,
    })
    if (res.data.state === 'success') {
      const { superAdmin, users, totalResource, currentResource, blackList } = res.data.data

      // 设置用户是否为超管
      commit(AUTHORIZATION_MANAGEMENT.SET_SUPER_ADMIN, superAdmin)

      commit(AUTHORIZATION_MANAGEMENT.SET_USER_LIST, users.data)
      commit(AUTHORIZATION_MANAGEMENT.SET_USER_COUNT, users.count)

      // 设置目录列表
      setDirectoryList(totalResource, currentResource || [], commit)

      // 设置黑名单
      if (blackList) {
        commit(AUTHORIZATION_MANAGEMENT.SET_BLACK_LIST, blackList)
      }
    } else {
      this.$Message.error('获取用户列表及业务目录、系统管理列表失败！')
    }
  },
  /**
   * 根据部门、角色、用户实体id获取人员管理目录
   * @param commit
   * @param state
   */
  async getPersonnelManagementDirectoryList({ commit, state }) {
    let type
    if (state.leftTab === 'department') {
      type = 1
    } else if (state.leftTab === 'role') {
      type = 2
    } else {
      type = 3
    }
    const res = await getDirectoryListByDomainId(state.domainId, type)
    if (res.data.state === 'success') {
      let { superAdmin, totalDept, currentDept, roleGroup, totalRole, currentRole, blackDept, blackRole } = res.data.data

      // 设置用户是否为超管
      commit(AUTHORIZATION_MANAGEMENT.SET_SUPER_ADMIN, superAdmin)

      // 列表排序，防止每次的列表顺序都不一致
      totalDept.sort((a, b) => a.id - b.id)
      totalRole.sort((a, b) => a.id - b.id)
      roleGroup.sort((a, b) => a.id - b.id)

      // 设置目录列表
      setPersonnelManagementDirectoryList(totalDept, currentDept || [], roleGroup, totalRole, currentRole || [], commit, state)

      // 设置黑名单
      if (blackDept) {
        commit(AUTHORIZATION_MANAGEMENT.SET_BLACK_DEPT, blackDept)
      }
      if (blackDept) {
        commit(AUTHORIZATION_MANAGEMENT.SET_BLACK_ROLE, blackRole)
      }
    } else {
      this.$Message.error('获取人员管理目录失败！')
    }
  },
  /**
 * 根据部门、角色、用户实体id获取数据权限目录
 * @param commit
 * @param state
 */
  async getDomainResourceList({ commit, state }, userId = '') {
    let type
    if (state.leftTab === 'department') {
      type = 1
    } else if (state.leftTab === 'role') {
      type = 2
    } else {
      type = 3
    }
    const res = await getDomainResource({ domainId: state.domainId, domainType: type, moduleType: state.dataAuthTab })
    if (res.data.state === 'success') {
      let { superAdmin, authModuleResource, groupList, moduleResourceList, canAuth } = res.data.data

      // 设置用户是否为超管
      commit(AUTHORIZATION_MANAGEMENT.SET_SUPER_ADMIN, superAdmin)

      let totalData = state.dataAuthTab !== '17' && groupList ? [...groupList, ...moduleResourceList] : [...moduleResourceList]
      // // 列表排序，防止每次的列表顺序都不一致
      totalData.sort((a, b) => a.id - b.id)
      for (let totalItem of totalData) {
        let current = authModuleResource.find(currItem => currItem.resId === totalItem.id)
        totalItem.controlView = current ? current.controlView : false
        totalItem.controlEdit = current ? current.controlEdit : false
        //登录人权限
        let record = canAuth.find(currItem => currItem.resId === totalItem.id)
        totalItem.controlViewLogin = record ? record.controlView : false
        totalItem.controlEditLogin = record ? record.controlEdit : false
        //若选择的角色为当前创建人，则此权限无权操作
        if (state.domainId && totalItem.creatorId && state.domainId === totalItem.creatorId) {
          totalItem.isCreator = true
          totalItem.controlView = true
          totalItem.controlEdit = true
        }
      }
      // // 设置目录列表
      let name = AUTHORIZATION_MANAGEMENT.SET_REPORT_MANAGER_LIST
      if (state.dataAuthTab === "1") {
        name = AUTHORIZATION_MANAGEMENT.SET_DIMENSION_MANAGER_LIST
      } else if (state.dataAuthTab === "20") {
        name = AUTHORIZATION_MANAGEMENT.SET_QUOTA_MANAGER_LIST
      } else if (state.dataAuthTab === "16") {
        name = AUTHORIZATION_MANAGEMENT.SET_CONTENT_MANAGER_LIST
      } else if (state.dataAuthTab === "17") {
        name = AUTHORIZATION_MANAGEMENT.SET_ASSETS_MANAGER_LIST
      }
      commit(name, totalData)
    } else {
      this.$Message.error('获取数据权限目录失败！')
    }
  },
  /**
   * 设置右侧tab搜索关键字
   * @param commit
   * @param payload
   */
  setRightTabKeywords({ commit }, payload) {
    commit(AUTHORIZATION_MANAGEMENT.SET_RIGHT_TAB_KEYWORDS, payload)
  },
  /**
   * 设置左侧tab选择的实体
   * @param commit
   * @param payload
   */
  setDomainId({ commit }, payload) {
    commit(AUTHORIZATION_MANAGEMENT.SET_DOMAIN_ID, payload)
  },
  /**
   * 设置左侧当前tab
   * @param commit
   * @param payload
   */
  setLeftTab({ commit }, payload) {
    commit(AUTHORIZATION_MANAGEMENT.SET_LEFT_TAB, payload)
  },
  /**
   * 设置用户列表页码
   * @param commit
   * @param payload
   */
  setUserListPageNum({ commit }, payload) {
    commit(AUTHORIZATION_MANAGEMENT.SET_USER_LIST_PAGE_NUM, payload)
  },
  /**
   * 设置用户列表搜索关键字
   * @param commit
   * @param payload
   */
  setUserListKeywords({ commit }, payload) {
    commit(AUTHORIZATION_MANAGEMENT.SET_USER_LIST_KEYWORDS, payload)
  },
  /**
   * 设置用于筛选用户的部门ids
   * @param commit
   * @param payload
   */
  setUserListDeptIds({ commit }, payload) {
    commit(AUTHORIZATION_MANAGEMENT.SET_USER_LIST_DEPT_IDS, payload)
  },
  /**
   * 设置左侧tab选择的资源
   * @param commit
   * @param payload
   */
  setResourceId({ commit }, payload) {
    commit(AUTHORIZATION_MANAGEMENT.SET_RESOURCE_ID, payload)
  },
  /**
   * 获取业务目录列表及部门列表
   * @param commit
   * @param state
   */
  async getBusinessDirectoryListAndDepartmentList({ commit, state }) {
    const res = await getDepartmentDirectoryByResourceId(state.resourceId)
    if (res.data.state === 'success') {
      let { totalResource, totalDomain, currentDomain } = res.data.data

      // 设置业务目录
      setBusinessDirectory(totalResource, commit)

      // 列表排序，防止每次的列表顺序都不一致
      totalDomain.sort((a, b) => a.id - b.id)

      // 设置部门列表
      setDepartmentList(totalDomain, currentDomain, commit)
    } else {
      this.$Message.error('获取业务目录列表及部门列表失败！')
    }
  },
  /**
   * 获取业务目录列表及角色列表
   * @param commit
   * @param state
   */
  async getBusinessDirectoryListAndRoleList({ commit, state }) {
    const res = await getRoleDirectoryByResourceId(state.resourceId)
    if (res.data.state === 'success') {
      let { roleGroup, totalResource, totalDomain, currentDomain } = res.data.data

      // 列表排序，防止每次的列表顺序都不一致
      roleGroup.sort((a, b) => a.id - b.id)
      totalDomain.sort((a, b) => a.id - b.id)

      // 设置业务目录
      setBusinessDirectory(totalResource, commit)

      // 设置角色列表
      setRoleList(roleGroup, totalDomain, currentDomain, commit)
    } else {
      this.$Message.error('获取业务目录列表及部门列表失败！')
    }
  },
  /**
   * 设置右侧当前tab
   * @param commit
   * @param payload
   */
  setRightTab({ commit }, payload) {
    commit(AUTHORIZATION_MANAGEMENT.SET_RIGHT_TAB, payload)
  },

  /**
 * 设置数据权限当前tab
 * @param commit
 * @param payload
 */
  setDataAuthTab({ commit }, payload) {
    commit(AUTHORIZATION_MANAGEMENT.SET_DATA_AUTH_TAB, payload)
  },

  setAuthorizedItem({ commit }, payload) {
    commit(AUTHORIZATION_MANAGEMENT.SET_AUTHORIZED_ITEM, payload);
  },

  setShowAuthSourceModal({ commit }, payload) {
    commit(AUTHORIZATION_MANAGEMENT.SET_SHOW_AUTH_SOURCE_MODAL, payload);
  },

  setPersonManageType({ commit }, payload) {
    commit(AUTHORIZATION_MANAGEMENT.SET_PERSON_MANAGE_TYPE, payload);
  },

  /**
   * 清空权限管理
   * @param commit
   */
  clearAuthManagement({ commit }) {
    commit(AUTHORIZATION_MANAGEMENT.SET_VIEW_DIMENSION, 'user')
    commit(AUTHORIZATION_MANAGEMENT.SET_DEPARTMENT_LIST, [])
    commit(AUTHORIZATION_MANAGEMENT.SET_ROLE_LIST, [])
    commit(AUTHORIZATION_MANAGEMENT.SET_USER_LIST, [])
    commit(AUTHORIZATION_MANAGEMENT.SET_USER_COUNT, 0)
    commit(AUTHORIZATION_MANAGEMENT.SET_USER_LIST_PAGE_NUM, 1)
    commit(AUTHORIZATION_MANAGEMENT.SET_USER_LIST_KEYWORDS, '')
    commit(AUTHORIZATION_MANAGEMENT.SET_TOTAL_BUSINESS_DIRECTORY_LIST, [])
    commit(AUTHORIZATION_MANAGEMENT.SET_TOTAL_SYSTEM_DIRECTORY_LIST, [])
    commit(AUTHORIZATION_MANAGEMENT.SET_TOTAL_PERSONNEL_DEPARTMENT_DIRECTORY_LIST, [])
    commit(AUTHORIZATION_MANAGEMENT.SET_TOTAL_PERSONNEL_ROLE_DIRECTORY_LIST, [])
    commit(AUTHORIZATION_MANAGEMENT.SET_REPORT_MANAGER_LIST, [])
    commit(AUTHORIZATION_MANAGEMENT.SET_QUOTA_MANAGER_LIST, [])
    commit(AUTHORIZATION_MANAGEMENT.SET_DIMENSION_MANAGER_LIST, [])
    commit(AUTHORIZATION_MANAGEMENT.SET_RIGHT_TAB_KEYWORDS, '')
    commit(AUTHORIZATION_MANAGEMENT.SET_DOMAIN_ID, '')
    commit(AUTHORIZATION_MANAGEMENT.SET_LEFT_TAB, 'department')
    commit(AUTHORIZATION_MANAGEMENT.SET_SUPER_ADMIN, false)
    commit(AUTHORIZATION_MANAGEMENT.SET_USER_LIST_DEPT_IDS, '')
    commit(AUTHORIZATION_MANAGEMENT.SET_RESOURCE_ID, '')
    commit(AUTHORIZATION_MANAGEMENT.SET_BUSINESS_DIRECTORY_LIST, [])
    commit(AUTHORIZATION_MANAGEMENT.SET_TOTAL_DEPARTMENT_LIST, [])
    commit(AUTHORIZATION_MANAGEMENT.SET_TOTAL_ROLE_LIST, [])
    commit(AUTHORIZATION_MANAGEMENT.SET_RIGHT_TAB, 'department')
  },
}

const mutations = {
  [AUTHORIZATION_MANAGEMENT.SET_VIEW_DIMENSION](state, payload) {
    state.viewDimension = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_DEPARTMENT_LIST](state, payload) {
    state.departmentList = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_ROLE_LIST](state, payload) {
    state.roleList = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_USER_LIST](state, payload) {
    state.userList = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_USER_COUNT](state, payload) {
    state.userCount = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_TOTAL_BUSINESS_DIRECTORY_LIST](state, payload) {
    state.totalBusinessDirectoryList = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_TOTAL_SYSTEM_DIRECTORY_LIST](state, payload) {
    state.totalSystemDirectoryList = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_TOTAL_PERSONNEL_DEPARTMENT_DIRECTORY_LIST](state, payload) {
    state.totalPersonnelDepartmentDirectoryList = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_TOTAL_PERSONNEL_ROLE_DIRECTORY_LIST](state, payload) {
    state.totalPersonnelRoleDirectoryList = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_REPORT_MANAGER_LIST](state, payload) {
    state.reportManagerList = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_QUOTA_MANAGER_LIST](state, payload) {
    state.quotaManagerList = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_DIMENSION_MANAGER_LIST](state, payload) {
    state.dimensionManagerList = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_RIGHT_TAB_KEYWORDS](state, payload) {
    state.rightTabKeywords = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_DOMAIN_ID](state, payload) {
    state.domainId = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_LEFT_TAB](state, payload) {
    state.leftTab = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_SUPER_ADMIN](state, payload) {
    state.superAdmin = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_USER_LIST_PAGE_NUM](state, payload) {
    state.userListPageNum = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_USER_LIST_KEYWORDS](state, payload) {
    state.userListKeywords = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_USER_LIST_DEPT_IDS](state, payload) {
    state.userListDeptIds = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_RESOURCE_ID](state, payload) {
    state.resourceId = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_BUSINESS_DIRECTORY_LIST](state, payload) {
    state.businessDirectoryList = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_TOTAL_DEPARTMENT_LIST](state, payload) {
    state.totalDepartmentList = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_TOTAL_ROLE_LIST](state, payload) {
    state.totalRoleList = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_RIGHT_TAB](state, payload) {
    state.rightTab = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_BLACK_LIST](state, payload) {
    state.blackList = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_BLACK_DEPT](state, payload) {
    state.blackDept = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_BLACK_ROLE](state, payload) {
    state.blackRole = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_DATA_AUTH_TAB](state, payload) {
    state.dataAuthTab = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_AUTHORIZED_ITEM](state, payload) {
    state.authorizedItem = payload;
  },
  [AUTHORIZATION_MANAGEMENT.SET_SHOW_AUTH_SOURCE_MODAL](state, payload) {
    state.showAuthSourceModal = payload;
  },
  [AUTHORIZATION_MANAGEMENT.SET_PERSON_MANAGE_TYPE](state, payload) {
    state.personManageType = payload;
  },
  [AUTHORIZATION_MANAGEMENT.SET_CONTENT_MANAGER_LIST](state, payload) {
    state.contentManagerList = payload
  },
  [AUTHORIZATION_MANAGEMENT.SET_ASSETS_MANAGER_LIST](state, payload) {
    state.assetsManagerList = payload
  },
}

/**
 * 设置业务目录、系统管理
 * @param totalResource
 * @param currentResource
 * @param commit
 */
const setDirectoryList = (totalResource, currentResource, commit) => {
  // 重置canView,canAuth
  for (let totalItem of totalResource) {
    let current = currentResource.find(currItem => currItem.id === totalItem.id)
    totalItem.canView = current ? current.canView : false
    totalItem.canAuth = current ? current.canAuth : false
    totalItem.viewFromOther = current ? !!current.viewFromOther : false
    totalItem.authFromOther = current ? !!current.authFromOther : false
  }
  totalResource.sort((a, b) => a.id - b.id)
  const totalBusinessDirectoryList = totalResource.filter(item => ![4, 6].includes(item.type))
  const totalSystemDirectoryList = totalResource.filter(item => [4, 6].includes(item.type))
  commit(AUTHORIZATION_MANAGEMENT.SET_TOTAL_BUSINESS_DIRECTORY_LIST, totalBusinessDirectoryList)
  commit(AUTHORIZATION_MANAGEMENT.SET_TOTAL_SYSTEM_DIRECTORY_LIST, totalSystemDirectoryList)
}

/**
 * 设置人员管理
 * @param totalDept
 * @param currentDept
 * @param totalRole
 * @param currentRole
 * @param commit
 */
const setPersonnelManagementDirectoryList = (totalDept, currentDept, roleGroup, totalRole, currentRole, commit, state) => {
  // 重置canAuth
  for (let totalItem of totalDept) {
    let current = currentDept.find(currItem => currItem.id === totalItem.id)
    if (state.leftTab === 'user') {
      totalItem.canAuth = current ? current.auth : false
      totalItem.authFromOther = current ? current.authFromOther : false
    } else {
      totalItem.canAuth = !!current
    }
  }

  for (let totalItem of totalRole) {
    let current = currentRole.find(currItem => currItem.id === totalItem.id)
    if (state.leftTab === 'user') {
      totalItem.canAuth = current ? current.auth : false
      totalItem.authFromOther = current ? current.authFromOther : false
    } else {
      totalItem.canAuth = !!current
    }
  }
  let roleList = [
    ...roleGroup.map(item => ({
      ...item,
      groupId: -1,
      type: 'group',
    })),
    ...totalRole,
  ]

  commit(AUTHORIZATION_MANAGEMENT.SET_TOTAL_PERSONNEL_DEPARTMENT_DIRECTORY_LIST, totalDept)
  commit(AUTHORIZATION_MANAGEMENT.SET_TOTAL_PERSONNEL_ROLE_DIRECTORY_LIST, roleList)
}

/**
 * 设置部门列表
 * @param totalDomain
 * @param currentDomain
 * @param commit
 */
const setDepartmentList = (totalDomain, currentDomain, commit) => {
  // 重置canView,canAuth
  for (let totalItem of totalDomain) {
    let current = currentDomain.find(currItem => currItem.id === totalItem.id)
    totalItem.canView = current ? current.canView : false
    totalItem.canAuth = current ? current.canAuth : false
  }
  commit(AUTHORIZATION_MANAGEMENT.SET_TOTAL_DEPARTMENT_LIST, totalDomain)
}

/**
 * 设置角色列表
 * @param roleGroup
 * @param totalDomain
 * @param currentDomain
 * @param commit
 */
const setRoleList = (roleGroup, totalDomain, currentDomain, commit) => {
  // 重置canAuth
  for (let totalItem of totalDomain) {
    let current = currentDomain.find(currItem => currItem.id === totalItem.id)
    totalItem.canView = current ? current.canView : false
    totalItem.canAuth = current ? current.canAuth : false
    totalItem.viewFromOther = false // 因为接口没有返回viewFromOther字段，所以都置为false
  }
  totalDomain.sort((a, b) => a.id - b.id)
  let roleList = [
    ...roleGroup.map(item => ({
      ...item,
      groupId: -1,
      type: 'group',
    })),
    ...totalDomain,
  ]
  commit(AUTHORIZATION_MANAGEMENT.SET_TOTAL_ROLE_LIST, roleList)
}

/**
 * 设置业务目录
 * @param totalResource
 * @param commit
 */
const setBusinessDirectory = (totalResource, commit) => {
  // 列表排序，防止每次的列表顺序都不一致
  totalResource.sort((a, b) => a.id - b.id)
  // 设置业务目录列表
  if (!state.businessDirectoryList.length) {
    // 避免刷新列表后，导致选中效果丢失
    totalResource = totalResource.filter(item => ![4, 6].includes(item.type))
    commit(AUTHORIZATION_MANAGEMENT.SET_BUSINESS_DIRECTORY_LIST, totalResource)
  }
}

/**
 *
 * @param parent
 * @param allData
 */
const recursiveIndicatorGroup = (parent, allData, deep = 1) => {
  parent.forEach(item => {
    item.deep = deep
    let children = allData
      .filter(d => {
        if (d.parentId) {
          return d.parentId == item.id
        } else if (d.groupId) {
          return d.groupId == item.id
        }
      }).map((child, i) => {
        if (child.parentId) {
          return {
            id: child.id,
            title: child.name,
            dataType: 1,
            expand: child.expand,
            disabled: true,
            treeType: 'group',
          }
        } else if (child.groupId) {
          return {
            id: child.id,
            title: child.name,
            dataType: 2,
            type: child.type,
            selected: child.selected,
            treeType: 'value',
          }
        }
      })
    if (children.length > 0) {
      item.children = children
      recursiveIndicatorGroup(children, allData, deep + 1)
    }
  })
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
}
