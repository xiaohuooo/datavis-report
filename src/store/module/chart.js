import { OUT_INDICATOR_TYPE } from "@/config/indicator"
import {getVariableList} from "@/api/dataset";

export default {
  state: {
    currentSelectChart: null, // 创建模板页面当前选中的图表
    dimensions: [], // 系统当前所有的维度
    indicators: [], // 系统当前所有的指标
    reportDate: [], // 报告全局时间
    style: {}, // 图表的样式
    deactivation: false, // 点击图表之外，是否可以取消选中
    chartDataNum: null, //图表的数据
    chartList: [], // 当前画布中所有的图表
    chartVNodeList: [], // 当前画布中所有图表的虚拟dom
    reportSetting: {}, // 报告设置
    filteredConditions: {}, //指标筛选出的维度
    chartLocalData: {}, //图表缓存远程数据，每一个请求作为一个key，每一个key在这里不重复
    snapshotData: [], // 编辑器快照数据
    snapshotIndex: -1, // 快照索引
    copyChart: {}, //复制的图表
    dateType: globalVariable.dateType, // 日期类型（海尔报告查看全局变量）
    dataSource: globalVariable.dataSource, // 数据源（海尔报告查看海尔全局变量）
    orgId: globalVariable.orgId, // 组织架构（资产分组-海尔报告查看海尔全局变量）
    globalVariableList: [], // 系统全局变量
  },
  getters: {
    allDimensions: state => state.dimensions,
    allIndicators: state => state.indicators,
    chartLocalData: state => state.chartLocalData,
    currentSelectChart(state) {
      return state.currentSelectChart || {};
    },

    currentChartType(state) {
      return state.currentSelectChart && state.currentSelectChart.id;
    },

    currentChartId(state) {
      return state.currentSelectChart && state.currentSelectChart.chartId;
    },

    currentSelectChartData(state) {
      return (state.currentSelectChart && state.currentSelectChart.data) || {};
    },

    currentSelectDimensions(state) {
      return (state.currentSelectChart && state.currentSelectChart.data && state.currentSelectChart.data.dimension) || [];
    },

    currentSelectSeries(state) {
      return state.currentSelectChart?.data?.series || [];
    },

    currentSelectIndicators(state) {
      return (state.currentSelectChart && state.currentSelectChart.data && state.currentSelectChart.data.indicator) || [];
    },

    currentSelectConfigData(state) {
      return (state.currentSelectChart && state.currentSelectChart.data && state.currentSelectChart.data.configData) || {};
    },

    currentChartStyle(state) {
      return (state.currentSelectChart && state.currentSelectChart.style) || {};
    },

    currentSelectChartFilteredCondition(state) {
      return state.currentSelectChart?.data?.configData?.variableCondition || {}
    },
    reportThemeStyle: state => state.reportSetting.themeStyle,
    currentChartList: state => state.chartList,
    snapshotData: state => state.snapshotData,
    snapshotIndex: state => state.snapshotIndex,
    reportSetting: state => state.reportSetting,
    // 当前选中图表是否选择外部指标
    isSelectOutIndicators(state, { currentSelectIndicators }) {
      return currentSelectIndicators.some(ele => ele.type === OUT_INDICATOR_TYPE);
    },
    dateType: state => state.dateType,
    dataSource: state => state.dataSource,
    orgId: state => state.orgId,
    globalVariableList: state => state.globalVariableList,
  },
  mutations: {
    setChartLocalData(state, localData) {
      Object.assign(state.chartLocalData, localData);
    },
    setChart(state, chart) {
      state.currentSelectChart = chart;
    },
    refreshDimensions(state, dimensions) {
      state.dimensions = dimensions;
    },
    refreshIndicators(state, indicators) {
      state.indicators = indicators;
    },
    refreshReportDate(state, reportDate) {
      state.reportDate = reportDate;
    },
    setDeactivation(state, deactivation) {
      state.deactivation = deactivation;
    },
    refreshChartDataNum(state, chartDataNum) {
      state.chartDataNum = chartDataNum;
    },
    setChartList(state, chartList) {
      state.chartList = chartList;
    },
    setChartVNodeList(state, payload) {
      state.chartVNodeList = payload;
    },
    setReportSetting(state, reportSetting) {
      state.reportSetting = reportSetting;
    },
    setFilteredConditions(state, filteredConditions) {
      state.filteredConditions = filteredConditions;
    },
    setSnapshotData(state, snapshotData) {
      state.snapshotData = snapshotData;
    },
    setSnapshotIndex(state, snapshotIndex) {
      state.snapshotIndex = snapshotIndex;
    },
    setCopyChart(state, chart) {
      state.copyChart = { ...chart };
    },
    setReportDateType(state, dateType) {
      state.dateType = dateType;
    },
    setReportDataSource(state, dataSource) {
      state.dataSource = dataSource;
    },
    setReportOrgId(state, orgId) {
      state.orgId = orgId;
    },
    setGlobalVariableList(state, globalVariableList) {
      state.globalVariableList = globalVariableList;
    },
  },
  actions: {
    storeDataToChartLocalData({ commit }, data) {
      commit("setChartLocalData", { ...data });
    },
    setCurrentSelectChart({ commit }, { chart }) {
      commit("setChart", { ...chart });
    },
    setDimensions({ commit }, { dimensions }) {
      commit("refreshDimensions", [...dimensions]);
    },
    setIndicators({ commit }, { indicators }) {
      commit("refreshIndicators", [...indicators]);
    },
    setReportDate({ commit }, { reportDate }) {
      commit("refreshReportDate", reportDate);
    },
    setChartDeactivation({ commit }, deactivation) {
      commit("setDeactivation", deactivation);
    },
    setChartDataNum({ commit }, chartDataNum) {
      commit("refreshChartDataNum", chartDataNum);
    },
    setChartList({ commit }, { chartList }) {
      commit("setChartList", [...chartList]);
    },
    setReportSetting({ commit }, { reportSetting }) {
      commit("setReportSetting", { ...reportSetting });
    },
    filteredConditionAction({ commit }, filteredConditions) {
      commit("setFilteredConditions", filteredConditions);
    },
    setSnapshotData({ commit }, { snapshotData }) {
      commit("setSnapshotData", [...snapshotData]);
    },
    setSnapshotIndex({ commit }, { snapshotIndex }) {
      commit("setSnapshotIndex", snapshotIndex);
    },
    setActionsCopyChart({ commit }, copyChart) {
      commit("setCopyChart", copyChart);
    },
    setReportDateType({ commit }, { dateType }) {
      commit("setReportDateType", dateType);
    },
    setReportDataSource({ commit }, { dataSource }) {
      commit("setReportDataSource", dataSource);
    },
    setReportOrgId({ commit }, { orgId }) {
      commit("setReportOrgId", orgId);
    },
    setGlobalVariableList({ commit }, { globalVariableList }) {
      commit("setGlobalVariableList", globalVariableList);
    },
  }
};
