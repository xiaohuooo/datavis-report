import { queryBusinessGroups, queryBusinessLabels } from '@/api/contentManagement'
import { querySysEnvironmentAll, getUserList } from "@/api/system"

const state = {
  businessLabels: [],
  businessGroups: [],
  envList: [],
  userList: []
}

const mutations = {
  SET_BUSINESS_GROUPS: (state, payload) => {
    state.businessGroups = payload
  },
  SET_BUSINESS_LABELS: (state, payload) => {
    state.businessLabels = payload
  },
  SET_ENV_LIST: (state, payload) => {
    state.envList = payload
  },
  SET_USER_LIST: (state, payload) => {
    state.userList = payload
  }
}

const actions = {
  getBusinessGroups({ commit }) {
    queryBusinessGroups().then( res => {
      const { state, data } = res.data;
      if (state === "success") {
        commit('SET_BUSINESS_GROUPS', data)
      }
    })
  },
  getBusinessLabels({ commit }) {
    queryBusinessLabels().then( res => {
      const { data, state } = res.data;
      if (state === "success") {
        commit('SET_BUSINESS_LABELS', data)
      }
    })
  },
  getEnvList({ commit }) {
    querySysEnvironmentAll({
      pageSize: 0,
      pageNum: 0,
    }).then( res => {
      const { state, data } = res.data;
      if (state === "success") {
        commit('SET_ENV_LIST', data)
      }
    })
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
}
