import { DATASET } from '../mutation-types'

const state = {
  dataSource: null, // 数据源
  tableId: null, // 表id
  folderList: [], // 仅文件夹列表
  datasetList: [], // 仅数据集列表
}

const actions = {
  setDataSource({ commit }, payload) {
    commit(DATASET.SET_DATA_SOURCE, payload)
  },
  setTableId({ commit }, payload) {
    commit(DATASET.SET_TABLE_ID, payload)
  },
  setFolderList({ commit }, payload) {
    commit(DATASET.SET_FOLDER_LIST, payload)
  },
  setDatasetList({ commit }, payload) {
    commit(DATASET.SET_DATASET_LIST, payload)
  },
}

const mutations = {
  [DATASET.SET_DATA_SOURCE](state, payload) {
    state.dataSource = payload
  },
  [DATASET.SET_TABLE_ID](state, payload) {
    state.tableId = payload
  },
  [DATASET.SET_FOLDER_LIST](state, payload) {
    state.folderList = payload
  },
  [DATASET.SET_DATASET_LIST](state, payload) {
    state.datasetList = payload
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
}
