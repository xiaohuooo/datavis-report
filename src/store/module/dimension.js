import { DIMENSION } from '../mutation-types'

const state = {
  businessDesc: '', // 业务描述
}

const getters = {
  
}

const actions = {
  setBusinessDesc({ commit }, payload) {
    commit(DIMENSION.SET_BUSINESS_DESC, payload)
  },
}

const mutations = {
  [DIMENSION.SET_BUSINESS_DESC](state, payload) {
    state.businessDesc = payload
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
