import { getIndicatorTreeData, queryDataSourceForCategory } from "@/api/indicator";
import { getDimensionTreeData } from "@/api/dimension";
import { recursiveIndicatorGroup } from "@/utils/formatTree";
import { getWarnTaskList } from "@/api/earlyWarningTarget";

export default {
  namespaced: true,
  state: {
    earlyWarningList: [], // 预警列表
    allIndicator: [], // 所有指标
    indicatorTreeData: [], // 指标树
    allDimension: [], // 所有维度
    dimensionTreeData: [], // 维度树
    dataSourceList: [], // 数据源
    indicatorGroupList: [], // 指标分组
    bigTreeNodesCache: [], // 指标巨树数据缓存
  },
  mutations: {
    setEarlyWarningList(state, payload) {
      state.earlyWarningList = payload;
    },
    setAllIndicator(state, payload) {
      state.allIndicator = payload;
    },
    setIndicatorTreeData(state, payload) {
      state.indicatorTreeData = payload;
    },
    setAllDimension(state, payload) {
      state.allDimension = payload;
    },
    setDimensionTreeData(state, payload) {
      state.dimensionTreeData = payload;
    },
    setDataSourceList(state, payload) {
      state.dataSourceList = payload;
    },
    setBigTreeNodesCache(state, payload) {
      state.bigTreeNodesCache = payload;
    },
    setAllIndicatorGroup(state, payload) {
      state.indicatorGroupList = payload;
    }
  },
  actions: {
    /**
     * 获取预警数据
     */
    async fetchEarlyWarningList(ctx) {
      const res = await getWarnTaskList({});
      if (res.data.state === "success") {
        ctx.commit("setEarlyWarningList", res.data.data);
      }
    },
    /**
     * 获取指标
     */
    async fetchIndicator(ctx) {
      const { data: res } = await getIndicatorTreeData();
      if (res.state === "success") {
        ctx.commit("setAllIndicator", res.data.indicatorList.map(item => {
          return {
            ...item,
            pid: item.groupId
          }
        }));
        ctx.commit("setAllIndicatorGroup", res.data.indicatorGroupList.map(item => {
          return {
            ...item,
            pid: item.parentId
          }
        }));
        let allData = [...res.data.indicatorGroupList, ...res.data.indicatorList];
        let root = [
          {
            deep: 1,
            id: -1,
            title: "指标组",
            expand: true,
            disabled: true,
            treeType: "group"
          }
        ];
        recursiveIndicatorGroup(root, allData);
        ctx.commit("setIndicatorTreeData", root);
      }
    },
    /**
     * 获取维度
     */
    async fetchDimension(ctx) {
      const { data } = await getDimensionTreeData();
      if (data.state === "success") {
        ctx.commit("setAllDimension", data.data.dimensionList);

        let groupData = data.data.dimensionGroupList;
        let dimensionData = data.data.dimensionList;
        let allData = groupData.concat(dimensionData);
        let root = [
          {
            deep: 1,
            id: -1,
            title: "维度组",
            expand: true,
            disabled: true,
            treeType: "group"
          }
        ];
        recursiveIndicatorGroup(root, allData);
        ctx.commit("setDimensionTreeData", root);
      }
    },

    /**
     * 获取数据源
     */
    async fetchDataSource(ctx) {
      const res = await queryDataSourceForCategory();
      if (res.data.state === "success") {
        ctx.commit("setDataSourceList", res.data.data);
      }
    },
    setAllIndicator({ commit }, payload) {
      commit('setAllIndicator', payload)
    },
    setAllIndicatorGroup({ commit }, payload) {
      commit('setAllIndicatorGroup', payload)
    }
  }
};
