import { INDICATOR } from '../mutation-types'
import { queryAllVersion } from "@/api/label";

const state = {
  indicatorEditable: false, // 区分编辑态、查看态
  indicatorName: null, // 指标名称
  indicatorAbbreviation: '', // 指标简称
  indicatorCode: null, // 指标编码
  indicatorAttribute: '一级指标', // 属性，默认一级指标
  syncDashboard: false, // 同步看板
  businessDate: true, // 业务日期
  relatedDimensions: [], // 相关维度
  allVersionList: [], // 所有版本号
  indicatorId: null, // 指标id
  indicatorKind: 2, // 指标种类，1-原子 2-复合 3-派生
  indicatorVersion: 'V1.0.0', // 指标版本号，默认V1.0.0
  indicatorVersions: [], // 历史指标版本列表
  businessDesc: '', // 业务描述
  businessModelTree: [], // 业务模型树
  businessModel: '', // 业务模型
  businessModelList: [], // 业务模型列表，每个业务模型中包含对应的技术口径列表
  column: null, // 列--原技术口径
  columnNvl: null, // 列为空值时的默认值
  columnNvlManualEntry: null, // 列为空值时的手动输入值
  summaryMethod: null, // 汇总方式
  numberFormatConfig: {
    showType: 0, // 数值格式 0-数值 1-百分比
    numberUnit: 'original', // 数值单位 0-原始值 1-万 2-亿
    prefix: '', // 前缀
    suffix: '', // 后缀
    decimalPlaces: 2, // 小数位数
    thousandsSeparator: false, // 是否使用千位分隔符
  }, // 数值格式
  showFilterConditions: false,
  filterConditions: [], // 过滤条件
  assetGroup: '', // 资产分组
  indicatorLabels: [], // 指标标签
  derivedFrom: '', // 派生自
  derivationRule: null, // 派生规则
  calcPeriod: null, // 计算周期
  calcIntervalType: 1, // 计算区间类型 1-当前 2-Year/Quarter/Mon/Week
  calcInterval: null, // 计算区间
  calcType: null, // 计算方式 1-环比 2-同比
  subCalcType: null, // 计算方式 1-月同比 2-年同比
  mobileCalcPeriod: null, // 移动计算-计算周期 1-按年 2-按月
  mobileCalcInterval: { start: [], end: [] }, // 移动计算-计算区间
  rankingType:null, // 排名类型
  rankingWay:null, // 排名方式
  basicIndicatorTree: [], // 原子指标树
  expression: null, // 复合指标的表达式
  expressionHtml: null, // 复合指标表达式html文本
  compositeIndicatorFilterConditions: null, // 复合指标的过滤条件
  indicators: [], // 全部指标列表
  compositeIndicatorBusinessModels: null, // 复合指标表达式中原子指标对应的业务描述对象
  assetGroups: [], // 资产分组列表
  publicVariableList: [], // 过滤条件--公共变量列表
  paramValue: "", // 外部指标--参数
  showConfig: {
    showName: true,
    showWarning: true,
    showValue: true,
    showYoy: true,
    showMom: true,
    showChart: true,
    chartIndicatorId: '',
    dateType: 1, // 近7天
    showTarget: true,
  },
  oldShowConfig: {
    showName: true,
    showWarning: true,
    showValue: true,
    showYoy: true,
    showMom: true,
    showChart: true,
    chartIndicatorId: '',
    dateType: 1, // 近7天
    showTarget: true,
  },
  indicatorValue: null, // 指标值
  indicatorTreeData: [], // 带分组的嵌套指标数据
};

const getters = {
  columns(state) { // 技术口径列表
    return state.businessModelList.find(
      item => Number(item.id) === Number(state.businessModel),
    )?.businessModelProperties ?? []
  },
  indicatorGroupTreeData: state => state.indicatorTreeData
}

const actions = {
  setIndicatorEditable({ commit }, payload) {
    commit(INDICATOR.SET_INDICATOR_EDITABLE, payload)
  },
  setIndicatorName({ commit }, payload) {
    commit(INDICATOR.SET_INDICATOR_NAME, payload)
  },
  setIndicatorAbbreviation({ commit }, payload) {
    commit(INDICATOR.SET_INDICATOR_ABBREVIATION, payload)
  },
  setIndicatorCode({ commit }, payload) {
    commit(INDICATOR.SET_INDICATOR_CODE, payload)
  },
  setIndicatorAttribute({ commit }, payload) {
    commit(INDICATOR.SET_INDICATOR_ATTRIBUTE, payload)
  },
  setSyncDashboard({ commit }, payload) {
    commit(INDICATOR.SET_SYNC_DASHBOARD, payload)
  },
  setBusinessDate({ commit }, payload) {
    commit(INDICATOR.SET_BUSINESS_DATE, payload)
  },
  setRelatedDimensions({ commit }, payload) {
    commit(INDICATOR.SET_RELATED_DIMENSIONS, payload)
  },
  async fetchAllVersionList({ commit }) {
    const res = await queryAllVersion();
    if (res.data.state === "success") {
      commit(INDICATOR.SET_ALL_VERSION_LIST, res.data.data);
    }
  },
  setIndicatorId({ commit }, payload) {
    commit(INDICATOR.SET_INDICATOR_ID, payload)
  },
  setIndicatorKind({ commit }, payload) {
    commit(INDICATOR.SET_INDICATOR_KIND, payload)
  },
  setBusinessDesc({ commit }, payload) {
    commit(INDICATOR.SET_BUSINESS_DESC, payload)
  },
  setBusinessModelTree({ commit }, payload) {
    commit(INDICATOR.SET_BUSINESS_MODEL_TREE, payload)
  },
  setBusinessModel({ commit }, payload) {
    commit(INDICATOR.SET_BUSINESS_MODEL, payload)
  },
  setBusinessModelList({ commit }, payload) {
    commit(INDICATOR.SET_BUSINESS_MODEL_LIST, payload)
  },
  setColumn({ commit }, payload) {
    commit(INDICATOR.SET_COLUMN, payload)
  },
  setColumnNvl({ commit }, payload) {
    commit(INDICATOR.SET_COLUMN_NVL, payload)
  },
  setColumnNvlManualEntry({ commit }, payload) {
    commit(INDICATOR.SET_COLUMN_NVL_MANUAL_ENTRY, payload)
  },
  setSummaryMethod({ commit }, payload) {
    commit(INDICATOR.SET_SUMMARY_METHOD, payload)
  },
  setNumberFormatConfig({ commit }, payload) {
    commit(INDICATOR.SET_NUMBER_FORMAT_CONFIG, payload)
  },
  setShowFilterConditions({ commit }, payload) {
    commit(INDICATOR.SET_SHOW_FILTER_CONDITIONS, payload)
  },
  setFilterConditions({ commit }, payload) {
    commit(INDICATOR.SET_FILTER_CONDITIONS, payload)
  },
  setAssetGroup({ commit }, payload) {
    commit(INDICATOR.SET_ASSET_GROUP, payload)
  },
  setIndicatorLabels({ commit }, payload) {
    commit(INDICATOR.SET_INDICATOR_LABELS, payload)
  },
  setDerivedFrom({ commit }, payload) {
    commit(INDICATOR.SET_DERIVED_FROM, payload)
  },
  setDerivationRule({ commit }, payload) {
    commit(INDICATOR.SET_DERIVATION_RULE, payload)
  },
  setCalcPeriod({ commit }, payload) {
    commit(INDICATOR.SET_CALC_PERIOD, payload)
  },
  setCalcIntervalType({ commit }, payload) {
    commit(INDICATOR.SET_CALC_INTERVAL_TYPE, payload)
  },
  setCalcInterval({ commit }, payload) {
    commit(INDICATOR.SET_CALC_INTERVAL, payload)
  },
  setCalcType({ commit }, payload) {
    commit(INDICATOR.SET_CALC_TYPE, payload)
  },
  setSubCalcType({ commit }, payload) {
    commit(INDICATOR.SET_SUB_CALC_TYPE, payload)
  },
  setMobileCalcPeriod({ commit }, payload) {
    commit(INDICATOR.SET_MOBILE_CALC_PERIOD, payload)
  },
  setMobileCalcInterval({ commit }, payload) {
    commit(INDICATOR.SET_MOBILE_CALC_INTERVAL, payload)
  },
  setRankingType({ commit }, payload) {
    commit(INDICATOR.SET_RANKING_TYPE, payload)
  },
  setRankingWay({ commit }, payload) {
    commit(INDICATOR.SET_RANKING_WAY, payload)
  },
  setIndicatorTree({ commit }, payload) {
    commit(INDICATOR.SET_BASIC_INDICATOR_TREE, payload)
  },
  setExpression({ commit }, payload) {
    commit(INDICATOR.SET_EXPRESSION, payload)
  },
  setExpressionHtml({ commit }, payload) {
    commit(INDICATOR.SET_EXPRESSION_HTML, payload)
  },
  setCompositeIndicatorFilterConditions({ commit }, payload) {
    commit(INDICATOR.SET_COMPOSITE_INDICATOR_FILTER_CONDITIONS, payload)
  },
  setIndicators({ commit }, payload) {
    commit(INDICATOR.SET_INDICATORS, payload)
  },
  setCompositeIndicatorBusinessModels({ commit }, payload) {
    commit(INDICATOR.SET_COMPOSITE_INDICATOR_BUSINESS_MODELS, payload)
  },
  setAssetGroups({ commit }, payload) {
    commit(INDICATOR.SET_ASSET_GROUPS, payload)
  },
  setPublicVariableList({ commit }, payload) {
    commit(INDICATOR.SET_PUBLIC_VARIABLE_LIST, payload)
  },
  setIndicatorVersion({ commit }, payload) {
    commit(INDICATOR.SET_INDICATOR_VERSION, payload)
  },
  setIndicatorVersions({ commit }, payload) {
    commit(INDICATOR.SET_INDICATOR_VERSIONS, payload)
  },
  setParamValue({ commit }, payload) {
    commit(INDICATOR.SET_PARAM_VALUE, payload)
  },
  setShowConfig({ commit }, payload) {
    commit(INDICATOR.SET_SHOW_CONFIG, payload)
  },
  setOldShowConfig({ commit }, payload) {
    commit(INDICATOR.SET_OLD_SHOW_CONFIG, payload)
  },
  setIndicatorValue({ commit }, payload) {
    commit(INDICATOR.SET_INDICATOR_VALUE, payload)
  },
  setIndicatorTreeData({ commit }, payload) {
    commit(INDICATOR.SET_INDICATOR_GROUP_TREE_DATA, payload)
  },
}

const mutations = {
  [INDICATOR.SET_INDICATOR_EDITABLE](state, payload) {
    state.indicatorEditable = payload;
  },
  [INDICATOR.SET_INDICATOR_NAME](state, payload) {
    state.indicatorName = payload;
  },
  [INDICATOR.SET_INDICATOR_ABBREVIATION](state, payload) {
    state.indicatorAbbreviation = payload;
  },
  [INDICATOR.SET_INDICATOR_CODE](state, payload) {
    state.indicatorCode = payload;
  },
  [INDICATOR.SET_INDICATOR_ATTRIBUTE](state, payload) {
    state.indicatorAttribute = payload;
  },
  [INDICATOR.SET_SYNC_DASHBOARD](state, payload) {
    state.syncDashboard = payload;
  },
  [INDICATOR.SET_BUSINESS_DATE](state, payload) {
    state.businessDate = payload;
  },
  [INDICATOR.SET_RELATED_DIMENSIONS](state, payload) {
    state.relatedDimensions = payload;
  },
  [INDICATOR.SET_ALL_VERSION_LIST](state, payload) {
    state.allVersionList = payload;
  },
  [INDICATOR.SET_INDICATOR_ID](state, payload) {
    state.indicatorId = payload
  },
  [INDICATOR.SET_INDICATOR_KIND](state, payload) {
    state.indicatorKind = payload
  },
  [INDICATOR.SET_BUSINESS_DESC](state, payload) {
    state.businessDesc = payload
  },
  [INDICATOR.SET_BUSINESS_MODEL_TREE](state, payload) {
    state.businessModelTree = payload
  },
  [INDICATOR.SET_BUSINESS_MODEL](state, payload) {
    state.businessModel = payload
  },
  [INDICATOR.SET_BUSINESS_MODEL_LIST](state, payload) {
    state.businessModelList = payload
  },
  [INDICATOR.SET_COLUMN](state, payload) {
    state.column = payload
  },
  [INDICATOR.SET_COLUMN_NVL](state, payload) {
    state.columnNvl = payload
  },
  [INDICATOR.SET_COLUMN_NVL_MANUAL_ENTRY](state, payload) {
    state.columnNvlManualEntry = payload
  },
  [INDICATOR.SET_SUMMARY_METHOD](state, payload) {
    state.summaryMethod = payload
  },
  [INDICATOR.SET_NUMBER_FORMAT_CONFIG](state, payload) {
    state.numberFormatConfig = payload ?? {
      showType: 0,
      numberUnit: 'original',
      prefix: '',
      suffix: '',
      decimalPlaces: 2,
      thousandsSeparator: false,
    }
  },
  [INDICATOR.SET_SHOW_FILTER_CONDITIONS](state, payload) {
    state.showFilterConditions = payload
  },
  [INDICATOR.SET_FILTER_CONDITIONS](state, payload) {
    state.filterConditions = payload
  },
  [INDICATOR.SET_ASSET_GROUP](state, payload) {
    state.assetGroup = payload
  },
  [INDICATOR.SET_INDICATOR_LABELS](state, payload) {
    state.indicatorLabels = payload
  },
  [INDICATOR.SET_DERIVED_FROM](state, payload) {
    state.derivedFrom = payload
  },
  [INDICATOR.SET_DERIVATION_RULE](state, payload) {
    state.derivationRule = payload
  },
  [INDICATOR.SET_CALC_PERIOD](state, payload) {
    state.calcPeriod = payload
  },
  [INDICATOR.SET_CALC_INTERVAL_TYPE](state, payload) {
    state.calcIntervalType = payload
  },
  [INDICATOR.SET_CALC_INTERVAL](state, payload) {
    state.calcInterval = payload
  },
  [INDICATOR.SET_CALC_TYPE](state, payload) {
    state.calcType = payload
  },
  [INDICATOR.SET_SUB_CALC_TYPE](state, payload) {
    state.subCalcType = payload
  },
  [INDICATOR.SET_MOBILE_CALC_PERIOD](state, payload) {
    state.mobileCalcPeriod = payload
  },
  [INDICATOR.SET_MOBILE_CALC_INTERVAL](state, payload) {
    state.mobileCalcInterval = payload
  },
  [INDICATOR.SET_RANKING_TYPE](state, payload) {
    state.rankingType = payload
  },
  [INDICATOR.SET_RANKING_WAY](state, payload) {
    state.rankingWay = payload
  },
  [INDICATOR.SET_BASIC_INDICATOR_TREE](state, payload) {
    state.basicIndicatorTree = payload
  },
  [INDICATOR.SET_EXPRESSION](state, payload) {
    state.expression = payload
  },
  [INDICATOR.SET_EXPRESSION_HTML](state, payload) {
    state.expressionHtml = payload
  },
  [INDICATOR.SET_COMPOSITE_INDICATOR_FILTER_CONDITIONS](state, payload) {
    state.compositeIndicatorFilterConditions = payload
  },
  [INDICATOR.SET_INDICATORS](state, payload) {
    state.indicators = payload
  },
  [INDICATOR.SET_COMPOSITE_INDICATOR_BUSINESS_MODELS](state, payload) {
    state.compositeIndicatorBusinessModels = payload
  },
  [INDICATOR.SET_ASSET_GROUPS](state, payload) {
    state.assetGroups = payload
  },
  [INDICATOR.SET_PUBLIC_VARIABLE_LIST](state, payload) {
    state.publicVariableList = payload
  },
  [INDICATOR.SET_INDICATOR_VERSION](state, payload) {
    state.indicatorVersion = payload
  },
  [INDICATOR.SET_INDICATOR_VERSIONS](state, payload) {
    state.indicatorVersions = payload
  },
  [INDICATOR.SET_PARAM_VALUE](state, payload) {
    state.paramValue = payload;
  },
  [INDICATOR.SET_SHOW_CONFIG](state, payload) {
    state.showConfig = payload ?? {
      showName: true,
      showWarning: true,
      showValue: true,
      showYoy: true,
      showMom: true,
      showChart: true,
      chartIndicatorId: '',
      dateType: 1, // 近7天
      showTarget: true,
    }
  },
  [INDICATOR.SET_OLD_SHOW_CONFIG](state, payload) {
    state.oldShowConfig = payload ?? {
      showName: true,
      showWarning: true,
      showValue: true,
      showYoy: true,
      showMom: true,
      showChart: true,
      chartIndicatorId: '',
      dateType: 1, // 近7天
      showTarget: true,
    }
  },
  [INDICATOR.SET_INDICATOR_VALUE](state, payload) {
    state.indicatorValue = payload
  },
  [INDICATOR.SET_INDICATOR_GROUP_TREE_DATA](state, payload) {
    state.indicatorTreeData = payload
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
