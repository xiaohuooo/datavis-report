import { getRelationDimension } from "@/api/indicatorWorkbench";
import { BUILT_IN_DIMENSION } from "@/config/DimensionSortDictionary";

export default {
  namespaced: true,
  state: {
    routeFrom: localStorage.getItem("indicatorWorkbench-routeFrom") || null, // 从哪里进入指标详情页面
    relationDimension: [], // 当前指标关联的维度
    historyId: '', // 当前故事id
    isReturnFromDetail: false, // 是否是从详情页返回
    storyBoardId: localStorage.getItem("indicatorWorkbench-storyBoardId") || "" // 故事看板页跳转指标详情页时，记录故事看板id
  },
  getters: {
    // 关联维度中的内置维度'日'的id，如果不存在就是第一个维度id
    dateDimensionId: state => state.relationDimension.find(ele => ele.name === BUILT_IN_DIMENSION.name)?.id || state.relationDimension[0]?.id
  },
  mutations: {
    setRouteFrom(state, payload) {
      if (payload) {
        localStorage.setItem("indicatorWorkbench-routeFrom", payload);
      } else {
        localStorage.removeItem("indicatorWorkbench-routeFrom");
      }
      state.routeFrom = payload;
    },
    setRelationDimension(state, payload) {
      state.relationDimension = payload;
    },
    setHistoryId(state, payload) {
      state.historyId = payload;
    },
    setIsReturnFromDetail(state, payload) {
      state.isReturnFromDetail = payload;
    },
    setStoryBoardId(state, payload) {
      if (payload) {
        localStorage.setItem("indicatorWorkbench-storyBoardId", payload);
      } else {
        localStorage.removeItem("indicatorWorkbench-storyBoardId");
      }
      state.storyBoardId = payload;
    }
  },
  actions: {
    /**
     * 获取指标的相关维度
     * @param {*} id 指标id
     * @param {*} type 指标类型
     */
    async fetchRelationDimension(ctx, { id, type }) {
      const res = await getRelationDimension({ indicatorId: id, type });
      if (res.data.state === "success") {
        const data = res.data.data.map(ele => ({
          id: ele.id,
          name: ele.name
        }));
        ctx.commit("setRelationDimension", data);
      }
    }
  }
};
