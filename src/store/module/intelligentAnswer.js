import { DEFAULT_MESSAGE, GUESS_SEARCH, INDICATOR_CARD, CHART_WRAPPER, TABLE_WRAPPER } from "@/view/IntelligentAnswer/IntelligentAnswerModal/constant";
import { msgArchiving, queryRefIndicatorAndDimension } from "@/api/intelligentAnswer";
import { deepCopy } from "@/utils/common";
import { v4 as uuidv4 } from "uuid";
import { queryIndicatorRelationByDimensionId } from "@/api/dimension";
import { matchingRelation } from "@/api/realTimeAnalysis";

export default {
  namespaced: true,
  state: {
    showIntelligentAnswer: false, // 是否展示智能问答弹窗
    answerModalId: null, // 此次问答窗口id
    answerList: [DEFAULT_MESSAGE] // 对话记录
  },
  getters: {},
  mutations: {
    setShowIntelligentAnswer(state, payload) {
      state.showIntelligentAnswer = payload;
    },
    setAnswerModalId(state, payload) {
      state.answerModalId = payload;
    },
    setAnswerList(state, payload) {
      state.answerList = payload;
    }
  },
  actions: {
    /**
     * 添加问答记录
     * @param {*} id 指标id
     * @param {*} type 指标类型
     */
    async addAnswer({ state, commit, rootState }, { type, params }) {
      let robot = {};
      console.log(11, params, type);
      switch (type) {
        case GUESS_SEARCH:
          robot = await getRefIndicatorAndDimension(params);
          break;
        case INDICATOR_CARD:
          robot = await getRelDataByDimInd(params, rootState);
          break;
        case CHART_WRAPPER:
          robot = await getRelDataByDimInd(params, rootState);
          getCommonRelDim(params.condition, robot);
          break;
        case TABLE_WRAPPER:
          robot = await getRelDataByDimInd(params, rootState);
          getCommonRelDim(params.condition, robot);
          break;
      }
      commonSet({ state, commit }, { type, params, robot });
    },
    updateAnswer({ state, commit }, { id, dateType }) {
      const msgList = deepCopy(state.answerList);
      msgList.forEach(ele => {
        if (ele.id === id) {
          ele.dateType = dateType;
        }
      });
      commit("setAnswerList", msgList);
    }
  }
};

/**
 * 只选择指标，未选择维度。默认取第一个关联维度
 */
const getCommonRelDim = (condition, robot) => {
  const { dimensions } = condition;
  const { dimensionRelList } = robot;
  if (!dimensions.length) {
    const firstDim = dimensionRelList.shift();
    dimensions.push(firstDim?.id || null);
  }
};

const commonSet = ({ state, commit }, { type, params, robot }) => {
  const msgList = deepCopy(state.answerList);
  const msgItem = {
    id: uuidv4(),
    type,
    user: { title: params.title },
    robot,
    condition: params.condition || {}
  };
  if (type === CHART_WRAPPER || type === TABLE_WRAPPER) {
    msgItem.dateType = params.dateType || 1;
  }
  msgList.push(msgItem);
  commit("setAnswerList", msgList);
  msgArchiving({ answerId: state.answerModalId, type: 1, content: JSON.stringify(msgList) }); // 记录问答消息
};

/**
 * 获取 猜你想搜 数据
 */
const getRefIndicatorAndDimension = async ({ id, title, type }) => {
  if (type === "10") {
    // 通过维度找【指标】
    const res = await queryIndicatorRelationByDimensionId(id);
    if (res.data.state === "success") {
      const indicator = res.data.data.slice(0, 5).map(ele => ({ id: ele.id, title: ele.name, type: ele.type }));
      return indicator.length ? { indicator } : { noDataMsg: "当前维度无关联指标" };
    } else {
      this.$Message.error("获取可关联指标失败");
      return {};
    }
  } else {
    // 通过检索词找【指标/维度】
    const res = await queryRefIndicatorAndDimension({ search: title });
    if (res.data.state === "success") {
      const { dimension, indicator } = res.data.data;
      return indicator.length || dimension.length
        ? { indicator: indicator.slice(0, 5), dimension: dimension.slice(0, 5) }
        : { noDataMsg: "当前输入内容暂不存在或无法解析，请校验后重新输入" };
    } else {
      this.$Message.error("获取可关联维度指标失败");
      return {};
    }
  }
};

/**
 * 获取某个指标的关联维度/指标
 */
const getRelDataByDimInd = async ({ condition: { dimensions, indicators } }, { earlyWarningTarget: { allDimension, allIndicator } }) => {
  const newIndicators = indicators.map(ele => ({ id: ele.id, type: ele.type }));
  const res = await matchingRelation({ dimensions, indicators: newIndicators });
  if (res.data.state === "success") {
    const { dimensions, indicators } = res.data.data;
    console.log(33, res.data.data);
    const dimensionRelList = dimensions.slice(0, 2).map(id => ({ id, title: allDimension.find(i => i.id == id)?.name, type: "10" }));

    let indicatorRelList = [];
    if (indicators === null) {
      // 全量(需排除已选指标)前3个
      indicatorRelList = allIndicator
        .filter(i => !newIndicators.map(ele => ele.id).includes(i.id))
        .slice(0, 3)
        .map(ele => ({ id: ele.id, type: ele.type, title: ele.name }));
    } else {
      indicatorRelList = indicators.slice(0, 3).map(ele => {
        const ind = allIndicator.find(i => i.id == ele.id) || {};
        return {
          id: ind.id,
          type: ind.type,
          title: ind.name
        };
      });
    }

    return {
      dimensionRelList,
      indicatorRelList
    };
  } else {
    this.$Message.error("获取数据失败");
    return {};
  }
};
