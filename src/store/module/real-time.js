/********************即席分析********************/
import { REAL_TIME } from '@/store/mutation-types'
import { deepCopy } from '@/utils/common'

const state = {
  snapshotIndex: -1, // 快照索引
  snapshotData: [], // 编辑器快照数据
}

const getters = {}

const actions = {
  setSnapShotIndex({ commit }, payload) {
    commit(REAL_TIME.SET_SNAPSHOT_INDEX, payload)
  },
  setSnapShotData({ commit }, payload) {
    commit(REAL_TIME.SET_SNAPSHOT_DATA, payload)
  },
  resetSnapShot({ commit }, payload) {
    commit(REAL_TIME.RESET_SNAPSHOT, payload)
  },
}

const mutations = {
  [REAL_TIME.SET_SNAPSHOT_INDEX](state, payload) {
    state.snapshotIndex += payload
  },
  [REAL_TIME.SET_SNAPSHOT_DATA](state, payload) {
    // 先清空后面的，再插入
    state.snapshotData = state.snapshotData.slice(0, state.snapshotIndex)
    state.snapshotData.push(deepCopy(payload))
  },
  [REAL_TIME.RESET_SNAPSHOT](state, payload) {
    state.snapshotIndex = -1
    state.snapshotData = []
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
