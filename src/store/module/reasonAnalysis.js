import { getReportCompareDataAndHeaders } from "@/api/report";

export default {
  namespaced: true,
  state: {
    // 分析报告的数据
    analysisData: {
      startDate: "", // 起始时间
      endDate: "", // 终止时间
      startIndicatorValue: "", // 起始时间指标值
      endIndicatorValue: "", // 终止时间指标值
      diffValue: "", // 指标差值
      diffRatio: "" // 指标差值占起始值比例
    },
    allChartData: [], // 所有维度下的图表数据
    dimensionSelected: [], // 当前选中的维度
    breadList: [{ id: 1, name: "全部" }]
  },
  getters: {
    allChartDataF({ allChartData, analysisData }) {
      return allChartData.map(item => {
        const data = item.data.map(ele => {
          const { name, start, end } = ele;
          const diffVal = end - start;
          const diffPer = ((diffVal / start) * 100).toFixed(2);
          const contPer = ((diffVal / analysisData.diffValue) * 100).toFixed(2);
          const isNotNum = start === "--" || end === "--";
          return {
            name,
            start,
            end,
            diffVal: isNotNum ? "--" : diffVal,
            diffPer: isNotNum ? "--" : `${diffPer}%`,
            contPer: isNotNum ? "--" : `${contPer}%`
          };
        });
        return {
          id: item.id,
          data
        };
      });
    }
  },
  mutations: {
    setAnalysisData(state, payload) {
      state.analysisData = payload;
    },
    setAllChartData(state, payload) {
      state.allChartData = payload;
    },
    setDimensionSelected(state, payload) {
      state.dimensionSelected = payload;
    },
    setBreadList(state, payload) {
      state.breadList = payload;
    }
  },
  actions: {
    /**
     * 获取分析报告中的指标相关数据
     */
    async fetchAnalysisData({ state, commit }, param) {
      const { currentData, momData } = await getMomData(param);
      const data = {
        start: momData?.[0]?.[0],
        end: currentData?.[0]?.[0]
      };

      const obj = {
        startDate: param.start,
        endDate: param.end
      };
      obj.startIndicatorValue = !data.start && data.start !== 0 ? "--" : data.start;
      obj.endIndicatorValue = !data.end && data.end !== 0 ? "--" : data.end;
      if (obj.startIndicatorValue === "--" || obj.endIndicatorValue === "--") {
        obj.diffValue = "--";
        obj.diffRatio = "--";
      } else {
        const v1 = obj.endIndicatorValue - obj.startIndicatorValue;
        obj.diffValue = v1 > 0 ? `+${v1}` : `${v1}`;
        const v2 = ((obj.diffValue / obj.startIndicatorValue) * 100).toFixed(2);
        obj.diffRatio = v2 > 0 ? `+${v2}%` : `${v2}%`;
      }
      commit("setAnalysisData", obj);
    },
    /**
     * 获取指标的相关维度
     */
    async fetchChartData({ state, commit }, { dim1 = [], ...param }) {
      const allData = [];
      for (const item of state.dimensionSelected) {
        const dimensionId = dim1.length ? [item.id, ...dim1] : [item.id];
        const { headers, currentData, momData } = await getMomData({ ...param, dimensionId });
        const dimIndex = headers.findIndex(ele => ele === Number(item.id));
        const indIndex = headers.findIndex(ele => ele === Number(param.id));
        const data = currentData.map(ele => ({
          name: ele[dimIndex],
          start: momData.find(i => i[dimIndex] === ele[dimIndex])?.[indIndex] || "--",
          end: ele[indIndex]
        }));
        allData.push({ id: item.id, data });
      }
      commit("setAllChartData", allData);
    }
  }
};

/**
 * 获取指标的相关数据
 */
const getMomData = async ({ id, type, start, end, dimensionId = [], dimensionConditionGroup = [], variables }) => {
  const params = {
    dimensionIds: dimensionId,
    indicatorVos: [{ id, type, variables: { businessDate: end, ...variables } }],
    momDate: { startDate: start, endDate: start },
    date: { startDate: end, endDate: end }
  };

  // 维度成员筛选
  if (dimensionConditionGroup.length) {
    params.dimensionConditionGroup = dimensionConditionGroup;
  }

  const res = await getReportCompareDataAndHeaders(params);
  if (res.data.state === "success") {
    return res.data.data;
  } else {
    return {};
  }
};
