import {
  getChartResource,
  getSingleResource,
  addSingleResource,
  updateSingleResource,
  addMoreResource,
  updateMoreResource
} from "@/api/resourceManage";
import { getAllIndicatorGroupList } from "@/api/indicator";
import { findAllDimension } from "@/api/dimension";

/**
 * W后缀的状态，用于跨组件执行函数（利用watch实现）
 */
export default {
  namespaced: true,
  state: {
    allResourceList: JSON.parse(localStorage.getItem("resourceManage-allResource")) || [], // 用户创建的所有资源
    selectedChart: [], // 组合资源框选的图表
    selectLine: {
      show: false, //是否展示组合框选的边线
      width: "",
      height: "",
      top: "",
      left: ""
    },
    saveResourceModalStatus: {}, // 保存资源弹窗的相关数据
    refreshResourceTabW: 0, // 何时刷新资源tab
    toggleChartType: "", //【新增/编辑常规组件】切换到的图表类型
    allDimensions: JSON.parse(localStorage.getItem("resourceManage-allDimensions")) || [], //【新增/编辑常规组件】左侧列表的维度数据
    allIndicators: JSON.parse(localStorage.getItem("resourceManage-allIndicators")) || [], //【新增/编辑常规组件】左侧列表的指标数据
    editChartData: JSON.parse(localStorage.getItem("resourceManage-editChartData")) || {}, // 要编辑的常规资源数据
    isCombineResourceEditPage: JSON.parse(localStorage.getItem("resourceManage-isCombineResourceEditPage")) || false, // 是否处于组合资源新增/编辑页面（用于控制图表相关组件的展示内容）
    combineResourceId: "", // 要编辑的组合资源的资源id
    saveResourceChartData: null // 要保存的资源
  },
  getters: {
    allResourceName(state) {
      return state.allResourceList.map(ele => ele.name);
    }
  },
  mutations: {
    setAllResourceList(state, payload) {
      localStorage.setItem("resourceManage-allResource", JSON.stringify(payload));
      state.allResourceList = payload;
    },
    setSelectLine(state, payload) {
      state.selectLine = payload;
    },
    setSelectedChart(state, payload) {
      state.selectedChart = payload;
    },
    setSaveResourceModalStatus(state, payload) {
      if (payload.clear) {
        state.saveResourceModalStatus = {};
      } else {
        state.saveResourceModalStatus = {
          ...state.saveResourceModalStatus,
          ...payload
        };
      }
    },
    setRefreshResourceTabW(state, payload) {
      state.refreshResourceTabW = payload;
    },
    setToggleChartType(state, payload) {
      state.toggleChartType = payload;
    },
    setAllDimensions(state, payload) {
      localStorage.setItem("resourceManage-allDimensions", JSON.stringify(payload));
      state.allDimensions = payload;
    },
    setAllIndicators(state, payload) {
      localStorage.setItem("resourceManage-allIndicators", JSON.stringify(payload));
      state.allIndicators = payload;
    },
    setEditChartData(state, payload) {
      localStorage.setItem("resourceManage-editChartData", JSON.stringify(payload));
      state.editChartData = payload;
    },
    setIsCombineResourceEditPage(state, payload) {
      localStorage.setItem("resourceManage-isCombineResourceEditPage", JSON.stringify(payload));
      state.isCombineResourceEditPage = payload;
    },
    setCombineResourceId(state, payload) {
      state.combineResourceId = payload;
    },
    setSaveResourceChartData(state, payload) {
      state.saveResourceChartData = payload;
    }
  },
  actions: {
    /**
     * 获取所有的资源数据
     */
    async fetchAllResourceList(ctx) {
      const { data } = await getChartResource({});
      if (data?.state === "success") {
        ctx.commit("setAllResourceList", data.data);
      }
    },
    /**
     * 获取全部维度
     */
    async fetchAllDimensions(ctx) {
      const { data } = await findAllDimension();
      if (data?.state === "success") {
        const allDimensions = data.data.map(item => ({
          id: Number(item.id),
          groupId: Number(item.groupId),
          name: item.name,
          title: item.name,
          type: 99
        }));
        ctx.commit("setAllDimensions", allDimensions);
      } else {
        this.$Message.error("获取全部维度失败");
      }
    },
    /**
     * 获取全部指标
     */
    async fetchAllIndicators(ctx) {
      const { data } = await getAllIndicatorGroupList();
      if (data?.state === "success") {
        const arr = data.data.filter(ele => ele.groupId === "-1"); // 根目录下的指标
        data.data
          .filter(ele => ele.groupId !== "-1") // 根目录下的指标分组
          .map(ele => ele.data)
          .forEach(ele => arr.push(...ele));
        const allIndicators = arr.map(item => ({
          ...item,
          id: Number(item.id),
          groupId: Number(item.groupId),
          title: item.name
        }));
        ctx.commit("setAllIndicators", allIndicators);
      } else {
        this.$Message.error("获取全部指标失败");
      }
    },
    /**
     * 获取某个常规资源的详情数据
     */
    async fetchEditChartData(ctx, { id }) {
      const res = await getSingleResource(id);
      if (res.data.state === "success") {
        const data = res.data.data;
        ctx.commit("setEditChartData", data);
      }
    },
    /**
     * 保存或修改某个常规资源
     * @param {Object} chartData 资源数据
     * @param {Object} value 附加数据
     * @param {Object} type 标识保存还是修改
     * @param {Object} resourceId 要修改的资源id
     */
    async saveCommonResource(ctx, { chartData, value, type, resourceId }) {
      const indicators = chartData.data.indicator.map(ele => ({
        id: ele.id,
        type: ele.type,
        title: ele.title
      }));
      let configData = {
        style: chartData.style,
        data: chartData.data.configData
      };
      if (chartData.id === "ComplexTextarea") {
        // 文本框需要内容
        configData.inputHtmlText = chartData.data.innerHtml;
      }
      if (chartData.id === "CustomImage") {
        // 图片需要内容
        configData.imgSrc = chartData.data.src;
      }
      const params = {
        name: value.resourceName,
        dimensions: chartData.data.dimension,
        indicators,
        type: chartData.id,
        width: chartData.width,
        height: chartData.height,
        configData: JSON.stringify({ ...configData, series: chartData.data.series }),
        groupId: value.groupId,
        labels: value.labels
      };
      let res = {};
      if (type === "add") {
        res = await addSingleResource(params);
      } else {
        params.id = resourceId;
        res = await updateSingleResource(params);
      }
      if (res?.data?.state === "success") {
        return "保存资源成功！";
      } else {
        return "保存资源失败！";
      }
    },
    /**
     * 保存或修改某个组合资源
     * @param {Object} value 附加数据
     * @param {Object} type 标识保存还是修改
     * @param {Object} resourceId 要修改的资源id
     */
    async saveCombineResource(ctx, { value, type, resourceId }) {
      let children = [];
      ctx.state.selectedChart.forEach(ele => {
        const indicators = ele.data.indicator.map(ele => ({
          id: ele.id,
          type: ele.type,
          title: ele.title
        }));
        let configData = {
          style: ele.style,
          data: ele.data.configData,
          combineWidth: parseInt(ctx.state.selectLine.width),
          combineHeight: parseInt(ctx.state.selectLine.height),
          combineX: parseInt(ctx.state.selectLine.left),
          combineY: parseInt(ctx.state.selectLine.top)
        };
        if (ele.id === "ComplexTextarea") {
          // 文本框需要内容
          configData.inputHtmlText = ele.data.innerHtml;
        }
        if (ele.id === "CustomImage") {
          // 图片需要内容
          configData.imgSrc = ele.data.src;
        }
        children.push({
          name: ele.style.title,
          dimensions: ele.data.dimension,
          indicators,
          type: ele.id,
          x: ele.x,
          y: ele.y,
          width: ele.width,
          height: ele.height,
          configData: JSON.stringify({ ...configData, series: ele.data.series })
        });
      });
      const params = {
        name: value.resourceName,
        groupId: value.groupId,
        labels: value.labels,
        children
      };
      let res = {};
      if (type === "add") {
        res = await addMoreResource(params);
      } else {
        params.id = resourceId;
        res = await updateMoreResource(params);
      }
      if (res?.data?.state === "success") {
        return "保存资源成功！";
      } else {
        return "保存资源失败！";
      }
    }
  }
};
