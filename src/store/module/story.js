import { STORY } from '../mutation-types'

const state = {
  folderList: [], // 仅文件夹列表
  storyList: [], // 仅列表
}

const actions = {
  setFolderList({ commit }, payload) {
    commit(STORY.SET_FOLDER_LIST, payload)
  },
  setStoryList({ commit }, payload) {
    commit(STORY.SET_STORY_LIST, payload)
  },
}

const mutations = {
  [STORY.SET_FOLDER_LIST](state, payload) {
    state.folderList = payload
  },
  [STORY.SET_STORY_LIST](state, payload) {
    state.storyList = payload
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
}
