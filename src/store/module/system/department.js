import { getAuthDepartmentList, getDepartmentList } from '@/api/systemManagement/departmentManagement'
import { buildTree } from '@/libs/util'

export default {
  namespaced: true,
  state: {
    deptTreeData: [], // 部门树
    deptDataList: [], // 部门平铺数据
  },
  mutations: {
    setDeptTreeData(state, payload) {
      state.deptTreeData = payload;
    },
    setDeptDataList(state, payload) {
      state.deptDataList = payload;
    }
  },
  actions: {
    /**
     * 获取部门树
     */
    async fetchDeptTree(ctx) {
      const res = await getAuthDepartmentList();
      // const res = await getDepartmentList()
      if (res.data.state === "success") {
        let list = res.data.data;
        list = list.map(item => ({
          ...item,
          id: Number(item.id),
          parentId: item.pid || item.groupId,
          title: item.name,
          expand: false,
          selected: false
        }));
        ctx.commit("setDeptDataList", list);
        // 插入根节点
        list.push({
          id: -1,
          parentId: null,
          title: "/",
          expand: true,
          selected: false
        });
        let deptArr = buildTree(list, null, false, false);
        ctx.commit("setDeptTreeData", deptArr[0].children);
      }
    },
  }
}
