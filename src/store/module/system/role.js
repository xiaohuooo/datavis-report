import { buildTree } from '@/libs/util'
import { getGroupAndRole } from '@/api/systemManagement/roleManagement'

export default {
  namespaced: true,
  state: {
    roleTreeData: [], // 角色树
  },
  mutations: {
    setRoleTreeData(state, payload) {
      state.roleTreeData = payload;
    },
  },
  actions: {
    /**
     * 获取角色树
     */
    async fetchRoleTree(ctx) {
      const res = await getGroupAndRole();
      if (res.data.state === "success") {
        const { sysGroupList, sysRoleList } = res.data.data;
        // this.roleTypeList = sysGroupList.map(item => ({ ...item, id: Number(item.id) }))
        let list = [
          ...sysGroupList.map(item => ({ ...item, parentId: -1, id: Number(item.id) })),
          ...sysRoleList.map(item => ({ ...item, parentId: item.groupId, id: Number(item.id) }))
        ];
        let roleList = buildTree(list, -1);
        ctx.commit("setRoleTreeData", roleList);
      }
    },
  }
}
