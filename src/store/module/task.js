import { TASK } from "../mutation-types";
import { getObjectList } from "@/api/aimManagement";

const state = {
  newTask: null, // 新任务
  targetList: [] // 目标列表
};

const getters = {
  newTask: state => state.newTask
};

const mutations = {
  [TASK.SET_NEW_TASK](state, payload) {
    state.newTask = payload;
  },
  setTargetList(state, payload) {
    state.targetList = payload;
  }
};

const actions = {
  setNewTask({ commit }, payload) {
    commit(TASK.SET_NEW_TASK, payload);
  },
  /**
   * 获取目标数据
   */
  async fetchTargetList(ctx) {
    const res = await getObjectList({ pageNum: 1, pageSize: 1000 });
    if (res.data.state === "success") {
      ctx.commit("setTargetList", res.data.data);
    }
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};
