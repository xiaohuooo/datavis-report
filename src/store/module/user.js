import { getMyInfo, login, logout, sendAuthCode, register, queryUser, changePasswordByUserSelf, feishuLogin , smsMobileLogin } from "@/api/systemManagement/system";
import { getToken, setToken } from "@/libs/util";

export default {
  state: {
    superAdmin: false, // 是否是超级管理员
    applyModel: false, // 公共资源池是否开启资源的申请模式
    nickName: "",
    userName: "",
    deptName: "",
    adminWorkbench: "", // 工作台配置
    userId: "",
    avatarImgPath: "",
    token: getToken(),
    hasGetInfo: false,
    unreadCount: 0,
    btnAllowList: [],
    configData: {} // 个人设置，目前包含权限管理不再提醒的配置
  },
  getters: {
    isSuperAdmin: state => state.superAdmin,
    isCanApply: state => state.applyModel,
    getUserId: state => state.userId
  },
  mutations: {
    setAvatar(state, avatarPath) {
      state.avatarImgPath = avatarPath;
    },
    setUserId(state, id) {
      state.userId = id;
    },
    setSuperAdmin(state, payload) {
      state.superAdmin = payload;
    },
    setApplyModel(state, payload) {
      state.applyModel = payload;
    },
    setNickName(state, name) {
      state.nickName = name;
    },
    setUserName(state, name) {
      state.userName = name;
    },
    setUserDeptName(state, deptName) {
      state.deptName = deptName;
    },
    setAdminWorkbench(state, payload) {
      state.adminWorkbench = payload;
    },
    setToken(state, token) {
      state.token = token;
      setToken(token);
    },
    setHasGetInfo(state, status) {
      state.hasGetInfo = status;
    },
    setBtnAllowList(state, btnAllowList) {
      state.btnAllowList = btnAllowList;
    },
    setConfigData(state, payload) {
      state.configData = payload;
    }
  },
  actions: {
    // 登录
    handleLogin({ state, commit }, loginInfo) {
      let { username, password, credentials } = loginInfo;
      if (username) {
        username = username.trim();
      }
      let params = {
        username,
        password,
        credentials
      };
      return new Promise((resolve, reject) => {
        login(params)
          .then(res => {
            const data = res.data;
            if (data.state === "success") {
              commit("setToken", data.data["token"]);
            }
            resolve(data);
          })
          .catch(err => {
            reject(err);
          });
      });
    },
    // 飞书登录
    handleFeiShuLogin({ state, commit }, loginInfo) {
      console.log(code);
      let { code, redirectUri } = loginInfo;
      return new Promise((resolve, reject) => {
        feishuLogin(code, redirectUri)
          .then(res => {
            const data = res.data;
            if (data.state === "success") {
              commit("setToken", data.data["token"]);
            }
            resolve(data);
          })
          .catch(err => {
            reject(err);
          });
      });
    },
    handleLoginVerification({ state, commit }, loginInfo) { // 验证码登录
      let { verifyCode, mobile } = loginInfo;
      let params = {
        verifyCode ,
        mobile ,
      };
      return new Promise((resolve, reject) => {
        smsMobileLogin(params)
          .then(res => {
            const data = res.data;
            if (data.state === "success") {
              commit("setToken", data.data["token"]);
            }
            resolve(data);
          })
          .catch(err => {
            reject(err);
          });
      });
    },
    // 发送验证码
    handleSendAuthCode({}, way) {
      return sendAuthCode(way);
    },

    handleQueryUser({}, params) {
      return queryUser(params);
    },

    // 修改密码
    handleChangePassword({}, params) {
      return changePasswordByUserSelf(params);
    },

    // 注册
    handleRegister({}, data) {
      return register(data);
    },
    // 退出登录
    handleLogOut({ state, commit }) {
      return new Promise((resolve, reject) => {
        // logout(state.token).then(() => {
        commit("setToken", "");
        resolve();
        // }).catch(err => {
        //   reject(err)
        // })
      });
    },
    // 获取用户相关信息
    getUserInfo({ state, commit, dispatch }) {
      return new Promise((resolve, reject) => {
        try {
          getMyInfo(state.token)
            .then(res => {
              const data = res.data.data;
              const authResourceDetail = data.authResourceDetail;
              let menus = [authResourceDetail.systemMenus, authResourceDetail.businessMenus];

              let configData = data.configData; // 个人配置
              if (configData) {
                configData = JSON.parse(configData);
              }
              commit("setDynamicRoute", menus || []);
              commit("setAvatar", data["headImgUrl"]);
              commit("setSuperAdmin", data.superAdmin);
              commit("setApplyModel", data.applyModel);
              commit("setNickName", data.nickname);
              commit("setUserId", data.userId);
              commit("setUserName", data.username);
              commit("setUserDeptName", data.deptName);
              commit("setAdminWorkbench", data.adminWorkbench?.id || "");
              commit("setHasGetInfo", true);
              commit("setBtnAllowList", authResourceDetail.buttonList || []);
              if (configData) {
                commit("setConfigData", configData);
              }
              // 缓存模块共享的数据到模块store中
              dispatch('role/fetchRoleTree', {}, { root: true })
              dispatch('department/fetchDeptTree', {}, { root: true })
              resolve();
            })
            .catch(err => {
              reject(err);
            });
        } catch (error) {
          reject(error);
        }
      });
    }
  }
};
