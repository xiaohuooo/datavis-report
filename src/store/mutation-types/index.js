/******************** 统一管理mutations常量 ********************/

import REAL_TIME from './real-time'
import INDICATOR from './indicator'
import DIMENSION from './dimension'
import DATASET from './dataset'
import STORY from './story'
import AUTHORIZATION_MANAGEMENT from './authorization-management'
import TASK from './task'
export {
  REAL_TIME,
  INDICATOR,
  DIMENSION,
  DATASET,
  STORY,
  AUTHORIZATION_MANAGEMENT,
  TASK
}
