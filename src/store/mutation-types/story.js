// 故事
const STORY = {
  SET_FOLDER_LIST: 'SET_FOLDER_LIST',
  SET_STORY_LIST: 'SET_STORY_LIST',
}

export default STORY
