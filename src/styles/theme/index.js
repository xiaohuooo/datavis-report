import './index.scss'
import dom from '../../config/dom'

const Plain = {
  $dom: dom,
  currentTheme: null,
  changeTheme(themeName, Vue) {
    !!this.currentTheme && dom.removeClass(document.body,`fa-theme-${this.currentTheme}`)
    dom.addClass(document.body, `fa-theme-${themeName}`)
    this.currentTheme = themeName
  },
  install(Vue, {
    theme = 'default'
  } = {}) {
    Vue.prototype.$plain = Plain
    this.changeTheme(theme, Vue)
  },
}

export default Plain
