/***
 *公共业务逻辑
 */
import BigNumber from 'bignumber.js'
import { ZERO_PRE_COMPARE_DATA_SHOW, NULL_DATA_NUMBER_SHOW, NULL_DATA_PERCENTAGE_SHOW } from '../config/ReportConstants'

/**
 * 按单位处理数据
 * @param data
 * @param unit
 * @returns {BigNumber}
 */
export const formatDataUnit = (data, unit) => {
  if (!data) {
    return new BigNumber(0)
  }
  if (!(data instanceof BigNumber)) {
    data = new BigNumber(data)
  }
  let temp
  if (unit) {
    unit = unit.trim()
    if (unit === '千' || unit === 'qian') {
      temp = data.div(1000)
    } else if (unit === '万' || unit === 'wan') {
      temp = data.div(10000)
    } else if (unit === '亿' || unit === 'yi') {
      temp = data.div(100000000)
    } else {
      temp = data
    }
  }
  return temp
}

/**
 *
 * 格式化数据
 *
 * @param data 本期数据
 * @param decimalDigits 小数点保留位数
 * @param numberUnit 数据单位
 * @param thousandth 是否使用千分位
 * @param showType 数据展示类型，1：百分比  0： 数值
 * @returns {string}
 */
export const formatData = (data, { decimalDigits = 2, numberUnit = 'normal', thousandth = false, showType = 0 }) => {
  if (data === null) {
    return showType === 1 ? NULL_DATA_NUMBER_SHOW : NULL_DATA_PERCENTAGE_SHOW
  }

  if (!(data instanceof BigNumber)) {
    data = new BigNumber(data)
  }

  if (typeof decimalDigits === 'string') {
    decimalDigits = parseInt(decimalDigits)
  }

  if (showType) {
    data = data.multipliedBy(100)
    return thousandth ? data.toFormat(decimalDigits) + '%' : data.toFixed(decimalDigits) + '%'
  } else {
    data = formatDataUnit(data, numberUnit)
    return thousandth ? data.toFormat(decimalDigits) : data.toFixed(decimalDigits)
  }
}

/**
 *
 * 格式化环比/同比数据
 *
 * @param currentTimeData 本期数据
 * @param preTimeData 上期数据
 * @param decimalDigits 小数点保留位数
 * @param numberUnit 数据单位
 * @param thousandth 是否使用千分位
 * @param showType 数据展示类型，1：百分比  0： 数值
 * @returns {string}
 */
export const formatCompareData = (currentTimeData, preTimeData, { decimalDigits = 2, numberUnit = 'normal', thousandth = false, showType = 0 }) => {

  if (!preTimeData || !currentTimeData) {
    return ZERO_PRE_COMPARE_DATA_SHOW
  }

  // 百分比(文本框可能有BP，所以把这里写清楚，不用 if（showType）)
  if (showType===1) {
    if (!(preTimeData instanceof BigNumber)) {
      preTimeData = new BigNumber(preTimeData)
    }
    if (preTimeData.comparedTo(0) === 0) {
      return ZERO_PRE_COMPARE_DATA_SHOW
    }

    if (!(currentTimeData instanceof BigNumber)) {
      currentTimeData = new BigNumber(currentTimeData)
    }

    return currentTimeData.minus(preTimeData).multipliedBy(100).div(preTimeData.abs()).toFixed(decimalDigits) + '%'
  } else {

    if (!(preTimeData instanceof BigNumber)) {
      preTimeData = new BigNumber(preTimeData)
    }

    if (!(currentTimeData instanceof BigNumber)) {
      currentTimeData = new BigNumber(currentTimeData)

    }
    let diff = currentTimeData.minus(preTimeData)
    diff = formatDataUnit(diff, numberUnit)

    return thousandth ? diff.toFormat(decimalDigits) : diff.toFixed(decimalDigits)
  }
}
/**
 * 百分比类型指标环比显示处理
 * @param currentTimeData
 * @param preTimeData
 * @param dataConfig
 * @returns {string|string}
 */
export const formatPercentTypeCompareData = (currentTimeData, preTimeData, dataConfig) => {
  if (dataConfig.dataShowType === 1) { // 环比/同比显示为'%'：（当期数值 - 上期数值）/ 上期数值 * 100 + '%',上期或者当前没有值显示成'--'
    return preTimeData && currentTimeData ? formatCompareData(currentTimeData * 100, preTimeData * 100, dataConfig) : ZERO_PRE_COMPARE_DATA_SHOW
  } else {
    // 环比/同比显示为'123': (当期数值 - 上期数值) * 100 + '%',上期或者当前没有值显示成'--'
    return preTimeData && currentTimeData ? formatData(new BigNumber(currentTimeData).minus(preTimeData || 0),  dataConfig) : ZERO_PRE_COMPARE_DATA_SHOW
  }
}
