export const buildMenus = (menus, isFilterHidden=true) => {
  // 过滤出顶层系统菜单
  let menuTree = isFilterHidden ? menus.filter(
    menu => menu.pid === -1 && !menu.hidden
  ).map(menu => {
    return {
      name: menu.name,
      id: menu.id,
      path: menu.path,
      type: menu.type,
      icon: menu.icon,
      sort: menu.sort
    }
  }) : menus.filter(
    menu => menu.pid === -1
  ).map(menu => {
    return {
      name: menu.name,
      id: menu.id,
      path: menu.path,
      type: menu.type,
      icon: menu.icon,
      sort: menu.sort
    }
  })
  menuTree.sort((i, j) => i.sort - j.sort)
  recursiveMenu(menuTree, menus)
  return menuTree
}

/**
 * 根据权限生成动态路由
 * @param {Object} localAuthenticateRoutes 本地配置的动态路由
 * @param {Array} authenticateMenus 接口返回的动态路由
 * @returns {Array}
 */
export const getDynamicRoutes = (localAuthenticateRoutes, authenticateMenus) => {
  return [{
    path: localAuthenticateRoutes.path,
    name: localAuthenticateRoutes.name,
    component: localAuthenticateRoutes.component,
    children: recursiveRoutes(localAuthenticateRoutes.children, authenticateMenus)
  }]
}

const recursiveRoutes = (localAuthenticateRoutes, authenticateMenus) => {
  let dynamicRoutes = []
  localAuthenticateRoutes.forEach(localRoute => {
    let authenticated = authenticateMenus.find(authenticateMenu => authenticateMenu.path === localRoute.path)
    if (authenticated) {
      let route = {
        path: localRoute.path,
        name: localRoute.name.startsWith('top_') ?  localRoute.name : authenticated.name, // 顶部配置的系统内部菜单需要根据前缀特殊处理
        component: localRoute.component,
      }

      if (localRoute.children && localRoute.children.length > 0) {
        route.children = recursiveRoutes(localRoute.children, authenticateMenus)
      }

      dynamicRoutes.push(route)
    }
  })
  return dynamicRoutes
}

const recursiveMenu = (menus, totalMenus) => {
  menus.forEach(businessMenu => {
    let children = totalMenus.filter(bm => bm.pid === businessMenu.id && !bm.hidden)
      .map(menu => {
        return {
          name: menu.name,
          id: menu.id,
          type: menu.type,
          path: menu.path,
          icon: menu.icon,
          sort: menu.sort
        }
      });
    children.sort((i, j) =>  i.sort - j.sort)
    if (children.length > 0) {
      businessMenu.children = children
      recursiveMenu(children, totalMenus)
    }
  })
}
