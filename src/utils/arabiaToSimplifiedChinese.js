export const arabiaToSimplifiedChinese = (num) => {
  let part = String(num), newChar = "";
  //小数点前进行转化
  for (let i = part.length - 1; i >= 0; i--) {
    let tmpNewChar = ""
    let perChar = part.charAt(i);
    switch (perChar) {
      case "0":  tmpNewChar = "零" + tmpNewChar;break;
      case "1": tmpNewChar = "一" + tmpNewChar; break;
      case "2": tmpNewChar = "二" + tmpNewChar; break;
      case "3": tmpNewChar = "三" + tmpNewChar; break;
      case "4": tmpNewChar = "四" + tmpNewChar; break;
      case "5": tmpNewChar = "五" + tmpNewChar; break;
      case "6": tmpNewChar = "六" + tmpNewChar; break;
      case "7": tmpNewChar = "七" + tmpNewChar; break;
      case "8": tmpNewChar = "八" + tmpNewChar; break;
      case "9": tmpNewChar = "九" + tmpNewChar; break;
    }
    switch (part.length - i - 1) {
      case 0: tmpNewChar = tmpNewChar; break;
      case 1: if (perChar != 0) tmpNewChar = tmpNewChar + "十"; break;
      case 2: if (perChar != 0) tmpNewChar = tmpNewChar + "百"; break;
      case 3: if (perChar != 0) tmpNewChar = tmpNewChar + "千"; break;
      case 4: tmpNewChar = tmpNewChar + "万"; break;
      case 5: if (perChar != 0) tmpNewChar = tmpNewChar + "十"; break;
      case 6: if (perChar != 0) tmpNewChar = tmpNewChar + "百"; break;
      case 7: if (perChar != 0) tmpNewChar = tmpNewChar + "千"; break;
      case 8: tmpNewChar = tmpNewChar + "亿"; break;
      case 9: tmpNewChar = tmpNewChar + "十"; break;
    }
    newChar = tmpNewChar + newChar;
  }
  if (newChar.indexOf("一十") == 0) {
    newChar = newChar.substr(1);
  }
  return newChar;
}
