/**
 * 图表日期处理相关方法
 * */
import moment from 'moment'
import {YEAR_HOLIDAYS} from '../config/YearHolidays'
import {DAY_REPORT_USE_HOLIDAYS} from '../config/ReportConstants'

const ONE_DAY_TIMESTAMP = 86400000
const SAME_COMPARE = 'SC'//同比 sameCompare=>SC
const COMPARE = 'C'//环比 compare=>C
const SAME_COMPARE_MONTH = 'mom'//月同比
const SAME_COMPARE_YEAR = 'yoy'//年同比
const formatDay = 'YYYY-MM-DD', formatMonth = 'YYYY-MM'
/**
 * 判断日期是否是合法字符串
 * @param v
 * @returns {boolean}
 */
export const checkDateValid = (v) => {
  let vArr = v.indexOf('/') !== -1 ? v.split('/') : v.split('-')
  if (vArr.length !== 3) {
    return false
  }
  let [yearStr, monthStr, dateStr] = vArr

  if (isNaN(yearStr) || isNaN(monthStr) || isNaN(dateStr)) {
    return false
  }
  let [year, month, date] = [parseInt(yearStr), parseInt(monthStr), parseInt(dateStr)]
  if (month > 12 || month < 1 || date < 1 || date > 31) {
    return false
  }
  if ((month === 4 || month === 6 || month === 9 || month === 11) && date > 30) {
    return false
  }
  if (month === 2 && date > 29) {
    return false
  }
  if (((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) && date > 28) {
    return false
  }
  return true
}

/**
 * 获取给定年月的天数
 * @param year
 * @param month
 * @return {number}
 */
export const getDayNum = (year, month) => {
  let days = 30
  switch (month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      days = 31
      break
    case 2:
      if (year % 4 === 0 && year % 100 !== 0) {
        days = 29
      } else if (year % 400 === 0) {
        days = 29
      } else {
        days = 28
      }
      break
  }
  return days
}

/**
 * 获取给定日期与时间跨度（天）之前（之后）的日期
 * @param date
 * @param offset 时间跨度，单位为天；整数；
 * @returns {Date}
 */
export const getDateByOffset = (date, offset) => {
  return new Date(date.getTime() + ONE_DAY_TIMESTAMP * offset)
}

/**
 * 获取给定日期前一年本月的第一天
 * @param date
 * @returns {Date}
 */
export const getPreYearMonthFirstDate = (date) => {
  let year = date.getFullYear() - 1
  let month = date.getMonth() + 1
  return new Date(year + '/' + month + '/1')
}

/**
 * 获取给定日期后一年本月的第一天
 * @param date
 * @returns {Date}
 */
export const getNextYearMonthFirstDate = (date) => {
  let year = date.getFullYear() + 1
  let month = date.getMonth() + 1
  return new Date(year + '/' + month + '/1')
}

/**
 * 获取给定日期前一个月的第一天
 * @param date
 * @returns {Date}
 */
export const getPreMonthFirstDate = (date) => {
  let year = date.getFullYear()
  let month = date.getMonth() + 1
  if (month !== 1) {
    month--
  } else {
    year--
    month = 12
  }
  return new Date(year + '/' + month + '/1')
}

/**
 * 获取给定日期后一个月的第一天
 * @param date
 * @returns {Date}
 */
export const getNextMonthFirstDate = (date) => {
  let year = date.getFullYear()
  let month = date.getMonth() + 1
  if (month !== 12) {
    month++
  } else {
    year++
    month = 1
  }
  return new Date(year + '/' + month + '/1')
}

/**
 * 判断给定日期是否是节假日（周六、周日、节假日）不包含补班
 * @param dateStr
 * @returns {boolean}
 */
export const judgeHoliday = (dateStr) => {
  let date = new Date(dateStr)
  // 排除周六周日
  let weekDay = date.getDay()
  if (weekDay === 0 || weekDay === 6) {
    return true
  }

  // 排除节假日
  let dateFormat = moment(date).format('YYYYMMDD')
  let holidayIndex = YEAR_HOLIDAYS.findIndex(i => i === dateFormat)
  return holidayIndex !== -1
}

/**
 * 获取给定日期或者最近的一个工作日
 * @param selectDateStr
 * @returns {*}
 */
export const getLastTradingDate = (selectDateStr) => {
  let lastTradingDate = selectDateStr
  if (judgeHoliday(selectDateStr)) {
    // 往前推一天
    const lastDate = moment(new Date(selectDateStr)).add(-1, 'days').format('YYYY-MM-DD')
    return getLastTradingDate(lastDate)
  } else {
    return lastTradingDate
  }
}

/**
 * 获取私有日期
 * @param configData
 * @param startDate
 * @param endDate
 * @param periodType
 * @returns {{endDate: string, startDate: string}}
 */
export const getChartPrivateDate = (configData, periodType, startDate, endDate) => {
  if (!configData.linkPublicDate) {
    return {
      startDate: configData.fixedStartDate,
      endDate: configData.fixedEndDate
    }
  }
  if (startDate === undefined) {
    let dateRange = getDateRange({periodType})
    startDate = moment(dateRange.startDate).format('YYYY-MM-DD')
    endDate = moment(dateRange.endDate).format('YYYY-MM-DD')
  }
  let type = configData.dateOrRange
  let v = type === 1 ? configData.dateBefore : configData.beforeRange
  if (periodType === '2') { // 天
    periodType = 'days'
  } else if (periodType === '3') { // 周
    periodType = 'weeks'
  } else if (periodType === '4') { // 月
    periodType = 'months'
  } else if (periodType === '7') { // 年
    periodType = 'years'
  }
  if (type === 1) {
    startDate = moment(new Date(startDate)).add(-1 * parseInt(v), periodType).format('YYYY-MM-DD')
    if (periodType === 'months') {
      let d = new Date(startDate)
      d.setMonth(d.getMonth() + 1)
      d.setDate(1)
      d.setDate(d.getDate() - 1)
      endDate = moment(d).format('YYYY-MM-DD')
    } else if (periodType === 'days') {
      startDate = getLastTradingDate(startDate)
      endDate = startDate
    } else {
      endDate = moment(new Date(endDate)).add(-1 * parseInt(v), periodType).format('YYYY-MM-DD')
    }
  } else {
    // 排除节假日
    if (!DAY_REPORT_USE_HOLIDAYS && periodType === 'days') {
      let realEndDate = getLastTradingDate(endDate)
      let realStartDate = realEndDate
      for (let i = 0; i < parseInt(v) - 1; i++) {
        let preDateStr = moment(new Date(realStartDate)).add(-1, periodType).format('YYYY-MM-DD')
        if (judgeHoliday(preDateStr)) {
          i--;
        }
        realStartDate = preDateStr
      }
      startDate = realStartDate
      endDate = realEndDate
    } else {
      startDate = moment(new Date(startDate)).add(-1 * (parseInt(v) - 1), periodType).format('YYYY-MM-DD')
    }
  }
  return {startDate, endDate}
}

// http://129.211.167.119/zentao/task-view-117.html
export const resetChartPrivateDate = (dateOrRange, v, periodType, fixedStartDate, fixedEndDate) => {
  let today = new Date()
  if (dateOrRange) {
    if (periodType === 'days') {
      let startDate = moment().add(-1 * parseInt(v), 'days').format('YYYY-MM-DD')
      startDate = getLastTradingDate(startDate)
      return {
        startDate,
        endDate: startDate
      }
    } else if (periodType === 'weeks') {
      let day = today.getDay()
      let endDate = getDateByOffset(today, -day)
      let startDate = getDateByOffset(endDate, -6)
      return {
        startDate: moment(startDate).add(-1 * parseInt(v), periodType).format('YYYY-MM-DD'),
        endDate: moment(endDate).add(-1 * parseInt(v), periodType).format('YYYY-MM-DD'),
      }
    } else if (periodType === 'months') {
      let date = today.getDate()
      let endDate = getDateByOffset(new Date(), -date)
      let startDate = new Date(endDate.getFullYear() + '/' + (endDate.getMonth() + 1) + '/1')
      return {
        startDate: moment(startDate).add(-1 * parseInt(v), periodType).format('YYYY-MM-DD'),
        endDate: moment(endDate).add(-1 * parseInt(v), periodType).format('YYYY-MM-DD'),
      }
    } else if (periodType === 'years') {
      let year = today.getFullYear()
      return {
        startDate: moment(new Date(year + '/1/1')).add(-1 * parseInt(v), periodType).format('YYYY-MM-DD'),
        endDate: moment(new Date(year + '/12/31')).add(-1 * parseInt(v), periodType).format('YYYY-MM-DD'),
      }
    }
  } else {
    let endDate
    if (periodType === 'days') {
      endDate = moment().add(-1, 'days').format('YYYY-MM-DD')
      return {
        startDate: moment(endDate).add(-1 * (parseInt(v) - 1), 'days').format('YYYY-MM-DD'),
        endDate
      }
    } else if (periodType === 'weeks') {
      let day = today.getDay()
      let endDate = getDateByOffset(today, -day)
      let startDate = getDateByOffset(endDate, -6)
      return {
        startDate: moment(startDate).add(-1 * parseInt(v), periodType).format('YYYY-MM-DD'),
        endDate: moment(endDate).format('YYYY-MM-DD'),
      }
    } else if (periodType === 'months') {
      let date = today.getDate()
      let endDate = getDateByOffset(new Date(), -date)
      let startDate = new Date(endDate.getFullYear() + '/' + (endDate.getMonth() + 1) + '/1')
      return {
        startDate: moment(startDate).add(-1 * parseInt(v), periodType).format('YYYY-MM-DD'),
        endDate: moment(endDate).format('YYYY-MM-DD'),
      }
    } else if (periodType === 'years') {
      let year = today.getFullYear()
      return {
        startDate: moment(new Date(year + '/1/1')).add(-1 * parseInt(v), periodType).format('YYYY-MM-DD'),
        endDate: year + '-12-31',
      }
    }
  }
}

/**
 * 计算累计时间范围
 * */
export const calculateAccumulateDate = (date, period) => {
  let startDate = date.startDate
  let endDate = date.endDate
  startDate = moment(startDate).startOf(period).format('YYYY-MM-DD')
  return {
    startDate,
    endDate
  }
}

/**
 * 根据给定周期范围获取对应的开始和结束时间
 * @param periodType
 * @returns {{endDate: Date, startDate: Date}}
 */
export const getDateRange = (data) => {
  let {periodType, dateView} = data
  let today = new Date()
  if (periodType === '2') {
    let startDate = moment().add(-1, 'days').format('YYYY-MM-DD')
    if (!DAY_REPORT_USE_HOLIDAYS) {
      startDate = new Date(getLastTradingDate(startDate))
    }
    return {
      startDate,
      endDate: startDate
    }
  } else if (periodType === '3') {
    let day = today.getDay()
    let endDate = getDateByOffset(today, -day)
    let startDate = getDateByOffset(endDate, -6)
    return {
      startDate, endDate
    }
  } else if (periodType === '4') {
    let date = today.getDate()
    let endDate = getDateByOffset(new Date(), -date)
    let startDate = new Date(endDate.getFullYear() + '/' + (endDate.getMonth() + 1) + '/1')
    return {
      startDate, endDate
    }
  } else if (periodType === '7') {
    let year = today.getFullYear()
    return {
      startDate: new Date((year - 1) + '/1/1'),
      endDate: new Date((year - 1) + '/12/31')
    }
  } else if (periodType === '99') {
    // 获取当前时间
    let day = (new Date(dateView.endDate).getTime() - new Date(dateView.startDate).getTime()) / ONE_DAY_TIMESTAMP
    let endDate = getDateByOffset(today, -1)
    return {
      startDate: getDateByOffset(endDate, -day),
      endDate
    }
  }
}

/**
 * 获取同比环比的开始时间和结束时间
 * @param type compare=> 环比，sameCompare=>同比
 * @param originData 原始时间{startDate,endDate,compareType=>compareType是同比的时候才会有且 mom 为月同比，yoy 为年同比}
 * @param globalReportDate 全局日期
 */
export const getCompareDate = (type, originData, globalReportDate) => {
  switch (type) {
    case COMPARE:
      return compareDate(originData)
    case SAME_COMPARE:
      return sameCompareDate(originData, globalReportDate)
  }
}

const getOriginDataOfDate = (originData) => {
  const startDateStr = originData.startDate
  const endDateStr = originData.endDate
  const startDate = new Date(startDateStr)
  const endDate = new Date(endDateStr)
  return {startDateStr, endDateStr, startDate, endDate}
}

const compareDate = (originData) => {
  const {startDate, endDate} = getOriginDataOfDate(originData)
  let intervalDays = moment(endDate).diff(moment(startDate), 'days')
  let compareStartDate = moment(startDate).subtract(intervalDays + 1, 'days').format(formatDay)
  let compareEndDate = moment(endDate).subtract(intervalDays + 1, 'days').format(formatDay)
  if (!DAY_REPORT_USE_HOLIDAYS && compareStartDate === compareEndDate) {
    compareStartDate = getLastTradingDate(compareStartDate)
    compareEndDate = compareStartDate
  }
  return {endDate: compareEndDate, startDate: compareStartDate}
}

const sameCompareDate = (originData, globalReportDate) => {
  const sameCompareType = originData.compareType
  if (!sameCompareType) return
  const {startDateStr, endDateStr, startDate, endDate} = getOriginDataOfDate(originData)
  let compareStartDate, compareEndDate
  switch (sameCompareType) {
    case SAME_COMPARE_MONTH:
      compareStartDate = moment(startDate).subtract(1, 'month').format(formatDay)
      compareEndDate = moment(endDate).subtract(1, 'month').format(formatDay)
      if (globalReportDate[3] === '4' &&
        moment(startDateStr).format('YYYY-MM') === moment(endDateStr).format(formatMonth)) {
        // 周期类型为月，开始时间和结束时间是同一个月

        if (moment(startDateStr).startOf('month').format(formatDay) === startDateStr &&
          moment(endDateStr).endOf('month').format(formatDay) === endDateStr) {
          // 开始时间是同一个月的第一天，结束时间是同一个月的最后一天
          compareEndDate = moment(compareEndDate).endOf('month').format(formatDay)
        }
      }
      return {
        startDate: compareStartDate,
        endDate: compareEndDate
      }
    case SAME_COMPARE_YEAR:
      compareStartDate = moment(startDate).subtract(1, 'year').format(formatDay)
      compareEndDate = moment(endDate).subtract(1, 'year').format(formatDay)
      return {
        startDate: compareStartDate,
        endDate: compareEndDate
      }
  }
}
