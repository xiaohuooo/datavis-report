/**
 * 公共js处理逻辑
 */
import { GLOBAL } from '@/config/ReportConstants'
import BigNumber from 'bignumber.js'

/**
 * 防抖函数
 * @param fn
 * @returns {function(): void}
 * 使用方法参考http://www.zyiz.net/tech/detail-135069.html
 */
export const debounce = (fn, delay = 500) => {
  let timeout = null // 创建一个标记用来存放定时器的返回值
  return function () {
    clearTimeout(timeout) // 每当用户输入的时候把前一个 setTimeout clear 掉
    timeout = setTimeout(() => {
      // 然后又创建一个新的 setTimeout, 这样就能保证输入字符后的
      // interval 间隔内如果还有字符输入的话，就不会执行 fn 函数
      fn.apply(this, arguments);
    }, delay);
  }
}

/**
 * 节流函数
 * @param callback
 * @param time
 * @return {Function}
 */
export const throttle = (callback, time) => {
  let timer,
    firstTime

  return function (...res) {
    const _this = this

    if (firstTime) {
      callback.apply(_this, res)
      return firstTime = false
    }


    if (timer) return

    timer = setTimeout(() => {
      clearTimeout(timer)
      timer = null
      callback.apply(_this, res)
    }, time || 25)
  }
}

/**
 * 属性数据过滤
 * @param exist 通过标签搜索出来的维度或者指标
 * */
export const filterTree = (arr, val, exist = []) => {
  if (!(arr && arr.length)) {
    return []
  }
  let children = []
  arr.forEach(item => {
    // 如果组名中包含查询值，则子项都包含
    if (item.title && item.title.indexOf(val) > -1) {
      children.push(item)
      return
    }
    // 如果搜索的标签包含该项
    if (exist.includes(item.id)) {
      children.push(item)
      return
    }
    let subs = filterTree(item.children, val, exist)
    let newChild = {
      ...item,
      render: item.render
    }
    if (item.type !== undefined) {
      newChild.type = item.type
    }
    if (subs && subs.length) {
      newChild.children = subs
      newChild.expand = true
      children.push(newChild)
    }
  })
  return children
}

// 登录时寻找第一个有权限的子菜单
export const findFirstMenuName = (array) => {
  let FirstItem = array[0]
  if (FirstItem.children && FirstItem.children.length > 0) {
    return findFirstMenuName(FirstItem.children)
  } else {
    return FirstItem.name
  }
}

/**
 * 深拷贝
 * @param obj
 * @returns {Array}
 */
export const deepCopy = (obj) => {
  let objClone = Array.isArray(obj) ? [] : {}
  if (obj && typeof obj === 'object') {
    for (let key in obj) {
      if (obj[key] && typeof obj[key] === 'object') {
        objClone[key] = deepCopy(obj[key])
      } else {
        objClone[key] = obj[key]
      }
    }
  }
  return objClone
}

/**
 * 得到分离后的原指标名称
 * @param {总名称 指标名加父指标组名} name
 * @param {name和originName和parentName对象数组} arr
 */
export const getOriginName = (name, arr) => {
  let originName = name
  let findItem = arr.find(item => {
    return name === (item.originName + GLOBAL.separator + item.parentName)
  })
  if (findItem) {
    originName = findItem.originName
  }
  return originName
}

/**
 * 根据headers调整顺序
 * @param {数据} data
 * @param {指标维度id} headers
 * @param {propertyData} param2
 * dimension 维度
 * series 系列
 * indicator 指标
 * @returns
 */
export const adjustDataByHeaders = (data = [], headers = [], { dimension = [], series = [], indicator = [] }) => {
  let newData = []
  if (!data) return newData
  if (!headers) return newData

  const newDimension = [...dimension, ...series];
  newDimension.forEach(id => {
    let columnIndex = headers.map(item => item.toString()).indexOf(id.toString())
    data.forEach((item, index) => {
      if (newData[index] === undefined) {
        newData[index] = []
      }
      newData[index].push(item[columnIndex])
    })
  })
  indicator.forEach(item => {
    let id = item.id
    let columnIndex = headers.map(item => item.toString()).indexOf(id.toString())
    data.forEach((item, index) => {
      if (newData[index] === undefined) {
        newData[index] = []
      }
      newData[index].push(item[columnIndex])
    })
  })
  return newData
}

/**
 * 邮箱校验
 * @param {邮箱} email
 */
export function emailVerify(email) {
  const re = /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}(\.[a-zA-Z0-9]{2,6})?$/
  return re.test(email)
}

/**
 * 排序
 * @param {数据} data
 * @param {propertyData} param1
 * configData 配置信息
 * dimension 维度
 * indicator 指标
 * @returns
 */
export const sortingProcess = (data = [], { configData = {}, dimension = [], indicator = [], series = [] }) => {
  let newData = []
  if (configData.isSort === 'true' && configData.sortBy !== '' && configData.sortBy !== undefined &&  configData.sortOrder !== '') {
    // isSort 是否排序
    // sortBy 排序依据 dimension_维度id 或 indicator_指标id
    // sortOrder 次序 asc升序 desc降序
    let sortData = formatDataBeforeSort({ data: deepCopy(data), dimension, indicator, series });
    const { sortBy, sortOrder } = configData
    if (sortOrder === 'asc') {
      sortData = sortData.sort(ascSort(sortBy))
    } else if (sortOrder === 'desc') {
      sortData = sortData.sort(descSort(sortBy))
    }
    // 将排序后的数据处理为之前的格式
    newData = formatDataAfterSort({ data, sortData, dimension, indicator, series });
  } else {
    newData = data
  }
  return newData
}

// 排序前，组装需要的数据格式
function formatDataBeforeSort({ data, dimension, indicator, series }) {
  if(series.length) {
    // 存在系列（单维度/单指标）
    return data.map(item => {
      const dimensionName = item.shift()
      const indicatorTotal = item.reduce((prev, next) => {
        // 指标值为null时 排序时转为0处理
        prev += next || 0;
        return prev;
      }, 0);
      return {
        ["dimension_" + dimension[0]]: dimensionName,
        ["indicator_" + indicator[0].id]: indicatorTotal
      };
    });
  } else {
    return data.map(item => {
      let obj = {}
      dimension.forEach((dimensionItem, index) => {
        obj['dimension_' + dimensionItem] = item[index]
      })
      let dimensionLength = dimension.length
      indicator.forEach((indicatorItem, index) => {
        // 指标值为null时 排序时转为0处理
        if (item[index + dimensionLength] === null) {
          obj['indicator_' + indicatorItem.id] = 0
        } else {
          obj['indicator_' + indicatorItem.id] = item[index + dimensionLength]
        }
      })
      let total = 0
      let indicatorKeys = Object.keys(obj).filter(item => item.startsWith('indicator_'))
      indicatorKeys.forEach(item => {
        total = new BigNumber(obj[item]).plus(total).toNumber()
      })
      obj['total'] = total
      return obj
    })
  }
}

// 排序后，还原成之前的数据格式
function formatDataAfterSort({ data, sortData, dimension, indicator, series }) {
  if(series.length) {
    // 存在系列（单维度/单指标）
    return sortData.map(item => {
      const dimensionName = item["dimension_" + dimension[0]];
      return data.find(ele => ele[0] === dimensionName);
    })
  } else {
    return sortData.map(item => {
      let arr = []
      dimension.forEach((dimensionItem, index) => {
        arr[index] = item['dimension_' + dimensionItem]
      })
      let dimensionLength = dimension.length
      indicator.forEach((indicatorItem, index) => {
        arr[index + dimensionLength] = item['indicator_' + indicatorItem.id]
      })
      return arr
    })
  }
}

// 升序排序
function ascSort(arg) {
  return function (a, b) {
    if (typeof a[arg] == 'number') {
      return a[arg] - b[arg]
    } else {
      return a[arg].localeCompare(b[arg])
    }
  }
}

// 降序排序
function descSort(arg) {
  return function (a, b) {
    if (typeof a[arg] == 'number') {
      return b[arg] - a[arg]
    } else {
      return b[arg].localeCompare(a[arg])
    }
  }
}

