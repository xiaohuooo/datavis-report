import _ from 'lodash'
/**
 * 数字类型的字段
 */
export const isNumberTypeFiled = (filedType) => {
  filedType = filedType.toUpperCase()
  const numberTypes = ["FLOAT","INT","DOUBLE","INT UNSIGNED","BIGINT","DECIMAL","NUMBER"]
  return _.indexOf(numberTypes, filedType) !== -1
}
/**
 * 字符串类型的字段
 */
export const isStringTypeFiled = (filedType) => {
  filedType = filedType.toUpperCase()
  const stringTypes = ["STIRNG","VARCHAR2","NVARCHAR2","VARCHAR","CHAR","CLOB","BLOB","TEXT"]
  return _.indexOf(stringTypes, filedType) !== -1
}
/**
 * 日期类型的字段
 */
export const isDateTypeFiled = (filedType) => {
  filedType = filedType.toUpperCase()
  const dateTypes = ["DATE","DATETIME","TIMESTAMP"]
  return _.indexOf(dateTypes, filedType) !== -1
}
export const getFieldTypeLabel = (filedType) => {
  let fieldTypeLabel = ''
  if (isNumberTypeFiled(filedType)) {
    fieldTypeLabel = '数字'
  } else if (isStringTypeFiled(filedType)) {
    fieldTypeLabel = '字符串'
  }  else if (isDateTypeFiled(filedType)) {
    fieldTypeLabel = '日期'
  }
  return fieldTypeLabel
}
