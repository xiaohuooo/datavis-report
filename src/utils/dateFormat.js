import { getDateByOffset, getLastTradingDate } from "@/utils/chartDate";
import { DAY_REPORT_USE_HOLIDAYS } from "@/config/ReportConstants";

/**
 * 获取当前日期的前/后几天
 * @param {Number} num
 * @param {Date} date 默认是当前日期
 * @returns
 */
export const dateChange = (num = 1, date = null) => {
  if (!date) {
    date = new Date(); //没有传入值时，默认是当前日期
    date = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
  }
  date += " 00:00:00"; //设置为当天凌晨12点
  date = Date.parse(new Date(date)) / 1000; //转换为时间戳
  date += 86400 * num; //修改后的时间戳
  const newDate = new Date(parseInt(date) * 1000); //转换为时间

  let month = newDate.getMonth() + 1;
  month = month < 10 ? `0${month}` : `${month}`;
  let day = newDate.getDate();
  day = day < 10 ? `0${day}` : `${day}`;
  return newDate.getFullYear() + "-" + month + "-" + day;
};

/**
 * 获取当前日期的格式化数据
 * @param {Date} timeData 时间数据
 * @param {Date} type 需要的格式
 * @returns
 */
export const getTimeFormat = ({ timeData, type = "YYYY-MM-DD hh:mm:ss" }) => {
  const time = new Date(timeData);
  let year = time.getFullYear();
  let month = time.getMonth() + 1;
  month = month < 10 ? `0${month}` : month;
  let day = time.getDate();
  day = day < 10 ? `0${day}` : day;
  let hour = time.getHours();
  hour = hour < 10 ? `0${hour}` : hour;
  let min = time.getMinutes();
  min = min < 10 ? `0${min}` : min;
  let secord = time.getSeconds();
  secord = secord < 10 ? `0${secord}` : secord;

  if (type === "YYYY-MM-DD hh:mm:ss") {
    return `${year}-${month}-${day} ${hour}:${min}:${secord}`;
  } else if (type === "YYYY-MM-DD") {
    return `${year}-${month}-${day}`;
  } else if (type === "hh:mm") {
    return `${hour}:${min}`;
  } else if (type === "hh:mm:ss"){
    return `${hour}:${min}:${secord}`;
  } else if("x月x日") {
    return `${month}月${day}日`;
  }
};

/**
 * 判断传入日期的前后关系
 * @param {Date} start 开始日期
 * @param {Date} end 结束日期
 * @returns
 */
export const judgeDateRelation = (start, end) => {
  const startTime = new Date(start).getTime();
  const endTime = new Date(end).getTime();
  if (startTime > endTime) {
    return ">";
  } else if (startTime < endTime) {
    return "<";
  } else if (startTime === endTime) {
    return "=";
  }
};

/**
 * 判断传入日期的相隔天数
 * @param {Date} start 开始日期
 * @param {Date} end 结束日期
 * @returns
 */
export const judgeDateApart = ({ start, end }) => {
  const startTime = new Date(start).getTime();
  const endTime = new Date(end).getTime();
  const day = (endTime - startTime) / (1000 * 3600 * 24);
  return day + 1;
};

/**
 * 获取业务日期，当前日期的前一天，需要跳过节假日、周末
 */
export const getBusinessDate = () => {
  let yesterday = getDateByOffset(new Date(), -1);
  if (!DAY_REPORT_USE_HOLIDAYS) {
    yesterday = new Date(getLastTradingDate(yesterday));
  }
  return getTimeFormat({ timeData: yesterday, type: "YYYY-MM-DD" });
};

/**
 * 星期转换: [1, 2, 3, 4] => ["周一", "周二", "周三", "周四", ...]
 */
export const numTransformWeek = weekArr => {
  let arr = [];
  weekArr.forEach(ele => {
    let weekday = null;
    switch (ele) {
      case 1:
        weekday = "周一";
        break;
      case 2:
        weekday = "周二";
        break;
      case 3:
        weekday = "周三";
        break;
      case 4:
        weekday = "周四";
        break;
      case 5:
        weekday = "周五";
        break;
      case 6:
        weekday = "周六";
        break;
      case 7:
        weekday = "周日";
        break;
    }
    arr.push(weekday);
  });

  return arr;
};

/**
 * 星期转换: ["周一", "周二", "周三", "周四", ...] => [1, 2, 3, 4, ...]
 */
export const weekTransformNum = weekArr => {
  return weekArr.map(ele => {
    let weekday = null;
    switch (ele) {
      case "周一":
        weekday = 1;
        break;
      case "周二":
        weekday = 2;
        break;
      case "周三":
        weekday = 3;
        break;
      case "周四":
        weekday = 4;
        break;
      case "周五":
        weekday = 5;
        break;
      case "周六":
        weekday = 6;
        break;
      case "周日":
        weekday = 7;
        break;
    }
    return weekday;
  });
};
