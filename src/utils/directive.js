/**
 * 防抖指令
 */
export const debounce= {
     //handler：绑定的方法  threshhold：多长时间执行一次  eventment：执行的事件    v-debounce="{handler:执行方法,wait:600,eventment:事件名称}"
    inserted: (el ,binding) => {
        if (!binding.value.handler || typeof binding.value.handler !='function' ) {
            return 
        }
        const wait = binding.value.wait || 600 //等待时间
        let timer
        const eventment = binding.value.eventment || 'input'  //事件名称
        el.addEventListener(eventment, () => {
            if (timer) {
                clearTimeout(timer)
            }
            timer = setTimeout(() => {
                binding.value.handler()
            }, wait)
        })
    }
}
/**
 * 节流指令
 */
export const throttle = {
        //handler：绑定的方法  threshhold：多长时间执行一次  eventment：执行的事件
        inserted: (el ,binding) => {
        if (!binding.value.handler || typeof binding.value.handler !='function' ) {     
         return }

        const threshhold = binding.value.threshhold || 1000
         let startTime = new Date().getTime()
         let  timer
         const eventment = binding.value.eventment || 'input'  //事件名称
         el.addEventListener(eventment, () => {
         const currentTime =  new Date().getTime()
           clearTimeout(timer)
          if (currentTime - startTime >= threshhold ) {
            binding.value.handler()
              startTime = currentTime
           }else{
              timer = setTimeout(() => {//最后一次的执行
                binding.value.handler()
             }, threshhold);
            }})
        }
}

export const floatingWindow = {
    inserted(el, binding) {
      const floatingWindow = document.createElement('div');
      floatingWindow.className = 'floating-window';
      floatingWindow.style.position = 'fixed';
      floatingWindow.style.display = 'none';
  
      const content = binding.value.content;
      floatingWindow.innerHTML = content;
  
      // 修改浮窗的样式
      floatingWindow.style.backgroundColor = '#ffffff';
      floatingWindow.style.border = '1px solid #cccccc';
      floatingWindow.style.padding = '2px 5px';
      floatingWindow.style.borderRadius = '5px';
      floatingWindow.style.fontSize = '12px';
      floatingWindow.style.boxShadow = '0 2px 4px rgba(0, 0, 0, 0.2)'; 
      document.body.appendChild(floatingWindow);
  
      const moveFloatingWindow = throttle2(function(event) {
        const x = event.clientX;
        const y = event.clientY;
        floatingWindow.style.left = x + 'px';
        floatingWindow.style.top = y + 'px';
      }, 100); // 节流时间间隔，根据实际需求进行调整
  
      el.addEventListener('mouseenter', () => {
        floatingWindow.style.display = 'block';
      });
  
      el.addEventListener('mousemove', moveFloatingWindow);
  
      el.addEventListener('mouseleave', () => {
        floatingWindow.style.display = 'none';
      });
  
      el._floatingWindowEvents = {
        moveFloatingWindow
      };
    },
    unbind(el) {
      const { moveFloatingWindow } = el._floatingWindowEvents;
  
      el.removeEventListener('mouseenter', () => {
        floatingWindow.style.display = 'block';
      });
      el.removeEventListener('mousemove', moveFloatingWindow);
      el.removeEventListener('mouseleave', () => {
        floatingWindow.style.display = 'none';
      });
  
      delete el._floatingWindowEvents;
    }
  };
  
  function throttle2(func, delay) {
    let timer = null;
    return function(...args) {
      if (!timer) {
        timer = setTimeout(() => {
          func.apply(this, args);
          timer = null;
        }, delay);
      }
    };
  }
  
  
