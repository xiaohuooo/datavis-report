/**
 * 数据显示格式处理方法：包括单位、小数精度、千分位等
 */

import _, {isNumber} from 'lodash'
// force强制保留precision位小数
const renderNumber = (number, unit, isFormat = true,force=true) => {
  const value = Number(number)
  if ((!number && number !== 0) || !isNumber(value)) {
    return '--'
  }
  // 为0时是否需要保留两位小数
  if (value === 0) {
    return `${force ? '0.00' : '0'}${unit || ''}`
  }
  const isMoreThan1 = Math.abs(value) >= 1
  const digit = _.split(String(value), '.')
  let realPrecision = 0
  let defaultPrecision = 2
  if (digit.length === 2) {
    realPrecision = digit[1].length
    realPrecision = realPrecision > 0 ? realPrecision : 0
    // 保留第一个不为0的数
    defaultPrecision = _.findIndex(digit[1].split(''), char => char !== '0') + 1
    defaultPrecision = defaultPrecision > 2 ? defaultPrecision : 2
  }
  const num = _.round(value, _.min([defaultPrecision, realPrecision]))
  if (String(num).indexOf('.') !== -1 && String(num).lastIndexOf(0) === String(num).length - 1) {
    return `${isFormat && isMoreThan1 ? Number(num).toLocaleString() : num}${unit || ''}0`
  }
  return `${isFormat && isMoreThan1 ? Number(num).toLocaleString() : num}${unit || ''}`
}

const renderPercent = number => renderMoney(number * 100, '%')

export const renderMoney = (number, unit, precision = 2,force=true, isFormat = true) => {
  const value = Number(number)
  if ((!number && number !== 0) || !isNumber(value)) {
    return '--'
  }
  // 为0时是否需要保留两位小数
  if (value === 0) {
    return `${force ? '0.00' : '0'}${unit || ''}`
  }
  let realPrecision = precision
  const isMoreThan1 = Math.abs(value) >= 1
  let needFull = true
  if (!force) {
    // 保留第一个不为0的数
    const digit = _.split(String(value), '.')
    if (digit.length === 2 &&  !isMoreThan1) {
      realPrecision = _.findIndex(digit[1].split(''), char => char !== '0') + 1
      if (realPrecision > precision) {
        needFull = false
      } else {
        realPrecision = precision
      }
    }
  }
  const data = _.round(value, realPrecision)
  const num = isFormat && isMoreThan1 ? Number(data).toLocaleString() : data
  const [integer, reminder = '0'] = String(num).split('.')
  return realPrecision === 0 ? `${integer}${unit || ''}` : `${integer}.${(reminder.length < realPrecision && needFull) ? _.padEnd(reminder, realPrecision, '0') : reminder}${unit || ''}`
}

/**
* indicatorValue 原始数值
* unit 单位 （万、亿）
* precision 小数精度
* isFormat 是否千分位处理
 * force 是否强制保留第一个不为0的数字
* */
const handleDataUnitAndFormat = (indicatorValue, unit, precision, isFormat, force) => {
  // format为2和4及空需要处理千分位

  let disPlayValue = null
  switch (unit) {
    case 'original': // 原始值
      disPlayValue = renderMoney(indicatorValue,'', precision, force, isFormat)
      break;
    case 'wan':
      disPlayValue = renderMoney(indicatorValue / 10000, '万', precision, force, isFormat)
      break;
    case 'yi':
      disPlayValue = renderMoney(indicatorValue / 100000000, '亿', precision, force, isFormat)
      break;
    default:
      const absValue = Math.abs(indicatorValue)
      if (absValue > 10000 && absValue < 100000000) {
        disPlayValue = renderMoney(indicatorValue / 10000, '万' ,precision, force, isFormat)
      } else if (absValue >= 100000000) {
        disPlayValue = renderMoney(indicatorValue / 100000000, '亿',precision, force, isFormat)
      } else {
        disPlayValue = renderMoney(indicatorValue, '', precision, force, isFormat)
      }
  }
  return disPlayValue
}

/**
* indicatorValue 原始数值
* unit 单位 （万、亿）
* precision 小数精度
* isFormat 是否千分位处理
* force 是否强制保留第一个不为0的数字
* */
const NumberFormat = (indicatorValue, unit = 'original', precision = 2, isFormat = false, force = true ) => {
  let disPlayValue = null
  switch(unit) {
    case 'original':
      disPlayValue = renderMoney(indicatorValue, '', precision, force, isFormat)
      break;
    case 'wan':
      disPlayValue = renderMoney(indicatorValue / 10000, '', precision, force, isFormat)
      break;
    case 'yi':
      disPlayValue = renderMoney(indicatorValue / 100000000, '', precision, force, isFormat)
      break;
  }
  return disPlayValue
}

/**
 * 千分位展示金额
 */
const getThousandNum = (num) => {
  return num.toString().replace(/\d+/, function (n) { // 先提取整数部分
    return n.replace(/(\d)(?=(\d{3})+$)/g, function ($1) { // 对整数部分添加分隔符
      return $1 + ','
    })
  })
}

export {renderNumber, renderPercent, handleDataUnitAndFormat, NumberFormat, getThousandNum}
