/**
 * 文字超出打点
 * @param {String} text 文字
 * @param {Number} num 最多展示几位
 */
export const extraTextDot = (text = "", num = 10) => {
  let str = "";
  if (text?.length > num) {
    str = text.slice(0, num) + "...";
  } else {
    str = text;
  }
  return str;
};

/**
 * 自动计算分页表格的序号。如: 第5页的1-10自动计算为41-50
 * @param {Number} index 原序号
 * @param {Number} currentPage 页码
 * @param {Number} pageSize 每页数量
 */
export const getPageTableIndex = (index, currentPage, pageSize) => {
  let dataIndex = "";
  if (currentPage === 1) {
    dataIndex = `${index}`;
  } else {
    dataIndex = `${pageSize * (currentPage - 1) + index}`;
  }
  return dataIndex;
};

/**
 * 判断地址栏的url是否存在某个参数
 * @param {String} key 参数名
 */
export const judgeIsExistParams = (searchStr, key) => {
  const allParams = searchStr.substring(1).split("&");
  for (const ele of allParams) {
    const param = ele.split("=");
    if (param[0] === key) {
      return true;
    }
  }
  return false;
};
