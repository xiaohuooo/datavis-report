/**
 * 数组结构数据递归为树结构
 * @param {父级数组} parent
 * @param {所以数据} allData
 */
const recursiveTree = (parent, allData) => {
  parent.forEach(pItem => {
    let children = allData
      .filter(dataItem => {
        if(dataItem.hasOwnProperty('parentId')) {
          return pItem.id === dataItem.parentId
        } else if(dataItem.hasOwnProperty('groupId')) {
          return pItem.id === dataItem.groupId
        }
      })
      .map(childItem => {
        if(childItem.hasOwnProperty('parentId')) {
          return {
            id: childItem.id,
            title: childItem.name,
            type: 'group'
          }
        } else if(childItem.hasOwnProperty('groupId')) {
          return {
            id: childItem.id,
            title: childItem.name,
            type: 'value'
          }
        }
      })
    if(children.length) {
      pItem.children = children
      recursiveTree(children, allData)
    }
  })
}

/**
 * 找到树形结构的第一个子节点，并将其父级展开
 * @param {树形结构数据} treeData
 * @returns 第一个子节点
 */
const selectTreeFirst = (treeData) => {
  if(treeData && treeData.length) {
    for(let i=0; i<treeData.length; i++) {
      if(treeData[i].type === 'value') {
        treeData[i].selected = true
        return treeData[i]
      }
      if(treeData[i].children && treeData[i].children.length) {
        let existFirstNode = selectTreeFirst(treeData[i].children)
        if(existFirstNode) {
          treeData[i].expand = true
          return existFirstNode
        }
      }
    }
  }
}

/**
 * 遍历整个树，id相等的选中 其他的selected均为false
 * 避免过滤时选中了一个节点，再清除过滤条件时会有两个选中节点
 * @param {树形结构数据} treeData
 * @param {仅选中一个节点的id} id
 */
const changeTreeSelectedOnly = (treeData, id) => {
  if(treeData && treeData.length) {
    for(let i=0; i<treeData.length; i++) {
      if(treeData[i].type === 'value') {
        if(treeData[i].id === id) {
          treeData[i].selected = true
        } else if(treeData[i].hasOwnProperty('selected')){
          treeData[i].selected = false
        }
      }
      if(treeData[i].children && treeData[i].children.length) {
        changeTreeSelectedOnly(treeData[i].children, id)
      }
    }
  }
}

    /**
     * 递归-生成指标分组树
     * @param parent 父节点
     * @param allData 全部数据
     * @param deep 层级，根节点为0
     */
   const recursiveIndicatorGroup = (parent, allData, deep = 1) => {
      for (let i = 0; i < parent.length; i++) {
        let item = parent[i]
        item.deep = deep;
        let children = allData
          .filter(d => {
            if (d.parentId) {
              return d.parentId == item.id;
            } else if (d.groupId) {
              return d.groupId == item.id;
            }
          })
          .map((child, i) => {
            if (child.parentId) {
              return {
                id: child.id,
                title: child.name,
                dataType: 1,
                expand: true,
                disabled: true,
                treeType: "group"
              };
            } else if (child.groupId) {
              return {
                id: child.id,
                title: child.name,
                dataType: 2,
                type: child.type,
                selected: child.selected,
                disabled: false,
                treeType: "value"
              };
            }
          });
        if (children.length > 0) {
          item.children = children;
          recursiveIndicatorGroup(children, allData, deep + 1);
        }
        if (item.treeType === 'group' && !item.children?.length) {
          parent.splice(i, 1)
          // 因为删除了一项，所以i需要往前移一位
          i--
        }
      }
    }

export { recursiveTree, selectTreeFirst, changeTreeSelectedOnly, recursiveIndicatorGroup };
