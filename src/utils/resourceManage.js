import { NO_RESOURCE_CHART_LIST, ONLY_INDICATOR_CHART_LIST, DIMENSION_INDICATOR_CHART_LIST } from "@/config/resourceManage";

/**
 * 判断是否是无效资源
 */
export const judgeIsInvalidResource = chartData => {
  const {
    id,
    data: { dimension, indicator, src, innerHtml }
  } = chartData;

  if (NO_RESOURCE_CHART_LIST.includes(id)) {
    // iframe、tab、筛选器不能保存为资源
    return false;
  } else if (ONLY_INDICATOR_CHART_LIST.includes(id)) {
    // 明细表可以不需要维度，但要有指标; 指标卡、仪表盘仅需要指标
    return indicator.length !== 0;
  } else if (id === "SimpleScatter") {
    // 散点图需要1个维度 + 2个指标
    return dimension.length !== 0 && indicator.length === 2;
  } else if (id === "SimpleBubble") {
    // 气泡图需要1个维度 + 3个指标
    return dimension.length !== 0 && indicator.length === 3;
  }else if (id === "CustomImage") {
    // 图片不需要维度、指标，但要有内容
    return !!src;
  } else if (id === "ComplexTextarea") {
    // 文本框可以不需要维度、指标，但要有内容
    return !!innerHtml;
  } else if (DIMENSION_INDICATOR_CHART_LIST.includes(id)) {
    // 需要维度+指标
    return dimension.length !== 0 && indicator.length !== 0;
  }
};
