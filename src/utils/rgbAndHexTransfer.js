/**
 * 十六进制转rgb || rgba
 * @param hex
 * @param alpha
 * @returns {string}
 */
export const hexToRgb = (hex, alpha) => {
  const reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/; //十六进制色的正则
  hex = hex?.toLowerCase() || "#ccc";//统一转成小写
  //如果是十六进制
  if(hex && reg.test(hex)) {
    //如果是缩写(#333 || #999)就变成(#333333 || #999999)
    if (hex.length === 4) {
      let hexColorFull = "#";
      for (let i = 1; i < hex.length; i++) {
        hexColorFull += hex.slice(i, i + 1).concat(hex.slice(i, i + 1));
      }
      hex = hexColorFull;
    }
    //处理六位的颜色值
    let sColorChange = [];
    for (let i = 1; i < 7; i += 2) {
      sColorChange.push(parseInt("0x" + hex.slice(i, i + 2)));
    }
    //判断是否需要需要alpha
    return alpha ? `rgba(${ sColorChange.join(",") } , ${ alpha })` : `rgb(${ sColorChange.join(",") })`;
  }
  return hex;
}
/**
 * rgb转十六进制
 * @param rgb || rgba
 * @returns {string}
 */
export const rgbToHex = (rgb) => {
  let _this = rgb;
  if(!_this) {
    return 'undefined'
  }
  //如果有alpha
  if(/^(rgba|RGBA)/.test(_this)) {
    _this = _this.replace(/(?:\(|\)|rgba|RGBA)*/g, "").split(",")
    _this.splice(_this.length - 1, 1)
  }
  //如果是rgb
  if (/^(rgb|RGB)/.test(_this)) {
    _this = _this.replace(/(?:\(|\)|rgb|RGB)*/g, "").split(",")
  }
  let hexColor;
  if(!_this instanceof Array) {
    hexColor = _this
  } else {
    const r = parseInt(_this[0]), g = parseInt(_this[1]), b = parseInt(_this[2]);
    hexColor = `#${((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1)}`
  }
  return hexColor;
}
