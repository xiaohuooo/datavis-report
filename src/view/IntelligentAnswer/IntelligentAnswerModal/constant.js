export const FIRST_MESSAGE = "FirstMessage"; // 首次提示文本块
export const GUESS_SEARCH = "GuessSearch";
export const INDICATOR_CARD = "IndicatorCard";
export const CHART_WRAPPER = "ChartWrapper";
export const TABLE_WRAPPER = "TableWrapper";

export const DEFAULT_MESSAGE = {
  id: "1",
  type: FIRST_MESSAGE,
  robot: {}
};
