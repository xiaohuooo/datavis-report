// 规则关系
export const RULE_TEXT = [
  { label: ">", value: 1, conditionText: "超出" },
  { label: "<", value: 2, conditionText: "低于" },
  { label: "≠", value: 3, conditionText: "不等于" },
  { label: "=", value: 4, conditionText: "等于" },
  { label: "≥", value: 5, conditionText: "超出" },
  { label: "≤", value: 6, conditionText: "低于" },
];

// 默认展示
export const DEFAULT_SHOW = "";

// 默认展示
export const DEFAULT_TASK_NAME = "未命名任务";

// 监测状态列表
export const WATCH_STATUS_LIST = [
  { label: "全部", value: 1 },
  { label: "待上线", value: -2 },
  { label: "已上线", value: 2 },
  { label: "已下线", value: 3 },
];
/* export const WATCH_STATUS_LIST = [
  { label: "正常", value: 1 },
  { label: "监测失败", value: -2 },
  { label: "一级预警", value: 2 },
  { label: "二级预警", value: 3 },
  { label: "三级预警", value: 4 },
  { label: "高风险", value: 5 },
  { label: "中风险", value: 6 },
  { label: "低风险", value: 7 },
];
 */
// 预警列表
export const EARLY_WARNING_TYPE_LIST = [
  { label: "正常", color: "#84C52F", value: 1 },
  { label: "一级预警", color: "#F86666", value: 2 },
  { label: "二级预警", color: "#F78C22", value: 3 },
  { label: "三级预警", color: "#FFCB5F", value: 4 },
];

// 目标列表
export const TARGET_TYPE_LIST = [
  { label: "正常", color: "#84C52F", value: 1 },
  { label: "高风险", color: "#F86666", value: 2 },
  { label: "中风险", color: "#F78C22", value: 3 },
  { label: "低风险", color: "#FFCB5F", value: 4 },
];
