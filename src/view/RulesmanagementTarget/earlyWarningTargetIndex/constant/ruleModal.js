import { dateChange } from "@/utils/dateFormat";

// 预警列表
export const EARLY_WARNING_LIST = [
  { label: "1", value: 1 },
  { label: "3", value: 3 },
  { label: "5", value: 5 },
  { label: "10", value: 10 },
  { label: "自定义", value: 0 },
];
/* export const EARLY_WARNING_LIST = [
  { label: "未定义", value: -1 },
  { label: "一级预警", value: 1 },
  { label: "二级预警", value: 2 },
  { label: "三级预警", value: 3 }
]; */

// 目标列表
export const TARGET_LIST = [
  { label: "未定义", value: -1 },
  { label: "高风险", value: 1 },
  { label: "中风险", value: 2 },
  { label: "低风险", value: 3 },
];

// 比较(左)的类型
export const MODE_TYPE_LIST = [
  {
    label: "指标",
    value: 1,
  },
  {
    label: "公式",
    value: 2,
  },
  {
    label: "动态指标",
    value: 3,
  },
  {
    label: "动态维度",
    value: 4,
  },
];

// 被比较(右)的类型
export const VARIABLE_TYPE_LIST = [
  {
    label: "常量",
    value: 1,
  },
  {
    label: "指标",
    value: 5,
  },
  {
    label: "公式",
    value: 6,
  },
  {
    label: "维度",
    value: 7,
  },
];

// 单个过滤条件对象
export const FILTER_CONDITION_ITEM = {
  modeType: 1,
  curIndicatorId: null, // 指标id-当前
  curIndicatorType: null, // 指标类型-当前
  curExpressionHtml: "", // 公式弹窗外的html-当前
  curExpressionModalHtml: "", // 公式弹窗内的html-当前规则
  curExpression: "", // 公式-当前规则
  curDimensionIdDyna: null, // 维度id-动态
  curIndicatorIdDyna: null, // 指标id-动态
  curIndicatorTypeDyna: null, // 指标id-动态

  conditionType: null, // 比较条件
  variableType: 1, // 常量
  variableValue: null,
  targetIndicatorId: null, // 指标id-目标
  targetIndicatorType: null, // 指标类型-目标
  targetExpressionHtml: "", // 公式弹窗外的html-目标
  targetExpressionModalHtml: "", // 公式弹窗内的html-目标
  targetExpression: "", // 公式-目标规则
};

// 比较条件
export const CONDITION_TYPE_LIST = [
  {
    label: "大于(>)",
    value: 1,
  },
  {
    label: "小于(<)",
    value: 2,
  },
  {
    label: "不等于(!=或<>)",
    value: 3,
  },
  {
    label: "等于(=)",
    value: 4,
  },
  {
    label: "大于等于(>=)",
    value: 5,
  },
  {
    label: "小于等于(<=)",
    value: 6,
  },
];

// 平台内推送方式
export const PLATFORM_PUSH_LIST = [
  { label: "邮件", value: 1 },
  { label: "短信", value: 2 },
  { label: "平台通知", value: 3 },
];

// 第三方推送方式
export const THIRD_PUSH_LIST = [
  { label: "飞书", value: 4 },
  { label: "钉钉", value: 5 },
  { label: "微信", value: 6 },
];

// 推送人
export const PUSH_MEMBER_ITEM = {
  userChecked: false,
  userList: [],
  associatedTableData: {
    show: false,
    checked: false, // 是否勾选
    connectId: "", // 数据库对应id
    tableId: "", // 库中的数据表对应id
    relColumnId: "", // 关联字段id
    useColumnId: "", // 匹配字段id
  },
  deptChecked: false,
  deptList: [],
  roleChecked: false,
  roleList: [],
};

// 推送频率
export const PUSH_CONDITION_OPTIONS = [
  { label: "实时通知", value: 1 },
  { label: "触发条件时次数推送", value: 2 },
];

export const PUSH_MODE_OPTIONS = [
  { label: "连续", value: 1 },
  { label: "累计", value: 2 },
];

// 监测
export const WATCH_DISPATCH = {
  startDate: dateChange(1),
  startTime: "00:00",
  rate: {
    name: 1,
    easy: {
      inputVal: 1,
      type: 3,
    },
    detail: {
      timeList: [{ value: "00/00" }], // 执行时间
      // 执行日
      execDate: {
        type: 1,
        selectVal: "",
      },
      monthVal: "1月, 2月, 3月, 4月, 5月, 6月, 7月, 8月, 9月, 10月, 11月, 12月", // 执行月
    },
  },
  advanced: {
    show: false,
    count: 1,
    interval: 1,
  },
};
