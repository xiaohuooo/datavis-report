import { INDICATOR_OPTION_LIST } from "@/config/indicator";
import { mapState } from "vuex";

export default {
  computed: {
    ...mapState("earlyWarningTarget", ["allIndicator"])
  },
  methods: {
    /**
     * 通过 规则表达式，获取展示的html
     * @param {String} expression
     */
    getExpressionHtml(expression) {
      return expression
        .replace(/(\/)/gi, '<span class="operator">/</span>')
        .replace(/(\+)/gi, '<span class="operator">+</span>')
        .replace(/(\-)/gi, '<span class="operator">-</span>')
        .replace(/(\*)/gi, '<span class="operator">*</span>')
        .replace(/(\()/gi, '<span class="operator">(</span>')
        .replace(/(\))/gi, '<span class="operator">)</span>')
        .replace(/\[(.*?)\]/gi, match => {
          const indicatorId = match.slice(1, -3);
          const indicatorType = match.slice(-2, -1);
          const imgIcon = INDICATOR_OPTION_LIST.find(ele => ele.value === Number(indicatorType)).iconActive;
          const indicator = this.allIndicator.find(item => item.id === Number(indicatorId));
          return indicator ? `<span class="indicator"><img src="${imgIcon}" /><span>${indicator.name}</span></span>` : "";
        });
    }
  }
};
