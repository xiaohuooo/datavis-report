// 预警列表
export const EARLY_WARNING_TYPE_LIST = [
    { label: "一级预警", color: "#F86666", value: 1 },
    { label: "二级预警", color: "#F78C22", value: 2 },
    { label: "三级预警", color: "#FFCB5F", value: 3 }
];

// 目标列表
export const TARGET_TYPE_LIST = [
    { label: "正常", color: "#84C52F", value: 'normal' },
    { label: "高风险", color: "#F86666", value: 'highRisk' },
    { label: "中风险", color: "#F78C22", value: 'midRisk' },
    { label: "低风险", color: "#FFCB5F", value: 'lowRisk' }
];