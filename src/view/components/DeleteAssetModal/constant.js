// 被删除的资产类型常量
export const TYPE_DIMENSION = '维度'
export const TYPE_INDICATOR = '指标'
export const TYPE_BUSINESS_MODEL = '业务模型'
export const TYPE_DATA_MODEL = '数据模型'
export const TYPE_FILE = '文件'

// 资源类型
export const RESOURCE_TYPE = {
  1: '维度',
  2: '原子指标',
  3: '复合指标',
  8: '派生指标',
  4: '业务模型',
  5: '报告',
  6: '资源', // 单组件资源
  7: '资源', // 多组件资源
  9: '故事',
  10: '预警/目标',
  98: '数据模型',
  97: '数据源表',
  96: '文件',
}

// 资源图标
export const RESOURCE_ICON = {
  1: require('@/assets/icons/dimensionIcon/dimension.png'),
  2: require('@/assets/icons/indicatorIcon/icon_basic_indicator.svg'),
  3: require('@/assets/icons/indicatorIcon/icon_composite_indicator.svg'),
  8: require('@/assets/icons/indicatorIcon/icon_derivative_indicator.svg'),
  4: require('@/assets/icons/deleteModal/business_model.svg'),
  5: require('@/assets/icons/deleteModal/report.svg'),
  6: require('@/assets/icons/deleteModal/resource.svg'), // 单组件资源
  7: require('@/assets/icons/deleteModal/resource.svg'), // 多组件资源
  9: require('@/assets/icons/deleteModal/story_board.svg'),
  10: require('@/assets/icons/deleteModal/early_warning.svg'),
  98: require('@/assets/icons/deleteModal/data_model.svg'),
  97: require('@/assets/icons/deleteModal/source_table.svg'),
  96: require('@/assets/icons/deleteModal/file.svg'),
}
