export const ERROR_DESCRIPTION = {
  '000000': '暂无数据',
  '100000': '系统错误',
  '100001': '指标不存在或者已被删除',
  '100002': '维度不存在或者已被删除',
  '100003': '指标对应的业务模型没有与表映射',
  '100004': '维度没有与业务模型属性关联',
  '100005': '指标动态变量未赋值',
  '100006': '列不存在或已被删除',
  '100007': '业务模型属性未被映射',
  '100008': 'redis连接超时',
  '100009': '复合指标表达式错误',
  '100010': '数据模型不存在或者已被删除',
  '100011': '数据服务不可用',
  '100012': '数据表未指定日期列',
  '100013': '数据连接不可用',
}
