/**
 * 字段配置的列
 */
export const fieldConfigTableColumns = [
  { title: '文件列名', key: 'comment', align: 'center' },
  { title: '字段名称', slot: 'name', align: 'center', width: 400 },
  { title: '字段类型', slot: 'type', align: 'center', width: 250 },
  {
    title: '字段长度',
    slot: 'size',
    align: 'center',
    width: 250,
    renderHeader: (h, { column }) => {
      return h('div', {
        style: {
          display: 'flex',
          alignItems: 'center',
        },
      }, [
        h('span', column.title),
        h('Tooltip', {
          attrs: {
            transfer: true,
            placement: 'top',
          },
          style: {
            marginLeft: '4px',
          },
        }, [
          h('Icon', {
            attrs: {
              type: 'ios-help-circle-outline',
              size: 16,
            },
          }),
          h('div', {
            slot: 'content',
          }, [
            h('p', '数值类型字段长度限制：'),
            h('p', '1. 数字（整型）：20'),
            h('p', '2. 数字（浮点型）：（1,1）~（20,6）'),
          ])
        ]),
      ])
    },
  },
]


// 数据库字段名称reg
export const DATABASE_FIELD_NAME_REG = /^[a-z][a-z0-9_]*$/

// 字符串类型字段长度reg
export const STRING_FIELD_SIZE_REG = /^[1-9]\d*$/

// 数值类型字段长度reg 数字（整型）:20 数字（浮点型）:(1，1)~(20，6)
export const NUMBER_FIELD_SIZE_REG = /^(1?\d|20)(\,[1-6])?$/

// 文件上传大小限制 字节 1M
export const FILE_UPLOAD_SIZE_LIMIT = 1048576

// 文件预览默认读取前11行（包含表头）
export const PREVIEW_SHEET_ROWS = 11
