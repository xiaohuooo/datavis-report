import { readFile } from '@/libs/util'
import xlsx from 'xlsx'

// 文件转json
export const fileToJson = async (file, sheetRows = 0) => {
  let data = await readFile(file)
  // sheetRows: 读取行数  codepage: 编码  raw: 日期数据是否加工
  let workbook = xlsx.read(data, { type: 'binary', sheetRows: sheetRows, codepage: 936, raw: true })
  // Excel源表存在多个sheet，默认读取第一个sheet页
  let worksheet = workbook.Sheets[workbook.SheetNames[0]]
  return xlsx.utils.sheet_to_json(worksheet, { header: 1 })
}
