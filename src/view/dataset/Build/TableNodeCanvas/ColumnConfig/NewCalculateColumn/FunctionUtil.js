import { CONDITION_TYPE } from '@/view/dataset/constant'

export default {
  /**
   * @description 创建函数dom
   * @param fx 函数key
   * @param hostContainer 宿主容器，此处指挂载的vue实例
   * @return {{firstInput: *, span: HTMLSpanElement}|null}
   */
  createFunction(fx, hostContainer) {
    const span = document.createElement('span')
    span.classList.add('fxContainer')
    span.setAttribute('symbol', fx)
    span.innerText = fx
    let bracketStart = document.createElement('span')
    bracketStart.innerText = '('
    bracketStart.setAttribute('symbol', '(')
    span.appendChild(bracketStart)
    let firstInput
    switch (fx) {
      case 'ABS':
      case 'TRIM':
      case 'MAX':
      case 'MIN':
      case 'SUM':
      case 'AVG':
      case 'COUNT':
        firstInput = this.createSingleInput(span, hostContainer)
        break
      case 'if':
        firstInput = this.createIf(span, hostContainer)
        break
      case 'substr':
        firstInput = this.createSubStr(span, hostContainer)
        break
      case 'CONCATENATE':
        firstInput = this.createConcat(span, hostContainer)
        break
      default:
        return null
    }
    let bracketEnd = document.createElement('span')
    bracketEnd.innerText = ')'
    bracketEnd.setAttribute('symbol', ')')
    span.appendChild(bracketEnd)
    return { span, firstInput }
  },

  createInput(hostContainer) {
    const input = this.createEmptyInput(hostContainer)
    input.classList.add('textArea_input_inner')
    return input
  },

  /**
   * @description 创建初始化输入框
   * @param 宿主对象
   */
  createEmptyInput(hostContainer) {
    const inputElement = document.createElement('input')
    inputElement.className = 'textArea_input'
    inputElement.addEventListener('focus', e => {
      hostContainer.activeInupt = e.currentTarget
    })
    inputElement.addEventListener('keyup', e => {
      let dom = e.target
      if (dom.getAttribute('type') === 'column') {
        dom.removeAttribute('type')
        dom.removeAttribute('columnId')
        dom.value = ''
        dom.focus()
      } else {
        if (dom.style.color !== '#333' && e.value !== '') {
          dom.style.background = 'none'
          dom.style.color = '#333'
        }
        if (e.keyCode === 8 && dom.value === '') {
          dom.style.background = null
          dom.style.color = null
        }
        dom.setAttribute('inputValue', dom.value)
        dom.name = 'formula-function-input'
      }
      e.stopPropagation()
    })
    return inputElement
  },

  /**
   * @description 创建单输入函数输入；abs、trim、sum、avg、max、min、count等等
   * @return {*|HTMLInputElement}
   */
  createSingleInput(parent, hostContainer) {
    let input = this.createInput(hostContainer)
    parent.appendChild(input)
    return input
  },

  /**
   * 创建IF
   * @return {*|HTMLInputElement}
   */
  createIf(parent, hostContainer) {
    let input = this.createSingleInput(parent, hostContainer)
    let compare = document.createElement('select')
    compare.className = 'compare ifSelect'
    compare.setAttribute('id', 'ifSelectVal')
    CONDITION_TYPE.forEach(c => {
      let option = document.createElement('option')
      option.innerText = c.label
      option.setAttribute('value', c.value)
      compare.appendChild(option)
    })
    parent.appendChild(compare)
    let separate = document.createElement('span')
    separate.innerText = ''
    separate.setAttribute('symbol', ',')
    parent.appendChild(separate)
    parent.appendChild(this.createInput(hostContainer))

    let ifTrue = document.createElement('span')
    ifTrue.innerText = '?'
    ifTrue.setAttribute('symbol', ',')
    parent.appendChild(ifTrue)
    parent.appendChild(this.createInput(hostContainer))
    let ifFalse = document.createElement('span')
    ifFalse.innerText = ':'
    ifFalse.setAttribute('symbol', ',')
    parent.appendChild(ifFalse)
    parent.appendChild(this.createInput(hostContainer))

    // 判断是否是between
    compare.addEventListener('change', (e) => {
      let myselect = document.getElementById('ifSelectVal');
      let index = myselect.selectedIndex;
      let selectValue = myselect.options[index].value;
      if(selectValue === '7'){
        let andVal = document.createElement('span');
        andVal.innerText = 'and';
        andVal.setAttribute('id', 'andFlag');
        andVal.setAttribute('symbol', ',');
        ifTrue.before(andVal)
        andVal.after(this.createInput(hostContainer))
      }else{
        let andValue = document.getElementById('andFlag');
        if(andValue){
          let andInput = andValue.nextSibling;
          if(andInput){
            parent.removeChild(andInput);
          }
          parent.removeChild(andValue);
        }
      }
    })

    return input
  },

  /**
   * 创建substr
   * @return {*|HTMLInputElement}
   */
  createSubStr(parent, hostContainer) {
    let input = this.createSingleInput(parent, hostContainer)
    let from = document.createElement('span')
    from.innerText = ' from '
    from.setAttribute('symbol', ',')
    parent.appendChild(from)
    parent.appendChild(this.createInput(hostContainer))
    let to = document.createElement('span')
    to.setAttribute('symbol', ',')
    to.innerText = ' to '
    parent.appendChild(to)
    parent.appendChild(this.createInput(hostContainer))
    return input
  },

  /**
   * 创建concat
   * @return {*|HTMLInputElement}
   */
  createConcat(parent, hostContainer) {
    let input = this.createSingleInput(parent, hostContainer)
    let split = document.createElement('span')
    split.innerText = ' , '
    split.setAttribute('symbol', ',')
    parent.appendChild(split)
    parent.appendChild(this.createInput(hostContainer))
    return input
  },

  /**
   * @description 适配函数
   */
  adapterFunction(expression) {
    if (expression.indexOf('ABS') !== -1) {
      expression = expression.replace(/ABS/g, 'Math.abs').replace(/[\[\]]/g, '')
    } else if (expression.indexOf('substr') !== -1) {
      let elements = expression.split(',')
      elements[0] = elements[0].slice(7, elements[0].length);
      if(elements[0].indexOf('[')!==-1 && elements[0].indexOf(']'!==-1)){
        elements[0] = [elements[0].slice(1, elements[0].length-1)];
      }else{
        return null
      }
      // 切割右侧括号
      if(elements[2] && elements[2].indexOf(')') !== -1){
        elements[2] = elements[2].slice(0, elements[2].length - 1)
      }
      if (
        !elements || elements.length !== 3 ||
        !elements[0] ||
        !elements[2] ||
        parseInt(elements[1]) > 50 ||
        parseInt(elements[2]) > 100 ||
        parseInt(elements[1]) > parseInt(elements[2])
      ) {
        return null
      }
      expression = `'${elements[0].join('')}'.substr(${elements[1]},${elements[2]})`
    } else if (expression.indexOf('if') !== -1) {
      let myselect = document.getElementById('ifSelectVal');
      let index = myselect.selectedIndex;
      let selectValue = myselect.options[index].value;  // 中间运算符的select值
      let elements = expression.split(',');
      elements.splice(1, 0, selectValue);
      elements[0] = elements[0].slice(3, elements[0].length);
      // between...and...
      if(elements[1] === '7'){
        // 切割右侧括号
        if(elements[5] && elements[5].indexOf(')') !== -1){
          elements[5] = elements[5].slice(0, elements[5].length - 1)
        }
        if (
          !elements ||
          elements.length !== 6 ||
          !elements[1] ||
          (!elements[2] && elements[2] !== 0) ||
          (!elements[3] && elements[3] !== 0) ||
          (!elements[4] && elements[4] !== 0) ||
          (!elements[5] && elements[5] !== 0)
        ) {
          return null
        }
        expression = `${elements[0]} > ${elements[2]} && ${elements[0]} < ${elements[3]} ? ${elements[4]} : ${elements[5]}`
      }else{
        // 切割右侧括号
        if(elements[4] && elements[4].indexOf(')') !== -1){
          elements[4] = elements[4].slice(0, elements[4].length - 1)
        }
        if (
          !elements ||
          elements.length !== 5 ||
          !elements[1] ||
          (!elements[2] && elements[2] !== 0) ||
          (!elements[3] && elements[3] !== 0) ||
          (!elements[4] && elements[4] !== 0)
        ) {
          return null
        }
        if(elements[1] === '8' || elements[1] === '9'){
          if(elements[0].indexOf('[')!==-1 && elements[0].indexOf(']'!==-1)){
            elements[0] = [elements[0].slice(1, elements[0].length-1)];
          }else{
            return null
          }
          expression = `[${elements[0]}]${this.filterSymbol(elements[1], elements[2])} ? ${elements[3]} : ${elements[4]}`
        }else{
          expression = `${elements[0]}${this.filterSymbol(elements[1], elements[2])}${elements[2]} ? ${elements[3]} : ${elements[4]}`
        }
      }
    } else if (expression.indexOf('CONCATENATE') !== -1) {
      let elementsStr = expression.slice(12, expression.length-1);
      let elements = elementsStr.split(',');
      if (
        !elements ||
        elements.length < 2 ||
        (!elements[0] && elements[0] !== 0) ||
        (!elements[1] && elements[1] !== 0)
      ) {
        return null
      }
      expression = elements.join('');
    }
    console.log(expression)
    return expression
  },
  filterSymbol(selectVal, value){
    let symbol = '';
    switch (selectVal) {
      case '1':
        symbol =  '>';
        break;
      case '2':
        symbol =  '<';
        break;
      case '3':
        symbol =  '!==';
        break;
      case '4':
        symbol =  '===';
        break;
      case '5':
        symbol =  '>=';
        break;
      case '6':
        symbol =  '<=';
        break;
      case '7':
        symbol =  'between';
        break;
      case '8':
        symbol =  '.indexOf(' + value + ') !== -1';
        break;
      default:
        symbol =  '.indexOf(' + value + ') === -1';
        break;
    }
    return symbol
  },
}
