const timeColumns = ['DT', 'YEAR', 'DAY', 'MONTH', '日期', '时间', 'DATE', 'TIME', 'TIMESTAMP']

const DATA_TYPE_DATE = ['DATE', 'DATETIME', 'DATETIME2', 'TIMESTAMP']
const DATA_TYPE_NAME_KEYS = ['DATE', 'DATETIME', 'PT_D', 'DT']
const DATE_COLUMN_LABEL_KEYS = ['日期', '时间', '交易日']

export function getDateColumn(columnName) {
  let flag = false
  if (columnName) {
    columnName = columnName.toUpperCase()
    for (let i = 0; i < timeColumns.length; i++) {
      if (columnName.includes(timeColumns[i])) {
        flag = true
        break
      }
    }
  }
  return flag
}

export function isDateColumn(dataType, columnName, columnLabel) {
  if (dataType) {
    dataType = dataType.toUpperCase()
    if (DATA_TYPE_DATE.includes(dataType)) {
      return true
    }
  }

  if (columnLabel) {
    for (let columnKey in DATE_COLUMN_LABEL_KEYS) {
      if (columnLabel.indexOf(DATE_COLUMN_LABEL_KEYS[columnKey]) !== -1) {
        return true
      }
    }
  }

  if (columnName) {
    columnName = columnName.toUpperCase()
    for (let columnKey in DATA_TYPE_NAME_KEYS) {
      if (columnName.indexOf(DATA_TYPE_NAME_KEYS[columnKey]) !== -1) {
        return true
      }
    }
  }

  return false
}
