// 调度频率
export const DISPATCH_RATE_NAME = [
  { label: "简单重复执行", value: 1 },
  { label: "只执行一次", value: 2 },
  { label: "明细频率设置", value: 3 }
];

export const DISPATCH_RATE_EASY_TYPE = [
  { label: "分钟", value: 1 },
  { label: "小时", value: 2 },
  { label: "天", value: 3 },
  { label: "周", value: 4 }
];

export const DISPATCH_RATE_DETAIL_EXECDATE_TYPE = [
  { label: "每天", value: 1 },
  { label: "每周", value: 2 },
  { label: "每月", value: 3 }
];

