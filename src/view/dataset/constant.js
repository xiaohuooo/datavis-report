/**
 * 字段数值类型集合
 * @type {string[]}
 */
export const COLUMN_TYPE_NUMBER = ['FLOAT', 'INT', 'DOUBLE', 'BIGINT', 'DECIMAL', 'NUMBER']

/**
 * 字段数值类型集合
 * @type {string[]}
 */
export const COLUMN_TYPE_DATE = ['DATE', 'DATETIME', 'TIMESTAMP']

/**
 * 无需设置长度的字段类型
 * @type {string[]}
 */
export const NO_SIZE_TYPES = ['TEXT', 'DATE', 'DATETIME', 'CLOB', 'BLOB', 'TIMESTAMP']

/**
 * 字符串字段可处理函数集合
 * @type {string[]}
 */
export const STR_FUNCTION_LIST = [
  'substr', 'if', 'CONCATENATE', 'TRIM', 'NVL'
]

/**
 * 数字运算符号
 * @type {string[]}
 */
export const ARITHMETIC_SYMBOL = ['+', '-', '*', '/', '(', ')']

/**
 * 新增列使用的函数列表
 * @type {*[]}
 */
export const FUNCTION_LIST = [
  { name: 'substr', explain: '用于提取字符串中介于两个指定下标之间的字符', example: 'substr(val from start for stop)' },
  { name: 'if', explain: '判断一个条件是否满足：如果满足返回一个值，如果不满足则返回另外一个值。', example: 'if valA compare valB ? trueVal : falseVal' },
  { name: 'ABS', explain: '返回给定数字的绝对值(即不带符号的数值)。', example: 'ABS(val)' },
  { name: 'CONCATENATE', explain: '将多个文本字符串合并成一个文本字符串。', example: 'CONCATENATE(text1, text2, ...)' },
  { name: 'TRIM', explain: '删除公式中文本的前导空格和尾部空格', example: 'TRIM(" First Quarter Earnings ")' },
  { name: 'NVL', explain: '第一个参数为空的时候返回第二参数的值', example: 'NVL(string,replace_with)' },
  { name: 'SUM', explain: '返回表达式中所有值的总计。SUM 只能用于数字字段。会忽略 Null 值。', example: 'SUM（字段）' },
  { name: 'AVG', explain: '返回表达式中所有值的平均值。AVG只能用于数字字段。会忽略 Null 值。', example: 'AVG（字段）' },
  { name: 'COUNT', explain: '返回组中的项目数。不对 Null 值计数。', example: 'COUNT（字段）' },
  { name: 'MAX', explain: '返回表达式中所有值的平均值。AVG只能用于数字字段。会忽略 Null 值。', example: 'MAX（字段）' },
  { name: 'MIN', explain: '返回表达式在所有记录中的最小值。MIN只能用于数字、日期、日期时间字段。', example: 'MIN（字段）' },
]

export const UNION_KEY = [
  { key: '5', value: '联合1（union）' },
  { key: '6', value: '联合2（union all）' },
]
export const JOIN_KEY = [
  { key: '1', value: '内连接（inner join）' },
  { key: '2', value: '左连接（left join）' },
  { key: '3', value: '右连接（right join）' },
  { key: '4', value: '全连接（full join）' }
]

/**
 * 新增列字段类型
 * @type {*[]}
 */
export const NEW_CALCULATE_COLUMN_DATA_TYPE = [
  { value: 'string', label: '字符串', size: '255' },
  { value: 'int', label: '数字（整型）', size: '4' },
  { value: 'decimal', label: '数字（浮点型）', size: '10,2' },
  { value: 'date', label: '日期', size: null },
  { value: 'datetime', label: '日期时间', size: null }
]

/**
 * @description 获取时间
 * @type {*[]}
 */
export const TIME_OPTION = [
  { label: '年份', value: 1 },
  { label: '季度', value: 2 },
  { label: '月份', value: 3 },
  { label: '月日', value: 4 },
  { label: '星期', value: 5 },
  { label: '日', value: 6 },
  { label: '周数', value: 7 },
  { label: '时', value: 8 },
  { label: '分', value: 9 },
  { label: '秒', value: 10 },
  { label: '年季度', value: 11 },
  { label: '年月', value: 12 },
  { label: '年周数', value: 13 },
  { label: '年月日', value: 14 },
  { label: '年月日时', value: 15 },
  { label: '年月日时分', value: 16 },
  { label: '年月日时分秒', value: 17 },
]

/**
 * 条件类型
 * @type {*[]}
 */
export const CONDITION_TYPE = [
  {
    label: '大于',
    value: 1,
  },
  {
    label: '小于',
    value: 2,
  },
  {
    label: '不等于',
    value: 3,
  },
  {
    label: '等于',
    value: 4,
  },
  {
    label: '大于等于',
    value: 5,
  },
  {
    label: '小于等于',
    value: 6,
  },
  {
    label: 'between',
    value: 7,
  },
  {
    label: '包含',
    value: 8,
  },
  {
    label: '不包含',
    value: 9,
  },
]

// 状态列表
export const STATUS_LIST = [
  { value: -1, label: '全部' },
  { value: 0, label: '未发布' },
  { value: 1, label: '已发布' },
  { value: 3, label: '已停用' },
]

// 上次调用结果列表
export const RESULT_LIST = [
  { value: -1, label: '全部' },
  { value: 1, label: '成功' },
  { value: 2, label: '失败' },
]
