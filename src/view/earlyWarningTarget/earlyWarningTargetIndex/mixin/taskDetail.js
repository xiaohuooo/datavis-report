/**
 * 任务详情回填
 */
import { getTimeFormat, numTransformWeek } from "@/utils/dateFormat";
import { deepCopy } from "@/utils/common";
import { getEarlyWarningTaskDetail } from "@/api/earlyWarningTarget";
import expression from "./expression";
import { WATCH_DISPATCH } from "@/view/earlyWarningTarget/earlyWarningTargetIndex/constant/ruleModal";
import { v4 as uuidv4 } from "uuid";

export default {
  mixins: [expression],
  methods: {
    /**
     * 获取某个预警任务的详情
     */
    async getTaskDetail(id) {
      const res = await getEarlyWarningTaskDetail(id);
      if (res.data.state === "success") {
        const data = res.data.data;
        const ruleList = this.getRuleData(data.conditionGroups);
        const pushData = this.getPushData(data, ruleList);
        const dispatch = this.getDispatch(data);

        return {
          headData: {
            taskType: data.taskType,
            name: data.name,
            nickName: data.managerName,
            userName: data.manager
          },
          ruleList,
          pushData,
          dispatch
        };
      }
    },
    /**
     * 组装 `rule` 数据
     */
    getRuleData(data) {
      return data.map(rule => ({
        id: uuidv4(),
        level: rule.level || -1,
        ruleConditionList: rule.groups.map(ele =>
          ele.conditions.map(item => {
            // 当前数据
            let modeType = null;
            let curIndicatorId = null;
            let curIndicatorType = null;
            let curExpressionHtml = "";
            let curExpression = "";
            let curDimensionIdDyna = null;
            let curIndicatorIdDyna = null;
            let curIndicatorTypeDyna = null;
            if (item.sourceType === 3) {
              // 指标
              modeType = 1;
              const arr = item.sourceValue.split("_");
              curIndicatorId = Number(arr[1]);
              curIndicatorType = Number(arr[0]);
            } else if (item.sourceType === 2) {
              // 公式
              modeType = 2;
              curExpression = item.sourceValue.replace(/\[(.*?)\]/gi, match => {
                const indicatorId = match.slice(3, -1);
                const indicatorType = match.slice(1, 2);
                return `[${indicatorId}_${indicatorType}]`;
              });
              curExpressionHtml = this.getExpressionHtml(curExpression);
            } else if (item.sourceType === 4) {
              // 动态指标
              modeType = 3;
              const arr1 = item.sourceValue.split("|");
              const arr2 = arr1[1].split("_");
              curDimensionIdDyna = Number(arr1[0]);
              curIndicatorIdDyna = Number(arr2[1]);
              curIndicatorTypeDyna = Number(arr2[0]);
            }

            // 目标数据
            let variableType = 1;
            let variableValue = null;
            let targetIndicatorId = null; // 指标id-目标
            let targetIndicatorType = null; // 指标类型-目标
            let targetExpressionHtml = ""; // 公式-目标
            let targetExpression = ""; // 公式-目标规则
            if (item.valueType === 1) {
              // 常量
              variableType = 1;
              variableValue = item.value;
            } else if (item.valueType === 3) {
              // 指标
              variableType = 5;
              const arr = item.value.split("_");
              targetIndicatorId = Number(arr[1]);
              targetIndicatorType = Number(arr[0]);
            } else if (item.valueType === 2) {
              // 公式
              variableType = 6;
              targetExpression = item.value.replace(/\[(.*?)\]/gi, match => {
                const indicatorId = match.slice(3, -1);
                const indicatorType = match.slice(1, 2);
                return `[${indicatorId}_${indicatorType}]`;
              });
              targetExpressionHtml = this.getExpressionHtml(targetExpression);
            }

            return {
              modeType,
              curIndicatorId,
              curIndicatorType,
              curExpressionHtml,
              curExpressionModalHtml: item.sourceExpressionHtml,
              curExpression,
              curDimensionIdDyna,
              curIndicatorIdDyna,
              curIndicatorTypeDyna,
              conditionType: item.conditionType,
              variableType,
              variableValue,
              targetIndicatorId,
              targetIndicatorType,
              targetExpressionHtml,
              targetExpressionModalHtml: item.valueExpressionHtml,
              targetExpression
            };
          })
        )
      }));
    },
    /**
     * 组装 `push` 数据
     */
    getPushData(data, ruleList) {
      const { innerPushType, outerPushType, targets, pushMode, countMode, countTimes, conditionGroups } = data;
      const pushMemberList = conditionGroups.map((item, index) => {
        const ele = targets.find(i => i.number === item.number);
        let associatedTableData = {
          show: false,
          checked: false,
          connectId: "",
          tableId: "",
          relColumnId: "",
          useColumnId: ""
        };
        if (ele?.execRelTable) {
          const { connectId, tableId, relColumnId, useColumnId } = ele.execRelTable;
          associatedTableData = {
            show: false,
            checked: true,
            connectId: `${connectId}`,
            tableId: `${tableId}`,
            relColumnId: `${relColumnId}`,
            useColumnId: `${useColumnId}`
          };
        }
        return {
          id: ruleList[index].id,
          userChecked: !!ele?.userList?.length || !!ele?.execRelTable,
          userList: ele?.userList || [],
          associatedTableData,
          deptChecked: !!ele?.deptList?.length,
          deptList: ele?.deptList || [],
          roleChecked: !!ele?.roleList?.length,
          roleList: ele?.roleList || []
        };
      });

      return {
        platformPushMode: innerPushType ? innerPushType.split(",").map(data => Number(data)) : [],
        thirdPartyPushMode: outerPushType ? outerPushType.split(",").map(data => Number(data)) : [],
        pushMemberTab: pushMemberList[0]?.id || null,
        pushMemberList,
        pushCondition: pushMode || 1,
        pushMode: countMode || 1,
        pushNum: countTimes || 5
      };
    },
    /**
     * 组装 `watch` 数据
     */
    getDispatch(data) {
      const { startTime, warningExec } = data;
      const dispatch = deepCopy(WATCH_DISPATCH);

      if (startTime) {
        dispatch.startDate = getTimeFormat({ timeData: startTime, type: "YYYY-MM-DD" });
        dispatch.startTime = getTimeFormat({ timeData: startTime, type: "hh:mm" });
      }
      if (warningExec) {
        const { execMode } = warningExec;
        dispatch.rate.name = execMode;
        if (execMode === 1) {
          const { cycleInterval, execCycle } = warningExec;
          dispatch.rate.easy.type = execCycle;
          dispatch.rate.easy.inputVal = cycleInterval;
        } else if (execMode === 3) {
          const { execTime, execCycle, execLimit, monthLimit } = warningExec;

          // 执行时间
          let timeList = [];
          execTime.split(",").forEach(ele => {
            const hour = ele.slice(0, 2);
            const minute = ele.slice(2, 4);
            timeList.push({
              value: `${hour}/${minute}`
            });
          });
          dispatch.rate.detail.timeList = timeList;

          // 执行日
          let newExecCycle = 1;
          switch (execCycle) {
            case 3:
              newExecCycle = 1;
              break;
            case 4:
              newExecCycle = 2;
              break;
            case 5:
              newExecCycle = 3;
              break;
          }
          dispatch.rate.detail.execDate.type = newExecCycle;

          let selectVal = "";
          if (execCycle === 4) {
            selectVal = numTransformWeek(execLimit.split(",").map(ele => Number(ele))).join(", ");
          } else if (execCycle === 5) {
            selectVal = execLimit
              .split(",")
              .map(ele => (ele < 10 ? `0${ele}` : ele))
              .join(", ");
          }
          dispatch.rate.detail.execDate.selectVal = selectVal;

          // 执行月
          dispatch.rate.detail.monthVal = monthLimit
            .split(",")
            .map(ele => `${ele}月`)
            .join(", ");
        }
      }

      return dispatch;
    }
  }
};
