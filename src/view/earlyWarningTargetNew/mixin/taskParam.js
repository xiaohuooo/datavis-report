/**
 * 获取任务参数
 */
import { weekTransformNum } from "@/utils/dateFormat";

export default {
  methods: {
    /**
     * 编辑预警接口参数
     * @param {Number} data 弹窗数据
     */
    getEditParams(data) {
      const { headData, ruleList, pushData, dispatch } = data;
      const headParam = this.getHeadParam(headData);
      const ruleParam = this.getRuleParam(ruleList);
      const pushParam = this.getPushParam(pushData);
      const disParam = this.getDisParam(dispatch);
      return {
        ...headParam,
        ...ruleParam,
        ...pushParam,
        ...disParam
      };
    },
    /**
     * 组装 `head` 参数
     */
    getHeadParam(data) {
      const { taskType, userName, refDeptId } = data;
      const params = {
        taskType, // 任务类型
        manager: userName, // 负责人
        refDeptId, //关联部门
      };
      return params;
    },
    /**
     * 组装 `rule` 参数
     */
    getRuleParam(data) {
      const conditionGroups = data.map((ruleItem, ruleIndex) => ({
        level: ruleItem.level !== -1 ? ruleItem.level : null,
        number: ruleIndex + 1,
        groups: ruleItem.ruleConditionList.map((ele, index) => ({
          sort: index + 1,
          conditions: ele.map((item, i) => {
            // 当前数据
            let curValueType = "";
            let curValue = "";
            if (item.modeType === 1) {
              curValueType = "3";
              curValue = `${item.curIndicatorType}_${item.curIndicatorId}`;
            } else if (item.modeType === 2) {
              curValueType = "2";
              curValue = item.curExpression.replace(/\[(.*?)\]/gi, match => {
                const indicatorId = match.slice(1, -3);
                const indicatorType = match.slice(-2, -1);
                return `[${indicatorType}_${indicatorId}]`;
              });
            } else if (item.modeType === 3) {
              curValueType = "4";
              curValue = `${item.curDimensionIdDyna}|${item.curIndicatorTypeDyna}_${item.curIndicatorIdDyna}`;
            }

            // 目标数据
            let targetValueType = "";
            let targerValue = "";
            if (item.variableType === 1) {
              // 常量
              targetValueType = "1";
              targerValue = item.variableValue;
            } else if (item.variableType === 5) {
              // 指标
              targetValueType = "3";
              targerValue = `${item.targetIndicatorType}_${item.targetIndicatorId}`;
            } else if (item.variableType === 6) {
              // 公式
              targetValueType = "2";
              targerValue = item.targetExpression.replace(/\[(.*?)\]/gi, match => {
                const indicatorId = match.slice(1, -3);
                const indicatorType = match.slice(-2, -1);
                return `[${indicatorType}_${indicatorId}]`;
              });
            }

            const obj = {
              sort: i + 1,
              sourceType: curValueType,
              sourceValue: curValue,
              conditionType: item.conditionType,
              valueType: targetValueType,
              value: targerValue
            };
            if (item.modeType === 2) {
              obj.sourceExpressionHtml = item.curExpressionModalHtml;
            } else if (item.variableType === 6) {
              obj.valueExpressionHtml = item.targetExpressionModalHtml;
            }

            return obj;
          })
        }))
      }));

      return {
        conditionGroups
      };
    },
    /**
     * 组装 `push` 参数
     */
    getPushParam(data) {
      const { platformPushMode, thirdPartyPushMode, pushMemberList, pushCondition, pushMode, pushNum } = data;

      const params = {
        pushMode: pushCondition // 推送频率-推送模式
      };

      if (platformPushMode.length) {
        params.innerPushType = platformPushMode.join(","); // 推送方式-平台 1,2,3
      }
      if (thirdPartyPushMode.length) {
        params.outerPushType = thirdPartyPushMode.join(","); // 推送方式-第三方 4,5,6
      }

      let targets = [];
      pushMemberList.forEach((ele, index) => {
        const { associatedTableData, userChecked, userList, deptChecked, deptList, roleChecked, roleList } = ele;
        const { checked, connectId, tableId, relColumnId, useColumnId } = associatedTableData;
        const obj = {
          number: index + 1
        };
        if (userChecked) {
          if (!checked) {
            obj.userList = userList; // 推送人-用户
            obj.execRelTable = null;
          } else {
            obj.userList = null;
            obj.execRelTable = {
              connectId,
              tableId,
              relColumnId,
              useColumnId
            };
          }
        }
        if (deptChecked) {
          obj.deptList = deptList; // 推送人-部门
        }
        if (roleChecked) {
          obj.roleList = roleList; // 推送人-角色
        }
        targets.push(obj);
      });
      params.targets = targets;

      if (pushCondition === 2) {
        params.countMode = pushMode; // 推送频率-计数模式
        params.countTimes = pushNum; // 推送频率-计数次数
      }

      return params;
    },
    /**
     * 组装 `watch` 参数
     */
    getDisParam(data) {
      const {
        startDate,
        startTime,
        rate: {
          name: rateType,
          easy: { inputVal: easyInputVal, type: easyType },
          detail: {
            timeList,
            execDate: { type: execDateType, selectVal: execDateSelectVal },
            monthVal
          }
        }
      } = data;

      const params = {
        startTime: new Date(`${startDate} ${startTime}`).getTime(), // 监测-开始时间（时间戳）
        warningExec: {
          execMode: rateType // 监测-频率类型
        }
      };

      if (rateType === 1) {
        // 简单
        params.warningExec.execCycle = easyType;
        params.warningExec.cycleInterval = easyInputVal;
      } else if (rateType === 3) {
        // 明细
        params.warningExec.execTime = timeList
          .map(ele => ele.value + "00")
          .join(",")
          .replace(/\//gi, "");

        let newExecDateType = 1;
        switch (execDateType) {
          case 1:
            newExecDateType = 3;
            break;
          case 2:
            newExecDateType = 4;
            break;
          case 3:
            newExecDateType = 5;
            break;
        }
        params.warningExec.execCycle = newExecDateType;

        if (newExecDateType !== 3) {
          const executeDayArr = execDateSelectVal.split(", ");
          if (newExecDateType === 4) {
            params.warningExec.execLimit = weekTransformNum(executeDayArr).join(",");
          }
          if (newExecDateType === 5) {
            params.warningExec.execLimit = executeDayArr.map(ele => parseInt(ele)).join(",");
          }
        }
        params.warningExec.monthLimit = monthVal
          .split(", ")
          .map(ele => parseInt(ele))
          .join(",");
      }

      return params;
    }
  }
};
