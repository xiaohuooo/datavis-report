/**
 * 递归数据源，构建树结构数据
 */
export const recursiveDataSourceGroup = (treeData, key) => {
  treeData.forEach(item => {
    item.title = item.label || item.name;
    item.expand = true;
    item.dataType = "group";
    if (item.children !== null) {
      if (item.children.length > 0) {
        recursiveDataSourceGroup(item.children, key);
      }
      if (item["tableMetadataList"] && item["tableMetadataList"].length > 0) {
        item["tableMetadataList"].forEach(table => {
          item.children.push({
            title: table.label || table.name,
            dataType: "node",
            dataKey: key,
            columns: table.columns,
            id: table.id,
            name: table.name,
            label: table.label,
            dateColumnId: table.dateColumnId
          });
        });
      }
    } else {
      item.children = item["tableMetadataList"].map(table => {
        return {
          title: table.label || table.name,
          dataType: "node",
          dataKey: key,
          columns: table.columns,
          id: table.id,
          name: table.name,
          label: table.label,
          dateColumnId: table.dateColumnId
        };
      });
    }
  });
};
