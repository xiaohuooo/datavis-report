export const TIME_LIST = [
  {
    id: "yesterday",
    title: "最近1天"
  },
  {
    id: "thisWeek",
    title: "本周"
  },
  {
    id: "thisMonth",
    title: "本月"
  },
  {
    id: "thisQuarter",
    title: "本季度"
  },
  {
    id: "thisYear",
    title: "本年度"
  }
];

export const SWITCH_CHART_TYPE_LIST = [
  {
    chartType: "SimpleLine",
    icon: require("@/assets/icons/chart/chart_type_change/line_black.svg"),
    chartName: "折线图"
  },
  {
    chartType: "SimpleBar",
    icon: require("@/assets/icons/chart/chart_type_change/bar_black.svg"),
    chartName: "柱状图"
  },
  {
    chartType: "SimplePie",
    icon: require("@/assets/icons/chart/chart_type_change/pie_black.svg"),
    chartName: "饼图"
  }
];

export const MESSAGE_LIST = [
  {
    title: "一站式管理",
    description: "数据专员、IT 甚至决策层皆可在FineInsight中“获取数据资产”、“分析数据”、“制作报表”等操作"
  },
  {
    title: "数据准备管理决策",
    description: "从获取源数据，再到数据模型的构建，为实现数据分析提供了必要的前提条件"
  },
  {
    title: "业务场景构建",
    description: "业务人员快速构建业务场景，自主分析数据，摆脱传统方式"
  },
  {
    title: "自助数据处理",
    description: "FineInsight 提供完善易操作的数据分析功能，确保数据资产质量"
  }
];
