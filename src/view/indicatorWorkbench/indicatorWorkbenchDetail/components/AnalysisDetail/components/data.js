export const data = {
  dep_pid: -1,
  dep_id: 0,
  label: "整体指标",
  dep_children: [
    {
      dep_pid: 0,
      dep_id: 2,
      label: "复合指标1",
      dep_children: [
        {
          dep_pid: 2,
          dep_id: 5,
          label: "原子指标3"
        },
        {
          dep_pid: 2,
          dep_id: 6,
          label: "原子指标4"
        },
        {
          dep_pid: 2,
          dep_id: 9,
          label: "原子指标5"
        },
        {
          dep_pid: 2,
          dep_id: 10,
          label: "原子指标6"
        }
      ]
    },
    {
      dep_pid: 0,
      dep_id: 3,
      label: "复合指标2",
      dep_children: [
        {
          dep_pid: 3,
          dep_id: 7,
          label: "原子指标7"
        },
        {
          dep_pid: 3,
          dep_id: 8,
          label: "原子指标8"
        }
      ]
    },
    {
      dep_pid: 0,
      dep_id: 1358363417904128,
      label: "原子指标1",
    },
    {
      dep_pid: 0,
      dep_id: 11,
      label: "原子指标2"
    }
  ]
};

export const newData = {
  dep_id: 0,
  label: "XXX科技有限公司",
  dep_children: [
    {
      dep_id: 2,
      label: "产品研发部",
      dep_children: [
        {
          dep_id: 5,
          label: "研发-前端"
        },
        {
          dep_id: 6,
          label: "研发-后端"
        },
        {
          dep_id: 9,
          label: "UI设计"
        }
      ]
    },
    {
      dep_id: 3,
      label: "销售部",
      dep_children: [
        {
          dep_id: 7,
          label: "销售一部"
        }
      ]
    },
    {
      dep_id: 4,
      label: "财务部"
    },
    {
      dep_id: 9,
      label: "HR人事"
    }
  ]
};

