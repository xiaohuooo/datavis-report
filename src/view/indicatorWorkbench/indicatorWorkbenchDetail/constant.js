import { getIndicatorById, getComplexIndicatorById, getDerivativeIndicatorById, getOutIndicatorById, getStaticIndicatorById } from "@/api/indicator";

// 各类型指标的详情接口
export const INDICATOR_DETAIL_FUNC = [
  {
    type: "1",
    func: getIndicatorById
  },
  {
    type: "2",
    func: getComplexIndicatorById
  },
  {
    type: "3",
    func: getDerivativeIndicatorById
  },
  {
    type: "4",
    func: getOutIndicatorById
  },
  {
    type: "5",
    func: getStaticIndicatorById
  }
];

// 可切换的图表
export const IMG_LIST = [
  {
    name: "折线图",
    checked: true,
    url1: require("@/assets/images/indicatorWorkbench/linew.svg"),
    url2: require("@/assets/images/indicatorWorkbench/linewa.svg")
  },
  {
    name: "柱状图",
    checked: false,
    url1: require("@/assets/images/indicatorWorkbench/barw.svg"),
    url2: require("@/assets/images/indicatorWorkbench/barwa.svg")
  },
  {
    name: "饼图",
    checked: false,
    url1: require("@/assets/images/indicatorWorkbench/piew.svg"),
    url2: require("@/assets/images/indicatorWorkbench/piewa.svg")
  }
];
