import NumericFormatMixin from "@/mixins/NumericFormat";

export default {
  mixins: [NumericFormatMixin],
  computed: {
    // x轴标签
    xAxisData() {
      return this.chartData.map(ele => ele.name);
    },
    // 带格式的指标值
    indicatorValueWithFormat() {
      return value => {
        return (
          this.indicatorValue_Prefix(this.indicatorConfig) +
          this.indicatorValue_YXQ(this.indicatorConfig, value) +
          this.indicatorValue_Unit(this.indicatorConfig) +
          this.indicatorValue_Suffix(this.indicatorConfig)
        );
      };
    }
  },
  methods: {
    /**
     * 将图表系列数据中的 null 改为 0
     * @param {Array} data 数值
     */
    getSeriesData(data) {
      return data.map(ele => ({
        name: ele.name,
        value: ele.value || 0
      }));
    }
  }
};
