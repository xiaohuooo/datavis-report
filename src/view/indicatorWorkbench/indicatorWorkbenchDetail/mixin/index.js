import { getReportCompareDataAndHeaders } from "@/api/report";
import { BUILT_IN_DIMENSION } from "@/config/DimensionSortDictionary";
import { INDICATOR_DETAIL_FUNC } from "@/view/indicatorWorkbench/indicatorWorkbenchDetail/constant";

export default {
  methods: {
    // 获取详情
    async getIndicatorDetail(id, type) {
      const fun = INDICATOR_DETAIL_FUNC.find(ele => ele.type === type).func;
      const res = await fun(id);
      if (res.data?.state === "success") {
        const resData = res.data.data;
        const indicatorConfig = resData.configData ? JSON.parse(resData.configData) : {};
        const conditionGroups = resData.conditionGroups
        return {
          indicatorConfig,
          conditionGroups
        };
      }
    },
    /**
     * 获取图表数据
     * @param {String} start 开始日期
     * @param {String} end 结束日期
     * @param {String} dimensionId 维度数据
     */
    async getChartData({ start, end, dimensionId,variables }) {
      console.log("刷新图表", start, end, dimensionId);

      const { id, type } = this.$route.query;
      const param = {
        dimensionIds: [dimensionId],
        indicatorVos: [{ id, type, variables: { businessDate: start , ...variables} }],
        date: {
          startDate: start,
          endDate: end
        }
      };
      const res = await getReportCompareDataAndHeaders(param);
      const date = [
        ["10月1日", 80.6],
        ["10月2日", 70.6],
        ["10月3日", 50.6],
        ["10月4日", null],
        ["10月5日", 30.6],
        ["10月6日", 20.6],
        ["10月7日", 30.6],
        ["10月8日", 40.6],
        ["10月9日", 50.6],
        ["10月10日", 60.6],

        ["10月11日", 80.6],
        ["10月12日", 70.6],
        ["10月13日", 50.6],
        ["10月14日", 40.6],
        ["10月15日", 30.6],
        ["10月16日", 20.6],
        ["10月17日", 30.6],
        ["10月18日", 40.6],
        ["10月19日", 50.6],
        ["10月20日", 60.6],

        ["10月21日", 80.6],
        ["10月22日", 70.6],
        ["10月23日", 50.6],
        ["10月24日", 40.6],
        ["10月25日", 30.6],
        ["10月26日", 20.6],
        ["10月27日", 30.6],
        ["10月28日", 40.6],
        ["10月29日", 50.6],
        ["10月30日", 60.6]
      ];
      const col = [
        ["10月1日", 80.6],
        ["10月2日", 70.6],
        ["10月3日", 60.6],
        ["10月4日", 40.6],
        ["10月5日", 30.6]
      ];
      if (res.data.state === "success") {
        const arr = res.data.data.currentData.map(ele => ({
          name: ele[0],
          value: ele[1],
          valueType: ele[1] < 1 ? "percentType" : "numberType" // 数值类型
        }));

        // 如果是内置的日期维度，则进行排序。其他维度（包括其他日期维度）按返回顺序展示
        if (dimensionId === BUILT_IN_DIMENSION.id) {
          arr.sort((pre, next) => new Date(pre.name).getTime() - new Date(next.name).getTime());
        }
        return arr;
      } else {
        return [];
      }
    }
  }
};
