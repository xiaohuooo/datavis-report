import { deleteIndicator, copyIndicator, syncToOtherTab, cancelCollect } from "@/api/indicatorWorkbench";
import { extraTextDot, getPageTableIndex } from "@/utils/formatString";
import { mapMutations, mapState } from "vuex";
import resourcePublish from "@/mixins/resourcePublish";

export default {
  mixins: [resourcePublish],
  computed: {
    ...mapState("intelligentAnswer", ["showIntelligentAnswer"]),
    formatName() {
      return (text, num) => extraTextDot(text, num);
    },
    formatIndex() {
      return index => getPageTableIndex(index, this.pageCondition.currentPage, this.pageCondition.pageSize);
    },
    yoyColor() {
      return status => {
        if (status === "同比增长") {
          return "#ff4d4f";
        } else if (status === "同比下降") {
          return "#6DD400";
        } else if (status === "同比持平") {
          return "#000000";
        }
      };
    }
  },
  methods: {
    ...mapMutations("indicatorWorkbench", ["setIsReturnFromDetail"]),
    ...mapMutations("intelligentAnswer", ["setShowIntelligentAnswer"]),
    /**
     * 查看详情
     * @param {*} id 指标id
     * @param {*} indicatorName 指标名称
     * @param {*} type 指标类型
     */
    gotoDetail({ id, indicatorName, type }) {
      this.setIsReturnFromDetail(true);
      this.$router.push({
        path: "/indicatorWorkbench/indicatorWorkbenchDetail",
        query: {
          id,
          name: indicatorName,
          type
        }
      });
    },
    /**
     * 编辑指标
     * @param {*} id 指标id
     * @param {*} type 指标类型
     */
    gotoEdit({ id, type }) {
      this.$router.push({
        path: "/manager/indicator/indicator_detail",
        query: {
          id,
          type,
          edit: 1
        }
      });
    },
    /**
     * 复制指标
     * @param {*} id 指标id
     * @param {*} type 指标类型
     */
    // async copyIndicator({ id, type }) {
    //   const res = await copyIndicator(id, type);
    //   if (res.data.state === "success") {
    //     this.$Message.success("指标复制成功！");
    //     this.$emit("refreshData");
    //   } else {
    //     this.$Message.warning(res.data.message);
    //   }
    // },
    copyIndicator(row) {
      this.$router.push({
        path: "/manager/indicator/indicator_detail",
        query: {
          type: row.type,
          add: 1,
          isCopy: 1,
          id: row.id
        }
      });
    },
    /**
     * 发布/下线指标
     */
    changeIndicatorStatus({ id, indicatorName, moduleType, status }) {
      this.changeStatus({ id, name: indicatorName, moduleType, status });
    },
    /**
     * 创建派生指标
     */
    createDerive({ id }) {
      if (this.showIntelligentAnswer) {
        this.setShowIntelligentAnswer(false);
      }
      this.$router.push({
        path: "/manager/indicator/indicator_detail",
        query: {
          type: 3,
          add: 1,
          derivedFrom: id
        }
      });
    },
    /**
     * 变更所有者
     */
    changeOwner({ id, moduleType }) {
      this.$emit("showModal", { type: "变更所有者", id, moduleType });
    },
    /**
     * 同步至
     * @param {Number} id 指标id
     * @param {Array} existedTabIDList 当前指标所在的tab标签
     */
    syncTo({ id, existedTabIDList }) {
      this.$emit("showModal", {
        type: "同步至",
        show: true,
        indicatorId: id,
        existedTabIDList
      });
    },
    /**
     * 权限分享
     */
    shareAuth({ id, indicatorName, moduleType }) {
      this.$emit("showModal", { type: "权限分享", id, indicatorName, moduleType });
    },
    /**
     * 收藏
     */
    async collectIndicator(item) {
      console.log(item);
      const { id } = item;
      const res = await syncToOtherTab({ tabId: 3, indicatorId: id });
      if (res.data.state === "success") {
        this.$Message.success(`收藏成功！`);
        this.$emit("updateData:front", { id, key: "isCollect", value: true });
      } else {
        this.$Message.warning(res.data.message);
      }
    },
    /**
     * 取消收藏
     */
    async cancelCollectIndicator({ id }) {
      const res = await cancelCollect({ tabId: 3, indicatorId: id });
      if (res.data.state === "success") {
        this.$Message.success(`取消收藏成功！`);
        this.$emit("updateData:front", { id, key: "isCollect", value: false });
      } else {
        this.$Message.warning(res.data.message);
      }
    },
    /**
     * 删除指标
     */
    deleteIndicator({ id, type, indicatorName }) {
      const text = extraTextDot(indicatorName, 12);
      this.$Modal.confirm({
        title: `确认删除“${text}”指标吗？`,
        content: "删除后不可撤销",
        okText: "确认",
        onOk: async () => {
          const res = await deleteIndicator(id, type);
          if (res.data.state === "success") {
            this.$Message.success(`删除指标成功！`);
            this.$emit("refreshData");
          } else {
            this.$emit("showModal:delete", { id, indicatorName, resData: res.data.data });
          }
        }
      });
    }
  }
};
