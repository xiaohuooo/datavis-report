const OBJECT_DATA = {
  objectTypeList: [
    {
      value: 1,
      label: '公司目标'
    },
    {
      value: 2,
      label: '部门目标'
    },
    {
      value: 3,
      label: '个人目标'
    },
  ],
  objectPeriodList: [
    {
      value: 1,
      label: '年度目标'
    },
    {
      value: 2,
      label: '月度目标（一次性）'
    },
    {
      value: 3,
      label: '日度目标（一次性）'
    },
    {
      value: 4,
      label: '月度目标（循环）'
    },
    {
      value: 5,
      label: '日度目标（循环）'
    },
  ],
  objectValueTypeList: [
    {
      value: '金额',
      label: '金额',
    },
    {
      value: '数量',
      label: '数量'
    },
    {
      value: '频次',
      label: '频次'
    },
    {
      value: '百分比',
      label: '百分比'
    },
  ],
  objectValueUnitList: [
    [
      {
        value: '万元',
        label: '万元',
      },
      {
        value: '元',
        label: '元',
      },
    ],
    [
      {
        value: '只',
        label: '只',
      },
      {
        value: '个',
        label: '个',
      },
      {
        value: '条',
        label: '条',
      },
      {
        value: '块',
        label: '块',
      },
      {
        value: '份',
        label: '份',
      },
      {
        value: '台',
        label: '台',
      },
    ],
    [
      {
        value: '次',
        label: '次',
      },
      {
        value: '千次',
        label: '千次',
      },
    ],
    [
      {
        value: '%',
        label: '%',
      },
      {
        value: '‰',
        label: '‰',
      },
    ],
  ],
}

export default OBJECT_DATA
