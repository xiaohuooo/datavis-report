import { getCompareDate } from "@/utils/chartDate";
import { ZERO_PRE_COMPARE_DATA_SHOW } from "@/config/ReportConstants";
import { formatCompareChangeRateData } from "@/view/report/indicator-browser/formatData";
import { getReportCompareDataAndHeaders } from '@/api/report'

export default {
  methods: {
    //进度条数据
    percent(num1, num2) {
      // console.log(num1, num2, '000')
      let percent = 0
      if (num1 !== '--') {
        percent = parseFloat(((num1 / parseFloat(num2)) * 100).toFixed(2))
      }
      return percent
    },
    //判断风险等级
    judgeRiskLevel(data) {
      const { lowRiskValue, midRiskValue, highRiskValue, indicatorValue } = data
      if (!indicatorValue || indicatorValue == '--') return null
      const indVal = parseFloat(indicatorValue)
      if (highRiskValue && parseFloat(highRiskValue) > indVal) {
        return 'high'
      }
      if (midRiskValue && parseFloat(midRiskValue) > indVal) {
        return 'mid'
      }
      if (lowRiskValue) {
        if (parseFloat(lowRiskValue) > indVal) {
          return 'low'
        } else {
          return 'none'
        }
      }
      return null
    },
    //风险等级返回规则
    riskLevelRule(data, type) {
      let riskLevel = this.judgeRiskLevel(data)
      let color = ''
      let text = ''
      switch (riskLevel) {
        case 'none':
          color = 'background-image: linear-gradient(270deg, #52C41A 0%, #00FA2C 100%);'
          text = '无风险'
          break;
        case 'low':
          color = 'background-image: linear-gradient(270deg, #4666F9 0%, #44A6EF 100%);'
          text = '低风险'
          break;
        case 'mid':
          color = 'background-image: linear-gradient(270deg, #F78C22 0%, #FFBD54 100%);'
          text = '中风险'
          break;
        case 'high':
          color = 'background-image: linear-gradient(270deg, #F86666 0%, #FF9696 100%);'
          text = '高风险'
          break;
      }
      return type == 'background' ? color : text
    },
    //进度条颜色
    strokeColor(data) {
      let riskLevel = this.judgeRiskLevel(data)
      let strokeColor
      switch (riskLevel) {
        case 'high':
          strokeColor = ['#F86666', '#F86666']
          break;
        case 'mid':
          strokeColor = ['#FFCB5F', '#FFCB5F']
          break;
        case 'low':
          strokeColor = ['#99C2FA', '#608FF4']
          break;
        case 'none':
          strokeColor = ['#52C41A', '#00FA2C']
          break;
      }
      return strokeColor
    },
    //查询同环比数据
    async getCompareData(indicatorId, dimensionId, dimensionMember, type, compareDate, curDate) {
      let params = {
        dimensionIds: (dimensionId && dimensionMember) ? [dimensionId] : [],
        indicatorVos: [{ id: indicatorId, type, variables: { businessDate: curDate } }],
        date: {
          startDate: curDate,
          endDate: curDate,
        },
        yoyDate: {
          [indicatorId]: compareDate,
        },
        momDate: getCompareDate("C", { startDate: curDate, endDate: curDate, compareType: "mom" }, [, , , "2"])
      }
      const res = await getReportCompareDataAndHeaders(params);
      if (res.data.state === "success") {
        const { currentData, yoyData, momData } = res.data.data;
        let cur = (dimensionId && dimensionMember) ? (currentData.find(item => item[0] == dimensionMember)?.[1] || null) : currentData[0]?.[0];
        const yoy = (dimensionId && dimensionMember) ? (yoyData[indicatorId].find(item => item[0] == dimensionMember)?.[1] || null) : yoyData[indicatorId][0]?.[0];
        let mom = (dimensionId && dimensionMember) ? (momData.find(item => item[0] == dimensionMember)?.[1] || null) : momData[0]?.[0];
        let yoyRate = formatCompareChangeRateData(cur, yoy, {}); //计算同比变化率
        let momRate = formatCompareChangeRateData(cur, mom, {}); //计算环比变化率
        if (momRate == 'Infinity%' || momRate == "NaN%") {
          momRate = '--'
        }
        cur = cur ? Number(cur) : cur
        return {
          indicatorValue: cur?.toFixed(2) || ZERO_PRE_COMPARE_DATA_SHOW,
          yoyRate,
          momRate,
        };
      } else {
        return res.data.message
      }
    },
  },
}
