import { REPORT_ASSET, DIMENSION_ASSET, INDICATOR_ASSET, DATA_MODEL_ASSET } from "@/config/resourcePublish";

export const TAB_PANE_LIST = [
  {
    label: "报告管理",
    name: REPORT_ASSET
  },
  {
    label: "维度管理",
    name: DIMENSION_ASSET
  },
  {
    label: "指标管理",
    name: INDICATOR_ASSET
  },
  // {
  //   label: "数据模型",
  //   name: DATA_MODEL_ASSET
  // }
];
