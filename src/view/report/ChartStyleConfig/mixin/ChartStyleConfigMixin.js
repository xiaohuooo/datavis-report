import {mapGetters} from "vuex";

export default {
  components: {},
  inject: ['recordSnapshot'],
  computed: {
    ...mapGetters(['currentSelectChart'])
  },
  methods: {
    addCurrentSelectChartStyle() {
      if(!this.currentSelectChart.style) {
        this.currentSelectChart.style = {}
      }
    },
    refreshCurrentSelectChartStyle(property, value, needRecordSnapshot=true) {
      this.addCurrentSelectChartStyle()
      this.$set(this.currentSelectChart.style, property, value)
      if (needRecordSnapshot) {
        this.recordSnapshot()
      }
    },
    refreshCurrentSelectChartConfigData(property, value) {
      this.$set(this.currentSelectChart.data.configData, property, value)
      this.recordSnapshot()
    },
  }
}
