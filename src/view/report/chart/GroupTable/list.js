export default {
  methods: {
    /**
     * 列表展示
     * @param res
     */
    buildListTable(res) {
      let data = this.buildTableDataByColumns(res)
      this.buildExpandTableData(data)
      this.tableData = data
    },
  },
}
