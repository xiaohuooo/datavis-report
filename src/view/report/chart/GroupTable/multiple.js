import _ from 'lodash'

export default {
  methods: {
    /**
     * 多列展示
     * @param res
     */
    buildMultipleColumnTable(res) {
      let data = this.buildTableDataByColumns(res)
      this.buildExpandTableData(data)
      this.setRowSpan(data)
      this.tableData = data
    },

    /**
     * 遍历所有数据，计算维成员应该怎么合并单元格
     * @param data
     */
    setRowSpan(data) {
      this.dimensionIds.forEach((dimensionId, dimensionIdIndex) => {
        // 去重，获取每个维度中的维成员
        const dimensionMember = _.uniq(data.map(dataItem => {
          let list = []
          for (let i = 0; i <= dimensionIdIndex; i++) {
            list.push(dataItem[this.dimensionIds[i]])
          }
          return list.join('_')
        }))

        dimensionMember.forEach(dimensionMemberItem => {
          // 找到所有含有该维成员的数据
          const filterData = data.filter(dataItem => {
            let list = [] // join('_')后用来与维成员比较，如：江苏省_南京市_建邺区
            for (let i = 0; i <= dimensionIdIndex; i++) {
              list.push(dataItem[this.dimensionIds[i]])
            }
            return list.join('_') === dimensionMemberItem
          })

          filterData.forEach((filterDataItem, filterDataItemIndex) => {
            // 该维成员数据中，第一项合并单元格，其他则隐藏，以rowSpan+第几个维度为字段名，设置合并单元格数
            filterDataItem[`rowSpan${dimensionIdIndex + 1}`] = filterDataItemIndex === 0 ? filterData.length : 0
          })
        })
      })
    },
  },
}
