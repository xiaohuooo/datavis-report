export default {
  computed: {
    fixedHeaderHeight(){
      return this.propertyStyle.height - 110;
    },
    calculationStyle: function () {
      return this.propertyData && this.propertyData.configData.calculationStyle || {}
    },
  },
  watch: {
    'propertyStyle.header': {
      handler: function (val, old) {
        if (val) {
          this.handleChangeTheadStyle()
        }
      },
      deep: true,
    },
    'propertyStyle.content': {
      handler: function (val, old) {
        if (val) {
          this.handleChangeTbodyStyle()
        }
      },
      deep: true,
    },
    'propertyStyle.total': {
      handler: function (val) {
        if (val && this.propertyData.configData.isOpenTotal) {
          this.handleChangeTotalStyle()
        }
      },
      deep: true,
    },
    'propertyStyle.borderColor': {
      handler: function (val, old) {
        if (val) {
          this.handleChangeDrawAreaStyle()
        }
      },
      deep: true,
    },
    'propertyStyle.backgroundColor': {
      handler: function (val, old) {
        if (val) {
          this.handleChangeDrawAreaStyle()
        }
      },
      deep: true,
    },
    'propertyStyle.borderLineType': {
      handler: function (val, old) {
        if (val) {
          this.handleChangeDrawAreaStyle()
        }
      },
      deep: true,
    },
    'propertyStyle.borderRadius': {
      handler: function (val, old) {
        if (val) {
          this.handleChangeDrawAreaStyle()
        }
      },
      deep: true,
    },
    'propertyStyle.borderWidth': {
      handler: function (val, old) {
        if (val) {
          this.handleChangeDrawAreaStyle()
        }
      },
      deep: true,
    },
    'propertyStyle.page': {
      handler: function (val, old) {
        if (val) {
          this.refreshData()
        }
      },
      deep: true,
    },
    'propertyStyle.page.pageSize': {
      handler: function (val, old) {
        if (val) {
          this.currentPage = 1
          this.refreshData()
        }
      },
      deep: true,
    },
    'propertyStyle.widthConfig': {
      handler: function (val, old) {
        if (val) {
          this.handleChangeWidthStyle()
          this.refreshData()
        }
      },
      deep: true,
    },
    'propertyStyle.expandAll'() {
      this.refreshData()
    },
  },
  methods: {
    /**
     * 表头样式处理
     */
    handleChangeTheadStyle() {
      /*
        titleColor        //背景
        fontWeight        //加粗
        fontFamily        //字体
        fontSize          //字号
        color             //字体颜色
        dimensionStyle    //维度排序
        indicatorStyle    //指标排序
        textDecoration    //下、中划线
        fontStyle         //斜体
        lineBreak         //自动换行
        freezeHeader      //冻结表头
       */
      const head = this.tableRef.$el.getElementsByClassName('ivu-table-header')
      if (this.propertyStyle && this.propertyStyle.header) {
        const {
          titleColor: backgroundColor,
          fontWeight,
          fontFamily,
          fontSize,
          color,
          dimensionStyle,
          indicatorStyle,
          textDecoration,
          fontStyle,
          lineHeight,
          lineBreak
        } = this.propertyStyle.header
        const dimensionLen = this.propertyData.dimension.length
        const groupTableType = this.propertyData.configData.groupTableType
        if (head) {
          let ths = head[0].getElementsByTagName('th')
          for (let i = 0; i < ths.length; i++) {
            if (groupTableType === 'tree') {
              const tableCellDom = ths[i].getElementsByClassName('ivu-table-cell')[0]
              // 树状展示，维度全在第一列
              if (i === 0) {
                tableCellDom.style.justifyContent = dimensionStyle === 'left' ? 'flex-start' : dimensionStyle === 'right' ? 'flex-end' : 'center'
              } else {
                tableCellDom.style.justifyContent = indicatorStyle === 'left' ? 'flex-start' : indicatorStyle === 'right' ? 'flex-end' : 'center'
              }
            } else {
              if (i < dimensionLen) {
                ths[i].style.textAlign = dimensionStyle
              } else {
                ths[i].style.textAlign = indicatorStyle
              }
            }
            this.setStyle(ths[i], {
              backgroundColor,
              fontWeight,
              fontFamily,
              fontSize: fontSize + 'px',
              color,
              fontStyle,
              height: lineHeight + 'px'
            })
            // 两种表头结构
            if(ths[i] && ths[i].children && ths[i].children[0] && ths[i].children[0].children[0] && ths[i].children[0].children[0].firstChild && ths[i].children[0].children[0].firstChild.style){
              ths[i].children[0].children[0].firstChild.style.textDecoration = textDecoration
              if(!lineBreak){
                ths[i].children[0].children[0].firstChild.style.maxWidth = '100%'
                ths[i].children[0].children[0].firstChild.style.overflow = 'hidden'
                ths[i].children[0].children[0].firstChild.style.textOverflow = 'ellipsis'
                ths[i].children[0].children[0].firstChild.style.whiteSpace = 'nowrap'
                ths[i].children[0].children[0].firstChild.style.display = 'inline-block';
                ths[i].children[0].children[0].firstChild.style.verticalAlign = 'middle';
              }else{
                ths[i].children[0].children[0].firstChild.style.maxWidth = ''
                ths[i].children[0].children[0].firstChild.style.overflow = ''
                ths[i].children[0].children[0].firstChild.style.textOverflow = ''
                ths[i].children[0].children[0].firstChild.style.whiteSpace = ''
                ths[i].children[0].children[0].firstChild.style.display = '';
                ths[i].children[0].children[0].firstChild.style.verticalAlign = '';
              }
            }else if(ths[i] && ths[i].children && ths[i].children[0] && !ths[i].children[0].children[0]){
              ths[i].children[0].firstChild.style.textDecoration = textDecoration
              if(!lineBreak){
                ths[i].children[0].firstChild.style.overflow = 'hidden'
                ths[i].children[0].firstChild.style.textOverflow = 'ellipsis'
                ths[i].children[0].firstChild.style.whiteSpace = 'nowrap'
              }else{
                ths[i].children[0].firstChild.style.overflow = ''
                ths[i].children[0].firstChild.style.textOverflow = ''
                ths[i].children[0].firstChild.style.whiteSpace = ''
              }
            }
          }
        }
      }
    },

    /**
     * 表体样式处理
     */
    handleChangeTbodyStyle() {
      /*
       even              //偶行背景色
       odd               //奇行背景色
       fontWeight        //加粗
       fontFamily        //字体
       fontSize          //字号
       color             //字体颜色
       dimensionStyle    //维度排序
       indicatorStyle    //指标排序
       textDecoration    //下、中划线
       fontStyle         //斜体
       lineColor         //网格线颜色
      */
      const body = this.tableRef.$el.getElementsByClassName('ivu-table-body')
      if (this.propertyStyle && this.propertyStyle.content && body) {
        const { even, odd, fontWeight, fontFamily,
          fontSize, color, dimensionStyle, indicatorStyle,
          textDecoration, fontStyle, lineHeight, lineColor, wordWrap
        } = this.propertyStyle.content
        const indicatorLen = this.propertyData.indicator.length
        if (body) {
          let trs = body[0].getElementsByTagName('tr')
          for (let i = 0; i < trs.length; i++) {
            let tr = trs[i], tds = tr.getElementsByTagName('td')
            // 从后往前推，指标有几个，就遍历几个，因为维度会合并单元格，顺序错乱
            for (let j = tds.length - 1; j >= tds.length - indicatorLen; j--) {
              let tableCellDom = tds[j].getElementsByClassName('ivu-table-cell')?.[0]
              if (tableCellDom) {
                tableCellDom.style.justifyContent = indicatorStyle === 'left' ? 'flex-start' : indicatorStyle === 'right' ? 'flex-end' : 'center'
              }
              tds[j].style.textAlign = indicatorStyle
            }
            // 剩下单元格全是维度
            for (let j = 0; j < tds.length - indicatorLen; j++) {
              tds[j].style.textAlign = dimensionStyle
              let groupItemDom = tds[j].getElementsByClassName('group-item')?.[0]
              if (groupItemDom) {
                groupItemDom.style.justifyContent = dimensionStyle === 'left' ? 'flex-start' : dimensionStyle === 'right' ? 'flex-end' : 'center'
              }
            }
            this.setStyle(tr, {
              fontWeight, fontFamily, fontSize: fontSize + 'px', color, fontStyle
            })

            for (let j = 0; j < trs[i].children.length; j++) {
              let item = trs[i].children[j]
              if (fontStyle === 'italic' && indicatorStyle === 'right' && item.getElementsByClassName('ivu-table-cell-tooltip-content')[0]) {
                item.getElementsByClassName('ivu-table-cell-tooltip-content')[0].style.paddingRight = '5px'
              }
              item.style.textDecoration = textDecoration
              item.style.borderColor = lineColor
              item.style.height = lineHeight + 'px'
              if (i % 2 === 0) {
                item.style.backgroundColor = even
              } else {
                item.style.backgroundColor = odd
              }
            }
          }
        }
      }
      if(this.propertyStyle && this.propertyStyle.widthConfig === '最佳比例'){
        this.handleChangeWidthStyle();
      }
    },

    /**
     * 绘图区样式
     */
    handleChangeDrawAreaStyle() {
      /*
       borderColor           //边框线颜色
       borderLineType        //边框线类型
       borderWidth           //边框线宽度
       borderRadius          //边框线圆角
      */
      const drawArea = this.$refs['groupTable'].getElementsByClassName('table-all')
      if (this.propertyStyle) {
        const {
          borderColor,
          borderLineType,
          borderRadius,
          borderWidth,
        } = this.propertyStyle
        if (drawArea && drawArea[0]) {
          drawArea[0].style.borderColor = borderColor;
          drawArea[0].style.borderStyle = borderLineType;
          drawArea[0].style.borderWidth = borderWidth + 'px';
          drawArea[0].style.borderRadius = borderRadius + 'px';
        }
      }
    },

    handleChangeWidthStyle(){
      const body = this.tableRef.$el.getElementsByClassName('ivu-table-body')
      if (this.propertyStyle && this.propertyStyle.content && body) {
        let { dimensionStyle, indicatorStyle } = this.propertyStyle.content;
        const dimensionLen = this.propertyData.dimension.length
        if (body) {
          let trs = body[0].getElementsByTagName('tr')
          for (let i = 0; i < trs.length; i++) {
            let tr = trs[i], tds = tr.getElementsByTagName('td')
            for (let j = 0; j < tds.length; j++) {
              let td = tds[j]
              let tdJust = td.children[0].getElementsByClassName('ivu-table-cell-tooltip')
              if (tdJust[0]) {
                if (dimensionLen) {
                  if (j < dimensionLen) {
                    if(dimensionStyle === 'left'){
                      tdJust[0].style.marginLeft = 0;
                      tdJust[0].style.marginTop = 0;
                      tdJust[0].style.marginBottom = 0;
                      tdJust[0].style.marginRight = 0;
                    }else if(dimensionStyle === 'right'){
                      tdJust[0].style.marginLeft = 'auto';
                      tdJust[0].style.marginTop = 0;
                      tdJust[0].style.marginBottom = 0;
                      tdJust[0].style.marginRight = 0;
                    }else if(dimensionStyle === 'center'){
                      tdJust[0].style.marginLeft = 'auto';
                      tdJust[0].style.marginTop = 0;
                      tdJust[0].style.marginBottom = 0;
                      tdJust[0].style.marginRight = 'auto';
                    }
                  } else {
                    if(indicatorStyle === 'left'){
                      tdJust[0].style.marginLeft = 0;
                      tdJust[0].style.marginTop = 0;
                      tdJust[0].style.marginBottom = 0;
                      tdJust[0].style.marginRight = 0;
                    }else if(indicatorStyle === 'right'){
                      tdJust[0].style.marginLeft = 'auto';
                      tdJust[0].style.marginTop = 0;
                      tdJust[0].style.marginBottom = 0;
                      tdJust[0].style.marginRight = 0;
                    }else if(indicatorStyle === 'center'){
                      tdJust[0].style.marginLeft = 'auto';
                      tdJust[0].style.marginTop = 0;
                      tdJust[0].style.marginBottom = 0;
                      tdJust[0].style.marginRight = 'auto';
                    }
                  }
                } else {
                  if (j === 0) {
                    if(dimensionStyle === 'left'){
                      tdJust[0].style.marginLeft = 0;
                      tdJust[0].style.marginTop = 0;
                      tdJust[0].style.marginBottom = 0;
                      tdJust[0].style.marginRight = 0;
                    }else if(dimensionStyle === 'right'){
                      tdJust[0].style.marginLeft = 'auto';
                      tdJust[0].style.marginTop = 0;
                      tdJust[0].style.marginBottom = 0;
                      tdJust[0].style.marginRight = 0;
                    }else if(dimensionStyle === 'center'){
                      tdJust[0].style.marginLeft = 'auto';
                      tdJust[0].style.marginTop = 0;
                      tdJust[0].style.marginBottom = 0;
                      tdJust[0].style.marginRight = 'auto';
                    }
                  } else {
                    if(indicatorStyle === 'left'){
                      tdJust[0].style.marginLeft = 0;
                      tdJust[0].style.marginTop = 0;
                      tdJust[0].style.marginBottom = 0;
                      tdJust[0].style.marginRight = 0;
                    }else if(indicatorStyle === 'right'){
                      tdJust[0].style.marginLeft = 'auto';
                      tdJust[0].style.marginTop = 0;
                      tdJust[0].style.marginBottom = 0;
                      tdJust[0].style.marginRight = 0;
                    }else if(indicatorStyle === 'center'){
                      tdJust[0].style.marginLeft = 'auto';
                      tdJust[0].style.marginTop = 0;
                      tdJust[0].style.marginBottom = 0;
                      tdJust[0].style.marginRight = 'auto';
                    }
                  }
                }
              }
            }
          }
        }
      }
    },

    /**
     * 合计样式
     */
    handleChangeTotalStyle() {
      /*
       titleColor        //背景
       fontWeight        //加粗
       fontFamily        //字体
       fontSize          //字号
       color             //字体颜色
       dimensionStyle    //维度排序
       indicatorStyle    //指标排序
       textDecoration    //下、中划线
       fontStyle         //斜体
       lineHeight        //行高
      */
      const total = this.tableRef.$el.getElementsByClassName('ivu-table-summary')
      if (this.propertyStyle.total) {
        const { titleColor: backgroundColor, fontWeight, fontFamily, fontSize, color, dimensionStyle, indicatorStyle, textDecoration, fontStyle, lineHeight } = this.propertyStyle.total
        const dimensionLen = this.propertyData.dimension.length
        if (total.length) {
          let trs = total[0].getElementsByTagName('tr')
          for (let i = 0; i < trs.length; i++) {
            let tr = trs[i], tds = tr.getElementsByTagName('td')
            for (let j = 0; j < tds.length; j++) {
              let td = tds[j]
              if (dimensionLen) {
                if (j < dimensionLen) {
                  td.style.textAlign = dimensionStyle
                } else {
                  td.style.textAlign = indicatorStyle
                }
              } else {
                if (j === 0) {
                  td.style.textAlign = dimensionStyle
                } else {
                  td.style.textAlign = indicatorStyle
                }
              }
            }
            this.setStyle(tr, { fontWeight, fontFamily, fontSize: fontSize + 'px', color, fontStyle })
            for (let j = 0; j < trs[i].children.length; j++) {
              let item = trs[i].children[j]
              item.style.textDecoration = textDecoration
              item.style.backgroundColor = backgroundColor
              item.style.height = lineHeight + 'px'
            }
          }
        }
      }
    },

    /**
     * 批处理样式
     * @param dom
     * @param css
     */
    setStyle(dom, css) {
      for (let key in css) {
        dom.style[key] = css[key]
      }
    },

    /**
     * 正负值及趋势图标render
     * @param indicator 指标
     * @param fastCalculateType 快速计算类型 monthOnMonth-环比 yearOnYear-同比
     */
    trendRender(indicator, fastCalculateType) {
      const typeKey = fastCalculateType === 'monthOnMonth' ? '_month_on_month' : '_year_on_year'

      return (h, { row }) => {
        const value = row[`${indicator.id}${typeKey}`]
        let type, color

        if (value === '--') {
          return h('span', value)
        }

        const numberTypeValue = Number(value.replace(/[,%]/g, ''))
        const style = this.calculationStyle[indicator.id]
        if (numberTypeValue > 0) {
          // 正值
          type = 'md-arrow-round-up'
          color = style?.upColor || '#9aba60'
        } else if (numberTypeValue < 0) {
          // 负值
          type = 'md-arrow-round-down'
          color = style?.downColor || '#e2534d'
        } else {
          return h('span', value)
        }

        // 趋势图标
        const showIcon = fastCalculateType === 'monthOnMonth' ? style?.compareIcon : style?.sameCompareIcon
        const trendIcon = showIcon ? h('Icon', {
          props: { type, color }
        }) : ''

        // 环比/同比正负值数字颜色设置
        const text = h('span', {
          style: {
            color: numberTypeValue === 0 ? '' : color + ' !important'
          }
        }, value)
        return h('span', [text, trendIcon])
      }
    },
  },
}
