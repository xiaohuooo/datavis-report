import _ from 'lodash'
import BigNumber from 'bignumber.js'

export default {
  methods: {
    /**
     * 树状展示
     * @param res
     */
    buildTreeTable(res) {
      const data = this.buildTableDataByColumns(res)
      const groupList = this.buildGroupListByData(data)
      const tree = this.buildTreeByGroupList(groupList)
      this.tableData = tree
    },

    /**
     * 构建树状展示的分组列表
     * 通过遍历维度列表，每个维度中的维成员去重
     * @param data
     */
    buildGroupListByData(data) {
      let groupList = []

      this.dimensionIds.forEach((dimensionId, dimensionIdIndex) => {
        const group = _.uniqWith(data.map(dataItem => {
          let list = []
          for (let i = 0; i <= dimensionIdIndex; i++) {
            list.push(dataItem[this.dimensionIds[i]])
          }
          let groupItem = {}
          let id = list.join('_')
          if (dataItem.id === id) {
            groupItem = { ...dataItem }
          }
          groupItem.id = id
          groupItem.parentId = dimensionIdIndex === 0 ? '/' : list.slice(0, -1).join('_')
          groupItem.title = dataItem[dimensionId]
          return groupItem
        }), _.isEqual)

        groupList.push(...group)
      })

      return groupList
    },

    /**
     * 根据分组列表，构建树
     * @param groupList
     * @param parentId
     */
    buildTreeByGroupList(groupList, parentId = '/') {
      let tree = []
      const { indicator: indicatorList, configData } = this.propertyData

      for (let item of groupList) {
        if (item.parentId === parentId) {
          const children = this.buildTreeByGroupList(groupList, item.id)
          if (children.length) {
            item.children = children

            // 如果有children，父级显示指标值的合计值
            for (let indicator of indicatorList) {
              let isPercent = false // 是否携带百分号
              let total = children.reduce((accumulator, currentValue) => {
                // 去掉千位分隔符，转为Number后相加
                if (currentValue[indicator.id].includes('%')) {
                  // 只要有一个值带有百分号，最后相加的结果就需要带上百分号
                  isPercent = true
                }
                const newCurrentValue = currentValue[indicator.id].replace(/[,%]/g, '')
                return new BigNumber(accumulator).plus(newCurrentValue)
              }, 0)
              if (isPercent) {
                total = total.div(100)
              }
              const isAllNaN = children.every(item => item[indicator.id].includes('--'))
              if (isAllNaN) {
                total = '--'
              }
              const numberType = isPercent ? '%' : '123'

              item[indicator.id] = this.processDataByNumberFormat(total, indicator, numberType)

              // 环比
              const monthOnMonth = configData[`${indicator.id}_month_on_month`]
              if (monthOnMonth?.open) {
                // 如果打开环比
                item[`${indicator.id}_month_on_month`] = this.getGroupMonthOnMonthValue(children, indicator)
              }

              // 同比
              const yearOnYear = configData[`${indicator.id}_year_on_year`]
              if (yearOnYear?.open) {
                // 如果打开同比
                item[`${indicator.id}_year_on_year`] = this.getGroupYearOnYearValue(children, indicator)
              }

              // 占比
              const ratio = configData[`${indicator.id}_ratio`]
              if (ratio) {
                // 如果打开占比
                item[`${indicator.id}_ratio`] = this.getGroupRatioValue(children, indicator)
              }
            }
          }
          if (this.propertyStyle?.expandAll) {
            item._showChildren = true
          }
          tree.push(item)
        }
      }

      return tree
    },

    /**
     * 获取分组的环比的值
     * @param children 子项
     * @param indicator 指标
     */
    getGroupMonthOnMonthValue(children, indicator) {
      let isPercent = false // 是否携带百分号
      let total = children.reduce((accumulator, currentValue) => {
        // 去掉千分位分隔符，转为Number后相加
        if (currentValue[`${indicator.id}_month_on_month`].includes('%')) {
          // 只要有一个值带有百分号，最后相加的结果就需要带上百分号
          isPercent = true
        }
        const newCurrentValue = Number(currentValue[`${indicator.id}_month_on_month`].replace(/[,%]|-{2}/g, ''))
        return new BigNumber(accumulator).plus(newCurrentValue)
      }, 0)
      if (isPercent) {
        total = total.div(100)
      }
      const isAllNaN = children.every(item => item[`${indicator.id}_month_on_month`].includes('--'))
      if (isAllNaN) {
        total = '--'
      }
      const numberType = isPercent ? '%' : '123'
      return this.processDataByNumberFormat(total, indicator, numberType)
    },

    /**
     * 获取分组的同比的值
     * @param children 子项
     * @param indicator 指标
     */
    getGroupYearOnYearValue(children, indicator) {
      let isPercent = false // 是否携带百分号
      let total = children.reduce((accumulator, currentValue) => {
        // 去掉千分位分隔符，转为Number后相加
        if (currentValue[`${indicator.id}_year_on_year`].includes('%')) {
          // 只要有一个值带有百分号，最后相加的结果就需要带上百分号
          isPercent = true
        }
        const newCurrentValue = Number(currentValue[`${indicator.id}_year_on_year`].replace(/[,%]|-{2}/g, ''))
        return new BigNumber(accumulator).plus(newCurrentValue)
      }, 0)
      if (isPercent) {
        total = total.div(100)
      }
      const isAllNaN = children.every(item => item[`${indicator.id}_year_on_year`].includes('--'))
      if (isAllNaN) {
        total = '--'
      }
      const numberType = isPercent ? '%' : '123'
      return this.processDataByNumberFormat(total, indicator, numberType)
    },

    /**
     * 获取分组的占比的值
     * @param children 子项
     * @param indicator 指标
     */
    getGroupRatioValue(children, indicator) {
      let isPercent = false // 是否携带百分号
      let total = children.reduce((accumulator, currentValue) => {
        // 去掉千分位分隔符，转为Number后相加
        if (currentValue[`${indicator.id}_ratio`].includes('%')) {
          // 只要有一个值带有百分号，最后相加的结果就需要带上百分号
          isPercent = true
        }
        const newCurrentValue = Number(currentValue[`${indicator.id}_ratio`].replace(/[,%]|-{2}/g, ''))
        return new BigNumber(accumulator).plus(newCurrentValue)
      }, 0)
      if (isPercent) {
        total = total.div(100)
      }
      const numberType = isPercent ? '%' : '123'
      return this.processDataByNumberFormat(total, indicator, numberType)
    },
  },
}
