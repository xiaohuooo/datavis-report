export const BUTTON_NAME = [
  {title: '页签标题修改', value: 1},
  {title: '移动至最前', value: 2},
  {title: '移动至最后', value: 3},
  {title: '分离', value: 4},
  // {title: '删除', value: 5}
]
export const CIRCUM_REC = [
  {class:'clickedAreaTop', position: 'top'},
  {class: 'clickedAreaRight', position: 'right'},
  {class: 'clickedAreaBottom', position: 'bottom'},
  {class: 'clickedAreaLeft', position: 'left'}
]
export const MOUSE_CLICKED_TITLE = {
  down: '拖入tab放置图表',
  up: '将其他组件拖入tab可为当前页签添加内容'
}
