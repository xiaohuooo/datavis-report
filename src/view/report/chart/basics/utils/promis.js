export const newPromise = (cb) => {
  return new Promise((resolve, reject) => {
    if (cb) (cb(resolve, reject))
  })
}
