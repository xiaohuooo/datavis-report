import {mapActions, mapGetters} from "vuex"

const store = {
  ...mapGetters(['chartLocalData']),
  ...mapActions(['storeDataToChartLocalData']),
}

export const findInVuex = function (objKey) {
  const data = store.chartLocalData.call(this);
  return data[objKey]
}
export const storeIntoVuex = function (storeKey, data) {
  const storeData = {};
  storeData[storeKey] = data
  store.storeDataToChartLocalData.call(this, storeData);
}
