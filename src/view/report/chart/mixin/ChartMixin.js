
export default {
  components: {},
  methods: {
    removeChartLink(configData) {
      if (configData) {
        configData.isMainLink = false
        delete configData.chartLinkIsOpen
        if (configData.hasOwnProperty('conditionValue')) {
          delete configData.conditionValue
          delete configData.filterDimension
        }
      }
    },
    /**
     * 下钻路径的点击上卷事件
     * @param e
     * @param item
     * @param idx
     */
    handleClickDrillDown(e, item, idx, drillDownChartType) {
      //设置点击状态
      let allItem = this.$refs[drillDownChartType].children[0].getElementsByClassName('drillDownItem'),
        drillDownOption = this.configData.drillDownOption;
      for(let i = 0; i < allItem.length; i++) {
        allItem[i].classList.remove('drillDownActive')
      }
      e.target.classList.add('drillDownActive')

      //上卷处理
      // this.$set(this.propertyData, 'dimension', [item.dimension.id])

      // let drillDownTagList = drillDownOption.drillDownTagList,
      //   prevDrillDownDimension = idx !== 0 ? drillDownTagList[idx - 1] : null;
      // for(let i = idx + 1; i < drillDownTagList.length; i++) {
      //   drillDownTagList[i].arrive = false
      // }
      //
      // if(prevDrillDownDimension !== null) {
      //   this.$set(this.configData, 'filterDimension', prevDrillDownDimension.dimension.id)
      //   this.$set(this.configData, 'conditionValue', drillDownTagList[idx].conditionValue)
      // } else {
      //   this.$set(this.configData, 'filterDimension', '')
      //   this.$set(this.configData, 'conditionValue', '')
      //   this.drillDownActive = 0
      // }
      //
      // this.configData.drillDownOption.drillDownStep = idx + 1
      const drillDownTagList = drillDownOption.drillDownTagList
      const prevDrillDownDimension = idx !== 0 ? drillDownTagList[idx - 1] : null;
      this.propertyData = _.assign(this.propertyData, {
        dimension: [item.dimension.id],
        configData: _.assign(this.configData, {
          drillDownOption: _.assign(drillDownOption, {
            drillDownTagList: _.map(drillDownTagList, (item, index) => {
              if (index === idx) {
                return {
                  ...item,
                  conditionValue: prevDrillDownDimension ? drillDownTagList[idx].conditionValue : '',
                  filterDimension: prevDrillDownDimension ? prevDrillDownDimension.dimension.id : '',
                }
              } else {
                return {
                  ...item,
                  arrive: index < idx
                }
              }
            }),
            drillDownStep: idx + 1
          })
        })
      })
      if (!prevDrillDownDimension) {
        this.drillDownActive = 0
      }
    },
  }
}
