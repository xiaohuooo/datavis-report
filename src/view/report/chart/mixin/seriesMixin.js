import { queryDimensionMember } from "@/api/report";

export default {
  methods: {
    /**
     * 获取维成员
     * @param {Array} series
     */
    async getDimensionMember({ series, indicator, startDate, endDate }) {
      let newStartDate = "";
      let newEndDate = "";
      if (startDate && endDate) {
        // 私有日期
        newStartDate = startDate;
        newEndDate = endDate;
      } else {
        // 全局日期
        if (this.$store.state.chart.reportDate.length) {
          newStartDate = this.$store.state.chart.reportDate[0];
          newEndDate = this.$store.state.chart.reportDate[1];
        }
      }
      const indicatorVos = indicator.map(item => ({
        id: item.id,
        type: item.type,
        variables: { businessDate: newStartDate }
      }));
      const { data: res } = await queryDimensionMember({
        dimensionIds: series,
        indicatorVos,
        date: { startDate: newStartDate, endDate: newEndDate }
      });
      if (res.state === "success") {
        return res.data;
      } else {
        this.$Message.error("获取维成员失败");
        return [];
      }
    },
    /**
     * 整合系列数据(单维度/单指标/单系列)
     * @param {Array} data
     * @param {Array} seriesMember 系列成员
     */
    adjustDataBySeries(data = [], seriesMember) {
      let newData = [];
      const dimArr = [...new Set(data.map(ele => ele[0]))]; // 维度
      dimArr.forEach(dim => {
        const list = data.filter(ele => ele[0] === dim);
        const indiArr = seriesMember.map(s => list.find(ele => ele[1] === s)?.[2] || null);
        newData.push([dim, ...indiArr]);
      });
      return newData;
    },
    /**
     * 获取图表数据——存在系列时
     * @param {*} newResData 调整后的接口数据
     * @param {*} dimensionMember 系列的所有成员
     * @param {*} indicator 图表的指标
     * @param {*} chartType 图表类型
     */
    getChartDataBySeries(newResData, dimensionMember, { indicator }, chartType) {
      let legend = [];
      let xAxis = [];
      let series = [];
      dimensionMember.forEach(ele => {
        series.push({
          name: ele,
          type: chartType,
          data: [],
          showType: indicator[0].showType
        });
        legend.push({
          name: ele,
          originName: ele,
          parentName: ele
        });
      });

      newResData.forEach(item => {
        xAxis.push(item[0]);
        for (let i = 1; i <= dimensionMember.length; i++) {
          series[i - 1].data.push(item[i] !== null ? item[i] : 0);
        }
      });

      return {
        legend,
        xAxis,
        series
      };
    }
  }
};
