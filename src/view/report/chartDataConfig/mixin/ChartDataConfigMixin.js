import { mapGetters } from "vuex";

export default {
  inject: ["recordSnapshot"],
  computed: {
    ...mapGetters(["currentSelectChart", "currentSelectChartData", "currentSelectConfigData", "currentSelectIndicators", "currentSelectDimensions"]),
    currentSelectChartConfigData() {
      return this.currentSelectConfigData;
    },
    currentSelectChartIndicator() {
      return this.currentSelectIndicators;
    },
    currentSelectChartDimension() {
      return this.currentSelectDimensions;
    }
  },
  methods: {
    refreshCurrentSelectChartConfigData(property, value, needRecordSnapshot = true) {
      this.$set(this.currentSelectChart.data.configData, property, value);
      if (needRecordSnapshot) {
        this.recordSnapshot();
      }
    }
  }
};
