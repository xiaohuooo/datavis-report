const isSupportDimensionCharts = [
  'SimpleTable',
  'SimpleBar',
  'CustomTable',
  'GroupTable',
  'SimpleLine',
  'SimplePie',
  'SimpleScatter',
  'MixLineBar',
  'DoublePie',
  'IndexBoard',
  'CustomTableTest',
  'SimpleMap',
  'BasicRadar',
  'SimpleBubble',
]

// 维度必选的图表
const REQUIRED_DIMENSION_CHARTS = [
  "SimpleBar",
  "SimpleLine",
  "SimplePie",
  "SimpleScatter",
  "SimpleBubble",
  "BasicRadar",
  "MixLineBar",
  "DoublePie",
  "SimpleMap"
];
// 维度可选的图表
const OPTIONAL_DIMENSION_CHARTS = ["CustomTable", "IndexBoard"];

const isSupportIndicatorCharts = [
  'SimpleTable',
  'SimpleBar',
  'CustomTable',
  'GroupTable',
  'SimpleLine',
  'SimplePie',
  'SimpleScatter',
  'SimpleTextarea',
  'MixLineBar',
  'SimpleGauge',
  'Indicator',
  'DoublePie',
  'IndexBoard',
  'CustomTableTest',
  'SimpleMap',
  'BasicRadar',
  'SimpleBubble',
]

// 支持单维度
const isSupportSingleDimensionCharts = ['SimpleBar', 'SimpleLine', 'SimplePie', 'SimpleScatter', 'MixLineBar', 'IndexBoard','SimpleMap', 'BasicRadar', 'SimpleBubble']
// 支持单指标
const isSupportSingleIndicatorCharts = ['SimpleTextarea', 'SimplePie', 'DoublePie', 'Indicator', 'SimpleGauge','SimpleMap']
// 支持双指标
const isSupportDoubleIndicatorCharts = ['SimpleScatter']
// 支持三个指标
const isSupportThreeIndicatorCharts = ['SimpleBubble']
// 支持五个指标
const isSupportFiveIndicatorCharts = ['BasicRadar']
// 支持系列
const supportSeriesCharts = ['SimpleLine', 'SimpleBar']

export {
  isSupportDimensionCharts,
  REQUIRED_DIMENSION_CHARTS,
  OPTIONAL_DIMENSION_CHARTS,
  isSupportIndicatorCharts,
  isSupportSingleDimensionCharts,
  isSupportSingleIndicatorCharts,
  isSupportDoubleIndicatorCharts,
  isSupportThreeIndicatorCharts,
  isSupportFiveIndicatorCharts,
  supportSeriesCharts,
}
