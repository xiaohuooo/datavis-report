import { deepCopy } from './utils/assist.js';

const convertColumnOrder = (columns, fixedType) => {
    let list = [];
    let other = [];
    columns.forEach((col) => {
        if (col.fixed && col.fixed === fixedType) {
            list.push(col);
        } else {
            other.push(col);
        }
    });
    return list.concat(other);
};

export {convertColumnOrder};

// set forTableHead to true when convertToRows, false in normal cases like table.vue
const getAllColumns = (cols, forTableHead = false) => {
    const columns = deepCopy(cols);
    const result = [];
    columns.forEach((column) => {
        if (column.children) {
            if (forTableHead) result.push(column);
            result.push.apply(result, getAllColumns(column.children, forTableHead));
        } else {
            result.push(column);
        }
    });
    return result;
};

export {getAllColumns};

const convertToRows = (columns, fixedType = false) => {
  // const originColumns = fixedType ? fixedType === 'left' ? deepCopy(convertColumnOrder(columns, 'left')) : deepCopy(convertColumnOrder(columns, 'right')) : deepCopy(columns);
  // let maxLevel = 1;
  // const traverse = (column, parent) => {
  //     if (parent) {
  //         column.level = parent.level + 1;
  //         if (maxLevel < column.level) {
  //             maxLevel = column.level;
  //         }
  //     }
  //     if (column.children) {
  //         let colSpan = 0;
  //         column.children.forEach((subColumn) => {
  //             traverse(subColumn, column);
  //             colSpan += subColumn.colSpan;
  //         });
  //         column.colSpan = colSpan;
  //     } else {
  //         column.colSpan = 1;
  //     }
  // };

  // originColumns.forEach((column) => {
  //     column.level = 1;
  //     traverse(column);
  // });

  // const rows = [];
  // for (let i = 0; i < maxLevel; i++) {
  //     rows.push([]);
  // }

  // const allColumns = getAllColumns(originColumns, true);

  // allColumns.forEach((column) => {
  //     if (!column.children) {
  //         column.rowSpan = maxLevel - column.level + 1;
  //     } else {
  //         column.rowSpan = 1;
  //     }
  //     rows[column.level - 1].push(column);
  // });

  // return rows;
  const originColumns = fixedType ? fixedType === 'left' ? deepCopy(convertColumnOrder(columns, 'left')) : deepCopy(convertColumnOrder(columns, 'right')) : deepCopy(columns);
  let maxLevel = 0; // 几层
  let maxDeep = 1; // 几列，树的深度
  const traverse = (column, parent) => {
      if (parent) {
        // 第一个子节点和父级在同一个层级上
        if(parent.children[0].__id === column.__id) {
          column.level = parent.level;
        } else {
          maxLevel += 1
          column.level = maxLevel
        }
        column.deep = parent.deep + 1;
        if (maxDeep < column.deep) {
          maxDeep = column.deep;
        }
      }
      if (column.children) {
        let rowSpan = 0;
        column.children.forEach((subColumn) => {
            traverse(subColumn, column);
            rowSpan += subColumn.rowSpan;
        });
        column.rowSpan = rowSpan;
      } else {
        column.rowSpan = 1;
      }
  };

  originColumns.forEach((column) => {
    maxLevel += 1;
    column.level = maxLevel;
    column.deep = 1;
    traverse(column);
  });

  const rows = [];
  for (let i = 0; i < maxLevel; i++) {
    rows.push([]);
  }

  const allColumns = getAllColumns(originColumns, true);

  allColumns.forEach((column) => {
      if (!column.children) {
          column.colSpan = maxDeep - column.deep + 1;
      } else {
          column.colSpan = 1;
      }
      rows[column.level - 1].push(column);
  });

  return rows;
};

export {convertToRows};

const getRandomStr = function (len = 32) {
    const $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    const maxPos = $chars.length;
    let str = '';
    for (let i = 0; i < len; i++) {
        str += $chars.charAt(Math.floor(Math.random() * maxPos));
    }
    return str;
};

export {getRandomStr};