// 维度、指标的type
export const TYPE = {
  DIMENSION: 1,
  BASIC_INDICATOR: 2,
  COMPLEX_INDICATOR: 3,
}

// 指标的血缘关系图配置
export const BLOOD_GRAPH_OPTIONS = {
  disableZoom: true,
  moveToCenterWhenResize: false,
  'layouts': [
    {
      'label': '中心',
      'layoutName': 'tree',
      'layoutClassName': 'seeks-layout-center',
      'defaultNodeShape': 1,
      'defaultLineShape': 1,
      'from': 'left',
      'max_per_width': '300',
      'min_per_width': '280',
      'min_per_height': '42',
      'max_per_height': '100',
      'defaultJunctionPoint': 'lr',
    },
  ],
  'defaultLineMarker': {
    'markerWidth': 8,
    'markerHeight': 8,
    'refX': 6,
    'refY': 6,
    'data': 'M2,2 L10,6 L2,10 L6,6 L2,2',
  },
  'defaultExpandHolderPosition': 'left',
  'defaultNodeShape': 1,
  // allowShowMiniToolBar: false,
  'defaultNodeWidth': '180',
  'defaultLineShape': 4,
  defaultLineWidth: 1,
  'defaultJunctionPoint': 'lr',
  'defaultNodeBorderWidth': 0,
  'defaultLineColor': '#cccccc',
}

// 基础指标form表单基础配置
export const BASIC_INDICATOR_FORM_CONFIG = {
  groupId: '',
  name: '',
  showType: '',
  type: 1,
  description: '',
  businessModelId: '',
  businessModelPropertyId: '', // 技术口径（A)id
  businessModelPropertyIdB: '', // 技术口径B的值
  businessModelPropertyIdManualEntry: '', // 技术口径-手动录入的值
  calculateType: '', // 汇总方式
  openDate: '0',
  dateStrObj: {
    showAble: false,
    grandTotal: 1,
    grandTotalType: 1,
    contentAble: '1',
    content: '',
    startDate: [],
    endDate: [],
  },
  showThreshold: false, // 阈值设置开关
  showConditionGroups: false, // 过滤条件开关
  thresholds: [
    {
      adverseDirection: null,
      name: null,
      valueType: null,
      value: null,
    },
  ],
  conditionGroups: [
    {
      id: null,
      type: 1, // 1:指标  2:报告
      name: null,
      relationId: null, // 关系id：比如指标id,报告id
      sort: null,
      conditions: [
        // 过滤条件数组
        {
          sourceId: null, // 业务模型属性id
          conditionType: null, // 1：大于  2：小于  3：不等于  4：等于  5：大于等于  6：小于等于  7：between  8：包含
          value: null, // between 以,分割
          selectedOpt: 1,
          iptVal: '',
          conditionGroupId: null,
          sort: null,
        },
      ],
    },
  ], // 过滤条件数据
  conditionGroupsSelectIndex: { // 过滤条件下拉框下标
    groupIndex: 0,
    conditionIndex: 0,
  },
  labels: [],
}
