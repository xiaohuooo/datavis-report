/************************** 即席分析图表配置 **************************/
import {formatData} from '../../formatData'

// 环形图
export const getCircularGraphOption = (data, indicator) => {
  let titleText = ''
  let total = 0
  let titleFontSize = 28
  let indicatorName = ''
  let maxTotal = 0

  if (indicator) {
    indicatorName = indicator.abbrName || indicator.title
    titleText = indicatorName + '（元）'

    if (indicator.total !== undefined) {
      // 指标总计超过10位数省略
      maxTotal = indicator.total.toString().slice(0, 10)
      titleFontSize = maxTotal.length <= 4 ? 28 : 28 - (maxTotal.length - 4) * 2
      total = indicator.total.toString().length > 10 ? maxTotal + '...' : indicator.total.toString()
    }
  }

  let chartWidth = document.querySelector('.circular-graph-chart').clientWidth

  return {
    title: [
      {
        show: true,
        text: titleText,
        left: 'center',
        top: 95,
        textStyle: {
          color: 'rgba(0, 0, 0, 0.45)',
          fontSize: 12,
        },
      },
      {
        show: true,
        text: total,
        left: 'center',
        top: 115,
        textStyle: {
          color: 'rgba(0, 0, 0, 0.85)',
          fontSize: titleFontSize,
        },
      },
    ],
    tooltip: {
      trigger: 'item',
      formatter: (params) => {
        let str = ''
        let numberFormat = indicator.numberFormat
        str += `${params.seriesName}<br/>`
        str += `${params.name}&nbsp;&nbsp;&nbsp;`
        // 前缀后缀添加
        if(numberFormat.prefix){
          str += `${numberFormat.prefix}` 
        }
        str += `${formatData(params.data.value, numberFormat)}`
        if (numberFormat.unit && numberFormat.unit !== '原始值') {
          str += numberFormat.unit
        }
        // 前缀和后缀添加
        if(numberFormat.suffix){
          str += `${numberFormat.suffix}` 
        }
        str += `<br/>`
        str += `占比&nbsp;&nbsp;&nbsp;${params.data.percent}`
        return str
      },
    },
    legend: {
      orient: 'vertical',
      bottom: 0,
      left: 'center',
      icon: 'circle',
      formatter: (name) => {
        let target
        for (let i = 0; i < data.length; i++) {
          if (data[i].name === name) {
            target = data[i].percent
          }
        }
        return '{a|' + name + '}{b|' + target + '}'
      },
      textStyle: {
        rich: {
          a: {
            fontSize: 12,
            color: 'rgba(0, 0, 0, 0.7)',
            width: chartWidth - 180,
          },
          b: {
            fontSize: 12,
            color: 'rgba(0, 0, 0, 0.7)',
            fontWeight: 'bold',
            width: 50,
          },
        },
      },
    },
    grid: {
      left: '30%',
      right: '30%',
      bottom: '12%',
      top: '4%',
      // containLabel: true,
    },
    series: [
      {
        name: indicatorName,
        type: 'pie',
        center: ['50%', 120],
        radius: [55, 100],
        avoidLabelOverlap: false,
        itemStyle: {
          borderWidth: 1,
          borderColor: '#fff',
        },
        label: {
          show: false,
          position: 'center',
        },
        labelLine: {
          show: false,
        },
        data: data,
      },
    ],
  }
}

// 条形图
export const getBarHorizontalOption = (yAxisData, series) => {
  return {
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow',
      },
      formatter: tooltipFormatter(series),
    },
    legend: {
      icon: 'rect',
      itemHeight: 10,
      itemWidth: 10,
      bottom: 10,
      right: '4%',
    },
    grid: {
      left: '3%',
      right: '6%',
      bottom: '12%',
      top: '4%',
      containLabel: true,
    },
    xAxis: {
      type: 'value',
      boundaryGap: [0, 0.01],
      axisLine: {
        show: true,
        lineStyle: {
          color: '#EAEAEA',
        },
      },
      axisLabel: {
        textStyle: {
          color: '#3C3C3C',
        },
        rotate: 45,
      },
      axisTick: {
        show: true,
      },
      splitLine: {
        show: false,
      },
    },
    yAxis: {
      type: 'category',
      data: yAxisData,
      axisTick: {
        show: false,
      },
      axisLine: {
        show: true,
        lineStyle: {
          color: '#EAEAEA',
        },
      },
      axisLabel: {
        margin: 60,
        textStyle: {
          align: 'left',
          color: '#3C3C3C',
        },
        formatter: (value) => {
          if (value.length > 6) {
            value = value.slice(0, 6) + '...'
          }
          if (value.length > 4) {
            value = value.slice(0, 4) + '\n' + value.slice(4)
          }
          return value
        },
      },
    },
    series: series,
  }
}

// 柱状图
export const getBarVerticalOption = (xAxisData, series) => {
  return {
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow',
      },
      formatter: tooltipFormatter(series),
    },
    legend: {
      icon: 'rect',
      itemHeight: 10,
      itemWidth: 10,
      bottom: 10,
      right: '4%',
    },
    grid: {
      left: '3%',
      right: '6%',
      bottom: '12%',
      top: '4%',
      containLabel: true,
    },
    xAxis: {
      type: 'category',
      data: xAxisData,
      axisLine: {
        show: true,
        lineStyle: {
          color: '#EAEAEA',
        },
      },
      axisLabel: {
        textStyle: {
          color: '#3C3C3C',
        },
        formatter: aAxisLabelFormatter,
      },
      axisTick: {
        show: true,
      },
      splitLine: {
        show: false,
      },
    },
    yAxis: {
      type: 'value',
      axisTick: {
        show: false,
      },
      axisLine: {
        show: true,
        lineStyle: {
          color: '#EAEAEA',
        },
      },
      axisLabel: {
        textStyle: {
          color: '#3C3C3C',
        },
      },
    },
    series: series,
  }
}

// 折线图
export const getLineChartOption = (xAxisData, series) => {
  return {
    tooltip: {
      trigger: 'axis',
      formatter: tooltipFormatter(series),
    },
    legend: {
      icon: 'circle',
      itemHeight: 10,
      itemWidth: 10,
      bottom: 10,
      right: '4%',
    },
    grid: {
      left: '3%',
      right: '6%',
      bottom: '12%',
      top: '4%',
      containLabel: true,
    },
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: xAxisData,
      axisTick: {
        show: false,
      },
      axisLine: {
        show: true,
        lineStyle: {
          color: '#EAEAEA',
        },
      },
      axisLabel: {
        textStyle: {
          color: '#3C3C3C',
        },
        formatter: aAxisLabelFormatter,
      },
    },
    yAxis: {
      type: 'value',
      axisLine: {
        show: true,
        lineStyle: {
          color: '#EAEAEA',
        },
      },
      axisLabel: {
        textStyle: {
          color: '#3C3C3C',
        },
      },
    },
    series: series,
  }
}

// 组合图
export const getCombinationChartOption = (xAxisData, series) => {
  return {
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'cross',
        crossStyle: {
          color: '#999',
        },
      },
      formatter: tooltipFormatter(series),
    },
    legend: {
      icon: 'circle',
      itemHeight: 10,
      itemWidth: 10,
      bottom: 10,
      right: '4%',
    },
    grid: {
      left: '3%',
      right: '6%',
      bottom: '12%',
      top: '4%',
      containLabel: true,
    },
    xAxis: {
      type: 'category',
      data: xAxisData,
      axisTick: {
        show: false,
      },
      axisLine: {
        show: true,
        lineStyle: {
          color: '#EAEAEA',
        },
      },
      axisLabel: {
        textStyle: {
          color: '#3C3C3C',
        },
        formatter: aAxisLabelFormatter,
      },
    },
    yAxis: {
      type: 'value',
      axisLine: {
        show: true,
        lineStyle: {
          color: '#EAEAEA',
        },
      },
      axisLabel: {
        textStyle: {
          color: '#3C3C3C',
        },
      },
    },
    series: series,
  }
}

// 对比图
export const getComparisonChartOption = (yAxisData, series) => {
  return {
    tooltip: {
      trigger: 'axis',
      axisPointer: { // 坐标轴指示器，坐标轴触发有效
        type: 'shadow', // 默认为直线，可选为：'line' | 'shadow'
      },
      formatter: tooltipFormatter(series, 'abs'),
    },
    legend: {
      icon: 'circle',
      itemHeight: 10,
      itemWidth: 10,
      bottom: 10,
      right: '4%',
    },
    grid: {
      left: '3%',
      right: '6%',
      bottom: '12%',
      top: '4%',
      containLabel: true,
    },
    xAxis: [
      {
        type: 'value',
        axisLine: {
          show: true,
          lineStyle: {
            color: '#EAEAEA',
          },
        },
        axisLabel: {
          rotate: 45,
          textStyle: {
            color: '#3C3C3C',
          },
          // 负数取绝对值变正数（x轴本来数据是正负才能分开两边展示的，所以我们只是把负数处理为正数在视觉上显示）
          formatter(value) {
            return Math.abs(value)
          },
        },
        splitLine: {
          show: false,
        },
      },
    ],
    yAxis: [
      {
        type: 'category',
        axisLine: {
          show: true,
          lineStyle: {
            color: '#EAEAEA',
          },
        },
        axisLabel: {
          textStyle: {
            color: '#3C3C3C',
          },
          formatter: yAxisLabelFormatter,
        },
        data: yAxisData,
      },
    ],
    color: ['#fea23f', '#8fb0f4'],
    series: series,
  }
}

const aAxisLabelFormatter = (value) => {
  // 最多6个字，多出省略，两个字一行
  let str = value.slice(0, 6)
  let strArr = []
  let wrapCount = Math.ceil(str.length / 2)
  for (let i = 0; i < wrapCount; i++) {
    strArr.push(str.slice(2 * i, 2 * (i + 1)))
  }
  str = strArr.join('\n')
  if (value.length > 6) {
    str += '\n...'
  }
  return str
}

const yAxisLabelFormatter = (value) => {
  // 最多6个字，超过6个字，加省略号
  let ellipsis = value.length > 6 ? '...' : ''
  return value.slice(0, 6) + ellipsis
}

const tooltipFormatter = (series, type) => {
  return (params) => {
    let str = params[0].name
    for (let param of params) {
      let numberFormat = series[param.seriesIndex].numberFormat
      let value = type === 'abs' ? Math.abs(param.value) : param.value
      value = formatData(value, numberFormat)
      if (numberFormat.unit && numberFormat.unit !== '原始值') {
        value += numberFormat.unit
      }
      // 前缀和后缀添加
      if(numberFormat.prefix){
        value = numberFormat.prefix + value
      }
      if(numberFormat.suffix){
        value = value + numberFormat.suffix
      }
      str += `<br/>${param.marker}${param.seriesName}&nbsp;&nbsp;&nbsp;${value}`
    }
    return str
  }
}
