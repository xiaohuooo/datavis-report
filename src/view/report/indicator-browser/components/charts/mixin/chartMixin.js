import { off, on } from '@/libs/tools'
import { getReportCompareDataAndHeaders } from '@/api/report'
import { CHART_TYPE } from '@/view/report/indicator-browser/constant'
import * as echarts from 'echarts'

export default {
  props: {
    chartType: String,
    chart: Object,
    dimensions: Array,
    indicators: Array,
    customFilter: Object,
    calendar: Object,
    initDelay: Boolean,
  },
  data() {
    return {
      dom: null,
      option: null,
      data: {},
      loaded: false,
    }
  },
  computed: {
    watchData() {
      return {
        dimensions: this.dimensions,
        indicators: this.indicators,
        dimensionConditionGroup: this.customFilter.dimensionConditionGroup,
        calendar: this.calendar,
        chart: this.chart,
      }
    },
    showChart() {
      return !!this.data.data?.length
    },
  },
  methods: {
    resize() {
      this.dom && this.dom.resize()
    },

    registerEvent() {
      this.registerResizeEvent()
    },

    registerResizeEvent() {
      off(window, 'resize', this.resize)
      on(window, 'resize', this.resize)
    },

    // 刷新图表
    async refreshChartData() {
      let dimensions = []
      let indicators = []
      let chartData = {}

      dimensions = this.dimensions.filter(item => item.id === this.chart.dimension)

      if (this.chartType == CHART_TYPE.CIRCULAR_GRAPH) {
        indicators = this.indicators.filter(item => item.key === this.chart.indicator)
      } else if (this.chartType == CHART_TYPE.COMBINATION) {
        let chartIndicators = [...this.chart.barIndicators, ...this.chart.lineIndicators]
        for (let chartIndicator of chartIndicators) {
          for (let indicator of this.indicators) {
            if (chartIndicator === indicator.key) {
              indicators.push(indicator)
              break
            }
          }
        }
      } else {
        for (let chartIndicator of this.chart.indicators) {
          for (let indicator of this.indicators) {
            if (chartIndicator === indicator.key) {
              indicators.push(indicator)
              break
            }
          }
        }
      }

      const { headers, currentData } = await this.getDataByDimensionAndIndicator(dimensions, indicators)
      if (headers) {
        // 图表自己处理返回的数据为展示所需要的数据
        chartData = { headers, data: currentData.slice(0, 10) }
      }
      this.data = chartData
      this.loaded = true
      if (this.showChart) {
        this.$nextTick(() => {
          if (!this.dom) {
            this.dom = echarts.init(this.$refs.dom)
            this.registerEvent()
          }
          this.refreshChart(this.data)
          this.dom.setOption(this.option, true)
          this.dom.resize()
        })
      }
    },

    // 根据维度、指标获取数据
    async getDataByDimensionAndIndicator(dimensions, indicators) {
      let data = {}
      let dimensionIds = []
      let indicatorVos = []
      let { startDate, endDate } = this.calendar

      if (!dimensions.length || !indicators.length) {
        return data
      }

      dimensionIds = dimensions.map(item => item.id)
      indicatorVos = indicators.map(item => ({
        id: item.id,
        type: item.type,
        variables: {},
        newCalculateType: item.summaryMethod || null,
        resultCondition: this.customFilter.indicatorCondition[item.id] || null,
      }))

      let params = {
        dimensionIds,
        indicatorVos,
        date: { startDate, endDate },
        dimensionConditionGroup: this.customFilter.dimensionConditionGroup,
      }
      const { data: resData } = await getReportCompareDataAndHeaders(params)

      if (resData?.state === 'success') {
        data = resData.data
      } else {
        this.$Message.warning(resData?.message || '获取即席分析数据失败')
      }

      return data
    },
  },
  watch: {
    watchData: {
      async handler() {
        await this.refreshChartData()
      },
      deep: true,
      immediate: true,
    },
  },
  beforeDestroy() {
    off(window, 'resize', this.resize)
  },
}
