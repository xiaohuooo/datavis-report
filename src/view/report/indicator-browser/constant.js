import {deepCopy} from '@/utils/common'

export const filterList = Object.freeze([
  {
    label: '大于',
    value: 1,
  },
  {
    label: '小于',
    value: 2,
  },
  {
    label: '不等于',
    value: 3,
  },
  {
    label: '等于',
    value: 4,
  },
  {
    label: '大于等于',
    value: 5,
  },
  {
    label: '小于等于',
    value: 6,
  },
  {
    label: 'between',
    value: 7,
  },
  {
    label: '包含',
    value: 8,
  },
  {
    label: '不包含',
    value: 9,
  },
])
export const calculateSymbol = [
  {
    key: '1',
    value: '大于（>）',
  },
  {
    key: '2',
    value: '小于（<）',
  },
  {
    key: '3',
    value: '不等于（!=或<>）',
  },
  {
    key: '4',
    value: '等于（=）',
  },
  {
    key: '5',
    value: '大于等于（>=）',
  },
  {
    key: '6',
    value: '小于等于（<=）',
  },
  {
    key: '7',
    value: '两者之间（between）',
  },
]

// 默认筛选配置
export const CUSTOM_FILTER_DEFAULT_CONFIG = [
  {
    list: [
      {
        sourceId: '', // 维度、指标id
        sourceType: '', // 区别维度、指标
        conditionType: '',
        value: null,
        value2: null, // 只有数值区间才会用到
        valueType: 'text', // 区别值类型 selector-选择器  text-文本输入框  number-数值输入框  numberBetween-数值区间输入框
        dimensionMember: [], // 维成员，只有维度有
      },
    ],
  },
]

// 维度的多级下拉菜单选项
export const DIMENSION_MULTIPLE_DROPDOWN_MENU = [
  {name: '重命名', selected: false},
  {name: '删除', selected: false},
]

// 指标的多级下拉菜单选项
export const INDICATOR_MULTIPLE_DROPDOWN_MENU = [
  {
    name: '汇总方式',
    selected: false,
    children: [
      {name: '无', value: null, selected: false},
      {name: '求和', value: 1, selected: false},
      {name: '平均数', value: 2, selected: false},
      {name: '计数', value: 5, selected: false},
      {name: '去重计数', value: 13, selected: false},
      // {name: '方差', selected: false},
      // {name: '标准差', selected: false},
    ],
  },
  {
    name: '高级计算',
    selected: false,
    children: [
      {
        name: '环比',
        selected: false,
        children: [
          {name: '环比值', value: '环比值', selected: false},
          {name: '环比变化率', value: '环比变化率', selected: false},
        ],
      },
      {
        name: '同比',
        selected: false,
        children: [
          {name: '同比值', value: '年同比值', selected: false},
          {name: '同比变化率', value: '年同比变化率', selected: false},
          {name: '同比值', value: '月同比值', selected: false},
          {name: '同比变化率', value: '月同比变化率', selected: false},
        ],
      },
      {name: '占比', value: '占比', selected: false, disabled: true},
      {
        name: '累计',
        selected: false,
        children: [
          {name: '全部累计', value: '全部累计', selected: false},
          {name: '组内累计', value: '组内累计', selected: false},
        ],
      },
      // {
      //   name: '方差',
      //   selected: false,
      //   children: [
      //     {name: '总体方差', selected: false},
      //     {name: '样本方差', selected: false},
      //   ],
      // },
      // {
      //   name: '标准差',
      //   selected: false,
      //   children: [
      //     {name: '总体标准差', selected: false},
      //     {name: '样本标准差', selected: false},
      //   ],
      // },
    ],
  },
  {
    name: '数值格式',
    selected: false,
    children: [
      {name: '整数', value: '数值', selected: false},
      {name: '百分比', value: '百分比', selected: false},
      {name: '自定义格式', value: '自定义格式', selected: false},
    ],
  },
  {name: '重命名', selected: false},
  {name: '复制', selected: false},
  {name: '删除', selected: false},
]

// 表格排序下拉菜单
export const TABLE_SORT_DROPDOWN = [
  {name: '不排序', selected: true},
  {name: '组内升序', selected: false},
  {name: '组内降序', selected: false},
  {name: '全局升序', selected: false},
  {name: '全局降序', selected: false},
]

// 纯指标 表格排序下拉菜单
export const TABLE_SORT_DROPDOWN_OF_PURE_INDICATOR = [
  {name: '不排序', selected: true},
  {name: '全局升序', selected: false},
  {name: '全局降序', selected: false},
]

// 自定义格式默认配置
export const CUSTOM_FORMAT_DEFAULT_CONFIG = {
  show: false,
  format: '数值',
  unit: '原始值',
  precision: 2,
  prefix: '',
  suffix: '',
  useThousandsSeparator: false,
}

// 指标默认配置
export const INDICATOR_DEFAULT_CONFIG = {
  type: 1,
  id: null,
  title: '',
  abbrName: '',
  key: '',
  multipleDropdownMenu: deepCopy(INDICATOR_MULTIPLE_DROPDOWN_MENU),
  sortDropdown: deepCopy(TABLE_SORT_DROPDOWN),
  summaryMethod: '',
  numberFormat: {},
  ringCompareValue: false,
  ringCompareChangeRate: false,
  yoyValue: false,
  yoyChangeRate: false,
  momValue: false,
  momChangeRate: false,
  ratio: false,
}

// 图表类型
export const CHART_TYPE = {
  TABLE: 'table',
  BAR_VERTICAL: 'barVertical',
  BAR_HORIZONTAL: 'barHorizontal',
  COMPARISON: 'comparison',
  LINE: 'line',
  CIRCULAR_GRAPH: 'circularGraph',
  COMBINATION: 'combination',
}
