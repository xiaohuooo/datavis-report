/*************************只用于即席分析自定义表格的format方法，防止与创建报告那边的format方法共用，导致后期难以维护*************************/

import {NULL_DATA_NUMBER_SHOW, NULL_DATA_PERCENTAGE_SHOW, ZERO_PRE_COMPARE_DATA_SHOW} from '@/config/ReportConstants'
import BigNumber from 'bignumber.js'

/**
 * 正常数据格式化
 * @param data 数据
 * @param format 格式：数值、百分比
 * @param unit 单位：原始值、千、万、亿
 * @param precision 精度
 * @param useThousandsSeparator 是否使用千位分隔符
 */
export const formatData = (data, {format = '数值', unit = '原始值', precision = 2, useThousandsSeparator = false}) => {
  if (isNullOrUndefined(data)) {
    return format === '数值' ? NULL_DATA_NUMBER_SHOW : NULL_DATA_PERCENTAGE_SHOW
  }

  if (!(data instanceof BigNumber)) {
    data = new BigNumber(data)
  }

  if (format === '数值') {
    if (unit === '千') {
      data = data.div(1e3)
    } else if (unit === '万') {
      data = data.div(1e4)
    } else if (unit === '亿') {
      data = data.div(1e8)
    }
    return useThousandsSeparator ? data.toFormat(precision) : data.toFixed(precision)
  } else if (format === '百分比') {
    data = data.multipliedBy(100)
    return useThousandsSeparator ? data.toFormat(precision) + '%' : data.toFixed(precision) + '%'
  }
}

/**
 * 环比值/同比值 数据格式化
 * @param currentData 本周期数据
 * @param previousData 上周期数据
 * @param format 格式：数值、百分比
 * @param unit 单位：原始值、千、万、亿
 * @param precision 精度
 * @param useThousandsSeparator 是否使用千位分隔符
 */
export const formatCompareValueData = (currentData, previousData ,{format = '数值', unit = '原始值', precision = 2, useThousandsSeparator = false}) => {
  if (isNullOrUndefined({currentData, previousData})) {
    return ZERO_PRE_COMPARE_DATA_SHOW
  }

  if (!(currentData instanceof BigNumber)) {
    currentData = new BigNumber(currentData)
  }

  if (!(previousData instanceof BigNumber)) {
    previousData = new BigNumber(previousData)
  }

  if (format === '数值') {
    let diff = currentData.minus(previousData)
    diff = formatDataByUnit(diff, unit)
    return useThousandsSeparator ? diff.toFormat(precision) : diff.toFixed(precision)
  } else if (format === '百分比') {
    let diff = currentData.minus(previousData).multipliedBy(100)
    diff = formatDataByUnit(diff, unit)
    return useThousandsSeparator ? diff.toFormat(precision) + '%' : diff.toFixed(precision) + '%'
  }
}

/**
 * 环比变化率/同比变化率 数据格式化
 * @param currentData 本周期数据
 * @param previousData 上周期数据
 * @param precision 精度
 */
export const formatCompareChangeRateData = (currentData, previousData, {precision = 2}) => {
  if (isNullOrUndefined({currentData, previousData}) || previousData === 0) {
    return ZERO_PRE_COMPARE_DATA_SHOW
  }

  if (!(currentData instanceof BigNumber)) {
    currentData = new BigNumber(currentData)
  }

  if (!(previousData instanceof BigNumber)) {
    previousData = new BigNumber(previousData)
  }

  return currentData.minus(previousData).multipliedBy(100).div(previousData).toFixed(precision) + '%'
}

/**
 * 占比 数据格式化
 * @param currentData 本周期数据
 * @param totalData 本周期全部数据之和
 */
export const formatProportionData = (currentData, totalData, {precision = 2}) => {
  if (isNullOrUndefined({currentData, totalData})) {
    return NULL_DATA_PERCENTAGE_SHOW
  }

  if (!(currentData instanceof BigNumber)) {
    currentData = new BigNumber(currentData)
  }

  if (!(totalData instanceof BigNumber)) {
    totalData = new BigNumber(totalData)
  }

  return currentData.div(totalData).multipliedBy(100).toFixed(precision) + '%'
}

/**
 * 根据单位处理数据
 * @param data 数据
 * @param unit 单位
 */
const formatDataByUnit = (data, unit) => {
  if (unit === '千') {
    data = data.div(10e3)
  } else if (unit === '万') {
    data = data.div(10e4)
  } else if (unit === '亿') {
    data = data.div(10e8)
  }
  return data
}

/**
 * 数据是否是null或undefined
 * @param data
 */
const isNullOrUndefined = (data) => {
  if (Object.prototype.toString.call(data) === '[object Object]') {
    for (let key in data) {
      if (isNullOrUndefined(data[key])) {
        return true
      }
    }
  } else if (Object.prototype.toString.call(data) === '[object Array]') {
    for (let item of data) {
      if (item === null || item === undefined) {
        return true
      }
    }
  } else {
    if (data === null || data === undefined) {
      return true
    }
  }
  return false
}
