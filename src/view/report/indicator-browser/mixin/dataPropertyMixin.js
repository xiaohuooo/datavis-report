import {matchingRelation} from "@/api/realTimeAnalysis";
import { deepCopy } from '@/utils/common'
import {queryDimensionAndIndicatorByLabel} from "@/api/label";
import _ from "lodash";
import {mapActions} from "vuex";
export default {
  methods: {
    ...mapActions('earlyWarningTarget', ['setAllIndicator', 'setAllIndicatorGroup']),
    async refreshTreeData(dimensions, indicators, currentNode) {
      if (dimensions.length || indicators.length) {
        const { data } = await matchingRelation({ dimensions, indicators })
        if (data?.state === 'success') {
          const dimensions = data.data.dimensions
          const indicators = data.data.indicators
          const newBigTreeNodes = this.matchRelatedPropertyTree(dimensions, indicators, this.bigTreeNodesBak, currentNode)
          const ancestorNodes = this.findAncestorNodesByPid(newBigTreeNodes, this.bigTreeNodesBak)
          this.bigTreeNodes = Array.from(new Set(newBigTreeNodes.concat(ancestorNodes)))
          this.bigTreeNodesBakF = deepCopy(this.bigTreeNodes) // 过滤后的备份数据
        } else {
          this.$Message.error('匹配关联维度或指标失败')
        }
      } else {
        this.bigTreeNodesBakF = this.bigTreeNodes = deepCopy(this.bigTreeNodesBak)
      }
    },

    /**
     * 根据接口返回的相关联维度指标，找出对应维度指标
     * @param dimensions
     * @param indicators
     * @param treeDataAll
     * @param currentNode
     */
    matchRelatedPropertyTree(dimensions, indicators, treeDataAll, currentNode) {
      const treeDataAllBak = deepCopy(treeDataAll)

      // 如果关联的维度和指标都为null，则显示完整的树
      if (dimensions === null && indicators === null) {
        return treeDataAllBak
      }

      // 如果关联维度为null，关联指标不为null，则显示全部维度及关联指标
      if (dimensions === null && indicators !== null) {
        const relatedDimensions = treeDataAllBak.filter(item => item.type === 99)
        const relatedIndicators = treeDataAllBak.filter(item => {
          const index = indicators.findIndex(indicator => item.id === indicator.id)
          return index > -1
        })
        return [...relatedDimensions, ...relatedIndicators]
      }

      // 如果关联指标为null，关联维度不为null，根据当前是否已选择维度判断指标是否需要过滤外部指标和静态指标，加上关联维度
      if (indicators === null && dimensions !== null) {
        let relatedIndicators = []
        if (this.currentChooseDimension.length) {
          // 选择了维度，需要过滤外部指标和静态指标
          relatedIndicators = treeDataAllBak.filter(
            item => item.type === 1 || item.type === 2 || item.type === 3
          )
        } else {
          // 返回所有指标
          relatedIndicators = treeDataAllBak.filter(
            item => item.type === 1 || item.type === 2 || item.type === 3 || item.type === 4 || item.type === 5
          )
        }
        const relatedDimensions = treeDataAllBak.filter(item => {
          const index = dimensions.findIndex(dimensionId => item.id === dimensionId)
          return index > -1
        })
        return [...relatedDimensions, ...relatedIndicators]
      }

      // 如果关联维度不为null，关联指标也不为null，则显示关联维度和关联指标
      if (dimensions !== null && indicators !== null) {
        const relatedDimensions = treeDataAllBak.filter(item => {
          const index = dimensions.findIndex(dimensionId => item.id === dimensionId)
          return index > -1
        })
        const relatedIndicators = treeDataAllBak.filter(item => {
          const index = indicators.findIndex(indicator => item.id === indicator.id)
          return index > -1
        })
        return [...relatedDimensions, ...relatedIndicators]
      }

      return []
    },
    /**
     *
     * @param treeDataNew
     * @param treeDataBak
     * @returns {[]}
     */
    findAncestorNodesByPid(treeDataNew, treeDataBak) {
      let ancestorNodes = []
      treeDataNew.forEach(item => {
        const parentNodes = this.getParentNodes(treeDataBak, item.id)
        ancestorNodes = ancestorNodes.concat(parentNodes)
      })
      return ancestorNodes
    },
    // 获取给定节点的所有祖先节点，即从该节点的父节点开始一直到根节点为止的所有节点
    getParentNodes(nodes, id) {
      const parentNodes = [];
      let currentNode = nodes.find(node => node.id == id);
      while (currentNode && currentNode.pid) {
        currentNode = nodes.find(node => node.id == currentNode.pid);
        if (currentNode) {
          parentNodes.unshift(currentNode);
        }
      }
      return parentNodes;
    },
    /**
     * 生成数据资产树
     */
    async generateTree() {
      const groups = await this.getGroups()
      const allDimensions = await this.getAllDimensions()
      const allIndicators = await this.getAllIndicators()
      this.setIndicators(deepCopy(allIndicators))

      const allIndicatorGroupData = [...groups, ...allIndicators]
      let root = [
        {
          id: -1,
          title: '指标组',
          expand: true,
          treeType: 'group',
          deep: 0,
        },
      ]
      const indicatorTree = this.generateTreeNode(root, allIndicatorGroupData)
      this.allTreeData = deepCopy(indicatorTree) // 完整的指标树
      this.setIndicatorTree(indicatorTree)

      // 巨树
      // 目录节点
      const groupNodes = groups.map(item => {
        return {
          id: item.id,
          name: item.name,
          pid: item.parentId,
          iconClose: require('@/assets/images/system/treePlusForManage.svg'),
          iconOpen: require('@/assets/images/system/treeMinusForManage.svg'),
        }
      })
      this.setAllIndicatorGroup(groupNodes)
      // 维度节点
      const allDimensionNodes = allDimensions.map(item => {
        return {
          ...item,
          pid: item.groupId,
          icon: require('@/assets/icons/dimensionIcon/dimIcon.svg'),
        }
      })
      // 指标节点
      const allIndicatorNodes = allIndicators.map(item => {
        return {
          ...item,
          pid: item.groupId,
          icon: this.getIndicatorIcon(item.type),
        }
      })
      this.setAllIndicator(allIndicatorNodes)
      const rootNode = [{
        id: -1,
        name: '数据资产',
        iconClose: require('@/assets/images/system/treePlusForManage.svg'),
        iconOpen: require('@/assets/images/system/treeMinusForManage.svg'),
      }]
      this.bigTreeNodes = [...rootNode, ...groupNodes, ...allDimensionNodes, ...allIndicatorNodes]
      this.bigTreeNodesBak = deepCopy(this.bigTreeNodes)
    },
    getIndicatorIcon(type) {
      let icon = null
      if (type === 1) { // 原子
        icon = require('@/assets/icons/indicatorIcon/icon_basic_indicator.svg')
      } else if (type === 2) { // 复合
        icon = require('@/assets/icons/indicatorIcon/icon_composite_indicator.svg')
      } else if (type === 3) { // 派生
        icon = require('@/assets/icons/indicatorIcon/icon_derivative_indicator.svg')
      } else if (type === 4) { // 外部
        icon = require('@/assets/icons/indicatorIcon/icon_out_indicator.svg')
      } else if (type === 5) { // 静态
        icon = require('@/assets/icons/indicatorIcon/icon_static_indicator.svg')
      }
      return icon
    },
    async onSearchDimensionOrIndicator(event) {
      const value = event.target.value
      const treeDataBak = this.isSelectDimOrInd ? this.bigTreeNodesBakF : this.bigTreeNodesBak
      if (value) {
        const params = {
          name: value,
          type: 1,
        }
        const res = await queryDimensionAndIndicatorByLabel(params)
        if (res.data.state === 'success') {
          const labelDimensions = res.data.data.map(item => item.id)
          const filterTreeDataByName = _.filter(treeDataBak, item => item.name.indexOf(value) !== -1)
          const filterTreeDataByLabel = []
          labelDimensions.forEach(item => {
            const targetData = _.find(treeDataBak, it => it.id === item)
            filterTreeDataByLabel.push(targetData)
          })
          let treeDataNew = Array.from(new Set(filterTreeDataByName.concat(filterTreeDataByLabel)))
          // 找出各自的父节点
          let ancestorNodes = this.findAncestorNodesByPid(treeDataNew, treeDataBak)
          treeDataNew = Array.from(new Set(treeDataNew.concat(ancestorNodes)))
          this.bigTreeNodes = treeDataNew
        } else {
          this.$Message.error('根据标签搜索维度失败')
        }
      } else {
        this.bigTreeNodes = deepCopy(treeDataBak)
      }
    },
  },
}
