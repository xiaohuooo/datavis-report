export default {
  methods: {
    // 设置菜单汇总方式下选项可选状态
    setCalculateTypeStatus({ type, calculateType }, menu) {
      // 派生指标不支持高级计算
      let advancedComputing = menu.find(item => item.name === '高级计算')
      advancedComputing.disabled = type === 3
      let advancedComputingMenu = advancedComputing.children
      advancedComputingMenu.forEach(item => {
        item.disabled = type === 3
      })

      // 计数和去重计数无法与其他汇总方式切换
      calculateType = Number(calculateType)
      let calculateMenu = menu.find(item => item.name === '汇总方式').children
      // 计数--5  去重计数--13
      if (calculateType === 5 || calculateType === 13) {
        calculateMenu.forEach(item => {
          item.disabled = item.value && item.value !== 5 && item.value !== 13
        })
      } else {
        calculateMenu.forEach(item => {
          item.disabled = item.value === 5 || item.value === 13
        })
      }
    },
  },
}
