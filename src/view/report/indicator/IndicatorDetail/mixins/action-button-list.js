import { deepCopy } from '@/utils/common'
import { FILTER_INDICATOR_ITEM } from '@/view/report/indicator/constant'
import {
  addComplexIndicator,
  addIndicator,
  queryAtomIndicatorUnionById,
  queryAtomIndicatorUsedById,
  updateComplexIndicator,
  updateIndicator,
} from '@/api/indicator'

export default {
  methods: {
    /**
     * 返回：
     * 1. 从指标管理页进入
     * 2. 从指标工作台进入
     * 3. 从公共资源池进入
     */
    goBackIndicatorManagement() {
      // 从指标管理页进入
      if (this.fromRoutePath === '/manager/indicator/indicator_manage_new') {
        // 详情页面获取存储的搜索数据
        let searchObj = sessionStorage.getItem('indicatorListSearch')
        // 转存到back session中去
        sessionStorage.setItem('indicatorListSearchBack', searchObj)
      }
      if (this.fromRoutePath === '/publicResourcePool') {
        let searchObj = sessionStorage.getItem('publicResourcePoolSearch')
        sessionStorage.setItem('publicResourcePoolSearchBack', searchObj)
      }
      this.$router.back()
    },

    /**
     * 编辑指标
     */
    onEditIndicator() {
      this.setIndicatorEditable(true)
      if (!this.filterConditions.length) {
        // 没有过滤条件时，给一个默认的空条件
        this.setFilterConditions([[deepCopy(FILTER_INDICATOR_ITEM)]])
      }
    },

    /**
     * 重置指标
     */
    onResetIndicator() {
      // 公共内容部分
      const bak = this.indicatorDetailBak
      if (bak) {
        this.setIndicatorName(bak.indicatorName)
        this.setIndicatorAbbreviation(bak.indicatorAbbreviation)
        this.setIndicatorCode(bak.indicatorCode)
        this.setIndicatorAttribute(bak.indicatorAttribute)
        this.setIndicatorVersion(bak.indicatorVersion)
        this.setSyncDashboard(bak.syncDashboard)
        this.setBusinessDesc(bak.businessDesc)
        this.setBusinessDate(bak.businessDate)
        this.setAssetGroup(bak.assetGroup)
        this.setIndicatorLabels(bak.indicatorLabels)

        if (this.indicatorKind === 1) {
          this.resetBasicIndicator()
        } else if (this.indicatorKind === 2) {
          this.resetCompositeIndicator()
        } else if (this.indicatorKind === 3) {
          this.resetDerivativeIndicator()
        } else if (this.indicatorKind === 4) {
          this.resetOutIndicator()
        } else if (this.indicatorKind === 5) {
          this.resetStaticIndicator()
        }
      } else {
        this.clearIndicator()
      }
    },

    /**
     * 保存指标
     */
    onSaveIndicator() {
      if (
        !this.indicatorId ||
        (this.indicatorId && this.indicatorVersion === this.oldIndicatorVersion)
      ) {
        // 新建或者版本未修改
        this.doIndicatorSave()
      } else {
        // 如果编辑态指标版本改变，需二次确认
        this.$Modal.confirm({
          title: '确定保存吗？',
          content: '版本变更升级后，历史版本的指标口径无法编辑。',
          onOk: () => {
            setTimeout(() => {
              this.doIndicatorSave()
            }, 300)
          },
        })
      }
    },

    async doIndicatorSave() {
      // 非空校验
      const valid = await this.indicatorFormValidate()
      if (!valid) return

      // 原子
      if (this.indicatorKind === 1) await this.basicIndicatorSave()
      // 复合
      if (this.indicatorKind === 2) await this.compositeIndicatorSave()
      // 派生
      if (this.indicatorKind === 3) await this.derivativeIndicatorSave()
      // 外部
      if (this.indicatorKind === 4) await this.outIndicatorSave()
      // 静态指标
      if (this.indicatorKind === 5) await this.staticIndicatorSave()
      this.setIndicatorEditable(false)
      await this.getGroupAndIndicator()
      await this.getIndicatorDetail()
    },
  },
}
