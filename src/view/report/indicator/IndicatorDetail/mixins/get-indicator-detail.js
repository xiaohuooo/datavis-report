import { getToken } from '@/libs/util'
import { getComplexIndicatorById, getDerivativeIndicatorById, getIndicatorById, getOutIndicatorById, getStaticIndicatorById } from '@/api/indicator'

export default {
  methods: {
    /**
     * 获取指标详情
     * @param id
     */
    async getIndicatorDetail(id) {
      // 原子指标
      if (this.indicatorKind === 1) {
        await this.getBasicIndicatorDetail(id)
      }
      // 复合指标
      if (this.indicatorKind === 2) {
        await this.getCompositeIndicatorDetail(id)
      }
      // 派生指标
      if (this.indicatorKind === 3) {
        await this.getDerivativeIndicatorDetail(id)
      }
      // 外部指标
      if (this.indicatorKind === 4) {
        await this.getOutIndicatorDetail(id)
      }
      // 静态指标
      if (this.indicatorKind === 5) {
        await this.getStaticIndicatorDetail(id)
      }
    },

    /**
     * 指标详情公共内容回填
     * @param resData
     */
    doIndicatorPublicContentBackFill(resData) {
      this.setIndicatorName(resData.name)
      this.setIndicatorAbbreviation(resData.abbrName)
      this.setIndicatorVersion(resData.versionCode)
      this.oldIndicatorVersion = resData.versionCode
      // 指标详情接口返回的indicatorVersions如果为null，表示是旧版本的指标
      if (resData.indicatorVersions) this.setIndicatorVersions(resData.indicatorVersions)
      // 历史版本不可编辑
      this.showEditBtn = !!resData.indicatorVersions
      this.setIndicatorAttribute(resData.attribute)
      this.setIndicatorCode(resData.assetsCode)
      this.setSyncDashboard(resData.syncBoard === 1)
      const businessDesc = this.buildBusinessDescContent(resData.description)
      this.setBusinessDesc(businessDesc)
      this.setBusinessDate(!!resData.openDate)
      this.setAssetGroup(resData.groupId)
      this.setIndicatorLabels(resData.labels ?? [])
    },

    /**
     * 构建业务描述字段显示内容
     * @param desc
     */
    buildBusinessDescContent(desc) {
      if (!desc) {
        return '<p><br></p>'
      }
      // 匹配所有的{token}占位符，替换为真实token
      const token = getToken()
      let businessDesc = desc.replace(/\${token}/gi, token)
      return businessDesc
    },

    /**
     * 获取显示设置配置数据
     * @param configData
     */
    getShowConfigData(configData) {
      const configDataObj = JSON.parse(configData)
      return {
        showName: configDataObj.showName ?? true,
        showWarning: configDataObj.showWarning ?? true,
        showValue: configDataObj.showValue ?? true,
        showYoy: configDataObj.showYoy ?? true,
        showMom: configDataObj.showMom ?? true,
        showChart: configDataObj.showChart ?? true,
        chartIndicatorId: configDataObj.chartIndicatorId ?? '',
        dateType: configDataObj.dateType ?? 1, // 近7天
        showTarget: configDataObj.showTarget ?? true,
      }
    },

    /**
     * 编辑态数据备份，用于重置功能
     * @param resData
     * @param column
     */
    doIndicatorPublicDetailBackup(resData, column) {
      this.indicatorDetailBak = {
        indicatorName: resData.name,
        indicatorAbbreviation: resData.abbrName,
        indicatorCode: resData.assetsCode,
        indicatorAttribute: resData.attribute,
        indicatorVersion: resData.versionCode,
        syncDashboard: resData.syncBoard === 1,
        businessDesc: resData.description,
        businessDate: !!resData.openDate,
        assetGroup: resData.groupId,
        indicatorLabels: resData.labels ?? [],
      }
    },

    /**
     * 获取原子指标详情
     * @param ids
     */
    async getBasicIndicatorDetail(ids) {
      const res = await getIndicatorById(ids || this.indicatorId)
      if (res.data?.state === 'success') {
        const resData = res.data.data
        // 指标是否被使用
        this.indicatorUsed = resData.used

        /* 数据回填 */
        this.doIndicatorPublicContentBackFill(resData)
        this.setBusinessModel(resData.businessModelId)

        // 列--原技术口径
        if (!this.columns.length) return
        let columnNvl, columnNvlManualEntry
        let columnArr = resData.businessModelPropertyId.split(',')
        columnArr[0] = columnArr[0].replace(/\[|\]/g, '')
        const column = this.columns.find(
          (item) => Number(item.id) === Number(columnArr[0]),
        )
        if (!column) return
        if (columnArr.length === 1) {
          columnNvl = null
          columnNvlManualEntry = null
        } else {
          if (columnArr[1].startsWith('[')) {
            columnArr[1] = columnArr[1].replace(/\[|\]/g, '')
            columnNvl = this.columns.find(
              (item) => Number(item.id) === Number(columnArr[1]),
            )?.id
            columnNvlManualEntry = null
          } else {
            columnNvl = 'manualEntry'
            columnNvlManualEntry = columnArr[1]
          }
        }

        this.setColumn(column.id)
        this.setColumnNvl(columnNvl)
        this.setColumnNvlManualEntry(columnNvlManualEntry)
        this.setSummaryMethod(resData.calculateType)
        this.oldSummaryMethod = resData.calculateType
        this.setNumberFormatConfig(JSON.parse(resData.configData))
        this.setShowConfig(this.getShowConfigData(resData.configData))
        this.setOldShowConfig(this.getShowConfigData(resData.configData))

        // 过滤条件
        const filterConditions = resData.conditionGroups.map((groupItem) => {
          return groupItem.conditions.map((item) => {
            const valueArr = item.value.split('|')
            return {
              sourceId: item.sourceId,
              conditionType: item.conditionType,
              variableType: Number(valueArr[0]),
              variableValue: valueArr[1],
            }
          })
        })
        this.setFilterConditions(filterConditions)
        this.setShowFilterConditions(resData.showConditionGroups)

        /* 备份 */
        this.doIndicatorPublicDetailBackup(resData)
        this.indicatorDetailBak = {
          ...this.indicatorDetailBak,
          businessModel: resData.businessModelId,
          column: column.id,
          columnNvl,
          columnNvlManualEntry,
          summaryMethod: resData.calculateType,
          configData: resData.configData,
          filterConditions,
        }
      } else {
        this.$Message.error('获取原子指标详情失败')
      }
    },

    /**
     * 获取复合指标详情
     * @param ids
     */
    async getCompositeIndicatorDetail(ids) {
      const { data: res } = await getComplexIndicatorById(ids || this.indicatorId)
      if (res?.state === 'success') {
        const resData = res.data

        /* 数据回填 */
        this.doIndicatorPublicContentBackFill(resData)
        // 从baseIndicators里找到指标的type，放入expression中
        let expression = resData.expression
        if (resData.expression.indexOf('[') > -1) {
          const indicatorList = resData.baseIndicators.split(',')
          for (let indicator of indicatorList) {
            const indicatorId = indicator.split('_')[0]
            expression = expression.replaceAll(indicatorId, indicator)
          }
        }
        this.setExpression(expression)
        this.setExpressionHtml(resData.expressionHtml)
        this.setNumberFormatConfig(JSON.parse(resData.configData))
        this.setShowConfig(this.getShowConfigData(resData.configData))
        this.setOldShowConfig(this.getShowConfigData(resData.configData))

        // 过滤条件
        let conditions = {}
        for (let key in resData.conditions) {
          let condition = resData.conditions[key]
          conditions[key] = condition.map((groupItem) => {
            return groupItem.conditions.map((item) => {
              const valueArr = item.value.split('|')
              return {
                sourceId: item.sourceId,
                conditionType: item.conditionType,
                variableType: Number(valueArr[0]),
                variableValue: valueArr[1],
              }
            })
          })
        }
        this.setCompositeIndicatorFilterConditions(conditions)
        this.setShowFilterConditions(resData.showConditionGroups)

        /* 备份 */
        this.doIndicatorPublicDetailBackup(resData)
        this.indicatorDetailBak = {
          ...this.indicatorDetailBak,
          expression: resData.expression,
          expressionHtml: resData.expressionHtml,
          configData: resData.configData,
          conditions,
        }
      } else {
        this.$Message.error('获取复合指标详情失败')
      }
    },

    /**
     * 获取派生指标详情
     * @param ids
     */
    async getDerivativeIndicatorDetail(ids) {
      const { data: res } = await getDerivativeIndicatorById(
        ids || this.indicatorId,
      )
      if (res?.state === 'success') {
        const resData = res.data

        /* 数据回填 */
        this.doIndicatorPublicContentBackFill(resData)
        this.setDerivedFrom(resData.sourceIndicatorId)
        this.setNumberFormatConfig(JSON.parse(resData.configData))
        this.setShowConfig(this.getShowConfigData(resData.configData))
        this.setOldShowConfig(this.getShowConfigData(resData.configData))
        this.setDerivationRule(resData.ruleType)

        // 解析ruleConfig
        const ruleConfig = JSON.parse(resData.ruleConfig)
        if (this.derivationRule === 1) {
          // 期间累计
          this.setCalcPeriod(ruleConfig.periodType)
          this.setCalcIntervalType(ruleConfig.periodValue === 0 ? 1 : 2) // 计算区间为0，则选中当前
          this.setCalcInterval(ruleConfig.periodValue)
          this.setSummaryMethod(ruleConfig.calculateType)
        }
        if (this.derivationRule === 2) {
          // 同/环比
          this.setCalcType(ruleConfig.compareType)
          this.setSubCalcType(ruleConfig.yearOnYearType)
        }
        if (this.derivationRule === 3) {
          // 累积计算
          this.setSummaryMethod(ruleConfig)
        }
        if (this.derivationRule === 4) {
          // 移动计算
          this.setMobileCalcPeriod(ruleConfig.byType)
          this.setMobileCalcInterval({
            start: ruleConfig.start.split('-'),
            end: ruleConfig.end.split('-'),
          })
          this.setSummaryMethod(ruleConfig.calculateType)
        }
        if (this.derivationRule === 6) {
          // 总排名
          this.setRankingType(ruleConfig.rankingType)
          this.setRankingWay(ruleConfig.rankingWay)
        }

        /* 备份 */
        this.doIndicatorPublicDetailBackup(resData)
        this.indicatorDetailBak = {
          ...this.indicatorDetailBak,
          derivedFrom: resData.sourceIndicatorId,
          configData: resData.configData,
          derivationRule: resData.ruleType,
        }
        if (this.derivationRule === 1) {
          // 期间累计
          this.indicatorDetailBak = {
            ...this.indicatorDetailBak,
            calcPeriod: ruleConfig.periodType,
            calcIntervalType: ruleConfig.periodValue === 0 ? 1 : 2,
            calcInterval: ruleConfig.periodValue,
            summaryMethod: ruleConfig.calculateType,
          }
        }
        if (this.derivationRule === 2) {
          // 同/环比
          this.indicatorDetailBak = {
            ...this.indicatorDetailBak,
            calcType: ruleConfig.compareType,
            subCalcType: ruleConfig.yearOnYearType,
          }
        }
        if (this.derivationRule === 3) {
          // 累积计算
          this.indicatorDetailBak = {
            ...this.indicatorDetailBak,
            summaryMethod: ruleConfig.calculateType,
          }
        }
        if (this.derivationRule === 4) {
          // 移动计算
          this.indicatorDetailBak = {
            ...this.indicatorDetailBak,
            mobileCalcPeriod: ruleConfig.byType,
            mobileCalcInterval: {
              start: ruleConfig.start,
              end: ruleConfig.end,
            },
            summaryMethod: ruleConfig.calculateType,
          }
        }
        if (this.derivationRule === 6) {
          // 总排名
          this.indicatorDetailBak = {
            ...this.indicatorDetailBak,
            rankingType: ruleConfig.rankingType,
            rankingWay: ruleConfig.rankingWay,
          }
        }
      } else {
        this.$Message.error('获取派生指标详情失败')
      }
    },

    /**
     * 获取外部指标详情
     * @param ids
     */
    async getOutIndicatorDetail(ids) {
      const { data: res } = await getOutIndicatorById(ids || this.indicatorId)
      if (res?.state === 'success') {
        const resData = res.data

        /* 数据回填 */
        this.doIndicatorPublicContentBackFill(resData)
        this.setParamValue(resData.params)

        /* 备份 */
        this.doIndicatorPublicDetailBackup(resData)
        this.indicatorDetailBak = {
          ...this.indicatorDetailBak,
          paramValue: resData.params,
        }
      } else {
        this.$Message.error('获取外部指标详情失败')
      }
    },

    /**
     * 获取静态指标详情
     * @param ids
     */
    async getStaticIndicatorDetail(ids) {
      const res = await getStaticIndicatorById(ids || this.indicatorId)
      if (res.data.state === 'success') {
        const resData = res.data.data

        /* 数据回填 */
        this.doIndicatorPublicContentBackFill(resData)
        this.setIndicatorValue(Number(resData.value))
        this.setNumberFormatConfig(JSON.parse(resData.configData))

        /* 备份 */
        this.doIndicatorPublicDetailBackup(resData)
        this.indicatorDetailBak = {
          ...this.indicatorDetailBak,
          value: Number(resData.value),
          configData: resData.configData,
        }
      } else {
        this.$Message.error('获取静态指标详情失败')
      }
    },
  },
}
