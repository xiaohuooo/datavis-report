import {
  deleteComplexIndicatorById,
  deleteDerivativeIndicatorById,
  deleteIndicatorById,
  deleteOutIndicatorById,
  deleteStaticIndicatorById,
} from '@/api/indicator'
import { TYPE_INDICATOR } from '@/view/components/DeleteAssetModal/constant'

export default {
  methods: {
    /**
     * 选择某个指标
     * @param currentNode
     */
    selectResourcesChange(currentNode) {
      this.setIndicatorEditable(false)
      this.currentIndicatorNode = currentNode
      this.setIndicatorKind(currentNode.type)
      this.setIndicatorId(currentNode.id)
      this.getIndicatorDetail()
      this.currentTab = 'basicInfo'
    },

    /**
     * 删除指标
     * @param title 指标名称
     * @param type 指标类型
     * @param id 指标id
     */
    deleteIndicator({ title, type, id }) {
      let indicatorName =
        title.length > 12 ? title.slice(0, 12) + '...' : title
      this.$Modal.confirm({
        title: `确认删除“${indicatorName}”指标吗？`,
        content: '删除后不可撤销',
        okText: '确认',
        onOk: () => {
          this.doDeleteIndicator(id, indicatorName, type)
        },
      })
    },

    /**
     * 确认删除指标
     * @param id 指标id
     * @param indicatorName 指标名称
     * @param type 指标类型
     */
    async doDeleteIndicator(id, indicatorName, type) {
      let deleteFunc
      if (type === 1) {
        // 原子指标
        deleteFunc = deleteIndicatorById
      } else if (type === 2) {
        // 复合指标
        deleteFunc = deleteComplexIndicatorById
      } else if (type === 3) {
        // 派生指标
        deleteFunc = deleteDerivativeIndicatorById
      } else if (type === 4) {
        // 外部指标
        deleteFunc = deleteOutIndicatorById
      } else if (type === 5) {
        // 静态指标
        deleteFunc = deleteStaticIndicatorById
      }
      const res = await deleteFunc(id)
      if (res.data.state === 'success') {
        this.$Message.success('删除成功！')
        // 如果删除的是当前选中的指标，默认选中第一个
        await this.getGroupAndIndicator()
        if (id === this.indicatorId) {
          let defaultIndicator = this.indicators[0]
          if (defaultIndicator) {
            this.selectResourcesChange(defaultIndicator)
          } else {
            this.clearIndicator()
          }
        }
      } else {
        // 无法删除的情况
        const deletedAsset = { id, name: indicatorName, type: TYPE_INDICATOR }
        this.unableToDelete(deletedAsset, res.data.data)
      }
    },

    /**
     * 在某个资产分组下创建指标
     * @param value
     */
    handleAddIndicator(value) {
      this.setIndicatorKind(value.indicatorKind)
      this.clearIndicator()
      this.setAssetGroup(value.assetGroup)
      this.setIndicatorEditable(true)
      this.currentTab = 'basicInfo'
      this.indicatorDetailBak = {
        assetGroup: value.assetGroup,
      }
    },
  },
}
