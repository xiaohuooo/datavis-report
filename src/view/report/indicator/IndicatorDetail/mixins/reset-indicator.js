export default {
  methods: {
    /**
     * 重置原子指标
     */
    resetBasicIndicator() {
      const bak = this.indicatorDetailBak
      this.setBusinessModel(bak.businessModel)
      this.setColumn(bak.column)
      this.setColumnNvl(bak.columnNvl)
      this.setColumnNvlManualEntry(bak.columnNvlManualEntry)
      this.setSummaryMethod(bak.summaryMethod)
      this.setNumberFormatConfig(JSON.parse(bak.configData))

      this.setShowConfig(this.getShowConfigData(bak.configData))
      this.setOldShowConfig(this.getShowConfigData(bak.configData))
      this.setFilterConditions(bak.filterConditions)
    },

    /**
     * 重置复合指标
     */
    resetCompositeIndicator() {
      const bak = this.indicatorDetailBak
      this.setExpression(bak.expression)
      this.setExpressionHtml(bak.expressionHtml)
      this.setCompositeIndicatorFilterConditions(bak.conditions)
    },

    /**
     * 重置派生指标
     */
    resetDerivativeIndicator() {
      const bak = this.indicatorDetailBak
      this.setDerivedFrom(bak.derivedFrom)
      this.setNumberFormatConfig(JSON.parse(bak.configData))
      this.setShowConfig(this.getShowConfigData(bak.configData))
      this.setOldShowConfig(this.getShowConfigData(bak.configData))
      this.setDerivationRule(bak.derivationRule)

      if (this.derivationRule === 1) {
        // 期间累计
        this.setCalcPeriod(bak.calcPeriod)
        this.setCalcIntervalType(bak.calcIntervalType)
        this.setCalcInterval(bak.calcInterval)
        this.setSummaryMethod(bak.summaryMethod)
      }
      if (this.derivationRule === 2) {
        // 同/环比
        this.setCalcType(bak.calcType)
        this.setSubCalcType(bak.subCalcType)
      }
      if (this.derivationRule === 3) {
        // 累积计算
        this.setSummaryMethod(bak.summaryMethod)
      }
      if (this.derivationRule === 4) {
        // 移动计算
        this.setMobileCalcPeriod(bak.mobileCalcPeriod)
        this.setMobileCalcInterval(bak.mobileCalcInterval)
        this.setSummaryMethod(bak.summaryMethod)
      }
      if (this.derivationRule === 6) {
        // 总排名
        this.setRankingType(bak.rankingType)
        this.setRankingWay(bak.rankingWay)
      }
    },

    /**
     * 重置外部指标
     */
    resetOutIndicator() {
      const bak = this.indicatorDetailBak
      this.setParamValue(bak.paramValue)
    },

    /**
     * 重置静态指标
     */
    resetStaticIndicator() {
      const bak = this.indicatorDetailBak
      this.setIndicatorValue(bak.value)
    },
  },
}
