import {
  addComplexIndicator,
  addDerivativeIndicator,
  addIndicator,
  addOutIndicator,
  addStaticIndicator,
  queryAtomIndicatorUnionById,
  queryAtomIndicatorUsedById,
  updateComplexIndicator,
  updateDerivativeIndicator,
  updateIndicator,
  updateOutIndicator,
  updateStaticIndicator,
} from '@/api/indicator'

export default {
  methods: {
    // 指标表单验证
    async indicatorFormValidate() {
      if (!this.indicatorName) {
        this.$Message.warning('请填写指标名称')
        return false
      }

      // 检查指标名称
      const indicatorNameValid = await this.checkIndicatorNameAvailable()
      if (!indicatorNameValid) return false

      if (!this.indicatorCode) {
        this.$Message.warning('请填写指标编码')
        return false
      }

      // 原子指标指标定义表单验证
      if (this.indicatorKind === 1) {
        const valid = await this.basicIndicatorDefinitionFormValidate()
        if (!valid) return false
      }

      // 复合指标指标定义表单验证
      if (this.indicatorKind === 2) {
        const valid = await this.compositeIndicatorDefinitionFormValidate()
        if (!valid) return false
      }

      // 派生指标指标定义表单验证
      if (this.indicatorKind === 3) {
        const valid = await this.derivativeIndicatorDefinitionFormValidate()
        if (!valid) return false
      }

      // 外部指标指标定义表单验证
      if (this.indicatorKind === 4) {
        const valid = this.outIndicatorDefinitionFormValidate()
        if (!valid) return false
      }

      // 静态指标指标定义表单验证
      if (this.indicatorKind === 5) {
        const valid = this.staticIndicatorDefinitionFormValidate()
        if (!valid) return false
      }

      if (!this.assetGroup) {
        this.$Message.warning('请选择资产分组')
        return false
      }
      return true
    },

    /**
     * 检查指标名称是否重复
     */
    async checkIndicatorNameAvailable() {
      if (!this.assetGroup) {
        this.$Message.warning('请选择资产分组')
        return false
      }
      let parentNode = this.findParentNode(this.assetGroup, this.allTreeData)
      if (parentNode?.hasOwnProperty('children')) {
        let allName = parentNode.children.map((item) => item.title),
          idx = allName.indexOf(this.indicatorName)
        if (idx !== -1 && parentNode.children[idx].id != this.indicatorId) {
          this.$Message.error('同组下有重复指标名')
          return false
        }
      }
      return true
    },

    /**
     * 原子指标指标定义表单验证
     */
    async basicIndicatorDefinitionFormValidate() {
      if (!this.businessModel) {
        this.$Message.warning('请选择业务模型')
        return false
      }
      if (!this.column) {
        this.$Message.warning('请选择列')
        return false
      }
      if (this.summaryMethodWithNvl && !this.columnNvl) {
        this.$Message.warning('请选择列的空值默认值')
        return false
      }
      if (
        this.summaryMethodWithNvl &&
        this.columnNvl === 'manualEntry' &&
        !this.columnNvlManualEntry
      ) {
        this.$Message.warning('请填写列的空值默认值')
        return false
      }
      if (!this.summaryMethod) {
        this.$Message.warning('请选择汇总方式')
        return false
      }
      return true
    },

    /**
     * 复合指标指标定义表单验证
     */
    async compositeIndicatorDefinitionFormValidate() {
      if (!this.expression) {
        this.$Message.warning('请填写表达式')
        return false
      }
      return true
    },

    /**
     * 派生指标指标定义表单验证
     */
    async derivativeIndicatorDefinitionFormValidate() {
      if (!this.derivedFrom) {
        this.$Message.warning('请选择派生自')
        return false
      }
      if (!this.derivationRule) {
        this.$Message.warning('请选择派生规则')
        return false
      }

      // 期间累计
      if (this.derivationRule === 1) {
        if (!this.calcPeriod) {
          this.$Message.warning('请选择计算周期')
          return false
        }
        if (this.calcInterval === null) {
          this.$Message.warning('请选择计算区间')
          return false
        }
        if (!this.summaryMethod) {
          this.$Message.warning('请选择聚合方式')
          return false
        }
      }

      // 同/环比
      if (this.derivationRule === 2) {
        if (!this.calcType) {
          this.$Message.warning('请选择计算方式')
          return false
        }
        if (this.calcType === 2 && !this.subCalcType) {
          this.$Message.warning('请选择计算方式')
          return false
        }
      }

      // 累积计算
      if (this.derivationRule === 3) {
        if (!this.summaryMethod) {
          this.$Message.warning('请选择聚合方式')
          return false
        }
      }

      // 移动计算
      if (this.derivationRule === 4) {
        if (!this.mobileCalcPeriod) {
          this.$Message.warning('请选择计算周期')
          return false
        }
        if (
          !this.mobileCalcInterval.start.length ||
          !this.mobileCalcInterval.end.length
        ) {
          this.$Message.warning('请选择计算区间')
          return false
        }
        if (!this.summaryMethod) {
          this.$Message.warning('请选择聚合方式')
          return false
        }
      }
      // 累积计算
      if (this.derivationRule === 6) {
        if (!this.rankingType) {
          this.$Message.warning('请选择排名类型')
          return false
        }
        if (!this.rankingWay) {
          this.$Message.warning('请选择排序方式')
          return false
        }
      }

      return true
    },

    /**
     * 外部指标指标定义表单验证
     */
    outIndicatorDefinitionFormValidate() {
      if (!this.paramValue) {
        this.$Message.warning('请填写参数')
        return false
      }
      return true
    },

    /**
     * 静态指标指标定义表单验证
     */
    staticIndicatorDefinitionFormValidate() {
      if (this.indicatorValue === null) {
        this.$Message.warning('请输入指标值')
        return false
      }
      return true
    },

    /**
     * 原子指标保存
     */
    async basicIndicatorSave() {
      if (this.indicatorId) {
        // 查询指标是否被用于union
        const res = await queryAtomIndicatorUnionById(this.indicatorId)
        if (res.data.state === 'success') {
          if (
            res.data.data.length &&
            this.summaryMethod !== this.oldSummaryMethod
          ) {
            // 原子指标被用于union并且汇总方式已修改
            this.saveFailIndicatorList = res.data.data
            this.showSaveFailModal = true
            return Promise.reject('此指标已被复合指标引用，无法修改汇总方式！')
          }
        } else {
          this.$Message.error('查询指标是否被用于union失败！')
        }
      }

      // 处理请求需要的数据
      let businessModelPropertyId = this.column
      if (this.summaryMethodWithNvl) {
        businessModelPropertyId =
          this.columnNvl === 'manualEntry'
            ? `[${this.column}],${this.columnNvlManualEntry}`
            : `[${this.column}],[${this.columnNvl}]`
      }
      let conditionGroups = this.filterConditions
        .map((group) => ({
          type: 1, // 固定为1，表示指标筛选器，报告管理页面使用
          conditions: group
            .filter(
              // 过滤无用条件（未填写完的）
              (item) =>
                item.sourceId && item.conditionType && item.variableValue,
            )
            .map((item) => ({
              sourceId: item.sourceId,
              conditionType: item.conditionType,
              value: `${item.variableType}|${item.variableValue}`,
            })),
        }))
        .filter((item) => item.conditions.length) // 最后过滤掉条件为空数组的group

      // 判断过滤条件是否存在变量条件
      let conditionIncludeVariable =
        JSON.stringify(conditionGroups).indexOf('2|') > -1

      // 参数
      const params = {
        type: this.indicatorKind,
        id: this.indicatorId,
        groupId: this.assetGroup,
        name: this.indicatorName,
        abbrName: this.indicatorAbbreviation,
        assetsCode: this.indicatorCode,
        versionCode: this.indicatorVersion,
        attribute: this.indicatorAttribute,
        syncBoard: this.syncDashboard ? 1 : 2, // 1-开启 2-关闭
        description: this.buildBusinessDescParam(),
        openDate: this.businessDate ? 1 : 0, // 1-开启 0-关闭
        businessModelId: this.businessModel,
        businessModelPropertyId,
        calculateType: this.summaryMethod,
        showType: this.numberFormatConfig.showType,
        configData: JSON.stringify({ ...this.numberFormatConfig, ...this.showConfig }),
        showConditionGroups: this.showFilterConditions, // 开启过滤，并且有生效的过滤条件
        conditionGroups,
        labels: this.indicatorLabels,
      }

      // 请求
      if (this.indicatorId) {
        // 编辑
        // 编辑前查询原子指标是否被其他模块使用，如果被使用，提示
        const { data: res } = await queryAtomIndicatorUsedById(
          this.indicatorId,
        )
        if (res?.state === 'success') {
          if (res.data === false) {
            // true-没有被使用 false-被其他模块使用
            return new Promise((resolve, reject) => {
              this.$Modal.confirm({
                title: '保存指标',
                content: `确认保存指标”${this.indicatorName}“吗？`,
                okText: '确认',
                onOk: async () => {
                  await this.doBasicIndicatorUpdate(
                    params,
                    conditionIncludeVariable,
                  )
                  resolve()
                },
              })
            })
          } else {
            await this.doBasicIndicatorUpdate(params, conditionIncludeVariable)
          }
        } else {
          this.$Message.error('查询原子指标是否被其他模块使用失败')
        }
      } else {
        // 新增
        const { data: res } = await addIndicator(params)
        if (res?.state === 'success') {
          this.$Message.success('创建原子指标成功')
          this.setIndicatorId(res.data)
          await this.getGroupAndIndicator()
        } else {
          this.$Message.error('创建原子指标失败')
        }
      }
    },

    /**
     * 确认编辑原子指标
     * @param params
     * @param conditionIncludeVariable
     */
    async doBasicIndicatorUpdate(params, conditionIncludeVariable) {
      this.checkLoading = true
      const { data: res } = await updateIndicator(params)
      if (res?.state === 'success') {
        if (this.indicatorUsed && conditionIncludeVariable) {
          this.$Modal.success({
            title: '编辑原子指标成功',
            content: '存在变量条件调整， 原报告指标需再次编辑生效',
            okText: '确认',
          })
        } else {
          this.$Message.success('编辑原子指标成功')
        }
        await this.getGroupAndIndicator()
      } else {
        this.$Message.error('编辑原子指标失败')
      }
      this.checkLoading = false
    },

    /**
     * 复合指标保存
     */
    async compositeIndicatorSave() {
      this.checkLoading = true
      // 处理expression，id无需拼接type
      const expression = this.expression.replace(/\_(.*?)[0-9]/gi, '')

      // 处理baseIndicators，id拼接type，并且逗号片接
      let baseIndicators = []
      // 判断表达式是否是union函数的，带有[]表示不是union函数的
      if (this.expression.indexOf('[') > -1) {
        const reg = /\[(.*?)\]/gi
        baseIndicators = this.expression
          .match(reg)
          .map((item) => item.replace(reg, '$1'))
          .join(',')
      } else {
        baseIndicators = this.expression.replace(/(\*[0-9]*.[0-9]*)/, '')
      }

      // 处理过滤条件
      let conditions = {}
      for (let key in this.compositeIndicatorFilterConditions) {
        let filterConditions = this.compositeIndicatorFilterConditions[key]
        conditions[key] = filterConditions
          .map((group) => ({
            type: 1, // 固定为1，表示指标筛选器，报告管理页面使用
            conditions: group
              .filter(
                // 过滤无用条件（未填写完的）
                (item) =>
                  item.sourceId && item.conditionType && item.variableValue,
              )
              .map((item) => ({
                sourceId: item.sourceId,
                conditionType: item.conditionType,
                value: `${item.variableType}|${item.variableValue}`,
              })),
          }))
          .filter((item) => item.conditions.length) // 最后过滤掉条件未空数组的group
      }

      const params = {
        type: this.indicatorKind,
        id: this.indicatorId,
        groupId: this.assetGroup,
        name: this.indicatorName,
        abbrName: this.indicatorAbbreviation,
        assetsCode: this.indicatorCode,
        versionCode: this.indicatorVersion,
        attribute: this.indicatorAttribute,
        syncBoard: this.syncDashboard ? 1 : 2,
        description: this.buildBusinessDescParam(),
        openDate: this.businessDate ? 1 : 0,
        expression,
        relationType: expression.indexOf('[') > -1 ? 1 : 2, // 1-普通表达式 2-union函数表达式
        expressionHtml: this.expressionHtml,
        configData: JSON.stringify({ ...this.numberFormatConfig, ...this.showConfig }),
        baseIndicators,
        showConditionGroups: this.showFilterConditions,
        conditions,
        labels: this.indicatorLabels,
      }
      // 请求
      if (this.indicatorId) {
        // 编辑
        const { data: res } = await updateComplexIndicator(params)
        if (res?.state === 'success') {
          this.$Message.success('编辑复合指标成功')
          await this.getGroupAndIndicator()
        } else {
          this.$Message.error('编辑复合指标失败')
        }
      } else {
        // 新增
        const { data: res } = await addComplexIndicator(params)
        if (res?.state === 'success') {
          this.$Message.success('创建复合指标成功')
          this.setIndicatorId(res.data.id)
          await this.getGroupAndIndicator()
        } else {
          this.$Message.error('创建复合指标失败')
        }
      }
      this.checkLoading = false
    },

    /**
     * 派生指标保存
     */
    async derivativeIndicatorSave() {
      this.checkLoading = true
      // 构建ruleConfig，json字符串格式
      let ruleConfigParams = null
      if (this.derivationRule === 1) {
        // 期间累计
        ruleConfigParams = {
          periodType: this.calcPeriod,
          periodValue: this.calcInterval,
          calculateType: this.summaryMethod,
        }
      }
      if (this.derivationRule === 2) {
        // 同/环比
        ruleConfigParams = {
          compareType: this.calcType,
          yearOnYearType: this.subCalcType,
        }
      }
      if (this.derivationRule === 4) {
        // 移动计算
        ruleConfigParams = {
          byType: this.mobileCalcPeriod,
          start: this.mobileCalcInterval.start.join('-'),
          end: this.mobileCalcInterval.end.join('-'),
          calculateType: this.summaryMethod,
        }
      }
      if (this.derivationRule === 6) {
        // 总排名
        ruleConfigParams = {
          rankingType: this.rankingType,
          rankingWay: this.rankingWay,
        }
      }
      // 派生规则为累计计算时（derivationRule === 3），ruleConfig为键值对，否则为JSON字符串
      let ruleConfig =
        this.derivationRule === 3
          ? this.summaryMethod
          : JSON.stringify(ruleConfigParams)

      // 参数
      const params = {
        type: this.indicatorKind,
        id: this.indicatorId,
        groupId: this.assetGroup,
        name: this.indicatorName,
        abbrName: this.indicatorAbbreviation,
        assetsCode: this.indicatorCode,
        versionCode: this.indicatorVersion,
        attribute: this.indicatorAttribute,
        syncBoard: this.syncDashboard ? 1 : 2,
        description: this.buildBusinessDescParam(),
        sourceIndicatorId: this.derivedFrom,
        showType: this.numberFormatConfig.showType,
        configData: JSON.stringify({ ...this.numberFormatConfig, ...this.showConfig }),
        ruleType: this.derivationRule,
        ruleConfig,
        labels: this.indicatorLabels,
      }
      // 请求
      if (this.indicatorId) {
        // 编辑
        const { data: res } = await updateDerivativeIndicator(params)
        if (res?.state === 'success') {
          this.$Message.success('编辑派生指标成功')
          await this.getGroupAndIndicator()
        } else {
          this.$Message.error('编辑派生指标失败')
        }
      } else {
        // 新增
        const { data: res } = await addDerivativeIndicator(params)
        if (res?.state === 'success') {
          this.$Message.success('创建派生指标成功')
          this.setIndicatorId(res.data.id)
          await this.getGroupAndIndicator()
        } else {
          this.$Message.error('创建派生指标失败')
        }
      }
      this.checkLoading = false
    },

    /**
     * 外部指标保存
     */
    async outIndicatorSave() {
      this.checkLoading = true
      const params = {
        id: this.indicatorId,
        name: this.indicatorName, // 指标名称
        abbrName: this.indicatorAbbreviation, // 指标简称
        assetsCode: this.indicatorCode, // 指标编码
        versionCode: this.indicatorVersion, // 版本号
        attribute: this.indicatorAttribute, // 属性
        syncBoard: this.syncDashboard ? 1 : 2, // 是否同步看板
        description: this.buildBusinessDescParam(), // 业务描述
        params: this.paramValue, // 参数
        groupId: this.assetGroup, // 资产分组
        labels: this.indicatorLabels, // 标签
      }

      if (this.indicatorId) {
        // 编辑
        const { data: res } = await updateOutIndicator(params)
        if (res?.state === 'success') {
          this.$Message.success('编辑外部指标成功')
          await this.getGroupAndIndicator()
        } else {
          this.$Message.error('编辑外部指标失败')
        }
      } else {
        // 新增
        const { data: res } = await addOutIndicator(params)
        if (res?.state === 'success') {
          this.$Message.success('创建外部指标成功')
          this.setIndicatorId(res.data.id)
          await this.getGroupAndIndicator()
        } else {
          this.$Message.error('创建外部指标失败')
        }
      }
      this.checkLoading = false
    },

    /**
     * 静态指标保存
     */
    async staticIndicatorSave() {
      this.checkLoading = true

      const params = {
        id: this.indicatorId,
        name: this.indicatorName, // 指标名称
        abbrName: this.indicatorAbbreviation, // 指标简称
        assetsCode: this.indicatorCode, // 指标编码
        versionCode: this.indicatorVersion, // 版本号
        attribute: this.indicatorAttribute, // 属性
        syncBoard: this.syncDashboard ? 1 : 2, // 是否同步看板
        description: this.buildBusinessDescParam(), // 业务描述
        value: this.indicatorValue, // 指标值
        configData: JSON.stringify({ ...this.numberFormatConfig, ...this.showConfig }),
        groupId: this.assetGroup, // 资产分组
        labels: this.indicatorLabels, // 标签
      }

      // 请求
      if (this.indicatorId) {
        // 编辑
        const res = await updateStaticIndicator(params)
        if (res.data.state === 'success') {
          this.$Message.success('编辑静态指标成功')
          await this.getGroupAndIndicator()
        } else {
          this.$Message.error('编辑静态指标失败')
        }
      } else {
        // 新增
        const res = await addStaticIndicator(params)
        if (res.data.state === 'success') {
          this.$Message.success('创建静态指标成功')
          this.setIndicatorId(res.data.data.id)
          await this.getGroupAndIndicator()
        } else {
          this.$Message.error('创建静态指标失败')
        }
      }

      this.checkLoading = false
    },
  },
}
