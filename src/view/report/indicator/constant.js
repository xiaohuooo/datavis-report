// 指标管理-派生规则
export const DERIVATION_RULES = [
  {
    name: '期间累计',
    value: 1,
    desc: '即年累计/月累计/季度累计/周累计。eg：月累计最大销售额、连续7天累计销售额',
  },
  {
    name: '同/环比',
    value: 2,
    desc: '即年同比、月同比、环比。eg：年销售额年环比',
  },
  {
    name: '累计计算',
    value: 3,
    desc: '有史以来的所有值参与计算。eg：每年最大日销售额',
  },
  {
    name: '移动计算',
    value: 4,
    desc: '指定区间的值参与计算。eg：2022年7月销售额',
  },
  {
    name: '总占比',
    value: 5,
    desc: '当前值/总值。eg：销售额占比',
  },
  {
    name: '总排名',
    value: 6,
    desc: '销售额从小到大排名',
  },
]

// 排序类型
export const RANKING_TYPES = [
  {
    name: '竞争排序',
    value: 1,
    desc: '排名值可重复且有间隔（如：1，2，2，4…）',
  },
  {
    name: '密集排序',
    value: 2,
    desc: '排名值可重复且无间隔（如：1，2，2，3…）',
  },
  {
    name: '唯一排序',
    value: 3,
    desc: '排名值不重复（如：1，2，3，4…）',
  },
  // {
  //   name: '百分位排序', // 百分位排序暂不开发
  //   value: 4,
  //   desc: '（如：1%，5%，20%…）',
  // },
]

//排序方式
export const RANKING_WAYS = [
  {
    name: '升序',
    value: 1,
  },
  {
    name: '降序',
    value: 2,
  }
]
// 比较
export const COMPARE_LIST = [
  { id: 1, label: '>' },
  { id: 2, label: '<' },
  { id: 3, label: '<=' },
  { id: 4, label: '>=' },
  { id: 5, label: '=' },
  { id: 6, label: 'is null' },
  { id: 7, label: 'is not null' },
]

// 汇总方式
export const SUMMARY_METHODS = [
  { name: '计数', value: 5 },
  { name: '去重计数', value: 13 },
  { name: '求和', value: 1 },
  { name: '平均值', value: 2 },
  { name: '最大值', value: 3 },
  { name: '最小值', value: 4 },
  { name: '求和(nvl)', value: 7 },
  { name: '平均值(nvl)', value: 8 },
]

// 指标类型
export const INDICATOR_TYPES = [
  { name: '数值', value: 0 },
  { name: '百分比', value: 1 },
]

// 数值单位
export const NUMBER_UNIT_LIST = [
  { name: '默认值', value: 'original' },
  { name: '万', value: 'wan' },
  { name: '亿', value: 'yi' },
]

// 单个过滤条件对象
export const FILTER_INDICATOR_ITEM = {
  sourceId: null,
  conditionType: null,
  variableType: 1, // 常量
  variableValue: null,
}

// 计算周期
export const CALC_PERIODS = [
  { name: '年', value: 1 },
  { name: '季度', value: 4 },
  { name: '月', value: 2 },
  { name: '周', value: 3 },
]

// 计算方式
export const CALC_TYPES = [
  { name: '环比', value: 1 },
  { name: '同比', value: 2 },
]

// 副计算方式
export const SUB_CALC_TYPES = [
  { name: '月同比', value: 1 },
  { name: '年同比', value: 2 },
]

// 移动计算-计算周期
export const MOBILE_CALC_PERIODS = [
  { name: '按年', value: 1 },
  { name: '按月', value: 2 },
]

// 属性
export const ATTRIBUTES = ['一级指标','二级指标','三级指标']

export const INDICATOR_TYPE_MAP = {
  1: 'BasicIndicator',
  2: 'CompositeIndicator',
  3: 'DerivativeIndicator',
  4: 'OutIndicator',
  5: 'StaticIndicator',
}
