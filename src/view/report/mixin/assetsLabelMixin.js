import { deepCopy } from "@/utils/common"

export default {
  methods: {
    /**
     * 标签变化
     * @param selected
     */
    async handleAssetsLabelSelect(selected) {
      // 根据checkType区分基础指标和复合指标
      const labels = this.checkType === 2 ? this.complexFormData.labels : this.formItem.labels
      const index = labels.findIndex(item => item.id === selected.id)
      const isAdd = index === -1

      if (isAdd) {
        labels.push(selected)
        if (this.cloneFormItem) {
          this.cloneFormItem.labels = deepCopy(this.formItem.labels)
        }
      } else {
        labels.splice(index, 1)
        if (this.cloneFormItem) {
          this.cloneFormItem.labels = deepCopy(this.formItem.labels)
        }
      }
    },
  },
}
