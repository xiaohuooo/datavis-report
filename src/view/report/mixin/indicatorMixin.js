import { deepCopy } from '@/utils/common'
import {
  addComplexIndicator,
  addDerivativeIndicator, getDerivativeIndicatorById, getIndicatorById,
  getIndicatorTreeData,
  queryDataForExecuteSql,
  testExecuteSql,
  updateComplexIndicator,
  updateDerivativeIndicator,
  addStaticIndicator,
  updateStaticIndicator,
} from '@/api/indicator'
import { getBusinessModelGroupAndColumn } from '@/api/indicator'
import {mapActions, mapState} from 'vuex'
import moment from 'moment/moment'
import {getToken} from "@/libs/util";
import {FILTER_INDICATOR_ITEM} from "@/view/report/indicator/constant";
import {INDICATOR_OPTION_LIST} from "@/config/indicator";
import {getVariableList} from "@/api/dataset";

export default {
  computed: {
    ...mapState('indicator', [
      'indicatorEditable',
      'indicatorName',
      'indicatorAbbreviation',
      'indicatorCode',
      'indicatorAttribute',
      'syncDashboard',
      'businessDate',
      'relatedDimensions',
      'indicatorId',
      'indicatorKind',
      'indicatorVersion',
      'businessDesc',
      'businessModel',
      'businessModelList',
      'column',
      'columnNvl',
      'columnNvlManualEntry',
      'summaryMethod',
      'numberFormatConfig',
      'showFilterConditions',
      'filterConditions',
      'assetGroup',
      'indicatorLabels',
      'derivedFrom',
      'derivationRule',
      'calcPeriod',
      'calcInterval',
      'calcType',
      'subCalcType',
      'mobileCalcPeriod',
      'mobileCalcInterval',
      'rankingType',
      'rankingWay',
      'expression',
      'expressionHtml',
      'compositeIndicatorFilterConditions',
      'indicators',
      'compositeIndicatorBusinessModels',
      'paramValue',
      'showConfig',
      'indicatorValue',
    ]),
  },
  methods: {
    ...mapActions('indicator', [
      'setIndicatorEditable',
      'setBusinessModelList',
      'setIndicatorName',
      'setIndicatorAbbreviation',
      'setIndicatorCode',
      'setIndicatorAttribute',
      'setIndicatorVersion',
      'setSyncDashboard',
      'setBusinessDate',
      'setRelatedDimensions',
      'setCompositeIndicatorBusinessModels',
      'setIndicatorKind',
      'setIndicatorTree',
      'setIndicatorId',
      'setIndicatorVersion',
      'setIndicatorVersions',
      'setBusinessDesc',
      'setBusinessModel',
      'setColumn',
      'setColumnNvl',
      'setColumnNvlManualEntry',
      'setSummaryMethod',
      'setNumberFormatConfig',
      'setShowFilterConditions',
      'setFilterConditions',
      'setAssetGroup',
      'setIndicatorLabels',
      'setIndicatorTree',
      'setDerivedFrom',
      'setDerivationRule',
      'setCalcPeriod',
      'setCalcIntervalType',
      'setCalcInterval',
      'setCalcType',
      'setSubCalcType',
      'setMobileCalcPeriod',
      'setMobileCalcInterval',
      'setRankingType',
      'setRankingWay',
      'setExpression',
      'setExpressionHtml',
      'setCompositeIndicatorFilterConditions',
      'setIndicators',
      'setAssetGroups',
      'setPublicVariableList',
      'setParamValue',
      'fetchAllVersionList',
      'setShowConfig',
      'setOldShowConfig',
      'setIndicatorValue',
    ]),
    // 获取指标分组数据
    getIndGroup() {
      getIndicatorTreeData().then(res => {
        let data = res.data.data
        let group = data.indicatorGroupList
        group.push({
          id: -1,
          title: '指标组',
          name: '指标组',
          expand: true,
          parentId: 0
        })
        group.forEach(item => {
          item.id = item.id.toString()
          item.parentId = item.parentId.toString()
        })
        this.groupTreeData = this.filterIndGroup(group)
      })
    },

    filterIndGroup(data) {
      let clone = deepCopy(data)
      return clone.filter(parent => {
        let branchArr = clone.filter(child => parent.id === child.parentId)
        parent.title = parent.name

        branchArr.length > 0 ? parent.children = branchArr : ''
        return parent.parentId === '0'
      })
    },

    // 获取业务模型数据
    async getBusinessModelTreeData() {
      let res = await getBusinessModelGroupAndColumn()
      let data = res.data
      if (data.state === 'success') {
        let groupData = data.data.groupList
        let businessModelList = data.data.businessModelList.map(item => {
          return { id: item.id, name: item.name, groupId: item.groupId }
        })
        let allData = groupData.concat(businessModelList)
        let allDataTree = allData.filter(item => item.parentId === -1 || item.groupId === -1)
        allDataTree = allDataTree.map(item => {
          let obj = {
            id: item.id,
            title: item.name,
          }
          if (item.hasOwnProperty('parentId')) {
            obj.expand = item.expand
          }
          if (item.parentId === -1) {
            obj.type = 'group'
          } else if (item.groupId === -1) {
            obj.type = 'value'
          }
          return obj
        })
        this.recursiveBusinessModelGroup(allDataTree, allData)

        let treeData = [{
          id: -1,
          title: '业务模型组',
          expand: true,
          type: 'group',
          children: allDataTree
        }]
        this.businessModelTreeData = treeData
        this.businessModelTreeDataClone = deepCopy(treeData)
        this.setBusinessModelList(data.data.businessModelList)
      }
    },

    // 递归构建业务模型树
    recursiveBusinessModelGroup(parent, allData, deep = 2) {
      parent.forEach(item => {
        item.deep = deep
        let children = allData
          .filter(d => {
            if (d.parentId) {
              return d.parentId === item.id
            } else if (d.groupId) {
              return d.groupId === item.id
            }
          }).map((child, i) => {
            if (child.parentId) {
              return {
                id: child.id,
                title: child.name,
                expand: child.expand,
                type: 'group'
              }
            } else if (child.groupId) {
              return {
                id: child.id,
                title: child.name,
                type: 'value'
              }
            }
          })
        if (children.length > 0) {
          item.children = children
          this.recursiveBusinessModelGroup(children, allData, deep + 1)
        }
      })
    },

    // 基础指标表单变化
    handleBasicIndicatorFormChange(value) {
      Object.assign(this.formItem, value)
      if (this.updateGraph) {
        this.updateGraph()
      }
    },

    // 基础指标sql模态框数据变化
    handleBasicIndicatorSqlChange(value) {
      Object.assign(this.sqlSearch, value)
    },

    findParentNode(id, treeData) {
      for (let i = 0; i < treeData.length; i++) {
        let item = treeData[i]
        if (item.id != id) {
          if (item.hasOwnProperty('children')) {
            let a = this.findParentNode(id, item.children)
            if (a) {
              return a
            }
          }
        } else {
          return item
        }
      }
    },

    handleCheckStartDate(data) {
      if (this.formItem.dateStrObj.startDate.length === 0) {
        return { type: false, message: '请输入起始日期' }
      }
      if (Number(this.formItem.dateStrObj.startDate[0]) > Number(data[0]) || (Number(this.formItem.dateStrObj.startDate[0]) === Number(data[0]) && Number(this.formItem.dateStrObj.startDate[1]) > Number(data[1]))) {
        return { type: false, message: '起始日期不能大于结束日期' }
      }
      return { type: true }
    },

    // 关闭sql查询模态框
    handleCloseSqlModal() {
      this.sqlSearch.modalVisible = false
      this.sqlSearch.sqlValue = ''
      this.isSqlChecked = null
      this.sqlResultColumns = []
      this.sqlResultError = null
      this.sqlResultData = []
    },

    onSqlValueInput(e) {
      this.sqlSearch.sqlValue = e.target.innerText
    },

    // 检查sql语句
    checkSqlSentence() {
      let text = this.sqlSearch.sqlValue
      if (text === null || text.trim() === '' || text.replaceAll(' ', '') === '') {
        this.sqlResultData = []
        this.isSqlChecked = false
        return
      }
      const reg = /\${(.+?)\}/gi
      let regList
      if (text.match(reg)) {
        regList = text.match(reg).map(item => (item.replace(/\s/gi, '')))
      }
      let allVar = Array.from(new Set(regList))
      if (allVar !== null && allVar.length !== 0) {
        this.sqlVarList.varList = allVar.map(item => {
          let day = new Date()
          day.setDate(day.getDate() - 1)
          let yesterday = moment(day).format('yyyyMMDD')
          return {
            label: item.slice(2, item.length - 1),
            value: item.slice(2, item.length - 1) === 'businessDate' ? yesterday : '',
            isShow: item.slice(2, item.length - 1) === 'businessDate' ? false : true
          }
        })
        this.sqlVarList.sql = text
        if (this.sqlVarList.varList.length === 1 && this.sqlVarList.varList[0].label === 'businessDate') {
          this.writeVarVisible = false
          this.sqlVarOk()
        } else {
          this.writeVarVisible = true
        }
      } else {
        let sql = this.replaceSqlSpace(text)
        this.sendSqlToExecute(sql)
      }
    },

    /**
     * sql语句变量替换-ok
     */
    sqlVarOk() {
      let { sql, varList } = deepCopy(this.sqlVarList)
      varList.forEach(item => {
        let reg = new RegExp('\\' + '$' + '{' + '(' + item.label + ')' + '\\' + '}', 'gi')
        sql = sql.replace(reg, item.value)
        sql = this.replaceSqlSpace(sql)
      })
      this.sendSqlToExecute(sql)
    },

    // sql查询-保存
    async sqlOk() {
      if (this.isSqlChecked) {
        let text = this.replaceSqlSpace(this.sqlSearch.sqlValue)
        if (text === '') {
          this.$Message.warning('未输入SQL语句')
          return false
        }
        let sql = this.sqlSearch.sqlValue
        if (this.sqlCopy !== undefined && sql !== this.sqlCopy) {
          if (sql === '') {
            this.$Message.error('请输入SQL语句')
          }
          this.$Message.error('SQL语句未校验')
          return false
        }
        const { groupIndex, conditionIndex } = this.formItem.conditionGroupsSelectIndex
        const conditionGroups = this.formItem.conditionGroups
        let conditionType = conditionGroups[groupIndex].conditions[conditionIndex].conditionType

        let res = await testExecuteSql({
          conditionType,
          value: `[${this.isCheckedSql !== undefined ? this.isCheckedSql : text}]`
        })
        if (res.data.state !== 'success') {
          this.$Message.error(res.data.message)
          this.sqlResultError = res.data.message
          return false
        }
        let condition = conditionGroups[groupIndex].conditions[conditionIndex]
        condition.value = `${condition.selectedOpt}|${text}`
        condition.sql = text
        this.$Message.success('保存成功')
        this.handleCloseSqlModal()
      } else {
        this.$Message.warning('SQL尚未校验')
      }
    },

    sendSqlToExecute(sql) {
      this.writeVarVisible = false
      const conditionGroups = this.formItem.conditionGroups
      const { groupIndex, conditionIndex } = this.formItem.conditionGroupsSelectIndex
      let conditionType = conditionGroups[groupIndex].conditions[conditionIndex].conditionType
      if (conditionType === null) {
        this.$Message.warning('请选择条件')
        return false
      }
      queryDataForExecuteSql({ businessModelId: this.formItem.businessModelId, value: `[${sql}]` }).then(res => {
        if (res.data.state === 'success') {
          this.isSqlChecked = true
          if (res.data.data.length > 0) {
            let header = res.data.data[0]
            let columns = []
            for (let k in header) {
              columns.push({
                title: k,//.replace(/alias_\d+_\d+_/, ''),
                key: k,
                minWidth: 150
              })
            }
            this.sqlResultColumns = columns
            let data = []
            for (let i = 0; i < 20 && i < res.data.data.length; i++) {
              let objVal = res.data.data[i]
              for (let key in objVal) {
                if (key.toLowerCase().indexOf('date') !== -1) {
                  if (!isNaN(objVal[key]) && isNaN(Date.parse(objVal[key]))) {
                    objVal[key] = moment(objVal[key]).format('yyyy-MM-DD')
                  }
                }
              }
              data.push(objVal)
            }
            this.sqlResultData = data
            this.sqlCopy = this.sqlSearch.sqlValue
            this.isCheckedSql = sql
          } else {
            this.sqlResultData = []
          }
        } else {
          this.isSqlChecked = false
          this.sqlResultData = []
          this.sqlResultError = res.data.message
        }
      })
    },

    recursiveIndicatorGroup(parent, allData, deep = 1) {
      parent.forEach((item) => {
        item.deep = deep
        let children = allData
          .filter((d) => {
            if (d.parentId) {
              return d.parentId == item.id
            } else if (d.groupId) {
              return d.groupId == item.id
            }
          })
          .map((child, i) => {
            if (child.parentId) {
              return {
                id: child.id,
                title: child.name,
                dataType: 1,
                expand: child.expand,
                disabled: true,
                treeType: 'group',
              }
            } else if (child.groupId) {
              return {
                id: child.id,
                title: child.name,
                dataType: 2,
                type: child.type,
                selected: child.selected,
                treeType: 'value',
                creatorId: child.creatorId, // 创建人
                controlEdit: child.controlEdit, // 是否有编辑权限
              }
            }
          })
        if (children.length > 0) {
          item.children = children
          this.recursiveIndicatorGroup(children, allData, deep + 1)
        }
      })
    },


    // 构建业务描述参数
    buildBusinessDescParam() {
      // 匹配所有的真实token，替换为${token}占位符
      const token = getToken()
      let businessDesc = this.businessDesc.replaceAll(token, '${token}')
      return businessDesc
    },

    // 获取默认指标编码
    getDefaultIndicatorCode() {
      const { initial } = INDICATOR_OPTION_LIST.find(
        (ele) => ele.value === this.indicatorKind,
      )
      return `${initial}${new Date().valueOf()}`
    },
    clearIndicator() {
      // 公共字段
      this.setIndicatorId(null)
      this.setIndicatorName(null)
      this.setIndicatorAbbreviation('')
      this.setIndicatorCode(this.getDefaultIndicatorCode())
      this.setIndicatorVersion('V1.0.0')
      this.setIndicatorVersions([])
      this.oldIndicatorVersion = 'V1.0.0'
      this.setIndicatorAttribute('一级指标')
      this.setSyncDashboard(false)
      this.setBusinessDesc('')
      this.setBusinessDate(true)
      this.setNumberFormatConfig(null)
      this.setShowConfig(null)
      this.setOldShowConfig(null)
      this.setSummaryMethod(null)
      this.setAssetGroup('')
      this.setIndicatorLabels([])

      // 原子指标
      this.setBusinessModel('')
      this.setColumn(null)
      this.setColumnNvl(null)
      this.setColumnNvlManualEntry(null)
      this.setSummaryMethod(null)
      this.setShowFilterConditions(false)
      this.setFilterConditions([[deepCopy(FILTER_INDICATOR_ITEM)]])

      // 复合指标
      this.setExpression(null)
      this.setExpressionHtml(null)
      this.setCompositeIndicatorFilterConditions(null)

      // 派生指标
      this.setDerivedFrom('')
      this.setDerivationRule(null)
      this.setCalcPeriod(null)
      this.setCalcInterval(null)
      this.setCalcType(null)
      this.setSubCalcType(null)
      this.setMobileCalcPeriod(null)
      this.setMobileCalcInterval({ start: [], end: [] })

      // 外部指标
      this.setParamValue('')

      // 静态指标
      this.setIndicatorValue(null)
    },
    // 获取复合指标表达式中，原子指标对应的业务模型
    async getBusinessModelsByBasicIndicatorId(indicatorArr) {
      let businessModels = {}
      for (let i = 0; i < indicatorArr.length; i++) {
        const indicator = indicatorArr[i].split('_')
        const indicatorId = indicator[0]
        const indicatorType = indicator[1]
        if (indicatorType === '1' || indicatorType === '2') {
          const query = indicatorType === '1' ? getIndicatorById : getDerivativeIndicatorById
          const res = await query(indicatorId)
          if (res.data.state === 'success') {
            businessModels[indicatorId] = indicatorType === '1' ? res.data.data.businessModelId : res.data.data.atomIndicator.businessModelId
          } else {
            this.$Message.error('获取表达式中相关指标业务模型失败！')
          }
        }
      }
      this.setCompositeIndicatorBusinessModels(businessModels)
    },
    /**
     * 递归生成子目录和节点
     * @param parent
     * @param all
     * @returns {[]}
     */
    generateTreeNode(parent, all) {
      let tree = []
      for (let parentItem of parent) {
        let treeItem = { ...parentItem }
        const children = all.filter(allItem => {
          let parentId = allItem.parentId || allItem.groupId
          return parentId === parentItem.id
        }).map(childrenItem => ({
          id: childrenItem.id,
          isGroup: childrenItem.hasOwnProperty('parentId'),
          // disabled: true,  // 左侧树点击交互禁用
          title: childrenItem.name,
          abbrName: childrenItem.abbrName,
          type: childrenItem.type,
          calculateType: childrenItem.calculateType,
          showType: childrenItem.showType,
          configData: childrenItem.configData,
          render: childrenItem.hasOwnProperty('groupId') ? this.nodeRenderContent : null,
          treeType: childrenItem.parentId ? 'group' : 'value',
        }))
        if (children.length) {
          treeItem.children = this.generateTreeNode(children, all)
          treeItem.expand = true
        }
        if ((treeItem.isGroup && treeItem.children?.length) || !treeItem.isGroup) {
          tree.push(treeItem)
        }
      }
      return tree
    },
    // 获取过滤条件--公共变量列表
    async getPublicVariableList() {
      const { data: res } = await getVariableList()
      if (res.state !== 'success')
        return this.$Message.error('获取公共变量列表失败')
      let publicVariableList = res.data.map((item) => ({
        name: item.name,
        value: `\${${item.varCode}\}`,
        labelType: item.labelType
      }))
      this.setPublicVariableList(publicVariableList)
    },
  },
  watch: {
    // 监听复合指标表达式，获取原子指标的业务模型
    expression() {
      if (!this.expression) return

      // 根据表达式获取指标id数组
      let indicatorArr = []
      // 判断表达式是否是union函数的，带有[]表示不是union函数的
      if (this.expression.indexOf('[') > -1) {
        const reg = /\[(.*?)\]/gi
        indicatorArr = this.expression
          .match(reg)
          .map((item) => item.replace(reg, '$1'))
          .filter(item => item.includes('_1')) // 只有原子指标支持过滤条件
      } else {
        indicatorArr = this.expression.split(',').filter(item => item.includes('_1'))
      }
      this.getBusinessModelsByBasicIndicatorId(indicatorArr)
    },
  },
}
