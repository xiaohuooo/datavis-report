import {getComplexIndicatorById, getDerivativeIndicatorById, getIndicatorById} from "@/api/indicator";

export default {
  methods: {
    async getIndicatorDetail(type, id) {
      if (type === 4 || type === 5) return {}
      let detailInfo = {}
      let res = null
      if (type === 1) {
        res = await getIndicatorById(id)
      } else if (type === 2) {
        res = await getComplexIndicatorById(id)
      } else if (type === 3) {
        res = await getDerivativeIndicatorById(id)
      }
      if (res.data?.state === 'success') {
        detailInfo = res.data.data
      }
      return detailInfo
    },

    getConditionItemGlobalVariable(conditionItem) {
      let globalVarItem = null
      const conditionItemValueArr = conditionItem.value.split('|')
      if (conditionItemValueArr && conditionItemValueArr.length) {
        const conditionType = conditionItemValueArr[0] * 1
        if (conditionType === 4) { // 类型是全局变量
          const variableCode = conditionItemValueArr[1].replace('$', '').replace('{', '').replace('}', '')
          const globalVariableList = this.globalVariableList || JSON.parse(localStorage.getItem('globalVariableList'))
          globalVarItem = globalVariableList.find(item => item.varCode === variableCode)
        }
      }
      return globalVarItem
    },
    /**
     * 检查指标是否需要配置了全局变量条件
     * @param indicatorList
     */
    getIndicatorGlobalVariableCondition(indicatorList) {
      let globalVarList = []
      for (let indicator of indicatorList) {
        const type = indicator.type * 1
        if (type === 1) {
          // 原子
          const conditionGroups = indicator.conditionGroups || []
          for (let conditionGroupItem of conditionGroups) {
            for (let conditionItem of conditionGroupItem.conditions) {
              const globalVarItem = this.getConditionItemGlobalVariable(conditionItem)
              if (globalVarItem) {
                globalVarList.push(globalVarItem)
              }
            }
          }
        } else if (type === 2) {
          // 复合
          const conditionObj = indicator.conditions || []// 复合指标的过滤条件会有多个原子指标，所以用原子指标id作为key
          for (let key in conditionObj) {
            const conditionGroups = conditionObj[key]
            for (let conditionGroupItem of conditionGroups) {
              for (let conditionItem of conditionGroupItem.conditions) {
                const globalVarItem = this.getConditionItemGlobalVariable(conditionItem)
                if (globalVarItem) {
                  globalVarList.push(globalVarItem)
                }
              }
            }
          }
        } else if (type === 3) {
          // 派生
          const conditionGroups = indicator.atomIndicator?.conditionGroups || []
          for (let conditionGroupItem of conditionGroups) {
            for (let conditionItem of conditionGroupItem.conditions) {
              const globalVarItem = this.getConditionItemGlobalVariable(conditionItem)
              if (globalVarItem) {
                globalVarList.push(globalVarItem)
              }
            }
          }
        }
      }

      return globalVarList
    },
  }
}
