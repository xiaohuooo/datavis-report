import {getGlobalVariableByPage} from "@/api/dataset";

export default {
  provide() {
    return {
      refreshCharts: this.refreshCharts
    }
  },
  methods: {
    /**
     * 联动时需刷新所有支持被联动的图表
     * @param relativeComponentList
     */
    refreshCharts(relativeComponentList) {
      if (relativeComponentList && relativeComponentList.length) {
        this.$children.forEach(item => {
          let chart = item.$refs.chart
          if(chart) {
            const isInRelativeComponentList = relativeComponentList.find(item => item === chart.chartId)
            if (isInRelativeComponentList) {
              chart.refreshData && chart.refreshData()
            }
          }
        })
      }
    },
    fetchPageGlobalVar() {
      getGlobalVariableByPage('report').then(res => {
        if(res.data.state === 'success' && res.data.data){
          this.globalVariableList = res.data.data.map(item => {
            let paramValueArr = []
            Object.keys(item.paramValue).forEach(key => {
              paramValueArr.push({
                value: key,
                label: item.paramValue[key]
              })
            })
            return {
              ...item,
              paramValue: paramValueArr
            }
          })
        }
      })
    }
  }
}
