import { supportSeriesCharts } from "@/view/report/components/reportDataSelector/reportDataSelectorConstant";
import { deepCopy } from "@/utils/common";

export default {
  data() {
    return {
      seriesDataClone: [] // 备份系列数据
    };
  },
  watch: {
    "chartData.data.series": {
      handler(newVal) {
        if (newVal?.length) {
          this.seriesDataClone = deepCopy(newVal); // 备份系列数据
        }
      },
      deep: true,
      immediate: true
    }
  },
  methods: {
    /**
     * 设置图表的系列
     */
    setSeries(chartType) {
      if (!supportSeriesCharts.includes(chartType)) {
        // 若图表不支持系列，去除 series 数据
        this.$set(this.chartData.data, "series", []);
      } else {
        // 若图表支持且选择系列，还原 series 数据
        if (!this.chartData.data.series?.length) {
          this.$set(this.chartData.data, "series", this.seriesDataClone);
        }
      }
    }
  }
};
