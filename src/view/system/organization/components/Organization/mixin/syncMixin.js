import { deleteUser, getDepTree, getUserList, resetPassword, saveOrUpdateUser, isFirst, syncTruncate, syncTrigger, getStatus } from "@/dmp/api/system";
import { nextTime } from "@/dmp/api/dataConnect";
import FirstEntry from "../components/FirstEntry.vue"; // 引入首次登录弹窗
import EditSynchronization from "../components/editsynchronization.vue"; // 引入编辑同步弹窗';
import Synchronization from "../components/synchronization.vue"; // 引入编辑同步弹窗';
import InterruptDialog from "../components/interSyncRuptDialog.vue"; // 引入编辑同步弹窗';
import OldSynchronization from "../components/oldSynchronization.vue"; // 引入历史同步数据';

export default {
  components: {
    FirstEntry, // 引入首次登录弹窗
    EditSynchronization, // 引入编辑同步弹窗
    Synchronization, // 立即同步弹窗
    InterruptDialog,
    OldSynchronization
  },
  data() {
    return {
      firsttime: true, // 第一次进入页面
      loadingFirstTime: false, // 第一次进入页面加载
      nextDataTime: "", // 下次请求时间
      isSynchronizin: false //是否正在同步
    };
  },
  async created() {
    await getStatus().then(res => {
      if (res.data.data) {
        if (res.data.data.status == "同步中") {
          this.isSynchronizin = true;
        }
      }
    });

    this.isFirstEntry(); // 判断是否第一次进入页面
  },
  methods: {
    isFirstEntry() {
      //是否首次进入
      this.loadingFirstTime = true;
      isFirst({ syncType: 3 })
        .then(res => {
          const { state } = res.data;
          if (state === "success") {
            this.loadingFirstTime = false;
            this.firsttime = res.data.data;
          } else {
            this.$message.error("获取首次进入登录状态失败");
          }
        })
        .catch(() => {
          this.$message.error("获取首次进入登录状态失败");
        });
    },
    showPopver() {
      // nextTime().then(res => {
      //   this.nextDataTime = res.data.data;
      // });
    },
    /**
     * 开始同步
     */
    synchronousUser() {
      this.$refs.firstEnTry.open();
    },
    /**
     * 立即同步
     */
    synchronization() {
      this.isSynchronizin = true;
      syncTrigger({ syncType: 3 }).then(res => {
        const id = res.data.data;
        this.$refs.synchronization.openDialog(id);
      });
    },
    /**
     * 刷新同步状态
     */
    resetSynchronization() {
      this.$refs.synchronization.openDialog();
    },
    /**
     * 编辑
     */
    editsynchronization() {
      this.$refs.editSynchronization.openDialog();
    },
    /**
     * 清空同步数据
     */
    emptysynchronization() {
      this.$confirm("清空同步数据将清空原有同步数据，包括部门、职务及对应权限等，原部门职务下人员将处于无组织状态，确认清空同步数据?", "清空同步数据", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        customClass: "clean-confirm",
        type: "warning"
      }).then(() => {
        syncTruncate({ syncType: 3 })
          .then((res) => {
            const { state, message } = res.data;
            if (state === "success") {
              this.$message.success("清空同步数据成功");
              this.isFirstEntry();
              this.getDepartmentTree();
            } else {
              this.$message.error(message);
            }
          })
          .catch(err => {
            this.$message.error(err);
          });
      });
    },
    /**
     * 查看历史同步结果
     */
    viewSynchronization() {
      this.$refs.oldSynchronization.openDialog();
    },
    /**
     * 异常数据中断
     */
    interruptsynchronization() {
      this.$refs.interruptDialog.openDialog();
      console.log("中断同步");
    },
    sumitConfirm(val) {
      this.$emit("managementChange", val);
    },
    successTrigger() {
      this.isSynchronizin = false;
      this.getDepartmentTree();
    }
  }
};
