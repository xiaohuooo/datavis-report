export const sqlImgList = [
  { name: "Microsoft SQL Server", imgUrl: require("@/dmp/assets/images/dataConnection/MicrosoftSQLServer.png"), type: "SQLSERVER" },
  { name: "MySQL", imgUrl: require("@/dmp/assets/images/dataConnection/MysQl.png"), type: "MYSQL" },
  { name: "ORACLE", imgUrl: require("@/dmp/assets/images/dataConnection/oracle.png"), type: "ORACLE" },
  { name: "PostgreSql", imgUrl: require("@/dmp/assets/images/dataConnection/PostgreSql.png"), type: "POSTGRESQL" }
];
