export const CANVAS_WIDTH_COL = 13;
export const CANVAS_HEIGHT_COL = 8;

export const DRAWEW_OPATION = [
  {
    name: "数据预览",
    img: require("@/dmp/assets/images/wordTable/list1.png"),
    imgNot: require("@/dmp/assets/images/wordTable/list1disable.png"),
    type: "ReportSelect",
    numType: 1,
    instructions: "核心数据指标首页预览",
    w: 9,
    h: CANVAS_HEIGHT_COL,
    x: 0,
    y: 0
  },
  {
    name: "个人信息",
    img: require("@/dmp/assets/images/wordTable/list2.png"),
    imgNot: require("@/dmp/assets/images/wordTable/list2disable.png"),
    type: "PersonInfo",
    numType: 2,
    instructions: "核心数据指标首页预览",
    w: 4,
    h: 1.5,
    x: 9,
    y: 0
  },
  {
    name: "快捷创建",
    img: require("@/dmp/assets/images/wordTable/list3.png"),
    imgNot: require("@/dmp/assets/images/wordTable/list3disable.png"),
    type: "QuickCreate",
    numType: 3,
    instructions: "核心数据指标首页预览",
    w: 4,
    h: 3.5,
    x: 9,
    y: 1.5
  },
  {
    name: "快捷访问",
    img: require("@/dmp/assets/images/wordTable/list4.png"),
    imgNot: require("@/dmp/assets/images/wordTable/list4disable.png"),
    type: "QuickJump",
    numType: 4,
    instructions: "核心数据指标首页预览",
    w: 4,
    h: 3.5,
    x: 9,
    y: 5
  },
  {
    name: "预警",
    img: require("@/dmp/assets/images/wordTable/list5.png"),
    imgNot: require("@/dmp/assets/images/wordTable/list5disable.png"),
    type: "EarlyWarning",
    numType: 5,
    instructions: "核心数据指标首页预览",
    w: 4,
    h: 3,
    x: 9,
    y: 8.5
  },
  {
    name: "目标",
    img: require("@/dmp/assets/images/wordTable/list6.png"),
    imgNot: require("@/dmp/assets/images/wordTable/list6disable.png"),
    type: "TargetSituation",
    numType: 6,
    instructions: "核心数据指标首页预览",
    w: 4,
    h: 3,
    x: 9,
    y: 11.5
  },
  {
    name: "任务",
    img: require("@/dmp/assets/images/wordTable/list7.png"),
    imgNot: require("@/dmp/assets/images/wordTable/list7disable.png"),
    type: "TaskSituation",
    numType: 7,
    instructions: "核心数据指标首页预览",
    w: 4,
    h: 2,
    x: 9,
    y: 14.5
  }
];
