import { QUICK_CREATION_LIST, QUICK_ACCESS_LIST } from "@/view/workbenchPreview/constant/icon";

export const CREATE_LIST = QUICK_CREATION_LIST;

export const PERSON_INFO = {
  name: "用户1",
  dept: "xx部门"
};

export const ACCESS_LIST = QUICK_ACCESS_LIST.map((ele, i) => ({
  id: i + 1,
  name: `菜单${i + 1}`,
  defaultIcon: ele.default,
  hoverIcon: ele.hover
}));

export const EARLY_WARNING_LIST = [
  {
    id: 1,
    name: "预警1",
    watchLabel: 2,
    currentValue: "10",
    limitValue: "100",
    manager: "用户1"
  },
  {
    id: 2,
    name: "预警2",
    watchLabel: 2,
    currentValue: "20%",
    limitValue: "100%",
    manager: "用户1"
  }
];

export const TARGET_LIST = [
  {
    id: 1,
    name: "目标1",
    manager: "用户1",
    progress: 70,
    riskLevel:'lowRisk'
  },
  {
    id: 2,
    name: "目标2",
    manager: "用户1",
    progress: 80,
    riskLevel:'midRisk'
  },
  {
    id: 3,
    name: "目标3",
    manager: "用户1",
    progress: 90,
    riskLevel:'highRisk'
  }
];

export const TASK_LIST = [
  {
    id: 1,
    name: "全部",
    num: "--"
  },
  {
    id: 2,
    name: "处理中",
    num: "--"
  },
  {
    id: 3,
    name: "待处理",
    num: "--"
  },
  {
    id: 4,
    name: "及时闭环",
    num: "--"
  },
  {
    id: 5,
    name: "超时闭环",
    num: "--"
  }
];
