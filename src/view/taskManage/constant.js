import all from "@/assets/icons/taskManage/all.svg";
import processing from "@/assets/icons/taskManage/processing.svg";
import pending_process from "@/assets/icons/taskManage/pending_process.svg";
import timely_closed from "@/assets/icons/taskManage/timely_closed.svg";
import overtime_closed from "@/assets/icons/taskManage/overtime_closed.svg";

export const TAB_LIST = [
  {
    name: "全部",
    icon: all,
    value: -1,
    bgColor: ""
  },
  {
    name: "待处理",
    icon: pending_process,
    value: 1,
    bgColor: "#F7B500"
  },
  {
    name: "处理中",
    icon: processing,
    value: 2,
    bgColor: "#2781FF"
  },
  {
    name: "及时闭环",
    icon: timely_closed,
    value: 3,
    bgColor: "#6DD400"
  },
  {
    name: "超时闭环",
    icon: overtime_closed,
    value: 4,
    bgColor: "#F86666"
  }
];

export const TASK_TYPE_LIST = [
  {
    label: "目标型",
    value: 1,
    alias: "目标"
  },
  {
    label: "预警型",
    value: 2,
    alias: "预警"
  },
  {
    label: "其他型",
    value: 3,
    alias: "指标"
  }
];
