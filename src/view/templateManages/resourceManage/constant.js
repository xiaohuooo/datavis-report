import tableIcon from "@/assets/icons/resourceManage/chart/table.svg"; // 常规资源图标
import lineIcon from "@/assets/icons/resourceManage/chart/line.svg";
import radarIcon from "@/assets/icons/chart/chart_type/radar_black.svg";
import barIcon from "@/assets/icons/resourceManage/chart/bar.svg";
import pieIcon from "@/assets/icons/resourceManage/chart/pie.svg";
import scatterIcon from "@/assets/icons/resourceManage/chart/scatter.svg";
import lineBarIcon from "@/assets/icons/resourceManage/chart/line_bar.svg";
import doublePieIcon from "@/assets/icons/resourceManage/chart/doublePie.svg";
import gaugeIcon from "@/assets/icons/resourceManage/chart/gauge.svg";
import imgIcon from "@/assets/icons/resourceManage/chart/img.svg";
import indicatorIcon from "@/assets/icons/resourceManage/chart/indicator.svg";
import indexBoardIcon from "@/assets/icons/resourceManage/chart/indexBoard.svg";
import inputIcon from "@/assets/icons/resourceManage/chart/input.svg";
import mapIcon from "@/assets/icons/resourceManage/chart/map.svg";
import bubbleIcon from "@/assets/icons/resourceManage/chart/bubble.svg";

export const COMMON_RESOURCE_ICON = [
  {
    chartType: "CustomTable",
    icon: tableIcon
  },
  {
    chartType: "SimpleLine",
    icon: lineIcon
  },
  {
    chartType: "BasicRadar",
    icon: radarIcon
  },
  {
    chartType: "SimpleBar",
    icon: barIcon
  },
  {
    chartType: "SimplePie",
    icon: pieIcon
  },
  {
    chartType: "SimpleScatter",
    icon: scatterIcon
  },
  {
    chartType: "SimpleBubble",
    icon: bubbleIcon
  },
  {
    chartType: "MixLineBar",
    icon: lineBarIcon
  },
  {
    chartType: "DoublePie",
    icon: doublePieIcon
  },
  {
    chartType: "Indicator",
    icon: indicatorIcon
  },
  {
    chartType: "IndexBoard",
    icon: indexBoardIcon
  },
  {
    chartType: "SimpleGauge",
    icon: gaugeIcon
  },
  {
    chartType: "ComplexTextarea",
    icon: inputIcon
  },
  {
    chartType: "SimpleMap",
    icon: mapIcon
  },
  {
    chartType: "CustomImage",
    icon: imgIcon
  }
];
