import add_indicator from "@/assets/icons/home/add_indicator.svg"; // 快捷创建
import add_target from "@/assets/icons/home/add_target.svg";
import add_early_warning from "@/assets/icons/home/add_early_warning.svg";
import add_task from "@/assets/icons/home/add_task.svg";
import add_report from "@/assets/icons/home/add_report.svg";
import add_analysis from "@/assets/icons/home/add_analysis.svg";
import access_icon1 from "@/assets/icons/home/access_icon1.svg"; // 快捷访问
import access_icon1_hover from "@/assets/icons/home/access_icon1_hover.svg";
import access_icon2 from "@/assets/icons/home/access_icon2.svg";
import access_icon2_hover from "@/assets/icons/home/access_icon2_hover.svg";
import access_icon3 from "@/assets/icons/home/access_icon3.svg";
import access_icon3_hover from "@/assets/icons/home/access_icon3_hover.svg";
import access_icon4 from "@/assets/icons/home/access_icon4.svg";
import access_icon4_hover from "@/assets/icons/home/access_icon4_hover.svg";
import access_icon5 from "@/assets/icons/home/access_icon5.svg";
import access_icon5_hover from "@/assets/icons/home/access_icon5_hover.svg";
import access_icon6 from "@/assets/icons/home/access_icon6.svg";
import access_icon6_hover from "@/assets/icons/home/access_icon6_hover.svg";
import access_icon7 from "@/assets/icons/home/access_icon7.svg";
import access_icon7_hover from "@/assets/icons/home/access_icon7_hover.svg";
import access_icon8 from "@/assets/icons/home/access_icon8.svg";
import access_icon8_hover from "@/assets/icons/home/access_icon8_hover.svg";

// 接口数据和前端路由的映射关系
export const FIELD_MAPPING_ROUTE = {
  earlyWarning: "/manager/earlyWarningTarget/earlyWarningTargetIndex",
  targetSituation: "/manager/objectManage/objectManage",
  taskSituation: "/manager/taskManage/taskManage"
};

// 快捷创建
export const QUICK_CREATION_LIST = [
  {
    id: 1,
    name: "创建指标",
    icon: add_indicator,
    path: "/manager/indicator/indicator_manage_new"
  },
  {
    id: 2,
    name: "创建目标",
    icon: add_target,
    path: "/manager/objectManage/objectManage"
  },
  {
    id: 3,
    name: "创建预警",
    icon: add_early_warning,
    path: "/manager/earlyWarningTarget/earlyWarningTargetIndex"
  },
  {
    id: 4,
    name: "创建任务",
    icon: add_task,
    path: "/manager/taskManage/taskManage"
  },
  {
    id: 5,
    name: "创建报告",
    icon: add_report,
    path: "/template_add"
  },
  {
    id: 6,
    name: "创建分析",
    icon: add_analysis,
    path: "/manager/indicator_browser/indicator_browser"
  }
];

// 快捷访问
export const QUICK_ACCESS_LIST = [
  {
    name: "收入利润预测",
    default: access_icon1,
    hover: access_icon1_hover
  },
  {
    name: "损益表-生物医疗",
    default: access_icon2,
    hover: access_icon2_hover
  },
  {
    name: "损益表-玛西普",
    default: access_icon3,
    hover: access_icon3_hover
  },
  {
    name: "生态收入",
    default: access_icon4,
    hover: access_icon4_hover
  },
  {
    name: "生态利润",
    default: access_icon5,
    hover: access_icon5_hover
  },
  {
    name: "损益表-金融投资",
    default: access_icon6,
    hover: access_icon6_hover
  },
  {
    name: "全月收入",
    default: access_icon7,
    hover: access_icon7_hover
  },
  {
    name: "入院人数",
    default: access_icon8,
    hover: access_icon8_hover
  }
];
