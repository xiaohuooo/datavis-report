const path = require('path')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const CompressionWebpackPlugin = require('compression-webpack-plugin')
const resolve = dir => {
  return path.join(__dirname, dir)
}
const webpack = require('webpack')

// 项目部署基础
// 默认情况下，我们假设你的应用将被部署在域的根目录下,
// 例如：https://www.my-app.com/
// 默认：'/'
// 如果您的应用程序部署在子路径中，则需要在这指定子路径
// 例如：https://www.foobar.com/my-app/
// 需要将它改为'/my-app/'
// iview-admin线上演示打包路径： https://file.iviewui.com/admin-dist/
const isProduction = process.env.NODE_ENV === 'production'
// 海尔环境将 /report-dev 改成/report
// 兴业环境将 /report-dev 改成/report
// 演示环境将 /report-dev 改成/
const BASE_URL = isProduction ? '/report-dev' : '/report'

module.exports = {
  // Project deployment base
  // By default we assume your app will be deployed at the root of a domain,
  // e.g. https://www.my-app.com/
  // If your app is deployed at a sub-path, you will need to specify that
  // sub-path here. For example, if your app is deployed at
  // https://www.foobar.com/my-app/
  // then change this to '/my-app/'
  publicPath: BASE_URL,
  // tweak internal webpack configuration.
  // see https://github.com/vuejs/vue-cli/blob/dev/docs/webpack.md
  // 如果你不需要使用eslint，把lintOnSave设为false即可
  lintOnSave: false,
  // resolve: {
  //   extensions: ['.js', '.vue']
  // },
  configureWebpack: config => {
    if (isProduction) {
      // 压缩代码，移除console.log
      config.plugins.push(
        new UglifyJsPlugin(
          {
            uglifyOptions: {
              output: {
                comments: false // 去掉注释
              },
              warnings: false,
              compress: {
                drop_console: true,
                drop_debugger: false,
                pure_funcs: ['console.log'] // 移除console
              }
            }
          }
        )
      )
      // 使用gzip
      // 增加浏览器CPU（需要解压缩），减少网络传输量和带宽消耗（需要衡量，一般小文件不需要压缩的）
      // 图片和PDF文件不应该被压缩（它们本身就是压缩的），试着压缩它们会浪费CPU资源而且可能潜在增加文件大小
      config.plugins.push(
        new CompressionWebpackPlugin({
          filename: '[path].gz[query]', // asset -> filename
          algorithm: 'gzip',
          test: /\.(js|css)$/,
          threshold: 10240, // 达到10kb的静态文件进行压缩 按字节计算
          minRatio: 0.8, // 只有压缩率比这个值小的资源才会被处理
          deleteOriginalAssets: false // 是否删除压缩的源文件
        })
      )
    }
  },
  chainWebpack: config => {
    // config.set('externals', {
    //   jquery: 'jQuery'
    // })
    config.plugin('provide').use(webpack.ProvidePlugin, [{
      $: 'jquery',
      jquery: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }])
    config.module
      .rule('vue')
      .use('iview')
      .loader('iview-loader')
      .options({ prefix: false })
    config.resolve.alias
      .set('@', resolve('src')) // key,value自行定义，比如.set('@@', resolve('src/components'))
      .set('_c', resolve('src/components'))
    const oneOfsMap = config.module.rule('scss').oneOfs.store
    oneOfsMap.forEach(item => {
      item
        .use('sass-resources-loader')
        .loader('sass-resources-loader')
        .options({
          // 全局变量资源路径
          resources: './src/styles/theme/themeVariable.scss'
          // 也可以选择全局变量路径数组, 如果有多个文件需要成为全局
          // resources: ['./path/to/vars.scss', './path/to/mixins.scss']
          // 或者将多个scss文件@import到一个main.scss用第一种方法
        })
        .end()
    })
    config.plugins.delete('preload')
    config.plugins.delete('prefetch')

    // set svg-sprite-loader
    config.module
      .rule('svg')
      .exclude.add(resolve('src/dmp/icons'))
      .end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/dmp/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()

    if (process.env.NODE_ENV === 'production') {
      if (process.env.npm_config_report) {
        config
          .plugin('webpack-bundle-analyzer')
          .use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin)
          .end()
      }
      // vuecli3默认开启prefetch(预先加载模块)，提前获取用户未来可能访问的内容
      // 移除prefetch插件，首屏就只会加载当前页面路由组件
      // config.plugins.delete('prefetch')
    }
  },
  // 设为false打包时不生成.map文件
  productionSourceMap: false,
  // 这里写你调用接口的基础路径，来解决跨域，如果设置了代理，那你本地开发环境的axios的baseUrl要写为 '' ，即空字符串
  devServer: {
    proxy: {
      '/api': {
        target: 'http://129.211.167.119:8688/api/'
      }
    }
  }
}
